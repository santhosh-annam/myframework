package browser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	
	public static void main(String[] data) throws JSONException, Exception {
	//data = new String[]{"{\"mobile_platform\":\"Android\",\"web_page_name\":\"\",\"web_page_description\":\"\",\"diff_page_id\":0,\"jar_type\":2,\"file_name\":\"airtel.apk\",\"bundle_id\":\"\",\"module_id\":752,\"user_id\":108,\"save_web_page_and_elements_url\":\"https://smartqe.io:443/CreateWebPage\"}\r\n" + 
	//		""};
		
		
	
	InputStream is = new FileInputStream(data[0]);
	@SuppressWarnings("resource")
	BufferedReader buf = new BufferedReader(new InputStreamReader(is));
	String line = buf.readLine();
	StringBuilder sb = new StringBuilder();
	while(line != null){ 
		sb.append(line).append("\n"); 
		line = buf.readLine(); 
	} 
	String finalData = sb.toString();

	//String finalData = data[0];

		System.out.println(finalData);
    	JSONObject jsonObj = new JSONObject(finalData);
    	DesiredCapabilitiesController.JSON = finalData;
    	DesiredCapabilitiesController.FILE_NAME = jsonObj.getString("file_name");
    	DesiredCapabilitiesController.JAR_TYPE = jsonObj.optInt("jar_type");
    	DesiredCapabilitiesController.DIFF_ID = jsonObj.optInt("diff_page_id");
    	DesiredCapabilitiesController.WEB_PAGE_NAME = jsonObj.optString("web_page_name");
    	DesiredCapabilitiesController.WEB_PAGE_DESC = jsonObj.optString("web_page_description");
    	DesiredCapabilitiesController.ELEMENTS_URL = jsonObj.getString("save_web_page_and_elements_url");
    	DesiredCapabilitiesController.BUNDLE_ID = jsonObj.optString("bundle_id");
    	DesiredCapabilitiesController.MOBILE_PLATFORM = jsonObj.optString("mobile_platform");
        launch(finalData); 
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("desire_capabilities_form.fxml"));
        Parent browser = loader.load();
		DesiredCapabilitiesController controller = loader.getController();
		StackPane root = new StackPane();
		root.getChildren().add(browser);
		Scene scene = new Scene(root, 300, 250, Color.WHITE);
		primaryStage.setTitle("Automation Framework");
		//primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/automaion_browser.png")));
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
            	try {
            		controller.closeApp();
            	} catch (Exception e) {
					e.printStackTrace();
				}
                Platform.exit();
                System.out.println("closed");
                System.exit(0);
            }
        });
		primaryStage.setMaximized(true);
	}
	 
}
