package browser;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.dto.PageDetails;
import com.dto.PageElements;
import com.google.gson.Gson;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class DesiredCapabilitiesController implements Initializable {
	
	@FXML
	private TextField deviceNameField;	
	@FXML
	private TextField platformNameField;
	@FXML
	private TextField versionField;
	@FXML
	private TextField udidField;	
	@FXML
	private TextField packageNameField;
	@FXML
	private TextField launcherField;
	@FXML
	private Label headerLabel;
	@FXML
	private Button submitButton;
	@FXML
	private Button apkChooserField;
	@FXML
	private Button elementsButton;
	@FXML
	private Label errorLabel;
	@FXML
	private ProgressIndicator progressIndicator;
	@FXML
	private ToggleGroup androidOrIOS;
	@FXML
	private RadioButton androidRadioBtn;
	@FXML
	private RadioButton iosRadioBtn;
	@FXML
	private Label packageNameLabel;
	@FXML
	private Label launcherActivityLabel;
	
	private String nodeJSPath = System.getenv("NODE_PATH");
	private String appiumServerJSPath = System.getenv("APPIUM_JS_PATH");
	private AppiumDriverLocalService appiumDriverService;
	@SuppressWarnings("rawtypes")
	private AppiumDriver driver;
	private String fileName = "";
	private String automationPath = System.getenv("AUTOMATION_PATH") + File.separator + "mobile" + File.separator + "android";
	private static String sdkPath =  System.getenv("ANDROID_HOME");
	private final String GET_PACKAGE_LAUNCHER_NAME = "package";
	private final String GET_ADB_DEVICES = "devices";
	private final String KILL_ADB = "killadb";
	private final String KILL_NODE = "killnode";
	private final String GET_PRODUCT_DETAILS = "productDetails";
	public boolean isAppiumStarted;
	private final String COLUMN1_MAP_KEY = "A";
	private final String COLUMN2_MAP_KEY = "B";
	private final String COLUMN3_MAP_KEY = "C";
    private final String OS = System.getProperty("os.name").toLowerCase();
	private String addBash = "";
	private String extension = "";
	private String toogleExtension = "*.apk";
	private String environMentVariables = System.getenv("AUTOMATION_PATH");
	
	public static String ELEMENTS_URL;
	public static int JAR_TYPE;
    private boolean isAPKAvailable;
	public static String JSON;
	public static String FILE_NAME;
	public static int DIFF_ID;
	public static String WEB_PAGE_NAME;
	public static String WEB_PAGE_DESC;
	public static String BUNDLE_ID;
	public static String MOBILE_PLATFORM;
	
	@Override
	public void initialize(URL url, ResourceBundle bundle) {
	
		apkChooserField.setStyle("-fx-text-fill: black; -fx-base: #EAE1DF;");
		elementsButton.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
		//elementsButton.setVisible(false);
		Platform.runLater(() -> {		
			if(submitButton.getText().toString().equalsIgnoreCase("connect")) { 
				elementsButton.setText("Ui Viewer");
				} else {
					elementsButton.setVisible(false);
				}
		});
		submitButton.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
		versionField.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d*(\\.\\d*)?")) {
	            	versionField.setText(oldValue);
	            }
	        }
	    });
		
		if (FILE_NAME != null && !FILE_NAME.isEmpty()) {
			if(FILE_NAME.contains(".apk")) {
				androidRadioBtn.setSelected(true);
				radioButtonsEnableOrDisable(false,true);
				setText("ANDROID");
				toogleExtension = "*.apk";
			} else {
				iosRadioBtn.setSelected(true);
				radioButtonsEnableOrDisable(true,false);
				setText("iOS");
				packageNameField.setText(BUNDLE_ID);
				toogleExtension = "*.ipa";
			}
			fileName = FILE_NAME;
		} else if(MOBILE_PLATFORM.equalsIgnoreCase("android")) {
			androidRadioBtn.setSelected(true);
			radioButtonsEnableOrDisable(false,true);
			toogleExtension = "*.apk";
			setText("ANDROID");
		}  else if(MOBILE_PLATFORM.equalsIgnoreCase("ios")) {
			iosRadioBtn.setSelected(true);
			radioButtonsEnableOrDisable(true,false);
			toogleExtension = "*.ipa";
			setText("iOS");
		} else {
			androidRadioBtn.setSelected(true);
			toogleExtension = "*.apk";
	    	deviceNameField.setText("Android");
			platformNameField.setText("Android");
		}
		androidOrIOS.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle,Toggle new_toggle) {
		          System.out.println(androidOrIOS.getSelectedToggle().toString());
		          String isAndroidOrIOS = androidOrIOS.getSelectedToggle().toString();
		          versionField.setText("");
		          udidField.setText("");
		          packageNameField.setText("");
		          launcherField.setText("");
		          errorLabel.setVisible(false);
		          setText(isAndroidOrIOS);
		      }
		});
		
		Platform.runLater(new Runnable() {
			@Override
		    public void run() {
				if (isWindows()) {
					extension = ".bat";
					System.out.println("This is Windows");
				} else if (isMac()) {
					String MAC_HOME_DIRECTORY = System.getProperty("user.home");
					extension = ".sh";
					automationPath = "bash " + MAC_HOME_DIRECTORY;
					createEnvironmentFile(MAC_HOME_DIRECTORY);
					ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
					if(envVariables.size() >= 3) {
					automationPath = envVariables.get(0);
					environMentVariables = automationPath;
					nodeJSPath = envVariables.get(1);
					appiumServerJSPath = envVariables.get(2);
					automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
					System.out.println("This is for Mac");
					addBash = "bash ";
					}
				} else if (isUnix() || isSolaris()) {
					extension = ".sh";
					automationPath = System.getProperty("user.home");
					ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
					automationPath = envVariables.get(0);
					sdkPath = envVariables.get(3);
					environMentVariables = automationPath;
					automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
					System.out.println("This is for unix/solaris");
				} else {
					System.out.println("Your OS is not support!!");
				}
				
		    	headerLabel.requestFocus();
		    	if (FILE_NAME != null && !FILE_NAME.isEmpty()) {
		    		isAPKAvailable = true;
		    		if(FILE_NAME.contains(".apk")) {
		    			apkChooserField.setText(automationPath + File.separator + FILE_NAME);
		    			createPackageBatFile(FILE_NAME);
		    		} else {
		    			apkChooserField.setText(environMentVariables + File.separator + "mobile" + File.separator + "ios" + File.separator + FILE_NAME);	
		    		}
					try {
						commandScripts(extension);
					} catch (Exception e) {
						e.printStackTrace();
					}
					//submitButton.fire();
				}
		    }
		});	
	}
	
	/**
	 * Creating the file in User Home directory to get the environment variables
	 * @param path
	 */
	private void createEnvironmentFile(String path) {
		 try {
			 File fileExists = new File(path + File.separator + "environmentVariables.sh");
				boolean exists = fileExists.exists();
				if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
					"printenv AUTOMATION_PATH\n" + 
					"printenv NODE_PATH\n" + 
					"printenv APPIUM_JS_PATH\n" + 
					"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Setting the permissions to read, write and execute
	 * @param path
	 * @throws Exception
	 */
	private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			} 
		} else {
			File file = new File(path);
			if(file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}
	
	/**
	 * Setting the text based on platform
	 * @param isAndroidOrIOS 
	 */
   private void setText(String isAndroidOrIOS) {
		 if(isAndroidOrIOS.contains("iOS")) {
      	  toogleExtension = "*.ipa";
      	  packageNameField.setPromptText("Enter Bundle Id");
      	  packageNameLabel.setText("Bundle Id");
      	  platformNameField.setText("iOS");
      	  launcherActivityLabel.setVisible(false);
      	  launcherField.setVisible(false);
      	  deviceNameField.setText("iPhone");
      	  apkChooserField.setText("Choose IPA File");
      	//  versionField.setText("11.3");
        } else if(isAndroidOrIOS.contains("ANDROID")) {
      	  packageNameField.setPromptText("Enter Package Name");
      	  packageNameLabel.setText("Package Name");
      	  platformNameField.setText("Android");
      	  toogleExtension = "*.apk";
      	  launcherActivityLabel.setVisible(true);
      	  launcherField.setVisible(true);
      	  deviceNameField.setText("Android");
      	  apkChooserField.setText("Choose APK File");
      	  //versionField.setText("6");
        }
	}
	
   /**
    * Validating the fields 
    * After Validation is done launching the Application in mobile
    */
	String errorMsg = "";
	@FXML
	private void handleSubmitButtonAction(ActionEvent event) {
		errorMsg = "";
		String text = submitButton.getText().toString();
		errorLabel.setVisible(false);
		if(text.equalsIgnoreCase("Processing..")) {
			return;
		}
		
		 new Thread() {
             public void run() {
            	 if (text.equalsIgnoreCase("connect")) {
            		 String fileTypeName = toogleExtension.contains("apk") ?  "Choose APK file" : "Choose IPA file";
         			isAppiumStarted = false;
         			String deviceName = deviceNameField.getText().toString().trim();
         			String platformName = platformNameField.getText().toString().trim();
         			String version = versionField.getText().toString().trim();
         		
         			String fileChooserField = apkChooserField.getText().toString().trim();
         			String udid = udidField.getText().toString().trim();
         			String packageName = packageNameField.getText().toString().trim();
         			String launcher = launcherField.getText().toString().trim();
         			
         			if (deviceName.isEmpty()) {
         				errorMsg = "Device Name";
         			} else if (platformName.isEmpty()) {
         				errorMsg = "Platform Name";
         			} else if (toogleExtension.contains("apk") && version.isEmpty()) {
         				errorMsg = "Platform Version";
         			} else if (fileChooserField.equalsIgnoreCase(fileTypeName)) {
         				errorMsg = fileTypeName;
         				Platform.runLater(() -> {
         				errorLabel.setText(errorMsg);
         				});
         				errorLabel.setVisible(true);
         				return;
         			} else if (udid.isEmpty()) {
         				errorMsg = "UDID";
         			} else if (packageName.isEmpty()) {
         				errorMsg = toogleExtension.contains("apk") ? "Package Name" : "Bundle Id";
         			} else if (toogleExtension.contains("apk") && launcher.isEmpty()) {
         				errorMsg = "Launcher Activity Name";
         			} else {
         				 Platform.runLater(() -> {
                			 submitButton.setText("Processing..");
                			 elementsButton.setVisible(false);
             					
             			});
         				errorLabel.setVisible(false);
         				appiumDriverService = setUpAppiumDriver();
         				isAppiumStarted = startAppiumServer();
         				androidRadioBtn.setDisable(false);
             			iosRadioBtn.setDisable(false);
         				launchApp(deviceName, 
         						platformName,
         						version, 
         						udid,
         						fileChooserField,
         						packageName,
         						launcher);
         				return;
         			   
         			}
         			errorLabel.setVisible(true);
         			Platform.runLater(() -> {
         				errorLabel.setText("Enter " + errorMsg);
        			});
         		} else if(text.equalsIgnoreCase("Disconnect")) {
         			catchBlock();
         			closeApp();
         		}
             }
         }.start();
	}
	
	private void catchBlock() {
		if (FILE_NAME != null && !FILE_NAME.isEmpty()) {
			if (FILE_NAME.contains(".apk")) {
				radioButtonsEnableOrDisable(false, true);
			} else {
				radioButtonsEnableOrDisable(true, false);
			}
		} else {
			radioButtonsEnableOrDisable(false, false);
		}
	}
	
	/**
	 * Android or iOS enabling
	 * @param isAndroid
	 * @param isiOS
	 */
	private void radioButtonsEnableOrDisable(boolean isAndroid, boolean isiOS) {
		androidRadioBtn.setDisable(isAndroid);
		iosRadioBtn.setDisable(isiOS);
	}
	
	
	/**
	 * used to grab the page source from URL and extracting the elements
	 */
	ArrayList<PageElements>  pageElementsList;
	@FXML
	private void handleElementsButtonAction(ActionEvent event) {
		
		if(elementsButton.getText().toString().equalsIgnoreCase("Ui Viewer")) {
			String automatorUiPath = sdkPath + File.separator + "tools" + File.separator + "bin" + File.separator + "uiautomatorviewer";
			if (isWindows()) {
				String killAdb = "adb kill-server";
				executeAutomatorCommand(killAdb);
			} else {
				String killAdb = "killall adb";
				executeAutomatorCommand(killAdb);
			}
			executeAutomatorCommand(automatorUiPath);
		} else {
		isRefresh = false;
		pageElementsList = new ArrayList<PageElements>();
		try {
			if (driver != null) {
				

				writeFile(driver.getPageSource(), "pageSource", environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator, ".xml");

				try {
					DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = domFactory.newDocumentBuilder();
					Document doc = builder.parse(environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator + "pageSource.xml");
					doc.getDocumentElement().normalize();
					pageElementsList = getEntryList(doc);

					pageElementsList.sort(Comparator.comparing(PageElements::getName).reversed());
					Node source = (Node) event.getSource();
					Window theStage = source.getScene().getWindow();
					getElements(theStage);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e1) {
			errorLabel.setVisible(true);
			Platform.runLater(() -> {	
			if(submitButton.getText().toString().equalsIgnoreCase("connect")) { 
			elementsButton.setText("Ui Viewer");
			} else {
				elementsButton.setVisible(false);
			}
			});
			//
			catchBlock();
			stopAppiumServer();
			errorLabel.setText("A session is either terminated or not started.");
			e1.printStackTrace();
		}
	 }
	}
	
	/**
	 * used to pick a file(.ipa/.apk) from local machine 
	 * @param event
	 * @throws Exception
	 */
	@FXML
	private void handleMouseClicked(ActionEvent event) throws Exception {
		if(isAPKAvailable) { 
			apkChooserField.setText(toogleExtension.contains("apk") ?  "Choose APK file" : "Choose IPA file"); 
			isAPKAvailable =  false;
		}
		errorLabel.setVisible(false);
		progressIndicator.setVisible(true);
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Choose file (" + toogleExtension + ")", toogleExtension);
		if(apkChooserField.getText().toString().contains(toogleExtension.replace("*", ""))) {
			fileChooser.setInitialDirectory(new File(apkChooserField.getText().toString().replace(File.separator + fileName, "")));
		}
		fileChooser.getExtensionFilters().add(extFilter);
		Node source = (Node) event.getSource();
		Window theStage = source.getScene().getWindow();
		File file = fileChooser.showOpenDialog(theStage);
		if (file != null) {
			System.out.println(file);
			fileName = file.getName();
			System.out.println("fileName : " + fileName);
			createPackageBatFile(file.getName());
			apkChooserField.setText(file.getPath());
			commandScripts(extension);
			
		} else {
			progressIndicator.setVisible(false);
		}

	}
	
	/**
	 * Creating the file for launcher activity name and package
	 * @param name
	 */
	public void createPackageBatFile(String name) {
		try {
			String batFile = "";
			String path = File.separator + "mobile" + File.separator + "android" + File.separator +name;
			if(isWindows()) {
				batFile = "%AUTOMATION_MOBILE_DRIVE%"
					+ "\ncd %AUTOMATION_PACKAGE%"
					+ "\naapt dumb badging %AUTOMATION_PATH%" + path
					+ "\n";
			} else if(isMac()){
				 batFile = "source ~/.bash_profile\ncd $AUTOMATION_PACKAGE"
						+ "\naapt dumb badging $AUTOMATION_PATH" + path
						+ "\n";
			} else {
				 batFile = "source ~/.bashrc\ncd $AUTOMATION_PACKAGE"
							+ "\naapt dumb badging $AUTOMATION_PATH" + path
							+ "\n";
			}
			createBatFile("package", batFile , extension);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fields enable or disable 
	 * @param isEnable
	 */
	private void setFieldsEnableorDisable(boolean isEnable) {
		deviceNameField.setDisable(isEnable);
		platformNameField.setDisable(isEnable);
		versionField.setDisable(isEnable);
		udidField.setDisable(isEnable);
		packageNameField.setDisable(isEnable);
		launcherField.setDisable(isEnable);
		apkChooserField.setDisable(isEnable);
	}

	/**
	 * Creating bat file/ shell script file
	 * @param batFileName
	 * @param data
	 * @param extension
	 * @throws Exception
	 */
	private void createBatFile(String batFileName, String data, String extension) throws Exception {
		try {
			writeFile(data, batFileName , environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator , extension);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	// ======================================== POPUP WINDOW ===========================================//
	/**
	 * used to displaying the elements in popup
	 */

	private boolean isRefresh;
	private Stage dialog;
	private ProgressIndicator progress;
	private Label errorTextField;
	private Button saveElements;
	private String XML_ENCODER_FORMAT = "ISO-8859-1";
	private Button refreshElements;
	private ArrayList<PageElements> refreshList;
	
	@SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
	private void getElements(Window theStage) {
		if (pageElementsList.size() > 0) {		   
			dialog = new Stage();
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initOwner(theStage);
			VBox dialogVbox = new VBox(20);
			dialog.initStyle(StageStyle.UTILITY);
			dialog.resizableProperty().setValue(Boolean.FALSE);
			HBox elementsHbox = new HBox();
			final Label label = new Label("Elements");
			TextField filterField = new TextField();
			
			filterField.setFocusTraversable(false);
			filterField.setFont(new Font("proximaNovaSemiBold", 12));
			filterField.setPromptText("Search Elements");
			
			filterField.setMinHeight(25);
			
			filterField.setMinWidth(200);
			label.setPadding(new Insets(0, 10, 0, 0));
			filterField.setPadding(new Insets(0, 10, 0, 0));
		
	

			final TextField textField = new TextField();
			final TextField descTextField = new TextField();
			progress = new ProgressIndicator();
			progress.setVisible(false);
			errorTextField = new Label();
			textColor(errorTextField);
			
			elementsHbox.getChildren().addAll(label, filterField);
			HBox hBox = new HBox();
			hBox.setAlignment(Pos.CENTER);

			saveElements = new Button("Save Elements");
			refreshElements = new Button("Add Elements");
			textColor(refreshElements);
			textColor(saveElements);

			hBox.getChildren().addAll(textField, descTextField, saveElements, refreshElements, progress);

			textField.setFont(new Font("proximaNovaSemiBold", 14));
			textField.setPromptText("Enter Page Name");
			textField.setMinWidth(200);

			descTextField.setFont(new Font("proximaNovaSemiBold", 14));
			descTextField.setPromptText("Enter Page Description");
			descTextField.setMinWidth(200);

			label.setFont(new Font("Arial", 20));
			hBox.setMargin(refreshElements, new Insets(5));
			hBox.setMargin(saveElements, new Insets(5));
			hBox.setMargin(descTextField, new Insets(5));
			hBox.setMargin(textField, new Insets(5));
			refreshElements.setMaxHeight(60);
			saveElements.setMaxHeight(60);
			HBox iframeBox = new HBox();

			TableColumn<Map, String> firstDataColumn = new TableColumn<>("Field Name");
			TableColumn<Map, String> secondDataColumn = new TableColumn<>("Field Type");
			TableColumn<Map, String> thirdDataColumn = new TableColumn<>("Field Locator");

			firstDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");

			firstDataColumn.setCellValueFactory(new MapValueFactory(COLUMN1_MAP_KEY));
			firstDataColumn.setMinWidth(200);

			secondDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");
			secondDataColumn.setCellValueFactory(new MapValueFactory(COLUMN2_MAP_KEY));
			secondDataColumn.setMinWidth(200);

			thirdDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");
			thirdDataColumn.setCellValueFactory(new MapValueFactory(COLUMN3_MAP_KEY));
			thirdDataColumn.setMinWidth(420);

			firstDataColumn.setSortable(false);
			secondDataColumn.setSortable(false);
			thirdDataColumn.setSortable(false);

			ObservableList<Map> data;

			TableView tableView = new TableView<>(data = generateDataInMap(pageElementsList));
			
			tableView.setEditable(true);
			tableView.setBackground(null);
			tableView.setFocusTraversable(false);
			tableView.getSelectionModel().setCellSelectionEnabled(true);

	
			tableView.getColumns().setAll(firstDataColumn, secondDataColumn, thirdDataColumn);
			Callback<TableColumn<Map, String>, TableCell<Map, String>> cellFactoryForMap = (
					TableColumn<Map, String> p) -> new TextFieldTableCell(new StringConverter() {
						@Override
						public String toString(Object t) {
							return t.toString();
						}

						@Override
						public Object fromString(String string) {
							return string;
						}
					});
			firstDataColumn.setCellFactory(cellFactoryForMap);
			secondDataColumn.setCellFactory(cellFactoryForMap);
			thirdDataColumn.setCellFactory(cellFactoryForMap);
			saveElements.setFocusTraversable(false);
			textField.setFocusTraversable(false);
			descTextField.setFocusTraversable(false);
			if(DIFF_ID > 0) {
				textField.setText(WEB_PAGE_NAME);
				descTextField.setText(WEB_PAGE_DESC);
				descTextField.setDisable(true);
				textField.setDisable(true);
			}
			
			
			saveElements.setOnAction(new EventHandler<ActionEvent>() {
				private boolean isSavingElements;

				@Override
				public void handle(ActionEvent actionEvent) {

					if (isSavingElements) {
						return;
					}

					new Thread() {
						public void run() {

							if (textField.getText().trim().length() > 0) {
								try {
									progress.setVisible(true);
									isSavingElements = true;
									JSONObject jsonObj = new JSONObject(JSON);
									PageDetails details = new PageDetails();
									details.setElements_list(pageElementsList);
									details.setWeb_page_name(textField.getText());
									details.setWeb_page_url("");
									details.setFile_name(fileName);
									details.setDiff_page_id(DIFF_ID);
									if(toogleExtension.contains(".apk")) {
										details.setBundle_id("");
									} else {
										details.setBundle_id(packageNameField.getText().toString().trim());
									}
									details.setModule_id(jsonObj.getInt("module_id"));
									details.setWeb_page_description(descTextField.getText() == null ? "" : descTextField.getText());
									details.setUser_id(jsonObj.getInt("user_id"));
									try {
										Gson gsonObj = new Gson();
										String json = gsonObj.toJson(details);
										boolean isPosted = doSaveElementsToServer(json);
										
										Platform.runLater(new Runnable() {
											@Override
											public void run() {
												isSavingElements = false;
												if (!isPosted) {
													progress.setVisible(false);
												} else if(isPosted && dialog != null) {
													dialog.close();
												}
											}
										});

									} catch (Exception e) {
										e.printStackTrace();
									}

								} catch (JSONException e1) {
									e1.printStackTrace();
								}
							} else {
								try {
									final Tooltip toolTip = new Tooltip("This field is required.");
									addToolTipAndBorderColor(textField, toolTip);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}.start();
				}
			});
			
			refreshElements.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent actionEvent) {
					try {	
						isRefresh = true;
						dialog.close();
						if (driver != null) {
							System.out.println(driver.getPageSource());
							System.out.println("sessionnnnnnnnnn"+ new JSONObject(driver.getSessionDetails()));
							writeFile(driver.getPageSource(), "source", environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator , ".xml");

							try {
								DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
								DocumentBuilder builder = domFactory.newDocumentBuilder();
								Document doc = builder.parse(environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator + "source.xml");
								doc.getDocumentElement().normalize();
								refreshList = getRefreshEntryList(doc);
								
								for (int i = 0; i < refreshList.size(); i++) {
									PageElements refreshPage = refreshList.get(i);
									String xpath = refreshPage.getChild_text();
									boolean isElementAvailable = true;
									for (int j = 0; j < pageElementsList.size(); j++) {
										PageElements page = pageElementsList.get(j);
										String pXpath = page.getChild_text();
										if (xpath.equals(pXpath)) {
											isElementAvailable = false;
											break;
										}
									}
									if (isElementAvailable) {
										pageElementsList.add(refreshPage);
									}
								}
								pageElementsList.sort(Comparator.comparing(PageElements::getName).reversed());
								Node source = (Node) actionEvent.getSource();
								Window theStage = source.getScene().getWindow();
								getElements(theStage);

							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					} catch (Exception e1) {
						errorLabel.setVisible(true);
						Platform.runLater(() -> {		
							if(submitButton.getText().toString().equalsIgnoreCase("connect")) { 
								elementsButton.setText("Ui Viewer");
								} else {
									elementsButton.setVisible(false);
								}
						});
						//elementsButton.setVisible(false);
						stopAppiumServer();
						errorLabel.setText("A session is either terminated or not started.");
						if (dialog != null) {
							dialog.close();
						}
						e1.printStackTrace();
					}
				}
			});
			
			// 1. Wrap the ObservableList in a FilteredList (initially display all data).
			FilteredList<Map> filteredData = new FilteredList<>(data, p -> true);

			// 2. Set the filter Predicate whenever the filter changes.
			filterField.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredData.setPredicate(person -> {
					// If filter text is empty, display all persons.
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}

					// Compare first name and last name of every person with filter text.
					String lowerCaseFilter = newValue.toLowerCase();

					if (person.get(COLUMN1_MAP_KEY).toString().toLowerCase().contains(lowerCaseFilter)) {
						return true; // Filter matches first name.
					} else if (person.get(COLUMN2_MAP_KEY).toString().toLowerCase().contains(lowerCaseFilter)) {
						return true; // Filter matches last name.
					}
					return false; // Does not match.
				});
			});

			// 3. Wrap the FilteredList in a SortedList.
			SortedList<Map> sortedData = new SortedList<>(filteredData);

			// 4. Bind the SortedList comparator to the TableView comparator.
			sortedData.comparatorProperty().bind(tableView.comparatorProperty());

			// 5. Add sorted (and filtered) data to the table.
			tableView.setItems(sortedData);

			final VBox vbox = new VBox();

			vbox.setSpacing(5);
			vbox.setPadding(new Insets(10, 0, 0, 10));
			hBox.setPadding(new Insets(10, 10, 10, 10));
			vbox.getChildren().addAll(elementsHbox, tableView, errorTextField, hBox, iframeBox);

			dialogVbox.getChildren().addAll(vbox);

			Scene dialogScene = new Scene(dialogVbox, 850, 500);
			dialog.setScene(dialogScene);
			dialog.show();
		}
	}
	
	private void textColor(Label errorTextField) {
		if (errorTextField != null) {
			errorTextField.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
			errorTextField.setStyle("-fx-text-fill: red; -fx-base: #ffffff;");
		}
	}

	private void textColor(Button btn) {
		btn.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
		btn.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
	}
	
	private static final String STILE_BORDER_VALIDATION = "-fx-border-color: #FF0000";
	private static void addToolTipAndBorderColor(Node n, Tooltip t) {
		Tooltip.install(n, t);
		n.setStyle(STILE_BORDER_VALIDATION);
	}
	
	/*----------------------------------------- SAVE ELEMENETS TO SERVER ----------------------*/
	/**
	 * This method used to push the json string to server 
	 * @param json
	 * @return boolean
	 * @throws Exception
	 */

	private boolean doSaveElementsToServer(String json) throws Exception {
		try {
			
			System.out.println(json);
			
			URL obj = new URL(ELEMENTS_URL);
			 TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}


					@Override
					public void checkClientTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void checkServerTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}
				} };

				// Install the all-trusting trust manager
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

				// Create all-trusting host name verifier
				HostnameVerifier allHostsValid = new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				};
				// Install the all-trusting host verifier
				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(200000);
			con.setConnectTimeout(200000);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			byte[] isoString = json.getBytes(XML_ENCODER_FORMAT);
			wr.write(isoString, 0, isoString.length);
			//wr.writeBytes(json);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(response.toString());

			JSONObject jsonObject = new JSONObject(response.toString());

			if (jsonObject.has("status") && !jsonObject.getString("status").equalsIgnoreCase("SUCCESS")) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (errorTextField != null) {
							textColor(errorTextField);
							errorTextField.setText(jsonObject.optString("message"));

						}
					}
				});
				return false;

			}
		} catch (Exception e) {
			e.printStackTrace();
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					if (errorTextField != null) {
						textColor(errorTextField);
						errorTextField.setText("Something went wrong.");
					}
				}
			});
			return false;
		}
		return true;
	}
	
	//========================================== PARSING THE ELEMENTS ANDROID AND IOS ======================================//

	/**
	 *  it is used to extract the Android fields like xpath, xpath by id, xpath by name, absolute xpath
	 */
	
	private void parseElements( List<PageElements> entries, String parentXPath, Element parent ) {
	
		String xPathByName = "";
		String xPathByID = "";
		String xPathByIndex = "";
		String xPathByTextWithIndex = "",xPathByText="";
		String xPathByIdWithIndex = "";
		String fieldName = "";
		String fieldType = "";
		String attributes = "";
		String idName = "";
		String indexVal = "";
		String textVal = "";
		String passwordValue = "";
		String absoluteXpath = "";
		String boundVal = "";
		
		String tagName = parent.getTagName();
		String previousAttributeValue = "";
	    NamedNodeMap attrs = parent.getAttributes();
	    for( int i = 0; i < attrs.getLength(); i++ ) {
	        Attr attr = (Attr)attrs.item( i );
	        String attrName = attr.getNodeName();
	        String attrValue =  StringEscapeUtils.escapeJava(attr.getNodeValue());
	        
	        if(attrValue == null || attrValue.trim().isEmpty()) continue;
	        if(attrName.equalsIgnoreCase("text")) {
	           previousAttributeValue = attrValue;
	          attributes = attributes + attrName + attrValue;
	          xPathByText = "//" + tagName + "[@" + attrName +"='" + attrValue + "']";
	          fieldName = attrValue;
	          textVal = attrValue;
	        } 
	        
	        if(attrName.equalsIgnoreCase("value")) {
		           //previousAttributeValue = attrValue;
		          //attributes = attributes + attrName + attrValue;
		         // xPathByValue = "//" + tagName + "[@" + attrName +"='" + attrValue + "']";
		          fieldName = attrValue;
		          //textVal = attrValue;
		     } 
	        
	        if(attrName.equalsIgnoreCase("name")) {
		           previousAttributeValue = attrValue;
		          attributes = attributes + attrName + attrValue;
		          xPathByName = "//" + tagName + "[@" + attrName +"='" + attrValue + "']";
		          fieldName = attrValue;
		          textVal = attrValue;
		     } 
	        
	        if(attrName.equalsIgnoreCase("bounds")) {
		          boundVal = attrValue;
		    } 
	        
	        if(attrName.equalsIgnoreCase("content-desc")) {
	        	xPathByName = "//" + parent.getTagName() + "[@" + attrName + "='" + attrValue + "']";
	        	fieldName = attrValue;
	        }
	        
	        if(attrName.equalsIgnoreCase("resource-id")) {
	        	attributes = attributes + attrName + attrValue;
	        	xPathByID = "//" + tagName + "[@" + attrName +"='" + attrValue + "']";
	        	if(attrValue != null && attrValue.contains("id/")) {
	        		idName = attrValue.split("id/")[1];
	        	}
	        } 
	        
	        if(attrName.equalsIgnoreCase("index")) {
	        	attributes = attributes + attrName + attrValue;
	        	xPathByIndex = "//" + tagName + "[@" + attrName +"='" + attrValue + "']";
	        	indexVal = attrValue;
	        } 
	        
			if (attrName.equalsIgnoreCase("password")) {
				passwordValue = attrValue;
			}
	        
	    }
	    
	    if(!idName.isEmpty() && !indexVal.isEmpty()) {
	    	xPathByIdWithIndex = "//" + tagName + "[contains(@resource-id,'" + idName + "') and @index='" + indexVal + "']";
        } 
	    
	    if(!textVal.isEmpty() && !indexVal.isEmpty()) {
	    	xPathByTextWithIndex = "//" + tagName + "[contains(@text,'" + textVal + "') and @index='" + indexVal + "']";
        }    
	    
	    if(tagName.contains("TextView")) {
	    	fieldType =  "Label";
	    } else if(tagName.contains("EditText")) {
	    	fieldType =  "InputText";
	    } else if(tagName.contains("Button") || tagName.equalsIgnoreCase("ImageButton") || tagName.equalsIgnoreCase("Checkbox") || tagName.equalsIgnoreCase("radiobutton")) {
	    	fieldType =  "Button";
	    } else if(tagName.contains("ImageView")) {
	    	fieldType =  "ImageView";
	    	//fieldName = "ImageView";
	    } else if(tagName.contains("ImageButton")) {
	    	fieldType =  "Button";
	    	//fieldName = "ImageView";
	    } else if(tagName.contains("Spinner")) {
	    	fieldType =  "Button";
	    }  else {
	    	fieldType = "Button";
	    }
	   	    
	    if(fieldName.trim().isEmpty()) {
	    	fieldName = idName;
	    }
	    
	    // For android password field not visible so getting the password fieldName is id/placeholder
	    if(tagName.equalsIgnoreCase("android.widget.EditText") && passwordValue.equals("true") && xPathByID.trim().isEmpty() && xPathByName.isEmpty()) {
			if (passwordValue.equals("true")) {
				absoluteXpath = "//" + tagName + "[@password = '" + true + "']";
				if (fieldName.isEmpty()) {
					fieldName = "Password_Text";
				}
			}
	    }
	    
	    if(tagName.equalsIgnoreCase("android.widget.ImageView")) {
	    	if (fieldName.isEmpty()) {
				fieldName = "ImageView - No content-desc - " + boundVal;
			}
	    }
	    
	    if(tagName.equalsIgnoreCase("android.widget.EditText")) {
	    	if(fieldName.isEmpty() && !previousAttributeValue.isEmpty()) {
	    		fieldName = previousAttributeValue;
	    		previousAttributeValue = "";
	    	} else if (fieldName.isEmpty()) {
				fieldName = "InputText-No Text - " + boundVal;
			}
	    }
	    
	    
	    PageElements pageElements = new PageElements();
		if(xPathByID!= null && !xPathByID.isEmpty() && parentXPath.trim().equalsIgnoreCase(xPathByID.trim())) {
			xPathByID = null;
		} 
		
		if(xPathByName!= null && !xPathByName.isEmpty() && parentXPath.equalsIgnoreCase(xPathByName)) {
			xPathByName = null;
		}
		
		String xpathById = "";
		if (xPathByID != null && !xPathByID.trim().isEmpty()) {
			xpathById = xPathByID;
		} else if (xPathByName != null && !xPathByName.trim().isEmpty()) {
			xpathById = xPathByName;
		} else if (parentXPath != null && !parentXPath.trim().isEmpty()) {
			xpathById = parentXPath;
		} else {
			xpathById = "Not Defined XPATH";
		}
		
		if(fieldName.trim().isEmpty()) {
			fieldName = idName;
		}
		if(!fieldName.equalsIgnoreCase("Password_Text")) {
			pageElements.setXpath("absolute_xpath" + seperatorVal + parentXPath);
		} else {
			pageElements.setXpath("absolute_xpath" + seperatorVal +  absoluteXpath);
		}
		pageElements.setRelative_xpath_by_name("xpath_text" + seperatorVal + xPathByText);
		pageElements.setRelative_xpath_by_id("xpath_id" + seperatorVal + xPathByID);
		pageElements.setName((fieldName));// field name
		pageElements.setInput_type(fieldType); // field type
		pageElements.setIs_validate(true);
		pageElements.setChildRelativeXpathId("xpath_id" + seperatorVal + xpathById); // only for display
		pageElements.setTag_name(tagName.replaceAll("android.widget.", ""));
		pageElements.setChild_id(pageElements.getRelative_xpath_by_id());
		preferField(pageElements.getRelative_xpath_by_id(),pageElements.getRelative_xpath_by_name(), pageElements.getXpath(), pageElements);
		
		pageElements.setAll_xpaths(
	
		(absoluteXpath.isEmpty() ? "" : ("absolute_xpath" + seperatorVal +  absoluteXpath + "XPATH_SEPARATOR" ))
		+ (xPathByText.isEmpty() ? "" :("xpath_text" + seperatorVal + xPathByText+ "XPATH_SEPARATOR"))
		+ (xPathByName.isEmpty() ? "" : ("xpath_name" + seperatorVal + xPathByName + "XPATH_SEPARATOR"))
		+ (xpathById.isEmpty() ? "" : ("xpath_id" + seperatorVal + xpathById+ "XPATH_SEPARATOR"))
		+ (xPathByIndex.isEmpty() ? "" : ("xpath_index" + seperatorVal + xPathByIndex+ "XPATH_SEPARATOR"))
		+ (xPathByIdWithIndex.isEmpty() ? "" : ("xpath_id_index" + seperatorVal + xPathByIdWithIndex+ "XPATH_SEPARATOR"))
		+ (xPathByTextWithIndex.isEmpty() ? "" : ("xpath_text_index" + seperatorVal + xPathByTextWithIndex+ "XPATH_SEPARATOR")));
		pageElements.setChild_text(attributes);
		
		/* Adding the elements to list */
		if (!tagName.contains("Layout") && !pageElements.getName().trim().isEmpty()) {
			if (isRefresh) {
				refreshList.add(pageElements);
			} else {
				pageElementsList.add(pageElements);
			}
			idName = "";
			absoluteXpath = "";
			fieldName = "";
		}
	    HashMap<String, Integer> nameMap = new HashMap<String, Integer>();
	    NodeList children = parent.getChildNodes();
	   
	    for( int i = 0; i < children.getLength(); i++ ) {
	    	org.w3c.dom.Node child = children.item(i);
	        if( child instanceof Text ) {
	        	for( int k = 0; k < attrs.getLength(); k++ ) {
	    	        Attr attr = (Attr)attrs.item( k );
	    	        String attrName = attr.getNodeName();
	    	        String attrValue = attr.getNodeValue();
	    	        if(attrValue == null || attrValue.trim().isEmpty()) continue;
	    	        if(attrName.equalsIgnoreCase("text")) {
	    	          xPathByName = "//" + parent.getTagName() + "[@" + attrName + "='" + attrValue + "']";
	    	        } 
	    	        
	    	        if(attrName.equalsIgnoreCase("resource-id")) {
	    	        	xPathByID = "//" + parent.getTagName() + "[@" + attrName + "='" + attrValue + "']";
	    	        }     	        
	    	    }
	        	String data = ((Text)child).getData();
	        	if(data != null && !data.trim().isEmpty()) {
	        		data = "='" + data+"'";
	        	} else {
	        		data = "";
	        	}
	           // entries.add( parentXPath + data + "\nBY ID : " + xPathByID + "\nXPATH BY NAME : " + xPathByName + "\nXPATH BY Class : " + xPathByClass + "\nFieldType : " + parent.getTagName() +"\n"+"\n\n\n");
	        } else if( child instanceof Element ) {
	            String childName = child.getNodeName();
	            Integer nameCount = nameMap.get( childName );
	            nameCount = nameCount == null ? 1 : nameCount + 1;
	            nameMap.put( child.getNodeName(), nameCount );
	            parseElements( entries, parentXPath + "/" + childName + "[" + nameCount + "]", (Element)child);
	        }
	    }
	}

	
	String selectPreferField = "";
	private void preferField(String xpathId,String xpathName, String absoluteXpath,PageElements pageElements) {
		//Id,Name,xpath Name,xpath text,Css xpath,absolutexpath,xpath,Linktext
		try {
			
			if (!xpathId.isEmpty() && !xpathId.equalsIgnoreCase("xpath_id=FIELD_VALUE=") && xpathId.contains("xpath_id" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_id");
				pageElements.setSelected_xpath(xpathId.split("xpath_id"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!xpathName.isEmpty() && !xpathName.equalsIgnoreCase("xpath_text=FIELD_VALUE=") && xpathName.contains("xpath_text" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_text");
				pageElements.setSelected_xpath(xpathName.split("xpath_text"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!absoluteXpath.isEmpty() && !absoluteXpath.equalsIgnoreCase("absolute_xpath=FIELD_VALUE=") && absoluteXpath.contains("absolute_xpath" + seperatorVal)) {
				pageElements.setPrefered_field("absolute_xpath");
				pageElements.setSelected_xpath(absoluteXpath.split("absolute_xpath"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *  it is used to extract the iOS elements like xpath, xpath by id, xpath by name, absolute xpath
	 */
	private String value = "";
	private String seperatorVal = "=FIELD_VALUE=";
	private  void parseiOSElements( List<PageElements> entries, String parentXPath, Element parent ) {
		String xPathByName = "";
		String xPathByID = "";
		String fieldName = "";
		String fieldType = "";
		String attributes = "";
		String idName = "";
		String absoluteXpath = "";
		
		String tagName = parent.getTagName();
	    NamedNodeMap attrs = parent.getAttributes();
	    for( int i = 0; i < attrs.getLength(); i++ ) {
	        Attr attr = (Attr)attrs.item( i );
	        String attrName = attr.getNodeName();
	        String attrValue =  StringEscapeUtils.escapeJava(attr.getNodeValue());
	       
	        if(attrValue == null || attrValue.trim().isEmpty()) continue;
	        if(attrName.equalsIgnoreCase("name")) {
	          attributes = attributes + attrName + attrValue;
	          xPathByName = "//" + tagName + "[@" + attrName + "='" + attrValue + "']";
	          fieldName = attrValue;
	        } else if(attrName.equalsIgnoreCase("id")) {
	        	attributes = attributes + attrName + attrValue;
	        	xPathByID = "//" + tagName + "[@" + attrName +"='" + attrValue + "']";
	        	if(attrValue != null && attrValue.contains("id/")) {
	        		idName = attrValue.split("id/")[1];
	        	}
	        } else if(attrName.equalsIgnoreCase("value")) {
	        	attributes = attributes + attrName + attrValue;
	        	value = attrValue;
	        	fieldName = attrValue;
	        }    
	        
	    }
	    
	    if(tagName.equalsIgnoreCase("UITextView") || tagName.equalsIgnoreCase("XCUIElementTypeStaticText")) {
	    	fieldType = "Label"; 
	    } else if(tagName.equalsIgnoreCase("UITextField") || tagName.equalsIgnoreCase("XCUIElementTypeTextView") || tagName.equalsIgnoreCase("XCUIElementTypeTextField") || tagName.equalsIgnoreCase("XCUIElementTypeSecureTextField")) {
	    	fieldType =  "InputText";
	    } else if(tagName.equalsIgnoreCase("XCUIElementTypeButton") || tagName.equalsIgnoreCase("UIButton") || tagName.equalsIgnoreCase("Checkbox") || tagName.equalsIgnoreCase("radiobutton")) {
	    	fieldType =  "Button";
	    } else if(tagName.equalsIgnoreCase("XCUIElementTypeImage")) {
	    	fieldType =  "ImageView";
	    	//fieldName = "ImageView";
	    } else if(tagName.equalsIgnoreCase("Spinner")) {
	    	fieldType =  "Button";
	    }  else {
	    	fieldType = "Button";
	    }
	   	    
	    if(fieldName.trim().isEmpty()) {
	    	fieldName = idName;
	    }
	    
	    PageElements pageElements = new PageElements();
		if(xPathByID!= null && !xPathByID.isEmpty() && parentXPath.trim().equalsIgnoreCase(xPathByID.trim())) {
			xPathByID = null;
		} 
		
		if(xPathByName!= null && !xPathByName.isEmpty() && parentXPath.equalsIgnoreCase(xPathByName)) {
			xPathByName = null;
		}
		
		String xpathById = "";
		if (xPathByID != null && !xPathByID.trim().isEmpty()) {
			xpathById = xPathByID;
		} else if (xPathByName != null && !xPathByName.trim().isEmpty()) {
			xpathById = xPathByName;
		} else if (parentXPath != null && !parentXPath.trim().isEmpty()) {
			xpathById = parentXPath;
		} else {
			xpathById = "Not Defined XPATH";
		}
		
		if(fieldName.trim().isEmpty()) {
			fieldName = idName;
			if(tagName.equalsIgnoreCase("UITextField") || tagName.equalsIgnoreCase("XCUIElementTypeTextView") || tagName.equalsIgnoreCase("XCUIElementTypeTextField") || tagName.equalsIgnoreCase("XCUIElementTypeSecureTextField")) {
				fieldName = value;
			}
		}
		
		pageElements.setXpath("absolute_xpath" + seperatorVal + (absoluteXpath.trim().equals("") ? parentXPath.replace("/AppiumAUT[1]", "") : absoluteXpath.replace("/AppiumAUT[1]", "")));
		
		pageElements.setRelative_xpath_by_name(StringEscapeUtils.unescapeJava("xpath_text" + seperatorVal + xPathByName));
		pageElements.setRelative_xpath_by_id("xpath_id" + seperatorVal + xPathByID);
		pageElements.setName(StringEscapeUtils.unescapeJava(fieldName));// field name
		pageElements.setInput_type(fieldType); // field type
		// System.out.println(pageElements.getName());
		pageElements.setChildRelativeXpathId("xpath_id" + seperatorVal + xpathById); // only for display
		pageElements.setTag_name(fieldType);
		pageElements.setChild_id("");
		pageElements.setChild_text(attributes);
		preferField(pageElements.getRelative_xpath_by_id(),pageElements.getRelative_xpath_by_name(), pageElements.getXpath(), pageElements);
		
		pageElements.setAll_xpaths(pageElements.getRelative_xpath_by_id() + "XPATH_SEPARATOR" + pageElements.getRelative_xpath_by_name() + "XPATH_SEPARATOR" + pageElements.getXpath());
		
		/* Adding the elements to list */
		if (!pageElements.getName().trim().isEmpty()) {
			if (isRefresh) {
				refreshList.add(pageElements);
			} else {
				pageElementsList.add(pageElements);
			}
			idName = "";
			absoluteXpath = "";
			fieldName = "";
		}
	    HashMap<String, Integer> nameMap = new HashMap<String, Integer>();
	    NodeList children = parent.getChildNodes();
	   
	    for( int i = 0; i < children.getLength(); i++ ) {
	    	org.w3c.dom.Node child = children.item(i);
	        if( child instanceof Text ) {
	        	for( int k = 0; k < attrs.getLength(); k++ ) {
	    	        Attr attr = (Attr)attrs.item( k );
	    	        String attrName = attr.getNodeName();
	    	        String attrValue = attr.getNodeValue();
	    	        if(attrValue == null || attrValue.trim().isEmpty()) continue;
	    	        if(attrName.equalsIgnoreCase("name")) {
	    	          xPathByName = "//" + parent.getTagName() + "[@" + attrName + "='" + attrValue + "']";
	    	        } 
	    	        
	    	        if(attrName.equalsIgnoreCase("id")) {
	    	        	xPathByID = "//" + parent.getTagName() + "[@" + attrName + "='" + attrValue + "']";
	    	        } 
	     
	    	        if(attrName.equalsIgnoreCase("class")) {
	    	        	//xPathByClass = "//" + parent.getTagName() + "[@" + attrName + "='" + attrValue + "']";
	    	        }
	    	        
	    	    }
	        	String data = ((Text)child).getData();
	        	if(data != null && !data.trim().isEmpty()) {
	        		data = "='" + data+"'";
	        	} else {
	        		data = "";
	        	}
	           // entries.add( parentXPath + data + "\nBY ID : " + xPathByID + "\nXPATH BY NAME : " + xPathByName + "\nXPATH BY Class : " + xPathByClass + "\nFieldType : " + parent.getTagName() +"\n"+"\n\n\n");
	        } else if( child instanceof Element ) {
	            String childName = child.getNodeName();
	            Integer nameCount = nameMap.get( childName );
	            nameCount = nameCount == null ? 1 : nameCount + 1;
	            nameMap.put( child.getNodeName(), nameCount );
	            parseiOSElements( entries, parentXPath + "/" + childName + "[" + nameCount + "]", (Element)child);
	        }
	    }
	}

	// ==================================== List of Page Elements =============================================//
	
	// Popup window Fields list
	
	@SuppressWarnings("rawtypes")
	private ObservableList<Map> generateDataInMap(List<PageElements> pagedetailsList) {
		ObservableList<Map> allData = FXCollections.observableArrayList();
		for (int i = 0; i < pagedetailsList.size(); i++) {
			
			System.out.println(pagedetailsList.get(i).getName());
			Map<String, String> dataRow = new HashMap<>();

			String value1 = pageElementsList.get(i).getName();
			String value2 = pageElementsList.get(i).getInput_type();
			String value3 = pageElementsList.get(i).getSelected_xpath();
			dataRow.put(COLUMN1_MAP_KEY, value1);
			dataRow.put(COLUMN2_MAP_KEY, value2);
			dataRow.put(COLUMN3_MAP_KEY, value3);
			allData.add(dataRow);
		}
		return allData;
	}
	
	// It will returns the elements list
	
	private ArrayList<PageElements> getEntryList(Document doc) {
		// pageElementsList = new ArrayList<PageElements>();
		Element root = doc.getDocumentElement();
		if (toogleExtension.contains(".ipa")) {
			parseiOSElements(pageElementsList, "/" + root.getNodeName() + "[1]", root);
		} else {
			parseElements(pageElementsList, "/" + root.getNodeName() + "[1]", root);
		}

		return pageElementsList;
	}
	
	
	/* It will returns the refresh elements list */
	
	private ArrayList<PageElements> getRefreshEntryList(Document doc) {
		refreshList = new ArrayList<PageElements>();
		Element root = doc.getDocumentElement();
		if (toogleExtension.contains(".ipa")) {
			parseiOSElements(refreshList, "/" + root.getNodeName() + "[1]", root);
		} else {
			parseElements(refreshList, "/" + root.getNodeName() + "[1]", root);
		}

		return refreshList;
	}
	
	// ====================================== ENVIRONMENT VARAIBLES =================== //
	
	/**
	 * Getting the environment variables from environmentVariables.sh
	 * returns @NODE_PATH/
	 * @APPIUM_PATH/
	 * @ANDROID_HOME/
	 * @AUTOMATION_PATH
	 */
	
	private ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + automationPath + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}
	
	//========================= USED TO GET THE DEVICE ID , PACKAGE NAME AND LAUNCHER ACTIVITY ==========================//
	
	/**
	 * used to get the android/iOS deviceId create and execute script file 
	 */
	private String deviceId = "";
	private String appiumServiceUrl;
	private void commandScripts(String extension) throws Exception {
		
		try {
			//String batFile = toogleExtension.contains(".apk") ? "adb devices" : "idevice_id -l" ;
			String batFile = toogleExtension.contains(".apk") ? "adb devices" : "ios-deploy -c";
			if(isWindows()) {
			} else if(isMac()){
				batFile = "source ~/.bash_profile\n" + batFile;
			} else {
				batFile = "source ~/.bashrc\n" + batFile;
			}
			createBatFile(GET_ADB_DEVICES, batFile , extension);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String  deviceName = "";
		if(toogleExtension.contains(".apk")) {
			executeCommand(GET_PACKAGE_LAUNCHER_NAME, "package",extension);
			 deviceId = executeCommand(GET_ADB_DEVICES, "adb",extension);
		} else {
			try {
				deviceId = executeCommand(GET_ADB_DEVICES, "adb", extension);
				String[] splitDevices = deviceId.split("-");
				if (splitDevices != null && splitDevices.length == 3) {
					deviceName = splitDevices[0];
					deviceId = splitDevices[2];
					platformNameField.setText("iOS");
					deviceNameField.setText(deviceName);
					String version = "";
					version = executeCommand("instruments", "INSTRUMENTS", extension);

					if(version != null && !version.trim().isEmpty()) {
						String[] finalSliptOfVersion = version.split("\\.");
						if (finalSliptOfVersion.length >= 1) {
							versionField.setText(finalSliptOfVersion[0] + "." + finalSliptOfVersion[1]);
						}
					
					}
				}
			} catch (Exception e) {
				System.out.println();
			}
		}
		
		if (deviceId != null && !deviceId.isEmpty()) {
			udidField.setText(deviceId);
			if(toogleExtension.contains(".apk")) {
				String batFile ="adb -s " + deviceId + " shell getprop ro.product.model"
						+ "\nadb -s " + deviceId + " shell getprop ro.build.version.release"
						+ "\n";
				
				if(isWindows()) {
				} else if(isMac()){
					batFile = "source ~/.bash_profile\n" + batFile;
				} else {
					batFile = "source ~/.bashrc\n" + batFile;
				}
				createBatFile("productDetails", batFile,extension);
				executeCommand(GET_PRODUCT_DETAILS, "", extension);
			}
		}
		
		executeCommand(KILL_ADB, "adb", extension);
		progressIndicator.setVisible(false);
	}
	
	/**
	 * used to execute the command
	 * @param batFilePath
	 * @param executionName
	 * @param extension
	 * @return android deviceId / iOS deviceId / node path /  package name / launcher activity name / device model / device version
	 */
	private String executeCommand(String batFilePath, String executionName, String extension) {
		try {
			String isMacProduct = null;
			String line;
			String packageName = "";
			String launcherActivity = "";
			boolean listDevices = false;
			Process p = Runtime.getRuntime().exec(environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator + batFilePath + extension);
			
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			Boolean isProductModel =  null;
			while ((line = bri.readLine()) != null) {
				if (executionName.equalsIgnoreCase("INSTRUMENTS")) {
					System.out.println(deviceId.replaceAll(" ", ""));
					if (line.contains(deviceId.replaceAll(" ", ""))) {
						String[] version = line.split("\\(");
						version = version[1].split("\\)");
						return version[0];
					}
				} else {
					if (executionName.equals("node")) {
						return line;
					} else if (executionName.equalsIgnoreCase("adb")) {
						if (toogleExtension.contains(".ipa")) {
							if (line.contains("Found") && line.contains("iPhone 5s")) {
								String[] udidSplit = line.split("]");
								String[] osUdid = udidSplit[1].split("\\(");
								String deviceName = osUdid[1].split(",")[1].replaceAll("\\s+", "");
								String platformName = osUdid[2].split(",")[2].replaceAll("\\s+", "");
								String finalString = osUdid[0].replaceAll("Found", "").replaceAll("\\s+", "");
								// String[] finalString = osUdid[1].split(",");
								return deviceName + "-" + platformName + "-" + finalString.replaceAll(" ", "");
							}
							
							else if (line.contains("Found")) {
								String[] udidSplit = line.split("]");
								String[] osUdid = udidSplit[1].split("\\(");
								String deviceName = osUdid[1].split(",")[1].replaceAll("\\s+", "");
								String platformName;
								try {
								 platformName = osUdid[1].split(",")[2].replaceAll("\\s+", "");
								} catch(Exception e) {
									try {
										platformName = osUdid[2].split(",")[1].replaceAll("\\s+", "");
										} catch (Exception e1) {
											platformName = "iPhone";
										}
								}
								String finalString = osUdid[0].replaceAll("Found", "").replaceAll("\\s+", "");
								// String[] finalString = osUdid[1].split(",");
								return deviceName + "-" + platformName + "-" + finalString.replaceAll(" ", "");
							}
						} else {
							if (listDevices) {
								String[] deviceUDID = line.split("device");
								System.out.println(deviceUDID[0]);
								listDevices = false;
								return deviceUDID[0];
							}
							if (line.contains("List of devices attached")) {
								listDevices = true;
							}
						}
					} else if (executionName.equalsIgnoreCase("package")) {
						if (line != null && line.contains("launchable-activity:")) {
							launcherActivity = line.split("=")[1].split(" ")[0];
							System.out.println("Launcher Activity : " + launcherActivity);
							launcherField.setText(launcherActivity.replace("'", ""));
							return (packageName + "$$" + launcherActivity).replaceAll("'", "");
						}

						if (line != null && line.contains("package:")) {
							packageName = line.split("=")[1].split(" ")[0];
							packageNameField.setText(packageName.replace("'", ""));
							System.out.println("packageName : " + packageName);
						}

					} else {
						if (isMac()) {
							if (!line.contains("adb") && !line.contains("daemon")) {

								if (isMacProduct == null) {
									isMacProduct = line;
									deviceNameField.setText(line);
								} else {
									if (line.trim().split("\\.").length > 0) {
										versionField.setText(line.trim().split("\\.")[0]);
									}
								}
							}

						} else {
							if (line != null && !line.isEmpty()) {
								if (line.contains("ro.product.model")) {
									isProductModel = true;
									continue;
								} else if (line.contains("ro.build.version")) {
									isProductModel = false;
									continue;
								}

								if (isProductModel != null) {
									if (isProductModel) {
										deviceNameField.setText(line);
									} else {
										if (line.trim().split("\\.").length > 0) {
											versionField.setText(line.trim().split("\\.")[0]);
										}
									}
								}
							}
						}
					}
				}
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
			
			System.out.println("Done.");
		
		} catch (Exception err) {
			err.printStackTrace();
		}
		return "";
	}
	
	
	// ============================================= APPIUM SETUP ==============================================//
	/**
	 * Configuring the node and appium path
	 * @return
	 */
	 
	private AppiumDriverLocalService setUpAppiumDriver() {	
		AppiumDriverLocalService appiumDriverService = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
				.usingAnyFreePort()
				.usingDriverExecutable(new File(nodeJSPath))
				.withAppiumJS(new File(appiumServerJSPath)));
		return appiumDriverService;
	}
	
	/**
	 * Used to start Appium Server 
	 * @return boolean appium is started or not
	 */
	private boolean startAppiumServer() {
		if (appiumDriverService != null) {
			appiumDriverService.start();
			
			return appiumDriverService.isRunning();
		}
		return false;
	}
	
	/**
	 * used to stop the Appium  server
	 */
	private void stopAppiumServer() {
		if (appiumDriverService != null) {
			
			//elementsButton.setVisible(false);
			Platform.runLater(() -> {
				
				setFieldsEnableorDisable(false);
				submitButton.setText("Connect");
				if(submitButton.getText().toString().equalsIgnoreCase("connect")) { 
					elementsButton.setText("Ui Viewer");
					} else {
						elementsButton.setVisible(false);
					}
				
			});
			
			appiumDriverService.stop();
			System.out.println(" Stopped Appium Server");
		}   
	}
	
	//======================================== LAUNCH APP =========================================//
	
	/**
	 * Desired Capabilities to launch iOS and Android
	 * @param deviceName
	 * @param paltformName
	 * @param platformVersion
	 * @param udid
	 * @param filePath
	 * @param packageName
	 * @param launcherActivity
	 */
	
	private void launchApp(String deviceName, String paltformName, String platformVersion, String udid,
			String filePath, String packageName, String launcherActivity) {
		try {
			if (appiumDriverService != null) {
				appiumServiceUrl = appiumDriverService.getUrl().toString();
				//appiumServiceUrl = "new URL(http://127.0.0.1:4723/wd/hub)"
				System.out.println(" The URL is : " + appiumServiceUrl);
				DesiredCapabilities caps = new DesiredCapabilities();
				caps.setCapability("deviceName", deviceName);
				caps.setCapability("platformName", paltformName);
				caps.setCapability("fullreset", true);
				caps.setCapability("udid", udid.trim());
				caps.setCapability("app", filePath);
				caps.setCapability("platformVersion", platformVersion);
				caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60*10); // ending the session 10 minutes
				if(toogleExtension.contains(".ipa")) {
					caps.setCapability("autoAcceptAlerts",true);
					iosRadioBtn.setDisable(false);
					androidRadioBtn.setDisable(true);
					caps.setCapability("bundleid", packageName);
					caps.setCapability("wdaLaunchTimeout",80000);
					caps.setCapability("wdaStartupRetries", 4);
					caps.setCapability("automationName", "XCUITest");
					driver = new IOSDriver<>(new URL(appiumServiceUrl), caps);
				} else {
					caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
					caps.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
					caps.setCapability("platformVersion", platformVersion);
					androidRadioBtn.setDisable(false);
					iosRadioBtn.setDisable(true);
					caps.setCapability("autoAcceptAlerts", true);
					caps.setCapability("appPackage", packageName);
					caps.setCapability("clearSystemFiles", true);
					caps.setCapability("appActivity", launcherActivity);
					driver = new AndroidDriver<>(new URL(appiumServiceUrl), caps);
					System.out.println("======================"+driver.getSessionDetails());
					
				}
			
				Platform.runLater(() -> {
					submitButton.setText("Disconnect");
				});
				setFieldsEnableorDisable(true);
				Platform.runLater(() -> {		
				elementsButton.setText("Get Elements");
				});
				elementsButton.setVisible(true);
				//stopAppiumServer();
			}
		} catch (Exception e) {
			if(MOBILE_PLATFORM.equalsIgnoreCase("ANDROID")) {
				androidRadioBtn.setDisable(false);
			} else {
				iosRadioBtn.setDisable(false);
			}
			errorLabel.setVisible(true);
			Platform.runLater(() -> {			
				submitButton.setText("Connect");
				String message = e.getLocalizedMessage();
				if(message.contains("Could not find a connected") ||  message.contains("No device connected")) {
					errorLabel.setText("Could not find a connected Android device.");
				} else if(message.contains("was not in the list of connected devices")) {
					errorLabel.setText("Device "+ udid +" was not in the list of connected devices");
				} else if(message.contains("Platform version must be")) {
					errorLabel.setText("Platform version must be 9.3 or above.");
				} else if(message.contains("Could not proxy command to remote server")) {
					errorLabel.setText("Device is offine (or) not connected.");
				} else if(message.contains("Could not install app: 'Command '")){
					errorLabel.setText("Device is not resgistered.");
				} else if(message.contains("Connection refused (Connection refused)")){
					submitButton.fire();
				} else {
					
					errorLabel.setText(e.getLocalizedMessage());
				}
			});
			
			e.printStackTrace();
		}	
		

	}
	
	
	/* Getting the OS */
	private boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	private boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
	
	/*
	 * Killing the node and adb while closing the app and on click of disconnect button
	 * 
	 */
	
	public void closeApp(){
		if(toogleExtension.contains(".apk")) {
		executeCommand(KILL_ADB, "adb", extension);
		} else {
		executeCommand(KILL_NODE, "node", extension);
		}
		stopAppiumServer();	
	}
	
	// creating the file 
	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator +  className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator +  className + fileExtension);
		} finally {
			out.close();
		}
	}
	
	
	private void executeAutomatorCommand(String command) {
		
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(command);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();


			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
	}
	
	/*
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
			String val = value.replaceAll(" ", "");
			val = val.replaceAll("[^a-z_A-Z0-9]", "");
			return val;
		} catch (Exception e) {
			return value;
		}
	}

}
