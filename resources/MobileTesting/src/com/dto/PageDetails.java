package com.dto;

import java.util.ArrayList;

public class PageDetails {

	private String web_page_name;
	private String web_page_description;
	private String web_page_url;
	private ArrayList<PageElements> elements_list;
	private int user_id;
	private int module_id;
	private String file_name;
	private int diff_page_id;
	private String bundle_id;
	
	public String getBundle_id() {
		return bundle_id;
	}

	public void setBundle_id(String bundle_id) {
		this.bundle_id = bundle_id;
	}

	public int getDiff_page_id() {
		return diff_page_id;
	}

	public void setDiff_page_id(int diff_page_id) {
		this.diff_page_id = diff_page_id;
	}
	
	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public ArrayList<PageElements> getElements_list() {
		return elements_list;
	}

	public void setElements_list(ArrayList<PageElements> elements_list) {
		this.elements_list = elements_list;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}


	public String getWeb_page_name() {
		return web_page_name;
	}

	public void setWeb_page_name(String web_page_name) {
		this.web_page_name = web_page_name;
	}

	public String getWeb_page_description() {
		return web_page_description;
	}

	public void setWeb_page_description(String web_page_description) {
		this.web_page_description = web_page_description;
	}

	public String getWeb_page_url() {
		return web_page_url;
	}

	public void setWeb_page_url(String web_page_url) {
		this.web_page_url = web_page_url;
	}

}
