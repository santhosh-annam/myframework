package com.dto;

public class PageElements {

	long element_id;

	int web_page_id;

	String name;

	String input_type;

	String relative_xpath_by_id;

	String relative_xpath_by_name;

	String xpath;

	boolean is_selected;

	String tag_name;

	String child_id;

	String child_text;
	
	boolean is_iframe;

	String iframe_name;
	String iframe_id;
	int iframe_position;
	
	String childRelativeXpathId;

	boolean is_frame;
	String frame_name;
	String frame_id;
	int frame_position;
	String all_xpaths;
	
	private String prefered_field;
	private String selected_xpath;
	private boolean is_validate;
	
	
	public boolean isIs_validate() {
		return is_validate;
	}

	public void setIs_validate(boolean is_validate) {
		this.is_validate = is_validate;
	}

	public String getPrefered_field() {
		return prefered_field;
	}

	public void setPrefered_field(String prefered_field) {
		this.prefered_field = prefered_field;
	}

	public String getSelected_xpath() {
		return selected_xpath;
	}

	public void setSelected_xpath(String selected_xpath) {
		this.selected_xpath = selected_xpath;
	}

	public String getAll_xpaths() {
		return all_xpaths;
	}

	public void setAll_xpaths(String all_xpaths) {
		this.all_xpaths = all_xpaths;
	}

	public String getChildRelativeXpathId() {
		return childRelativeXpathId;
	}

	public void setChildRelativeXpathId(String childRelativeXpathId) {
		this.childRelativeXpathId = childRelativeXpathId;
	}

	public long getElement_id() {
		return element_id;
	}

	public void setElement_id(long element_id) {
		this.element_id = element_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInput_type() {
		return input_type;
	}

	public void setInput_type(String input_type) {
		this.input_type = input_type;
	}

	public String getRelative_xpath_by_id() {
		return relative_xpath_by_id;
	}

	public void setRelative_xpath_by_id(String relative_xpath_by_id) {
		this.relative_xpath_by_id = relative_xpath_by_id;
	}

	public String getRelative_xpath_by_name() {
		return relative_xpath_by_name;
	}

	public void setRelative_xpath_by_name(String relative_xpath_by_name) {
		this.relative_xpath_by_name = relative_xpath_by_name;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public boolean isIs_selected() {
		return is_selected;
	}

	public void setIs_selected(boolean is_selected) {
		this.is_selected = is_selected;
	}

	public String getTag_name() {
		return tag_name;
	}

	public void setTag_name(String tag_name) {
		this.tag_name = tag_name;
	}

	public String getChild_id() {
		return child_id;
	}

	public void setChild_id(String child_id) {
		this.child_id = child_id;
	}

	public String getChild_text() {
		return child_text;
	}

	public void setChild_text(String child_text) {
		this.child_text = child_text;
	}

	public int getWeb_page_id() {
		return web_page_id;
	}

	public void setWeb_page_id(int web_page_id) {
		this.web_page_id = web_page_id;
	}

	public boolean isIs_iframe() {
		return is_iframe;
	}

	public void setIs_iframe(boolean is_iframe) {
		this.is_iframe = is_iframe;
	}

	public String getIframe_name() {
		return iframe_name;
	}

	public void setIframe_name(String iframe_name) {
		this.iframe_name = iframe_name;
	}

	public boolean isIs_frame() {
		return is_frame;
	}

	public void setIs_frame(boolean is_frame) {
		this.is_frame = is_frame;
	}

	public String getFrame_name() {
		return frame_name;
	}

	public void setFrame_name(String frame_name) {
		this.frame_name = frame_name;
	}

	public String getFrame_id() {
		return frame_id;
	}

	public void setFrame_id(String frame_id) {
		this.frame_id = frame_id;
	}

	public int getFrame_position() {
		return frame_position;
	}

	public void setFrame_position(int frame_position) {
		this.frame_position = frame_position;
	}

	public String getIframe_id() {
		return iframe_id;
	}

	public void setIframe_id(String iframe_id) {
		this.iframe_id = iframe_id;
	}

	public int getIframe_position() {
		return iframe_position;
	}

	public void setIframe_position(int iframe_position) {
		this.iframe_position = iframe_position;
	}
}
