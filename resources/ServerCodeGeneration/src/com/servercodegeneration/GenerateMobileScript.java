package com.servercodegeneration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.dto.DatasetsOfTestcase;
import com.dto.Element;
import com.dto.PrimaryInfo;
import com.dto.Screen;
import com.dto.WebScript;
import com.google.gson.Gson;
import com.utils.Utils;

/**
 * This class used to generating the API Testcase's and Dataset's 
 * @TESTNG bat file creation
 * @Tescase File Creation
 * @Screens Creation
 * 
 */

public class GenerateMobileScript { 
	
	private static String mobilePageClasses;
	private static String mobileTestClasses;
	public static PrimaryInfo primaryInfo;
	private static String screenObjects = "";
	private String pageClassPath = "";
	private String projectName;
	public String mobilePlatform;

	/**
	 * Generating the testscases/testsets 
	 * @param testSet
	 * @param output
	 * @param environmentVariable
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public void generateCode(boolean testSet, WebScript output, String environmentVariable) throws Exception {
		
		boolean isDriverInIframe = false;
		boolean isDriverInFrame = false;
		String previousIframeName = "";
		String previousFrameName = "";
		screenObjects = "";
		primaryInfo =  output.getPrimaryInfo();
		mobilePlatform = primaryInfo.getMobilePlatform();
		String testcaseClassStart = "";
		String chromeDriverPath = "chromeDriverPath", URL, screenObjs = "", screenImports = "";
		String fieldType = "", 
				fieldName = "", 
				fieldValue = "", 
				fieldsInfo = "", 
				screenName = "",
				propertiesInfo = "\n\t", 
				xmlFile = "";
		System.out.println("" + primaryInfo.getTestcaseName());	
		URL = primaryInfo.getProjectUrl();
		String testCaseName = Utils.removeUnWantedChars(toTitleCase(primaryInfo.getTestcaseName()));
		projectName = Utils.removeUnWantedChars(primaryInfo.getProjectName()).replaceAll(" ", "").toLowerCase();
		int projectId = primaryInfo.getProjectId();
		String reportsPath = "reportsPath";
		boolean executeAction = primaryInfo.getIsExecute();
		boolean generateAction = primaryInfo.getIsGenerate();
		boolean testcaseOverwrite = primaryInfo.isTestcase_overwrite();
		String baseURL = primaryInfo.getProjectUrl();
		long timeStamp = primaryInfo.getExecutedTimestamp();
		String moduleFolder = Utils.removeUnWantedChars(primaryInfo.getModuleName().toLowerCase());
		String subModule = primaryInfo.getSubModuleName() == null ? "" : primaryInfo.getSubModuleName();
		String subModuleFolder = Utils.removeUnWantedChars(subModule.toLowerCase());
		String testSetName = primaryInfo.getTestsetName() == null ? "" : primaryInfo.getTestsetName();
		String testsetName = Utils.removeUnWantedChars(testSetName);

		String modulename = moduleFolder + ".";
		if (!subModuleFolder.equals("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			modulename = modulename + subModuleFolder + ".";
		}

		mobilePageClasses = "mobile." + projectName + "." + modulename + "mobilepageclasses";
		mobileTestClasses = "mobile." + projectName + "." + modulename + "mobiletestclasses";

		String apiFolder = environmentVariable + File.separator + "src" + File.separator + "test"+ File.separator + "java"+ File.separator + "mobile";
		createDirectory(apiFolder);

		String projectFolder = apiFolder + File.separator +  projectName;
		createDirectory(projectFolder);

		String packageNameFolder = projectFolder + File.separator +  moduleFolder;
		// create module
		createDirectory(packageNameFolder);

		// create sub module
		if (!subModuleFolder.equals("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			packageNameFolder = packageNameFolder + File.separator +  subModuleFolder;
			createDirectory(packageNameFolder);
		}

		String testClassPath = packageNameFolder + File.separator + "mobiletestclasses";
		pageClassPath = packageNameFolder + File.separator +  "mobilepageclasses";

		/* Creating directories if not exists */
		createDirectory(String.valueOf(testClassPath));

		createDirectory(String.valueOf(pageClassPath));
		 	
		propertiesInfo += "PrimaryInfo = " + StringEscapeUtils.escapeJava(new Gson().toJson(primaryInfo)) + "\n\t";
	//	propertiesInfo += "URL = " + StringEscapeUtils.escapeJava(new Gson().toJson(primaryInfo)) + "\n\t";
		if(!testSet) {
		xmlFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >"
				+ "\n<suite name=\"Main Test Suite\" verbose=\"1\">"
				+ "\n\t<test name=\"test\">\n\t\t<classes>"
				+ "\n\t\t\t<class name=\"" + mobileTestClasses +"."
				+ testCaseName + "\" />"
				+ "\n\t\t\t<class name=\"com.fileupload.ReportUpload\" />"
				+ "\n\t\t</classes>\n\t\t</test>\n</suite>";
		
		
		// chromeDriverPath ="environmentVariable\\Resources";
		writeFile(xmlFile, "testng", environmentVariable, ".xml");
		}
		
		
		/*String configFile = writeXml(testCaseName,testCaseName,testCaseName);
		writeFile(configFile, "extent-config", environmentVariable, ".xml");*/
		
		File directory = new File(String.valueOf(testClassPath));
		if (!directory.exists()) {
			directory.mkdir();
		}
		
		int screensCount = 0;
		
		List<DatasetsOfTestcase> datasets = output.getDatasetsOfTestcase();
		int size = datasets.size();
		String names = "";
		for(int i = 0; i < size; i++) {
			propertiesInfo += "dataset" +(i+1) + " = " + datasets.get(i).getDatasetName()+ "\n\t";
		}
		//String datasetNames = "\n\tString[] datasetNames = new String[] {\"" + names + "\"};";
		System.out.println(names);
		boolean isScreensCreated =  false;
		
		propertiesInfo += "datasetsLength = " + size + "\n\t";
		
		/* This loop for generating config file and Screens Creating (GenerateSeleniumScreenObjectScript(screenName, fieldsInfo)) */
		
		for(int i = 0; i < size; i++) {
			if (i == 1) {
				isScreensCreated = true;
			}
			screensCount = screensCount + 1;
			int datasetScreencount = 0;
			for (Screen screen : datasets.get(i).getScreens()) {
				datasetScreencount = datasetScreencount + 1;
				// System.out.println(""+screen.getScreenId());
				System.out.println("screen name" + screen.getName());
				screenName = Utils.removeUnWantedChars(toTitleCase(screen.getName()));
				fieldsInfo = "";
				for (Element elements : screen.getScreenElements()) {

					/**
					 * Config files data
					 */
					if (elements.getFieldType().equals("InputText")) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elements.getFieldName())) + screensCount + datasetScreencount + " = " + (elements.getFieldValue())
								+ "\n\t";
					} else if (elements.getFieldType().equals("Label") && elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elements.getFieldName()))  + screensCount +  datasetScreencount +  " = " + (elements.getValidateText()) + "\n\t";
					} else if (elements.getFieldType().equalsIgnoreCase("Link") && elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elements.getFieldName()))  + screensCount +  datasetScreencount +  " = " + (elements.getValidateText()) + "\n\t";
					} else if (elements.getFieldType().equalsIgnoreCase("Button") && elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elements.getFieldName()))  + screensCount +  datasetScreencount +  " = " + (elements.getValidateText()) + "\n\t";
					} 
					
					if (elements.isElementWait()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elements.getFieldName()))  + screensCount +  datasetScreencount +  "_wait = " + (elements.isElementWait()) + "\n\t";
					}
					
					if (elements.isDisplayed()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elements.getFieldName()))  + screensCount +  datasetScreencount +  "_displayed = " + (elements.isDisplayed()) + "\n\t";
					}
					/**
					 * deciding the xpath for display in UI
					 */
					String xpathFinal = elements.getSelectedXpath();
					
					if (fieldsInfo.equals(""))
						fieldsInfo += elements.getFieldName() + "##" + elements.getFieldType() + "##"
								+ elements.getFieldValue() + "##" + xpathFinal + "##" + elements.getIsSelected() + "##" + elements.getTagFieldName() + "##" + elements.isClick() 
								+ "##" + elements.isIs_optionalElement()
								+ "##" + elements.isScroll_required()
								+ "##" + elements.isDisplayed()
								+ "##" +  elements.isElementWait()
								+ "##" + elements.isScrollUp()
								+ "##" + elements.isScrollDown();
					else
						fieldsInfo += "&&" + elements.getFieldName() + "##" + elements.getFieldType() + "##"
								+ elements.getFieldValue() + "##" + xpathFinal + "##" + elements.getIsSelected() + "##" + elements.getTagFieldName() + "##" + elements.isClick() 
								+ "##" + elements.isIs_optionalElement()
								+ "##" + elements.isScroll_required()
								+ "##" + elements.isDisplayed()
								+ "##" +  elements.isElementWait()
								+ "##" + elements.isScrollUp()
								+ "##" + elements.isScrollDown();

				}
				/**
				 * here checking the screens created or not
				 * more than one time we are not going to create based on boolean(isScreensCreated) condition 
				 */
				
				if (!isScreensCreated) {
					screenObjects =  screenObjects + "\t\t\t" +  "if(isElementDispalyed) { " +screenName + "Test(datasets);}\n";
					GenerateScreensScript(screenName, fieldsInfo);
					screenObjs += "public " + screenName + " obj" + screenName + ";\n\t";
					screenImports += "import " + mobilePageClasses + "." + screenName + ";\n";
				}
			}
		}
		// Config files
		writeFile(propertiesInfo, testCaseName.toLowerCase(), environmentVariable + File.separator + "ConfigFiles", ".properties"); // writing the config file
		
		/*------------------------------------------ TestCase -------------------------------------------------------*/
		
		File classisExists = new File(String.valueOf(testClassPath + File.separator + toTitleCase(testCaseName) + ".java"));
		if (classisExists.exists() && !testcaseOverwrite) {
			return;
		} 
		
		String imports = "package " + mobileTestClasses + ";"
				+ "\nimport org.testng.annotations.Test;\n"
				+ screenImports
				+ "import com.utilities.MobileBase;"
				+ "\nimport com.utilities.ConfigFilesUtility;\n";
		
		String startClass =
				 "\npublic class " + testCaseName + " extends MobileBase {"
				+ "\n\tprivate ConfigFilesUtility configFileObj;"
				+ "\n\tpublic boolean isElementDispalyed = false;"
				+ "\n\tString configFileData;";
				
		List<Screen>  screens = datasets.get(0).getScreens();
					
		String constructor =  "\n\n\tpublic " + testCaseName + "() throws Exception {"
				+ "\n\t\tconfigFileObj = new ConfigFilesUtility();"
				+ "\n\t\tconfigFileObj.loadPropertyFile(\"" + testCaseName.toLowerCase() + ".properties\");"
				//+ "\n\t\ttest = reports.startTest(\"" + testCaseName + " - \" + datasetNames[0]);"
				+ "\n\t\n}";
				
			
		
			
		testcaseClassStart += imports + startClass + constructor;
		int datasetScreencount = 0;
		for (Screen screen : screens) {
			datasetScreencount = datasetScreencount + 1;
			System.out.println("" + screen.getScreenId());
			System.out.println("screen name" + screen.getName());
			screenName = Utils.removeUnWantedChars(toTitleCase(screen.getName()));
			String screenMethodStart = ""
					+ "\n\tpublic void " + screenName + "Test(int i) throws Exception "
					+ "{\n\n\t"
					+ " try {"
					//+ "\n\t\taddScreensExecutionCount();"
					+ "\n\t\t"
					+ "int datasetScreencount = " + datasetScreencount + ";"
					+ "\n\t\t"
					+ screenName +" obj" + screenName + " = new " + screenName + "(appiumDriver);"
					+ "\n\t\ttestLogHeader( \"Verify " + screenName + " page\");\n";
			String screenMethodImpl = "";
			int elementCount = 0;
			String datasetCount = "\"+ i + datasetScreencount";
			
			for (Element elements : screen.getScreenElements()) {
				elementCount = elementCount + 1;
				fieldName = toTitleCase(elements.getFieldName());
				fieldValue = elements.getFieldValue();
				fieldType = elements.getFieldType();
				String tagName = Utils.removeUnWantedChars(elements.getTagFieldName());
				boolean isValidate = elements.getIsSelected();		
				boolean isClick = elements.isClick();
				boolean isOptionalElement = elements.isIs_optionalElement();
				
				// ====================== Skip Element ============================
				String appendOptionalElementOpen = "";
				String appendOptionalElementClose = "";
				
				if(isOptionalElement) {
					appendOptionalElementOpen = "\n\t\tif(skipifElementisNotDisplayed()) {	";
					appendOptionalElementClose = "\t\t}\n";
				}
				
				
				if(elements.isElementWait()) {
					
					screenMethodImpl +=   "\n\t\t elementWait();\n";
				}
				
					
				if (fieldType.equals("Button")) {
					if (isValidate) {
						String isActionClick = "";
						if(isClick) {
							isActionClick = "\n\t\t\ttestPass(\"Clicked on Button : \" + configFileData);";
						}
						
						screenMethodImpl +=   "\t\tString text" + elementCount + " = obj" + screenName + ".clk" + tagName + Utils.removeUnWantedChars(toTitleCase(fieldName)) 
								+ "();\t\t configFileData = configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  + ");"
								+ appendOptionalElementOpen + "\n\t\tif(text" + elementCount + ".equalsIgnoreCase(configFileData))"
								+ "{"  
								+ "\n\t\t\ttestPass(\"Validated Button Text : \" + configFileData);"
							    + isActionClick
								+ "\n\t\t"
								+ "} else {" 
								+ "\n\t\t\ttestFail(\"Button Text is displayed as : \" + configFileData, appiumDriver);"  
								+ "\n\t\t"
								+ "}\n" + appendOptionalElementClose;
					} else {
						String isActionClick = "";
						if(isClick) {
							isActionClick = appendOptionalElementOpen +  "\n\t\ttestInfo(\"Clicked on Link : \" + \"" + (fieldName) + "\");" + appendOptionalElementClose;
						}
						
						screenMethodImpl +=  "\t\tobj" + screenName + ".clk"+ tagName + Utils.removeUnWantedChars(toTitleCase(fieldName))+ "();"
											+ isActionClick
											+ "\n\t\t";
					}
				} else if (fieldType.equals("Link")) {
					if (isValidate) {
						String isActionClick = "";
						String isActionClickFail = "";
						if(isClick) {
							isActionClick = "\n\t\t\ttestPass(\"Clicked on Link: \" + configFileData);";
							isActionClickFail = "\n\t\t\ttestFail(\"Clicked on Link : \" + configFileData, appiumDriver);";
						}
						
						screenMethodImpl +=  "\t\tString text" + elementCount + " = obj" + screenName + ".clk"
								+ tagName + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "();\t\t" 
								+ "\n\t\tconfigFileData = configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  + ");\n\t\t"
								+ appendOptionalElementOpen + "if(text" + elementCount + ".equalsIgnoreCase(configFileData))"
								+ "{"
								+ "\n\t\t\ttestPass(\"Validated Link Text : \" + configFileData);"
								+ isActionClick
								+ "\n\t\t} else {" 
								+ "\n\t\t\ttestFail(\"Link Text is displayed as : \" + configFileData, appiumDriver);"  
								+ "\n\t\t"
								+ "}\n" + appendOptionalElementOpen;

					} else {
						String isActionClick = "";
						if(isClick) {
							isActionClick = "\n\t\t\ttestPass(\"Clicked on Link: \" + configFileData);";
						}
						screenMethodImpl += "\t\tobj" + screenName + ".clk" + tagName + Utils.removeUnWantedChars(toTitleCase(fieldName))+ "();"
								+ appendOptionalElementOpen 
								+ isActionClick 
								+ "\n\t\ttestPass(\"Link Text is displayed as : " + toTitleCase(StringEscapeUtils.escapeJava(elements.getFieldName())) + "Link\");"
								+ "\n" + appendOptionalElementClose;
					}
				}  else if (fieldType.equalsIgnoreCase("imageView")) {
					if (false) {
						// Image validation
					} else {
						screenMethodImpl +=  "\t\tobj" + screenName + ".clk" + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "();"
								+ appendOptionalElementOpen + "\n\t\ttestInfo(\"Clicked on \" + \"" +  (StringEscapeUtils.escapeJava(elements.getFieldName())) + " Image\");"
								+ "\n" + appendOptionalElementClose;
					}
				} else if (fieldType.equals("InputText")) {	
					screenMethodImpl +=  "\t\tconfigFileData = configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  + ");"
							+ "\n\t\t" 
							+ "obj" + screenName + ".fill" + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "(configFileData);"
							+ appendOptionalElementOpen +  "\n\t\ttestPass(\"Entered " + (StringEscapeUtils.escapeJava(elements.getFieldName())) + " : \" + configFileData);" + appendOptionalElementClose
							+ "\n\t\thideKeyboard();"
							+ "\n";
				} else if (fieldType.equals("Label")) {
					if (isValidate) {
						String isActionClick = "";
						if(isClick) {
							isActionClick = "\n\t\ttestPass(\"Clicked on Button : "+ (StringEscapeUtils.escapeJava(elements.getFieldName())) +"\");\n";
						}else {
							isActionClick =   "\n\t\ttestInfo(\"Label text value is : "+(StringEscapeUtils.escapeJava(elements.getFieldName())) +"\");\n"; 
						}
						
						screenMethodImpl +=  "\t\tconfigFileData = configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  + ");\n\t\t"
								+ "String text" + elementCount + " = obj" + screenName  + "." + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "();"
								+ appendOptionalElementOpen
								+ "\n\t\t" 
								+ "if(text" + elementCount + ".equalsIgnoreCase(configFileData)){"
								+ "\n\t\ttestPass(\"Validated Text is displayed as : \" + configFileData);" 
								+ isActionClick
								+ "\n\t\t} else {" 
								+ "\n\t\ttestFail(\"Label Text is displayed as : \" + configFileData,   appiumDriver);"   
								+ "\n\t\t}\n" + appendOptionalElementClose;

					} else {
						String isActionClick = "";
						if(isClick) {
							isActionClick = "\n\t\ttestPass(\"Clicked on Button : "+ (StringEscapeUtils.escapeJava(elements.getFieldName())) +"\");\n";
						}  else {
							isActionClick =   "\n\t\ttestInfo(\"Label text value is : "+(StringEscapeUtils.escapeJava(elements.getFieldName())) +"\");\n"; 
						}
						screenMethodImpl +=  "\t\tobj" + screenName + "." + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "();"
								+ appendOptionalElementOpen 
								+  isActionClick 
								+ appendOptionalElementClose;
					}
				}
			}
			
			String catchBlock = "   } catch (Exception e) {\r\n" + 
					"		  isElementDispalyed = false;\r\n" +
					"		  testFail(\"Element is not found : \", appiumDriver);\n" + 
					"	   }";
			
			String closeMethod = "\n\t" + catchBlock + "\n\t}\n\t\n";
			testcaseClassStart +=  screenMethodStart  + screenMethodImpl + closeMethod;	
		}
		
		String datasetsLoop = "\t @Test(dataProvider = \"capabilities\")\r\n" + 
				"	 public void setUP(String deviceName,\r\n" + 
				"	                   String platformName,\r\n" + 
				"	                   String platformVersion,\r\n" + 
				"	                   String appiumVersion,\r\n" + 
				"	                   String deviceOrientation,\r\n" + 
				"	               	   String projectName,\r\n" + 
				"	                   String testObjId,\r\n" + 
				"	                   String app) throws Exception {"
				+ "\r\n\t\tisElementDispalyed = true;\r\n" + 
				"		for(int datasets = 1; datasets <= configFileObj.getIntProperty(\"datasetsLength\"); datasets++) {"
				+ 		"\r\n\t\t\tif(!isElementDispalyed) return;\n"+
				"			setTestcaseName(\"" + testCaseName + " - \" + configFileObj.getProperty(\"dataset\" + (datasets)),configFileObj);\n" +
				"\t\t\tappiumDriver = this.createDriver(deviceName,platformName, platformVersion, appiumVersion, deviceOrientation, projectName, testObjId, this.getClass().getSimpleName(), app);\r\n" + 

				 screenObjects + "\n" + 
				"		}\r" + 
				"	}\n}";
		
		testcaseClassStart += datasetsLoop;
	
		writeFile(testcaseClassStart, toTitleCase(testCaseName), testClassPath, ".java");

	}
	
	/*---------------------------------------- Screens -----------------------------------------------------*/
	
	/**
	 * Generating the screens
	 * @param screenName
	 * @param fieldsInfo
	 * @throws IOException
	 */

	private void GenerateScreensScript(String screenName, String fieldsInfo) throws IOException {

		String labelCheck = "false";
		String testcaseClassStart;
		String xpath;
		
		String fieldName, funcName;
		String  methodInsideBlock = "";
		String fieldType = "Button", fieldNamePrefix = "";
		String  catchBlock = "";
		String method = "";
		String fieldValue = "", fieldName1 = "", text = "",
				returnString = "";
		String[] fieldValues, fieldString;
		
		String clickAction = "";
		String Beforestr6 = "";
		String endClass = "\n}";
		
		String packages = "package " + mobilePageClasses + ";"
		+"\nimport org.openqa.selenium.WebDriver;"
		+ "\nimport org.openqa.selenium.WebElement;"
		+ "\nimport org.openqa.selenium.support.FindBy;"
		+ "\nimport org.openqa.selenium.support.How;"
		+ "\nimport com.utilities.MobileBase;"
		+"\nimport io.appium.java_client.AppiumDriver;\r\n" + 
		"import io.appium.java_client.MobileElement;\r\n" + 
		"import io.appium.java_client.pagefactory.AndroidFindBy;\r\n" + 
		"import io.appium.java_client.pagefactory.AppiumFieldDecorator;\r\n"
		+ "import org.openqa.selenium.support.PageFactory;\r\n";
		
		
		//String primryInfo = "\n\tpublic static String primaryInfo  = \""
			//	+ StringEscapeUtils.escapeJava(new Gson().toJson(primaryInfo)) + "\";\n";
		String prjctName = "public static String projectName = \"" + projectName + "\";";
		
		String startClass = "\n@SuppressWarnings(\"unused\")"
				+ "\npublic class " + screenName + " extends MobileBase {"
				+ "\n\t" + prjctName
				+ "\n\n\tpublic " + screenName +  "(AppiumDriver<MobileElement> appiumDriver) "
				+ "{\n\t\t"
				+ "this.appiumDriver = appiumDriver;"	
				+"\n\t\tPageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);"
				+ "\n\t"
				+ "}\n";
	
		String xpathStart = "\n\t@FindBy(how = How.XPATH, using = \"";
		String webEleemnt = "\")\t\n\tprivate WebElement\t";
		
		testcaseClassStart = packages + startClass;
		if (!fieldsInfo.isEmpty()) {
			fieldString = fieldsInfo.split("&&");
			System.out.println("fieldString.length: " + fieldString.length);
			for (int i = 0; i < fieldString.length; i++) {
				Beforestr6 = "()";
				methodInsideBlock = " {\n\t\t";
				returnString = "\n\tpublic String ";
				clickAction = ".click();";
				text = ".getText();";
				fieldValues = fieldString[i].split("##");
				fieldName = StringEscapeUtils.escapeJava(fieldValues[0]).replaceAll("^\\d+", "");
				fieldType = fieldValues[1];
				fieldValue = fieldValues[2];
				xpath = fieldValues[3];
				labelCheck =fieldValues[4];
				String tagName = Utils.removeUnWantedChars(fieldValues[5]);
				boolean isClick = Boolean.parseBoolean(fieldValues[6]);
				
				
				boolean scrollRequired = Boolean.parseBoolean(fieldValues[8]);
				
				if(Boolean.parseBoolean(fieldValues[12]) || Boolean.parseBoolean(fieldValues[11])) {
					scrollRequired = true;
				}
				
				String element = "MobileElement ";
				String finalElement = "";
				// this for ios condition
				boolean skipElement = Boolean.parseBoolean(fieldValues[7]);
				String optionalElementOpen = StringUtils.EMPTY;
				String optionalElementClose = StringUtils.EMPTY;
				if((fieldType.equalsIgnoreCase("imageView") || fieldType.equalsIgnoreCase("InputText"))) { 	
				 optionalElementClose = "";	
				} else if(skipElement && labelCheck.equals("false"))  {
					optionalElementClose = "\n\t\treturn empty;";	
				} else if(skipElement && labelCheck.equals("true"))  {
					optionalElementClose = "\n\t\treturn empty;";	
				}  else if(!skipElement && labelCheck.equals("false"))  {
					optionalElementClose = "\n\t\treturn empty;";	
				}else if(!skipElement && labelCheck.equals("true"))  {
					optionalElementClose = "\n\t\t";	
				}
				
				if(skipElement) {
					optionalElementOpen = "\n\t\tif(skipifElementisNotDisplayed()) {";
					optionalElementClose = "\n\t\t}"  + optionalElementClose;
				} 
				
				
				finalElement = " = findElement(\"" + xpath + "\", " + skipElement + ", " + scrollRequired + ");" + optionalElementOpen;
				
				
				catchBlock = optionalElementClose + "\n\t}\n";
									
				if (fieldType.equals("Button") || fieldType.equals("Link")) {
					if (labelCheck.equals("true")) {
						fieldNamePrefix = "clk"+ tagName;

						System.out.println("********************FieldName1*********************: " + fieldValue);
					} else {
					returnString = "\n\tpublic String ";
					fieldNamePrefix = "clk"+ tagName;
					}
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName));

					methodInsideBlock = Beforestr6 + methodInsideBlock;

					System.out.println(
							"********************FieldName1 in Link and  Button*********************: " + fieldName1);
				} if (fieldType.equalsIgnoreCase("imageView")) {
					
					fieldNamePrefix = "clk"+ tagName;
					
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName));

					methodInsideBlock = Beforestr6 + methodInsideBlock;
					returnString = "\n\tpublic void ";
					System.out.println("********************FieldName1 in Link and  Button*********************: " + fieldName1);
				} else if (fieldType.equals("InputText")) {
					fieldNamePrefix = "fill"+ tagName;
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName));
					methodInsideBlock = "(String varInputValue)" + methodInsideBlock;
					catchBlock =  tagName +".sendKeys(varInputValue);" + catchBlock;
					returnString = "\n\tpublic void ";
					System.out.println("********************FieldName1 in InputText*********************: " + fieldName1);
				} else if (fieldType.equals("Label")) {
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName));
					if (labelCheck.equals("true")) {
						fieldNamePrefix = tagName;

						System.out.println("********************FieldName1*********************: " + fieldValue);
					} else {
						fieldNamePrefix = tagName;
						returnString = "\n\tpublic String ";
						System.out.println("********************FieldName1*********************: " + fieldValue);
					}
					// catchBlock=Beforestr9+"\n\t\treturn "+toTitleCase(fieldName1)+text+catchBlock;
					methodInsideBlock = Beforestr6 + methodInsideBlock;
					System.out.println("********************FieldName1 in Label*********************: " + fieldName1);
				} 
				
				
				if (fieldType.equalsIgnoreCase("imageView")) {
					String isActionClick = "";
					if(isClick) {
						isActionClick = 	"\n\t\t" + fieldName1 + tagName + clickAction ;
					}
					
					returnString = "\n\tpublic void ";
					funcName = fieldNamePrefix + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + methodInsideBlock + element +
							(!element.equals("") ?(Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + finalElement +";") : "" ) + "\t\t" 
							+ isActionClick
							+ catchBlock;
					

				} else if (!fieldType.equals("InputText")) {
					String isActionClick = "";
					if(isClick) {
						isActionClick = "\n\t\t" + Utils.removeUnWantedChars(toTitleCase(fieldName1))+ tagName  + clickAction; 
					}
					funcName = fieldNamePrefix + Utils.removeUnWantedChars(toTitleCase(fieldName1)) 
							+ methodInsideBlock + element + (!element.equals("") ?(Utils.removeUnWantedChars(toTitleCase(fieldName1))+ tagName + finalElement ) : "" ) + "\t\t" 
							+ "\n\t\tString text = " + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + text 
							+ isActionClick;
							if(labelCheck.equals("true")) {
								funcName = funcName + "\n\t\treturn text;";
							}
							funcName = funcName + catchBlock;

				} else {
					
					funcName = fieldNamePrefix + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + methodInsideBlock 
							+ element + (!element.equals("") ? (Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + finalElement) : "") + "\n\t\t" 
							+  Utils.removeUnWantedChars(toTitleCase(fieldName1))+tagName + ".clear();\n\t\t" + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + catchBlock;
				}
				
				if(element.isEmpty()) {
					method += xpathStart + xpath + webEleemnt + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName +";"+ returnString + funcName;
				} else {
					method += returnString + funcName;
				}

			}
		} else {
			System.out.println("Invalid input string!");
		}
		testcaseClassStart += method + endClass;
		writeFile(testcaseClassStart, toTitleCase(screenName), pageClassPath, ".java");
	}
	
	/**
	 * it returns first letter is capital given string
	 * @param givenString
	 * @return
	 */

	private String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].trim().equals(""))
				continue;
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append("");
		}
		return sb.toString().trim();
	}
	
	/**
	 * Writing the testcases
	 * @param testcaseClassStart
	 * @param className
	 * @param filePath
	 * @param fileExtension
	 * @throws IOException
	 */

	private void writeFile(String testcaseClassStart, String className, String filePath, String fileExtension)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(testcaseClassStart);
		} finally {
			out.close();
		}
	}
	/**
	 * Creating the directory
	 * @param directory
	 * @return
	 */
	private boolean createDirectory(String directory) {
		File fileDirectory = new File(directory);
		if (!fileDirectory.exists()) {
			fileDirectory.mkdir();
			return true;
		}
		return false;
	}
}
