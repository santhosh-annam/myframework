package com.servercodegeneration;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.dto.WebScript;
import com.google.gson.Gson;
import com.utils.DockerConf;
import com.utils.GitopsAutomationConfiguaration;
import com.utils.Utils;

/**
 * This class used for progress information 
 */
public class ScriptGenerate {
	private static String environmentVariable = System.getenv("AUTOMATION_PATH");
	private static String testClasses = "";
	private static String testsetName = "";
	private static String newTestsetName = "";
	private static boolean executeAction;
	private static String finalData;
	private int testSetLength = 0;
	private boolean isWeb;
	private String mobilePlatform;
	public String fileName;
	public String version;
	public String bundleId;
	private String frameworkPath;
	private String gitURL;
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static String addBash = "";
	private String tcGoal = "clean test";

	public boolean execJenkins;
	private String jenkinsUsername;
	private String jenkinsPassword;
	private String jenkinsURL;
	private String jenkinsToken;
	private String projectName;
	private String commitMsg;
	private int testcaseId;
	private int testsetId;
	private boolean isTestSet = false;
	private String executionEnvironment;
	private boolean generateAction = false;
	private boolean iscentOS = false;
	private String dockerFilesDirectory;
	private String dockerFilesDirectoryURL;
	private String dockerFileName;
	/**
	 * Here will get the json data from Main.class 
	 * For MAC/Linux getting the home directory and executing the environmentVariables.sh for getting the AUTOMATION_PATH from environment variables 
	 */

	public ScriptGenerate(String jsonData) {
		execJenkins = false;
		// checking linux or windows
		System.out.println(OS);
		iscentOS = osType();
		if (isWindows()) {
			environmentVariable = System.getenv("AUTOMATION_PATH");
			System.out.println("This is Windows");
		} else if (isMac()) {
			String MAC_HOME_DIRECTORY = System.getProperty("user.home");
			environmentVariable = "bash " + MAC_HOME_DIRECTORY;
			createEnvironmentFile(MAC_HOME_DIRECTORY);
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables.size() > 0) {
				environmentVariable = envVariables.get(0);
				System.out.println("This is for Mac");
				addBash = "bash ";
			}
		} else if (isUnix() || isSolaris()) {

			environmentVariable = "bash " + System.getProperty("user.home");
			//createEnvironmentFile(environmentVariable);
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables.size() > 0) {
				environmentVariable = envVariables.get(0);
				System.out.println(iscentOS ? "This is for CentOs" : "This is for linux");
				addBash = "bash ";
			}
		} else {
			System.out.println("Your OS is not support!!");
		}

		finalData = jsonData;

		call();

	}


	//Finding the OS type in Linux Platform (Centos or Ubuntu)
	private boolean osType() {
		boolean osType = false;
		try {
			String[] cmd = { "/bin/sh", "-c", "egrep 'NAME' cat /etc/*-release" };
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String finalOutput = "";
			String line = "";
			while ((line = bri.readLine()) != null) {
				finalOutput += (line + "\n");
			}
			bri.close();
			if (finalOutput.toLowerCase().contains("centos")) {
				osType = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			osType = false;
		}
		return osType;
	}
	String userId;
	/**
	 * used to create environmentVariables.sh file if not exists in Home Directory
	 * @param path
	 */
	private void createEnvironmentFile(String path) {
		try {
			File fileExists = new File(path + File.separator + "environmentVariables.sh");
			boolean exists = fileExists.exists();
			if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
						"printenv AUTOMATION_PATH\n" + 
						"printenv NODE_PATH\n" + 
						"printenv APPIUM_JS_PATH\n" + 
						"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * files : setting the permissions to read, write and execute
	 * @param path
	 * @throws Exception
	 */
	private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}

	/**
	 * Checking the testcase or testset 
	 * if it is testset here looping the testcases 
	 * Creating the testset bat file
	 * Creating the profiles in pom.xml for running the testset.
	 * running the batch file execution
	 * dividing 10 levels for showing the execution progress 
	 */
	private String browsers;
	private String testCaseName;
	private String newTestCaseName;
	private String mobileType;
	private String executionPlatform;

	public String call() {
		String  msg = "Something went wrong";

		try {

			
			//finalData = "[{\"primary_info\":{\"browser_type\":\"chrome\",\"testcase_overwrite\":false,\"report_upload_url\":\"http://localhost:8050/UploadReportFile\",\"user_id\":7,\"executed_user_id\":7,\"docker_files_directory\":\"C:\\\\Users\\\\RBattagiri\\\\AppData\\\\Local\\\\Temp\\\\tomcat.9127949342393669482.8050\\\\webapps\\\\pdownload\\\\DockerFiles\",\"docker_file_name\":\"DockerFile_7_SE_TC_752\",\"docker_files_directory_url\":\"http://localhost:8050/pdownload/DockerFiles\",\"is_generate\":false,\"is_execute\":false,\"is_web\":true,\"mobile_platform\":null,\"file_name\":null,\"bundle_id\":null,\"device_os_version\":null,\"repository_url\":\"3BuwMHuCPjwLB24NvsQR6Zpl54HMbLKTRfH90pKfDRmnCRdhFkN5TgE4O1JSxjjiR/p20saesUVaiKrXKGdkqM7vR8BVJgOmQxVWzltXoBbKT/B+K7Q+2TyXqgDOn27B\",\"framework_path\":\"\\\\home\\\\prolifics\\\\QualityFusion\\\\QF-Users\\\\7\\\\Selenium\\\\TestopsDemo\",\"jenkins_url\":\"http://10.11.12.244:8085/\",\"jenkins_username\":\"admin\",\"jenkins_password\":\"admin\",\"jenkins_token\":\"11bc723a0c697d1a158eeb7bd717a80e39\",\"repository_commit_message\":\"\",\"execution_environment\":\"gitops\",\"aes_secret_key\":\"_PROLIFICS_PROLIFICS\",\"client_timezone_id\":\"Asia/Calcutta\",\"input_command\":null,\"project_folder_path\":null,\"project_written_language\":null,\"desktop_automation\":false,\"module_type_st\":\"WEB_AUTOMATION\",\"module_type\":2,\"mobile_type\":null,\"project_url\":\"http://ec2-52-15-194-156.us-east-2.compute.amazonaws.com:8081/\",\"project_name\":\"TestopsDemo\",\"project_description\":\"Description\",\"project_id\":674,\"module_name\":\"WebDemo\",\"module_description\":\"\",\"sub_module_id\":0,\"sub_module_name\":null,\"sub_module_description\":null,\"module_id\":1035,\"testcase_name\":\"TC_registationin\",\"testcase_id\":752,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1635492492439},\"sauce_device_info\":null,\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"dataset_rs\",\"is_db_dataset\":false,\"dataset_id\":934,\"apis\":null,\"regression_screens\":null,\"screens\":[{\"screen_id\":1473,\"name\":\"RegistrationScreen\",\"description\":\"desc\",\"created_at\":null,\"created_by\":7,\"module_id\":1035,\"web_page_id\":1801,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"selected\":\"\",\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":1473,\"web_page_element_id\":833005,\"element_order\":1,\"element_id\":833005,\"web_page_id\":1801,\"field_name\":\"Name *\",\"field_value\":\"ranga\",\"field_type\":\"InputText\",\"secondary_field_type\":\"\",\"selected_xpath\":\"//input[@id='txt_name']\",\"prefered_field\":\"xpath_id\",\"relative_xpath_by_id\":null,\"relative_xpath_by_field_name\":null,\"xpath\":null,\"is_optionalElement\":false,\"scroll_required\":false,\"is_selected\":false,\"tag_field_name\":\"input\",\"child_id\":\"Name *\",\"child_text\":\"Name *\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"input\",\"dropdown_value\":\"\",\"is_click\":false,\"db_table_name\":null,\"db_column_name\":null,\"validate_text\":null,\"custom_code\":false,\"displayed\":false,\"element_wait\":false,\"scrollup\":false,\"scrolldown\":false,\"is_random\":false,\"is_enter\":false,\"db_results\":null,\"dataset_values_temp\":null},{\"screen_id\":1473,\"web_page_element_id\":833008,\"element_order\":2,\"element_id\":833008,\"web_page_id\":1801,\"field_name\":\"Email *\",\"field_value\":\"battagiriranga@gmail.com\",\"field_type\":\"InputText\",\"secondary_field_type\":\"\",\"selected_xpath\":\"//input[@id='txt_email']\",\"prefered_field\":\"xpath_id\",\"relative_xpath_by_id\":null,\"relative_xpath_by_field_name\":null,\"xpath\":null,\"is_optionalElement\":false,\"scroll_required\":false,\"is_selected\":false,\"tag_field_name\":\"input\",\"child_id\":\"Email *\",\"child_text\":\"Email *\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"input\",\"dropdown_value\":\"\",\"is_click\":false,\"db_table_name\":null,\"db_column_name\":null,\"validate_text\":null,\"custom_code\":false,\"displayed\":false,\"element_wait\":false,\"scrollup\":false,\"scrolldown\":false,\"is_random\":false,\"is_enter\":false,\"db_results\":null,\"dataset_values_temp\":null},{\"screen_id\":1473,\"web_page_element_id\":833004,\"element_order\":3,\"element_id\":833004,\"web_page_id\":1801,\"field_name\":\"Phone *\",\"field_value\":\"8885425848\",\"field_type\":\"InputText\",\"secondary_field_type\":\"\",\"selected_xpath\":\"//input[@id='txt_phone']\",\"prefered_field\":\"xpath_id\",\"relative_xpath_by_id\":null,\"relative_xpath_by_field_name\":null,\"xpath\":null,\"is_optionalElement\":false,\"scroll_required\":false,\"is_selected\":false,\"tag_field_name\":\"input\",\"child_id\":\"Phone *\",\"child_text\":\"Phone *\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"input\",\"dropdown_value\":\"\",\"is_click\":false,\"db_table_name\":null,\"db_column_name\":null,\"validate_text\":null,\"custom_code\":false,\"displayed\":false,\"element_wait\":false,\"scrollup\":false,\"scrolldown\":false,\"is_random\":false,\"is_enter\":false,\"db_results\":null,\"dataset_values_temp\":null},{\"screen_id\":1473,\"web_page_element_id\":833000,\"element_order\":4,\"element_id\":833000,\"web_page_id\":1801,\"field_name\":\"Subject *\",\"field_value\":\"pending details\",\"field_type\":\"InputText\",\"secondary_field_type\":\"\",\"selected_xpath\":\"//input[@id='txt_subject']\",\"prefered_field\":\"xpath_id\",\"relative_xpath_by_id\":null,\"relative_xpath_by_field_name\":null,\"xpath\":null,\"is_optionalElement\":false,\"scroll_required\":false,\"is_selected\":false,\"tag_field_name\":\"input\",\"child_id\":\"Subject *\",\"child_text\":\"Subject *\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"input\",\"dropdown_value\":\"\",\"is_click\":false,\"db_table_name\":null,\"db_column_name\":null,\"validate_text\":null,\"custom_code\":false,\"displayed\":false,\"element_wait\":false,\"scrollup\":false,\"scrolldown\":false,\"is_random\":false,\"is_enter\":false,\"db_results\":null,\"dataset_values_temp\":null},{\"screen_id\":1473,\"web_page_element_id\":833006,\"element_order\":5,\"element_id\":833006,\"web_page_id\":1801,\"field_name\":\"Message *\",\"field_value\":\"request for lang change\",\"field_type\":\"InputText\",\"secondary_field_type\":\"\",\"selected_xpath\":\"//textarea[@id='txt_message']\",\"prefered_field\":\"xpath_id\",\"relative_xpath_by_id\":null,\"relative_xpath_by_field_name\":null,\"xpath\":null,\"is_optionalElement\":false,\"scroll_required\":false,\"is_selected\":false,\"tag_field_name\":\"textarea\",\"child_id\":\"Message *\",\"child_text\":\"Message *\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"textarea\",\"dropdown_value\":\"\",\"is_click\":false,\"db_table_name\":null,\"db_column_name\":null,\"validate_text\":null,\"custom_code\":false,\"displayed\":false,\"element_wait\":false,\"scrollup\":false,\"scrolldown\":false,\"is_random\":false,\"is_enter\":false,\"db_results\":null,\"dataset_values_temp\":null},{\"screen_id\":1473,\"web_page_element_id\":833003,\"element_order\":6,\"element_id\":833003,\"web_page_id\":1801,\"field_name\":\"SEND\",\"field_value\":\"input\",\"field_type\":\"Button\",\"secondary_field_type\":\"\",\"selected_xpath\":\"//input[@id='btn_send']\",\"prefered_field\":\"xpath_id\",\"relative_xpath_by_id\":null,\"relative_xpath_by_field_name\":null,\"xpath\":null,\"is_optionalElement\":false,\"scroll_required\":false,\"is_selected\":false,\"tag_field_name\":\"input\",\"child_id\":\"SEND\",\"child_text\":\"SEND\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"input\",\"dropdown_value\":\"\",\"is_click\":true,\"db_table_name\":null,\"db_column_name\":null,\"validate_text\":null,\"custom_code\":false,\"displayed\":false,\"element_wait\":false,\"scrollup\":false,\"scrolldown\":false,\"is_random\":false,\"is_enter\":false,\"db_results\":null,\"dataset_values_temp\":null}]}]}]}]";

			//WEB
			//finalData = "[{&quot;primary_info&quot;:{&quot;browser_type&quot;:&quot;chrome&quot;,&quot;report_upload_url&quot;:&quot;https://192.168.1.142:8080/TAF_Automation_DR/UploadReportFile&quot;,&quot;user_id&quot;:7,&quot;is_generate&quot;:false,&quot;is_execute&quot;:true,&quot;is_web&quot;:true,&quot;mobile_platform&quot;:null,&quot;file_name&quot;:null,&quot;bundle_id&quot;:null,&quot;device_os_version&quot;:null,&quot;project_url&quot;:&quot;http://newtours.demoaut.com/&quot;,&quot;project_name&quot;:&quot;NewTours&quot;,&quot;project_description&quot;:&quot;&quot;,&quot;project_id&quot;:172,&quot;module_name&quot;:&quot;NewToursTest&quot;,&quot;module_description&quot;:&quot;&quot;,&quot;sub_module_id&quot;:0,&quot;sub_module_name&quot;:null,&quot;sub_module_description&quot;:null,&quot;module_id&quot;:323,&quot;testcase_name&quot;:&quot;TC_FlowTest&quot;,&quot;testcase_id&quot;:361,&quot;testset_name&quot;:null,&quot;testset_id&quot;:0,&quot;executed_timestamp&quot;:1547030643429},&quot;regression_datasets_of_testcase&quot;:null,&quot;datasets_of_testcase&quot;:[{&quot;dataset_name&quot;:&quot;DS1&quot;,&quot;dataset_id&quot;:458,&quot;apis&quot;:null,&quot;regression_screens&quot;:null,&quot;screens&quot;:[{&quot;screen_id&quot;:827,&quot;name&quot;:&quot;LoginScreen&quot;,&quot;description&quot;:&quot;s&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:323,&quot;web_page_id&quot;:919,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:827,&quot;web_page_element_id&quot;:285893,&quot;element_order&quot;:1,&quot;element_id&quot;:285893,&quot;web_page_id&quot;:919,&quot;field_name&quot;:&quot;userName&quot;,&quot;field_value&quot;:&quot;nrasamalla&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='userName']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[2]/TD[3]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;userName&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:827,&quot;web_page_element_id&quot;:285896,&quot;element_order&quot;:2,&quot;element_id&quot;:285896,&quot;web_page_id&quot;:919,&quot;field_name&quot;:&quot;password&quot;,&quot;field_value&quot;:&quot;Test123&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='password']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[2]/TD[3]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[3]/TD[2]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;password&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:827,&quot;web_page_element_id&quot;:285898,&quot;element_order&quot;:3,&quot;element_id&quot;:285898,&quot;web_page_id&quot;:919,&quot;field_name&quot;:&quot;login&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='login']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[2]/TD[3]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/DIV[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;login&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:828,&quot;name&quot;:&quot;ReservationPageScreen&quot;,&quot;description&quot;:&quot;s&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:323,&quot;web_page_id&quot;:920,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:828,&quot;web_page_element_id&quot;:286045,&quot;element_order&quot;:1,&quot;element_id&quot;:286045,&quot;web_page_id&quot;:920,&quot;field_name&quot;:&quot;Acapulco&quot;,&quot;field_value&quot;:&quot;select&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//SELECT[@name='fromPort']/OPTION[1]&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;select&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;[{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[1]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Acapulco\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[1]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[2]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Frankfurt\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[2]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[3]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;London\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[3]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;New York\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[4]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[5]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Paris\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[5]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[6]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Portland\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[6]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[7]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;San Francisco\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[7]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[8]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Seattle\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[8]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[9]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Sydney\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[9]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[10]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Zurich\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[10]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;}]&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;select&quot;,&quot;dropdown_value&quot;:&quot;{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[2]/SELECT[1]/OPTION[3]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;London\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='fromPort']/OPTION[3]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;}&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:828,&quot;web_page_element_id&quot;:286046,&quot;element_order&quot;:2,&quot;element_id&quot;:286046,&quot;web_page_id&quot;:920,&quot;field_name&quot;:&quot;Acapulco&quot;,&quot;field_value&quot;:&quot;select&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//SELECT[@name='toPort']/OPTION[1]&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;select&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;[{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[1]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Acapulco\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[1]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[2]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Frankfurt\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[2]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[3]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;London\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[3]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;New York\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[4]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[5]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Paris\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[5]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[6]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Portland\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[6]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[7]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;San Francisco\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[7]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[8]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Seattle\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[8]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[9]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Sydney\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[9]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[10]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Zurich\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[10]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;}]&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;select&quot;,&quot;dropdown_value&quot;:&quot;{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[2]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;New York\\\\n\\&quot;,\\&quot;input_type\\&quot;:\\&quot;Link\\&quot;,\\&quot;relative_xpath_by_name\\&quot;:\\&quot;//SELECT[@name='toPort']/OPTION[4]\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;}&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:828,&quot;web_page_element_id&quot;:286013,&quot;element_order&quot;:3,&quot;element_id&quot;:286013,&quot;web_page_id&quot;:920,&quot;field_name&quot;:&quot;findFlights&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='findFlights']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[14]/TD[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;findFlights&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:829,&quot;name&quot;:&quot;ReservationScreen2&quot;,&quot;description&quot;:&quot;d&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:323,&quot;web_page_id&quot;:921,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:829,&quot;web_page_element_id&quot;:286091,&quot;element_order&quot;:1,&quot;element_id&quot;:286091,&quot;web_page_id&quot;:921,&quot;field_name&quot;:&quot;reserveFlights&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='reserveFlights']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/P[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;reserveFlights&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:830,&quot;name&quot;:&quot;PurchaseScreen1&quot;,&quot;description&quot;:&quot;s&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:323,&quot;web_page_id&quot;:922,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:830,&quot;web_page_element_id&quot;:286248,&quot;element_order&quot;:1,&quot;element_id&quot;:286248,&quot;web_page_id&quot;:922,&quot;field_name&quot;:&quot;passFirst0&quot;,&quot;field_value&quot;:&quot;test&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='passFirst0']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;passFirst0&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:830,&quot;web_page_element_id&quot;:286247,&quot;element_order&quot;:2,&quot;element_id&quot;:286247,&quot;web_page_id&quot;:922,&quot;field_name&quot;:&quot;passLast0&quot;,&quot;field_value&quot;:&quot;user&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='passLast0']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;passLast0&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:830,&quot;web_page_element_id&quot;:286275,&quot;element_order&quot;:3,&quot;element_id&quot;:286275,&quot;web_page_id&quot;:922,&quot;field_name&quot;:&quot;creditnumber&quot;,&quot;field_value&quot;:&quot;123456&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='creditnumber']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[6]/TD[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;creditnumber&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:830,&quot;web_page_element_id&quot;:286279,&quot;element_order&quot;:4,&quot;element_id&quot;:286279,&quot;web_page_id&quot;:922,&quot;field_name&quot;:&quot;buyFlights&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='buyFlights']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[23]/TD[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;buyFlights&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:831,&quot;name&quot;:&quot;Purchase2Screen&quot;,&quot;description&quot;:&quot;d&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:323,&quot;web_page_id&quot;:923,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:831,&quot;web_page_element_id&quot;:286448,&quot;element_order&quot;:1,&quot;element_id&quot;:286448,&quot;web_page_id&quot;:923,&quot;field_name&quot;:&quot;$44 USD&quot;,&quot;field_value&quot;:&quot;FONT&quot;,&quot;field_type&quot;:&quot;Label&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:null,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/TABLE[1]/TBODY[1]/TR[12]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/FONT[1]/FONT[1]/FONT[1]/B[1]/FONT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;FONT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;FONT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:831,&quot;web_page_element_id&quot;:286414,&quot;element_order&quot;:2,&quot;element_id&quot;:286414,&quot;web_page_id&quot;:923,&quot;field_name&quot;:&quot;image&quot;,&quot;field_value&quot;:&quot;IMG&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:&quot;&quot;,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:null,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[7]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[3]/A[1]/IMG[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;IMG&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;IMG&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]}]}]}]";
			//data = new String[] {"[{&quot;primary_info&quot;:{&quot;browser_type&quot;:&quot;chrome&quot;,&quot;report_upload_url&quot;:&quot;https://192.168.1.142:8080/TAF_Automation_DR/UploadReportFile&quot;,&quot;user_id&quot;:7,&quot;is_generate&quot;:false,&quot;is_execute&quot;:true,&quot;is_web&quot;:true,&quot;mobile_platform&quot;:null,&quot;file_name&quot;:null,&quot;bundle_id&quot;:null,&quot;device_os_version&quot;:null,&quot;project_url&quot;:&quot;http://selenium4testing.com/hms/index.php&quot;,&quot;project_name&quot;:&quot;WindowHandleTest&quot;,&quot;project_description&quot;:&quot;&quot;,&quot;project_id&quot;:166,&quot;module_name&quot;:&quot;WindowHandleTest&quot;,&quot;module_description&quot;:&quot;&quot;,&quot;sub_module_id&quot;:0,&quot;sub_module_name&quot;:null,&quot;sub_module_description&quot;:null,&quot;module_id&quot;:303,&quot;testcase_name&quot;:&quot;TC_EndToEndScenario&quot;,&quot;testcase_id&quot;:336,&quot;testset_name&quot;:null,&quot;testset_id&quot;:0,&quot;executed_timestamp&quot;:1546581472703},&quot;regression_datasets_of_testcase&quot;:null,&quot;datasets_of_testcase&quot;:[{&quot;dataset_name&quot;:&quot;Ds1&quot;,&quot;dataset_id&quot;:428,&quot;apis&quot;:null,&quot;regression_screens&quot;:null,&quot;screens&quot;:[{&quot;screen_id&quot;:799,&quot;name&quot;:&quot;LoginScreen&quot;,&quot;description&quot;:&quot;d&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:303,&quot;web_page_id&quot;:889,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:799,&quot;web_page_element_id&quot;:282112,&quot;element_order&quot;:1,&quot;element_id&quot;:282112,&quot;web_page_id&quot;:889,&quot;field_name&quot;:&quot;username&quot;,&quot;field_value&quot;:&quot;admin&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='username']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/FORM[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;username&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:799,&quot;web_page_element_id&quot;:282115,&quot;element_order&quot;:2,&quot;element_id&quot;:282115,&quot;web_page_id&quot;:889,&quot;field_name&quot;:&quot;password&quot;,&quot;field_value&quot;:&quot;admin&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='password']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/FORM[1]/INPUT[2]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;password&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:799,&quot;web_page_element_id&quot;:282113,&quot;element_order&quot;:3,&quot;element_id&quot;:282113,&quot;web_page_id&quot;:889,&quot;field_name&quot;:&quot;submit&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='submit']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/FORM[1]/INPUT[3]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;submit&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:800,&quot;name&quot;:&quot;FeedBackClickScreen&quot;,&quot;description&quot;:&quot;s&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:303,&quot;web_page_id&quot;:890,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:800,&quot;web_page_element_id&quot;:282152,&quot;element_order&quot;:1,&quot;element_id&quot;:282152,&quot;web_page_id&quot;:890,&quot;field_name&quot;:&quot;Feedback&quot;,&quot;field_value&quot;:&quot;A&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:&quot;//UL[@id='navigation']/LI[3]/A[1]&quot;,&quot;relative_xpath_by_field_name&quot;:null,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/UL[1]/LI[3]/A[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;A&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;A&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:804,&quot;name&quot;:&quot;LogoutScreen&quot;,&quot;description&quot;:&quot;d&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:303,&quot;web_page_id&quot;:891,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282208,&quot;element_order&quot;:1,&quot;element_id&quot;:282208,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Your Name:&quot;,&quot;field_value&quot;:&quot;rajasekhar@gmail.com&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;//INPUT[@id='name']&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;name&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282210,&quot;element_order&quot;:2,&quot;element_id&quot;:282210,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Your Email:&quot;,&quot;field_value&quot;:&quot;rajasekhar@gmail.com&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;//INPUT[@id='email']&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[2]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;email&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282213,&quot;element_order&quot;:3,&quot;element_id&quot;:282213,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Volvo&quot;,&quot;field_value&quot;:&quot;select&quot;,&quot;field_type&quot;:&quot;link&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;//SELECT[@id='car']/OPTION[1]&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;select&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;[{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[1]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;What's my options:\\&quot;,\\&quot;name\\&quot;:\\&quot;Volvo\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[1]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[2]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Saab\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[2]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[3]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Mercedes\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[3]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Audi\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[4]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[5]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Other\\\\u2026\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[5]\\&quot;}]&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;select&quot;,&quot;dropdown_value&quot;:&quot;{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Audi\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[4]\\&quot;}&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282215,&quot;element_order&quot;:4,&quot;element_id&quot;:282215,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Upload a file:&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[6]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282221,&quot;element_order&quot;:5,&quot;element_id&quot;:282221,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Logout&quot;,&quot;field_value&quot;:&quot;A&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:null,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/H3[1]/A[1]&quot;,&quot;is_selected&quot;:true,&quot;tag_field_name&quot;:&quot;A&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;A&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]}]}]}]"};
			//MOBILE ANDROID
			//data = new String[] {"[{\"primary_info\":{\"browser_type\":\"6\",\"report_upload_url\":\"http://192.168.1.142:8030/TAF_Automation_DR/UploadReportFile\",\"user_id\":7,\"is_generate\":false,\"is_execute\":true,\"is_web\":false,\"mobile_platform\":\"Android\",\"file_name\":\"app-staging-debug.apk\",\"bundle_id\":\"\",\"device_os_version\":\"6\",\"project_url\":null,\"project_name\":\"MobileDemo\",\"project_description\":\"Having Android and iOS Applications\",\"project_id\":147,\"module_name\":\"AndroidDemo\",\"module_description\":\"-Having Android Test cases\",\"sub_module_id\":0,\"sub_module_name\":null,\"sub_module_description\":null,\"module_id\":260,\"testcase_name\":\"TC_Demo_001\",\"testcase_id\":213,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1532068327279},\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"Dataset\",\"dataset_id\":252,\"apis\":null,\"regression_screens\":null,\"screens\":[{\"screen_id\":609,\"name\":\"SplashScreen\",\"description\":\"SplashScreen Elemnts\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":699,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":609,\"web_page_element_id\":206766,\"element_order\":1,\"element_id\":206766,\"web_page_id\":699,\"field_name\":\"LOGIN\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/loginBTN']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='LOGIN']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[2]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/loginBTNtextLOGIN\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":610,\"name\":\"LoginScreen\",\"description\":\"LoginScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":700,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":610,\"web_page_element_id\":206779,\"element_order\":1,\"element_id\":206779,\"web_page_id\":700,\"field_name\":\"Cancel\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/cancelTV']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Cancel']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/cancelTVtextCancel\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":610,\"web_page_element_id\":206772,\"element_order\":2,\"element_id\":206772,\"web_page_id\":700,\"field_name\":\"Username\",\"field_value\":\"buck.l\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/usernameET']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='Username']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/usernameETtextUsername\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":610,\"web_page_element_id\":206770,\"element_order\":3,\"element_id\":206770,\"web_page_id\":700,\"field_name\":\"passwordEditText\",\"field_value\":\"legionco1\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/passwordEditText']\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/passwordEditText\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":610,\"web_page_element_id\":206776,\"element_order\":4,\"element_id\":206776,\"web_page_id\":700,\"field_name\":\"Login\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/login']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Login']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/logintextLogin\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":611,\"name\":\"DashboardScreen\",\"description\":\"DashboardScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":701,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":611,\"web_page_element_id\":206795,\"element_order\":1,\"element_id\":206795,\"web_page_id\":701,\"field_name\":\"Schedule\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/tab_text']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Schedule']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.TabWidget[1]/android.widget.RelativeLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":true,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/tab_texttextSchedule\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":612,\"name\":\"ScheduleOverviewScreen\",\"description\":\"ScheduleOverviewPage elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":702,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":612,\"web_page_element_id\":206824,\"element_order\":1,\"element_id\":206824,\"web_page_id\":702,\"field_name\":\"MAY 22\\\\n-\\\\nMAY 28\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/weekDateTV']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='MAY 22\\\\n-\\\\nMAY 28']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.ListView[1]/android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/weekDateTVtextMAY 22\\\\n-\\\\nMAY 28\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":613,\"name\":\"PastScheduleScreen\",\"description\":\"PastScheduleScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":703,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":613,\"web_page_element_id\":206873,\"element_order\":1,\"element_id\":206873,\"web_page_id\":703,\"field_name\":\"SUMMARY\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='SUMMARY']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"textSUMMARY\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":613,\"web_page_element_id\":206859,\"element_order\":2,\"element_id\":206859,\"web_page_id\":703,\"field_name\":\"nextIv\",\"field_value\":\"ImageView\",\"field_type\":\"ImageView\",\"relative_xpath_by_id\":\"//android.widget.ImageView[@resource-id='co.legion.client.staging:id/nextIv']\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.ImageView[2]\",\"is_selected\":false,\"tag_field_name\":\"ImageView\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/nextIv\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"ImageView\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":613,\"web_page_element_id\":206877,\"element_order\":3,\"element_id\":206877,\"web_page_id\":703,\"field_name\":\"More\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/tabTv']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='More']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.TabWidget[1]/android.widget.RelativeLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/tabTvtextMore\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":614,\"name\":\"ProfileScreen\",\"description\":\"ProfileScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":704,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":614,\"web_page_element_id\":206930,\"element_order\":1,\"element_id\":206930,\"web_page_id\":704,\"field_name\":\"Profile\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/profile']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Profile']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[2]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/profiletextProfile\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":616,\"name\":\"ProfileUpdateScreen\",\"description\":\"ProfileScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":705,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":616,\"web_page_element_id\":206969,\"element_order\":1,\"element_id\":206969,\"web_page_id\":705,\"field_name\":\"hello test\",\"field_value\":\"buck\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/firstName']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='hello test']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/firstNametexthello test\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206973,\"element_order\":2,\"element_id\":206973,\"web_page_id\":705,\"field_name\":\"dgved\",\"field_value\":\"Buck.l\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/nickName']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='dgved']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/nickNametextdgved\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206978,\"element_order\":3,\"element_id\":206978,\"web_page_id\":705,\"field_name\":\"adsfsd\",\"field_value\":\"Dublin\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/streetEdittext']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='adsfsd']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[5]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/streetEdittexttextadsfsd\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206980,\"element_order\":4,\"element_id\":206980,\"web_page_id\":705,\"field_name\":\"Save\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/imageviewOk']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Save']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.widget.RelativeLayout[1]/android.widget.TextView[2]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/imageviewOktextSave\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206975,\"element_order\":5,\"element_id\":206975,\"web_page_id\":705,\"field_name\":\"closeSetup\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"//android.widget.ImageButton[@resource-id='co.legion.client.staging:id/closeSetup']\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.widget.RelativeLayout[1]/android.widget.ImageButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/closeSetup\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":618,\"name\":\"MoreScreen\",\"description\":\"MoreScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":703,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":618,\"web_page_element_id\":206877,\"element_order\":1,\"element_id\":206877,\"web_page_id\":703,\"field_name\":\"More\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/tabTv']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='More']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.TabWidget[1]/android.widget.RelativeLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/tabTvtextMore\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":615,\"name\":\"LogoutScreen\",\"description\":\"LogoutScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":704,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":615,\"web_page_element_id\":206938,\"element_order\":1,\"element_id\":206938,\"web_page_id\":704,\"field_name\":\"Logout\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/logout']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Logout']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[2]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[7]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/logouttextLogout\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":617,\"name\":\"LogoutAlertScreen\",\"description\":\"LogoutScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":706,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":617,\"web_page_element_id\":206986,\"element_order\":1,\"element_id\":206986,\"web_page_id\":706,\"field_name\":\"Logout\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/saveTv']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Logout']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[2]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/saveTvtextLogout\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]}]}]}]\r\n" + 
			//	""};

			//Mobile iOS
			//data = new String[] {"[{\"primary_info\":{\"browser_type\":\"iOS\",\"report_upload_url\":\"http://192.168.1.142:8030/TAF_Automation_DR/UploadReportFile\",\"user_id\":7,\"is_generate\":false,\"is_execute\":true,\"is_web\":false,\"mobile_platform\":\"iOS\",\"file_name\":\"Notifii Track.ipa\",\"bundle_id\":\"com.DevRabbit.LIC\",\"device_os_version\":\"iOS\",\"project_url\":null,\"project_name\":\"MobileDemo\",\"project_description\":\"Having Android and iOS Applications\",\"project_id\":147,\"module_name\":\"iOSDemo\",\"module_description\":\"iOS Mobile Test Cases\",\"sub_module_id\":0,\"sub_module_name\":null,\"sub_module_description\":null,\"module_id\":261,\"testcase_name\":\"TC_DemoiOS_001\",\"testcase_id\":214,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1532068564500},\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"Dataset1\",\"dataset_id\":254,\"apis\":null,\"regression_screens\":null,\"screens\":[{\"screen_id\":619,\"name\":\"LoginScreen\",\"description\":\"LoginScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":707,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":619,\"web_page_element_id\":206990,\"element_order\":1,\"element_id\":206990,\"web_page_id\":707,\"field_name\":\"Username\",\"field_value\":\"devacc1\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"valueUsername\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":619,\"web_page_element_id\":206991,\"element_order\":2,\"element_id\":206991,\"web_page_id\":707,\"field_name\":\"Password\",\"field_value\":\"devacc1@123\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"valuePassword\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":619,\"web_page_element_id\":206995,\"element_order\":3,\"element_id\":206995,\"web_page_id\":707,\"field_name\":\"Log In\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Log In']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameLog In\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":620,\"name\":\"LogPackageOutScreen\",\"description\":\"Click on More button\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":708,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":620,\"web_page_element_id\":207003,\"element_order\":1,\"element_id\":207003,\"web_page_id\":708,\"field_name\":\"??? More\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='\\\\u2022\\\\u2022\\\\u2022 More']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[4]\",\"is_selected\":true,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"name\\\\u2022\\\\u2022\\\\u2022 More\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":621,\"name\":\"MoreScreen\",\"description\":\"MoreScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":709,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":621,\"web_page_element_id\":207055,\"element_order\":1,\"element_id\":207055,\"web_page_id\":709,\"field_name\":\"Search Resident\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeStaticText[@name='Search Resident']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[3]/XCUIElementTypeStaticText[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"nameSearch ResidentvalueSearch Resident\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":623,\"name\":\"SearchResidentScreen\",\"description\":\"SearchResidentScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":710,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":623,\"web_page_element_id\":207101,\"element_order\":1,\"element_id\":207101,\"web_page_id\":710,\"field_name\":\"First Name\",\"field_value\":\"Naveen\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207097,\"element_order\":2,\"element_id\":207097,\"web_page_id\":710,\"field_name\":\"Last Name\",\"field_value\":\"QA\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[2]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207085,\"element_order\":3,\"element_id\":207085,\"web_page_id\":710,\"field_name\":\"Unit Number\",\"field_value\":\"1256455646\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[3]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207104,\"element_order\":4,\"element_id\":207104,\"web_page_id\":710,\"field_name\":\"Email\",\"field_value\":\"naveen@gmail.com\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[4]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207108,\"element_order\":5,\"element_id\":207108,\"web_page_id\":710,\"field_name\":\"Cellphone\",\"field_value\":\"1234567890\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[5]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207090,\"element_order\":6,\"element_id\":207090,\"web_page_id\":710,\"field_name\":\"Search\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Search']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameSearch\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":625,\"name\":\"SearchResultsScreen\",\"description\":\"SearchResultsScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":711,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":625,\"web_page_element_id\":207118,\"element_order\":1,\"element_id\":207118,\"web_page_id\":711,\"field_name\":\"Search Results\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeStaticText[@name='Search Results']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]\",\"is_selected\":true,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"nameSearch ResultsvalueSearch Results\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":625,\"web_page_element_id\":207124,\"element_order\":2,\"element_id\":207124,\"web_page_id\":711,\"field_name\":\"Name\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Name']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]\",\"is_selected\":true,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameNamevalue1\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":625,\"web_page_element_id\":207126,\"element_order\":3,\"element_id\":207126,\"web_page_id\":711,\"field_name\":\"HeaderBackIcon\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='HeaderBackIcon']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameHeaderBackIcon\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":624,\"name\":\"SearchResidentBackScreen\",\"description\":\"SearchResidentBackScreen\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":710,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":624,\"web_page_element_id\":207099,\"element_order\":1,\"element_id\":207099,\"web_page_id\":710,\"field_name\":\"HeaderBackIcon\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='HeaderBackIcon']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameHeaderBackIcon\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":622,\"name\":\"LogOutScreen\",\"description\":\"LogOutScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":709,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":622,\"web_page_element_id\":207072,\"element_order\":1,\"element_id\":207072,\"web_page_id\":709,\"field_name\":\"Log Out\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Log Out']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameLog Out\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]}]}]}]\r\n" + 
			//	""};
			//API
			//data = new String[] {"[{\"primary_info\":{\"browser_type\":null,\"report_upload_url\":\"http://192.168.1.142:8030/TAF_Automation_DR/UploadReportFile\",\"user_id\":7,\"is_generate\":false,\"is_execute\":true,\"is_web\":false,\"mobile_platform\":null,\"file_name\":null,\"bundle_id\":null,\"device_os_version\":null,\"project_url\":\"http://veda-dev.internal.nio.io\",\"project_name\":\"NIO\",\"project_description\":\"NIO Project\",\"project_id\":80,\"module_name\":\"Laser Tag\",\"module_description\":\"Laser Tag URL\\nhttp://veda-dev.internal.nio.io:\",\"sub_module_id\":94,\"sub_module_name\":\"Tag\",\"sub_module_description\":\"Sub module of laser tag\",\"module_id\":77,\"testcase_name\":\"TC_Tag_007\",\"testcase_id\":199,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1532068010728},\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"DS1\",\"dataset_id\":165,\"apis\":[{\"api_name\":\"API_Tag_Post_007\",\"api_description\":\"Tag Post 007 with only mandatory key values \",\"api_id\":408,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":913,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":885,\"raw_text\":\" {\\n        \\\"value\\\": null,\\n        \\\"startevent\\\": 0,\\n        \\\"tagdefinitionoid\\\": \\\"5ad679b4734a7f000b9136bd\\\",\\n        \\\"endevent\\\": 0,\\n        \\\"tripoid\\\": \\\"\\\",\\n        \\\"endtimestamp\\\": 0,\\n        \\\"entityoid\\\": \\\"\\\",\\n        \\\"vehicleoid\\\": \\\"5a6a46b4406b28000c6d2051\\\",\\n        \\\"entitytype\\\": \\\"event\\\",\\n        \\\"starttimestamp\\\": 0\\n    }\",\"raw_type_id\":3,\"dataset_id\":913},\"request_type\":2,\"body_type\":3},{\"api_name\":\"API_Tag_Get_007\",\"api_description\":\"Tag Get 007\",\"api_id\":409,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":912,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":0,\"raw_text\":null,\"raw_type_id\":0,\"dataset_id\":0},\"request_type\":1,\"body_type\":-1},{\"api_name\":\"API_Tag_Put_007\",\"api_description\":\"Tag TC 007 Updating Start time stamp\",\"api_id\":410,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":911,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":886,\"raw_text\":\" {\\n        \\\"value\\\": null,\\n        \\\"startevent\\\": 0,\\n        \\\"tagdefinitionoid\\\": \\\"5ad679b4734a7f000b9136bd\\\",\\n        \\\"endevent\\\": 0,\\n        \\\"tripoid\\\": \\\"\\\",\\n        \\\"endtimestamp\\\": 0,\\n        \\\"entityoid\\\": \\\"\\\",\\n        \\\"vehicleoid\\\": \\\"5a6a46b4406b28000c6d2051\\\",\\n        \\\"entitytype\\\": \\\"event\\\",\\n        \\\"starttimestamp\\\": 13254125\\n    }\",\"raw_type_id\":3,\"dataset_id\":911},\"request_type\":3,\"body_type\":3},{\"api_name\":\"API_Tag_Delete_007\",\"api_description\":\"Tag Tc 007 Delete with oid\",\"api_id\":411,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":910,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":0,\"raw_text\":null,\"raw_type_id\":0,\"dataset_id\":0},\"request_type\":4,\"body_type\":3}],\"regression_screens\":null,\"screens\":null}]}]\r\n"};

			System.out.println(finalData);
			JSONArray array = new JSONArray(finalData);
			isTestSet = array.length() > 1;

			try {
				JSONObject jsonObject = array.getJSONObject(0);
				JSONObject primaryInfoData = jsonObject.getJSONObject("primary_info");
				frameworkPath = primaryInfoData.optString("framework_path");

				environmentVariable = frameworkPath;
				Utils.writeLog("FrameworkPath"  + environmentVariable);
				String secretKey = primaryInfoData.optString("aes_secret_key");
				gitURL = new AESEncryptionUtils().decrypt(primaryInfoData.optString("repository_url"), secretKey);;
				commitMsg = primaryInfoData.optString("repository_commit_message");
				jenkinsURL = primaryInfoData.optString("jenkins_url");//"http://local:8081/";
				jenkinsUsername = primaryInfoData.optString("jenkins_username");;
				jenkinsPassword = primaryInfoData.optString("jenkins_password");;
				jenkinsToken = primaryInfoData.optString("jenkins_token");
				executionEnvironment = primaryInfoData.optString("execution_environment"); //jenkins or Docker
				projectName = primaryInfoData.optString("project_name");
				dockerFilesDirectory = primaryInfoData.optString("docker_files_directory","/opt/tomcat9/webapps/pdownload/DockerFiles");
				dockerFilesDirectoryURL = primaryInfoData.optString("docker_files_directory_url");
				dockerFileName = primaryInfoData.optString("docker_file_name");
				userId = primaryInfoData.optString("user_id");
				// ===========PULL FROM GIT =======
				executeCommands(frameworkPath,gitURL,false,true,false);//pull from git	
			} catch (Exception e) {
				e.printStackTrace();
			}
			//==========END ==============

			if (isTestSet) {
				testSetLength = array.length();

				// testcases loop : TestSet
				for (int i = 0; i < testSetLength; i++) {
					JSONObject jsonObject = array.getJSONObject(i);
					JSONObject primaryInfoObj = jsonObject.getJSONObject("primary_info");
					testCaseName = removeUnWantedChars(primaryInfoObj.optString("testcase_name"));
					newTestCaseName = primaryInfoObj.optString("testcase_name");

					browsers = primaryInfoObj.optString("browser_type");
					String moduleName = removeUnWantedChars(primaryInfoObj.optString("module_name").toLowerCase());
					String subModuleName = primaryInfoObj.optString("sub_module_name") == null ? "" : primaryInfoObj.optString("sub_module_name");
					String subModuleFolder = removeUnWantedChars(subModuleName.toLowerCase());
					executeAction = primaryInfoObj.optBoolean("is_execute");
					generateAction = primaryInfoObj.optBoolean("is_generate");
					String projectName = removeUnWantedChars(primaryInfoObj.optString("project_name")).replaceAll(" ", "").toLowerCase();
					testsetName = removeUnWantedChars(primaryInfoObj.optString("testset_name") == null ? "" : primaryInfoObj.optString("testset_name").toLowerCase());
					newTestsetName = primaryInfoObj.optString("testset_name") == null ? "" : primaryInfoObj.optString("testset_name").toLowerCase();

					isWeb = primaryInfoObj.optBoolean("is_web");
					mobilePlatform = primaryInfoObj.optString("mobile_platform").trim();
					mobileType = primaryInfoObj.getString("mobile_type");
					fileName = primaryInfoObj.optString("file_name").trim();
					version = primaryInfoObj.optString("device_os_version").trim();
					bundleId = primaryInfoObj.optString("bundle_id").trim();
					testsetId = primaryInfoObj.optInt("testset_id");

					executionPlatform = primaryInfoObj.optString("execution_environment","local");
					String subModule = ((!subModuleFolder.equals("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) ? ("." + subModuleFolder) :"");
					String packageName = "";
					/*if(!mobilePlatform.equalsIgnoreCase("null") && mobilePlatform != null && !mobilePlatform.isEmpty()) {					
									if(fileName!= null && !fileName.isEmpty() ) {
										new CreateDesiredCapabilties().presetText(fileName, version, bundleId);
									}
									packageName= "mobile." + projectName  + "." 
											+ moduleName + subModule + ".mobiletestclasses" + "."
											+ toTitleCase(testCaseName) ;
								} else */

					if(!mobilePlatform.equalsIgnoreCase("null") && mobilePlatform != null && !mobilePlatform.isEmpty()) {	
						packageName= "mobile." + projectName  + "." 
								+ moduleName + subModule + ".mobiletestclasses" + "."
								+ removeUnWantedChars(toTitleCase(primaryInfoObj.optString("testcase_name"))) ;
					} else if(isWeb) {

						packageName= "web." + projectName  + "." 
								+ moduleName + subModule + ".webtestclasses" + "."
								+(removeUnWantedChars(toTitleCase(primaryInfoObj.optString("testcase_name")))) ;
					} else {
						packageName= "api." + projectName  + "." 
								+ moduleName + subModule + ".apitestclasses" + "."
								+ removeUnWantedChars(toTitleCase(primaryInfoObj.optString("testcase_name"))) ;
					}
					testClasses = testClasses 
							+ "\r\t\t\t<class name=\"" + packageName + "\" />";
				}


				com.utils.Utils.createReportPortalProperties(environmentVariable,projectName, testsetName);

				if (isWindows()) {
					createBatFile(testsetName, ".bat");
				} else if (isUnix() || isSolaris() || isMac()) {
					createBatFile(testsetName, ".sh");
				} 

				testClasses = testClasses + "\n\t\t\t<class name=\"com.fileupload.ReportUpload\" />";
				writeTestSuiteFile(testsetName, testClasses);
				createProfiles(testsetName);
			} else {
				testSetLength = 1;

				JSONObject primaryInfoJsonObj= array.getJSONObject(0).getJSONObject("primary_info");
				isWeb = primaryInfoJsonObj.optBoolean("is_web");
				bundleId = primaryInfoJsonObj.optString("bundle_id").trim();
				mobilePlatform = primaryInfoJsonObj.getString("mobile_platform");
				mobileType = primaryInfoJsonObj.getString("mobile_type");
				executionPlatform = primaryInfoJsonObj.optString("execution_environment","local");
				fileName = primaryInfoJsonObj.optString("file_name").trim();
				version = primaryInfoJsonObj.optString("device_os_version").trim();
				executeAction = primaryInfoJsonObj.optBoolean("is_execute");
				generateAction = primaryInfoJsonObj.optBoolean("is_generate");
				newTestCaseName = primaryInfoJsonObj.optString("testcase_name");
				newTestsetName = primaryInfoJsonObj.optString("testset_name") == null ? "" : primaryInfoJsonObj.optString("testset_name").toLowerCase();

				testCaseName = removeUnWantedChars(primaryInfoJsonObj.optString("testcase_name"));
				testcaseId = primaryInfoJsonObj.optInt("testcase_id");
				projectName =removeUnWantedChars(primaryInfoJsonObj.optString("project_name")).replaceAll(" ", "").toLowerCase();
				// String testcaseName = primaryInfoJsonObj.optString("testcase_name");
				com.utils.Utils.createReportPortalProperties(environmentVariable, projectName, testCaseName);

			}


			String gitopsName = "testing";
			// Checking the API (or) WEB (or) Mobile scripts
			
			if(!mobileType.equals("null") && mobileType != null && !mobileType.isEmpty() && mobileType.equalsIgnoreCase("mobileWeb")) {
				gitopsName = "mobile web";
				if(executionPlatform.equalsIgnoreCase("local")) {
					fileName = (mobilePlatform.equalsIgnoreCase("android") ?"mobiletest.apk": "mobiletest.ipa");
					new CreateDesiredCapabilties().presetText(environmentVariable,fileName, version, bundleId);
				} else if(executionPlatform.equalsIgnoreCase("local")) {
					fileName = (mobilePlatform.equalsIgnoreCase("android") ?"mobiletest.apk": "mobiletest.ipa");
					new CreateDesiredCapabilties().presetText(environmentVariable,fileName, version, bundleId);
				} else {
					JSONObject sauceObj =  array.getJSONObject(0).getJSONObject("mobile_device_info");
					
					String userName = sauceObj.optString("userName");
					String accessKey = sauceObj.optString("accessKey");
					String testObjApiKey = sauceObj.optString("objApiKey");
					String url = sauceObj.optString("url");
					String devices = sauceObj.getJSONArray("devices").toString();
					new CreateDesiredCapabilties().createSauceDesiredCaps(environmentVariable,projectName,userName,accessKey, testObjApiKey, devices, url, executionPlatform);
				}
				
				System.out.println("-------------" + finalData);
				JSONArray jsonArray = new JSONArray(finalData);		
				for (int i = 0; i < jsonArray.length(); i++) {
					WebScript output = jsonParse(jsonArray.get(i).toString());
					new GenerateWebScript3().generateCode(isTestSet, output, environmentVariable);
				}
			} else if(!mobilePlatform.equals("null") && mobilePlatform != null && !mobilePlatform.isEmpty()) {
				gitopsName = "mobile native";
				if(executionPlatform.equalsIgnoreCase("local")) {
					new CreateDesiredCapabilties().presetText(environmentVariable,fileName, version, bundleId);
				} else {
					JSONObject sauceObj =  array.getJSONObject(0).getJSONObject("mobile_device_info");
					
					String userName = sauceObj.optString("userName");
					String accessKey = sauceObj.optString("accessKey");
					String testObjApiKey = sauceObj.optString("objApiKey");
					String url = sauceObj.optString("url");
					String devices = sauceObj.getJSONArray("devices").toString();
					new CreateDesiredCapabilties().createSauceDesiredCaps(environmentVariable,projectName,userName,accessKey, testObjApiKey, devices, url, executionPlatform);
				}
				
				System.out.println("-------------" + finalData);
				JSONArray jsonArray = new JSONArray(finalData);		
				for (int i = 0; i < jsonArray.length(); i++) {
					WebScript output = jsonParse(jsonArray.get(i).toString());
					new GenerateMobileScript().generateCode(isTestSet, output, environmentVariable);
				}
			}
			
			
			else if (isWeb) {
				gitopsName = "Web";
				System.out.println("-------------" + finalData);
				JSONArray jsonArray = new JSONArray(finalData);		
				for (int i = 0; i < jsonArray.length(); i++) {
					WebScript output = jsonParse(jsonArray.get(i).toString());
					Utils.writeLog("StartedGenerating Scripts"  + output);
					new GenerateWebScript3().generateCode(isTestSet, output, environmentVariable);

				}
			} else {
				
				gitopsName = "API";
				for (int i = 0; i < array.length(); i++) {
					GenerateApiScript scriptGenerate = new GenerateApiScript();
					scriptGenerate.generateCode(isTestSet, i, array.getJSONObject(i), array.length(), environmentVariable);
				}
			}

			// batch file execution
			if (executeAction) {
				if (isWindows()) {
					bacthFileExecution(isTestSet == true ? testsetName + ".bat" : "testng.bat");
				} else if (isUnix() || isSolaris() || isMac()) {
					Utils.writeLog("bacthFileExecution"  + (isTestSet == true ? testsetName + ".sh" : "testng.sh"));
					bacthFileExecution(isTestSet == true ? testsetName + ".sh" : "testng.sh");
				} 			
			}
			

			msg = !generateAction == true ? "Sucessfully Executed" : "Sucessfully generated files";
			// ===========Push to GIT =======
			try {
				try {

					File file = new File(environmentVariable + File.separator + ".gitlab-ci.yml");

					if(file.exists() && file.delete()) {
						System.out.println("File deleted successfully");
					} else {
						System.out.println("Failed to delete the gitlab-ci.yml file");
					}

					if(executionEnvironment.equalsIgnoreCase("Jenkins")
							|| executionEnvironment.equalsIgnoreCase("kobiton")
							|| executionEnvironment.equalsIgnoreCase("saucelabvirtural") 
							|| executionEnvironment.equalsIgnoreCase("saucelabdevice")) {
						Utils.writeLog( "createJenkinsConfigurationFile" + gitURL + tcGoal);
						createJenkinsConfigurationFile(gitURL, tcGoal);	
					} else if(executionEnvironment.equalsIgnoreCase("gitops")) {

						String config = GitopsAutomationConfiguaration.setupGitOps("mvn " + tcGoal, System.currentTimeMillis(), gitopsName);
						Utils.writeLog("Gitops creation" + config);
						writeFile(config, ".gitlab-ci", environmentVariable, ".yml");
						Utils.writeLog("Gitops File " + new File(environmentVariable + File.separator + ".gitlab-ci.yml").exists());

					} else if(executionEnvironment.equalsIgnoreCase("gitopsdocker")) {


						boolean isWindows = false;

						Utils.writeLog( "GitopsDOcker");
						String finalName = (newTestsetName ==null || newTestsetName.isEmpty() || newTestsetName.equalsIgnoreCase("null")) ? newTestCaseName : newTestsetName;


						String branchName = "master";
						if (finalName.contains("@")) {
							String test[] = finalName.split("@");

							if (!test[1].toLowerCase().equalsIgnoreCase("windows")) {
								branchName = test[1];
							} else {
								String name = finalName.toLowerCase();
								if (name.contains("windows"))
									isWindows = true;
							}
						}

						DockerConf.dockerFile(branchName, gitURL, newTestCaseName, newTestsetName,testsetName, environmentVariable, dockerFileName,true);
						String config = GitopsAutomationConfiguaration.setupGitOpsDocker("sudo docker build -f "+ dockerFileName+ " -t " + projectName.toLowerCase() + " .", System.currentTimeMillis(), gitopsName);
						Utils.writeLog("Gitops creation" + config);
						writeFile(config, ".gitlab-ci", environmentVariable, ".yml");

					} else if(executionEnvironment.equalsIgnoreCase("Docker")) {
						Utils.writeLog( "createJenkinsDockerConfigurationFile");
						createJenkinsDockerConfigurationFile();
						copyDockerFIles(frameworkPath); //Docker files to git
					}
				} catch (Exception e) {
					Utils.writeLog( "Excception" + e.getLocalizedMessage());
					System.out.println("Failed to execute on server");
					e.printStackTrace();
				}

				Utils.writeLog(frameworkPath+","+ gitURL +",false,false,true");
				executeCommands(frameworkPath,gitURL,false,false,true);//push to git	

			} catch (Exception e) {
				e.printStackTrace();
			}
			//==========END ==============


		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("completed");
		System.exit(1);
		return msg;
	}

	//Docker files copy Execution

	private void copyDockerFIles(String frameworkPath) throws Exception {
		try {
			String dockerExecutionPath = frameworkPath.substring(0, frameworkPath.lastIndexOf(File.separator));
			dockerExecutionPath  = dockerExecutionPath.substring(0, dockerExecutionPath.lastIndexOf(File.separator));
			String pathForming = "cp -R " + frameworkPath + File.separator + "DockerFiles" + File.separator + "* " + dockerFilesDirectory + "\n";

			String command =  pathForming +
					"\n" + 
					"";
			Utils.writeLog( "Writing file " + command + "copyDockerFiles"+ environmentVariable + ".sh");
			writeFile(command, "copyDockerFiles", environmentVariable, ".bat");
			writeFile(command, "copyDockerFiles", environmentVariable, ".sh");

			if (isWindows()) {
				bacthFileExecution("copyDockerFiles.bat");
			} else if (isUnix() || isSolaris() || isMac()) {
				bacthFileExecution("copyDockerFiles.sh");
			}
		} catch (IOException | InterruptedException e) {
			Utils.writeLog("Copy files Exception " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}




	//********************************************** GIT Execution *************************************//


	private void executeCommands(String frameworkPath,String gitURL, boolean isClone, boolean isPull, boolean isPush) throws Exception {
		try {	
			if(isPush) execJenkins =true;
			File f = new File(frameworkPath);
			String finalwords = "";
			String pathForming = "git config --global http.sslverify false\n";
			String cloneForming = "git config --global http.sslverify false\n";
			String source = "";

			if(isMac()) {
				source = "source ~/.bash_profile\n";
			} else if(isWindows()) {
				source = "";
			}

			if(isClone && !f.isDirectory()) {
				try {

					finalwords = "/home/prolifics/QualityFusion/QF-Users/" + userId + "/Selenium/";
					cloneForming += "mkdir -p " + finalwords + "\n";
					cloneForming += "chmod -R 777 " + finalwords + "\n";
					cloneForming += "cd " + finalwords + "\n";
					cloneForming += "git clone " + gitURL + "\n";
					cloneForming += "git clone " + gitURL + "\n";

					String command = source + cloneForming + "\n" + "";
					Utils.writeLog("Writing file " + command + "gitcommands" + environmentVariable + ".sh");
					// writeFile(command, "gitcommands", "", ".bat");
					Utils.writeFile(command, "gitcommands", "/home/prolifics/QualityFusion/", ".sh");
					Process p = Runtime.getRuntime().exec("/home/prolifics/QualityFusion/gitcommands.sh");
				} catch (Exception e) {
					// TODO: handle exception
				}

			} 
			if(commitMsg == null || commitMsg.isEmpty()) {
				commitMsg = "Test Commit";
			}
			
			
			if (f.exists() && f.isDirectory() && isPull) {
				pathForming += "cd " + frameworkPath + 
						"\ngit commit -m \"" + commitMsg + "\"\n"
						+ "git pull " + gitURL + "\n";
			} 
			if (f.exists() && f.isDirectory() && isPush) {
				pathForming += "cd " + frameworkPath + "\ngit init\n git add .\n"
						+ "git add .gitlab-ci.yml"
						+ "\ngit rm -r --cached target\n"
						+ "git rm -r --cached test-output\n"
						+ "git rm -r --cached gitcommands.bat\n"
						+ "git rm -r --cached gitcommands.sh\n"
						+ "git rm -r --cached APIReports\n"
						+ "git rm -r --cached MobileReports\n"
						+ "git rm -r --cached WebReports\n"
						+ "git rm -r --cached jenkins*\n"
						+ "git rm -r --cached config*\n"
						+ "git rm -r --cached DockerFiles\n"
						+ "git rm -r --cached screenshots\n"
						+ "git rm -r --cached copyDockerFiles.bat\n"
						+ "git rm -r --cached copyDockerFiles.sh\n"
						+ "git commit -m \""+ commitMsg +"\"\n" 
						+ "git remote add origin "+ gitURL +"\n" 
						+ "git push -u origin master"+ "\n";
			}

			String command = source
					+"cd " + frameworkPath + "\n" 
					+ pathForming +
					"\n" + 
					"";
			Utils.writeLog( "Writing file " + command + "gitcommands" + environmentVariable+ ".sh");
			writeFile(command, "gitcommands", environmentVariable, ".bat");
			writeFile(command, "gitcommands", environmentVariable, ".sh");

			if (isWindows()) {
				bacthFileExecution("gitcommands.bat");
			} else if (isUnix() || isSolaris() || isMac()) {
				Utils.writeLog( "Git  Exceution file gitcommands.sh");
				bacthFileExecution("gitcommands.sh");
			}


		} catch (IOException | InterruptedException e) {
			Utils.writeLog("Git Exception " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}



	/**
	 * conversion : jsonobject to model
	 *  return dataset object;
	 */
	private static WebScript jsonParse(String output) {
		Gson gson = new Gson();
		WebScript datasetValues = (WebScript) gson.fromJson(output, WebScript.class);
		return datasetValues;
	}


	/**
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
			String val = value.replaceAll(" ", "");
			val = val.replaceAll("[^a-z_A-Z0-9]", "");
			return val;
		}catch (Exception e) {
			return value;
		}
	}

	/**
	 * File writer method
	 */

	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			Utils.writeLog("Writen files" + str);
			setPermissionsToFile(filePath + File.separator + className + fileExtension);
		} finally {
			out.close();
		}
	}



	/**
	 *  Using this method we can execute the Batch/sh file 
	 */
	private String response, errorResponse;
	private void bacthFileExecution(String batFileName) throws Exception {

		try {	
			Process p = Runtime.getRuntime().exec(environmentVariable + File.separator +  batFileName);
			final BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			final BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			try {
				ExecutorService worker = Executors.newSingleThreadExecutor();
				Callable<String> task = new Callable<String>() {

					public String call() throws IOException {
						String line1;
						while (((line1 = (bri.readLine())) != null)) {
							System.out.println(line1);
							response = response + line1;
							Utils.writeLog("bacthFileExecution" + response);
						}

						return response;
					}

				};
				Future<String> futureResult = worker.submit(task);
				futureResult.get(10, TimeUnit.SECONDS);
			} catch (Exception e) {}

			try {
				ExecutorService executor = Executors.newSingleThreadExecutor();
				Callable<String> tasker = new Callable<String>() {
					public String call() throws IOException {
						String line1;
						while (((line1 = (bre.readLine())) != null)) {
							System.out.println(line1);
							errorResponse = errorResponse + line1;
							Utils.writeLog("bacthFileExecution Error" + errorResponse);
						}
						return errorResponse;
					}
				};
				Future<String> futureResult = executor.submit(tasker);
				futureResult.get(10, TimeUnit.SECONDS);

			} catch (Exception e) {
				Utils.writeLog("bacthFileExecution Error" + e.getLocalizedMessage());

			}


			bre.close();
			bri.close();
			Utils.writeLog("Exceution DOne ");
			System.out.println("Done.");


		} catch (Exception err) {
			Utils.writeLog("bacthFileExecution Error" + err.getLocalizedMessage());
			err.printStackTrace();
		}
	}

	//================================= Docker Config File Creation ==========================================//
	private void createJenkinsDockerConfigurationFile() throws Exception {

		Utils.writeLog("createJenkinsDockerConfigurationFile started");
		//String dockerFilePath = "$HOME/"+ projectName +"/DockerFiles";
		//	String dockerFileName = "DockerFile_" + userId + "_SE_" +  ((testcaseId != 0) ? "TC_" + testcaseId : "TS_" + testsetId); 
		boolean isWindows = false;


		String finalName = (newTestsetName ==null || newTestsetName.isEmpty() || newTestsetName.equalsIgnoreCase("null")) ? newTestCaseName : newTestsetName;


		String branchName = "master";
		if (finalName.contains("@")) {
			String test[] = finalName.split("@");

			if (!test[1].toLowerCase().equalsIgnoreCase("windows")) {
				branchName = test[1];
			} else {
				String name = finalName.toLowerCase();
				if (name.contains("windows"))
					isWindows = true;
			}
		}




		DockerConf.dockerFile(branchName, gitURL, newTestCaseName, newTestsetName,testsetName, environmentVariable, dockerFileName,false);

		String cmd = "$HOME" + File.separator + "SQEDockerFiles";

		String finalCommand= "";
		if(isWindows) {
			finalCommand = "<hudson.tasks.BatchFile>\r\n" + 
					"<command>mkdir -p %userprofile%\\SQEDockerFiles"
					+ "\r\ncd %userprofile%\\SQEDockerFiles\r\n"
					+ "curl " + dockerFilesDirectoryURL + "/" + dockerFileName + " --output "+ dockerFileName
					+ "\r\ncd %userprofile%\\SQEDockerFiles\r\n"
					+ "docker build -f "+ dockerFileName+ " -t " + projectName.toLowerCase() + " .</command>\r\n" + 
					"</hudson.tasks.BatchFile>\r\n";
		} else {
			finalCommand = 	"    <hudson.tasks.Shell>\r\n" + 
					"      <command> mkdir -p " + cmd + " &amp;&amp; "
					+ " cd "+ cmd + " &amp;&amp; curl " + dockerFilesDirectoryURL + "/" + dockerFileName + " --output "+ dockerFileName + " &amp;&amp; "
					+ "cd  "+ cmd + " &amp;&amp; " 
					+ (iscentOS ? "" : "sudo") + " docker build -f "+ dockerFileName +" -t " + projectName.toLowerCase() + " .</command>\r\n" + 
					"    </hudson.tasks.Shell>\r\n";

		}


		String dockerJenkinsConfig = "<?xml version='1.1' encoding='UTF-8'?>\r\n" + 
				"<project>\r\n" + 
				"  <actions/>\r\n" + 
				"  <description></description>\r\n" + 
				"  <keepDependencies>false</keepDependencies>\r\n" + 
				"  <properties/>\r\n" + 
				"  <scm class=\"hudson.scm.NullSCM\"/>\r\n" + 
				"  <canRoam>true</canRoam>\r\n" + 
				"  <disabled>false</disabled>\r\n" + 
				"  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\r\n" + 
				"  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\r\n" + 
				"  <triggers/>\r\n" + 
				"  <concurrentBuild>false</concurrentBuild>\r\n" + 
				"  <builders>\r\n" + 
				finalCommand+
				"  </builders>\r\n" + 
				"  <publishers/>\r\n" + 
				"  <buildWrappers/>\r\n" + 
				"</project>";

		Utils.writeLog("Docker Writing files started" + dockerJenkinsConfig + "," + "config" + dockerFileName + "," + environmentVariable+","+".xml");

		//writeFile(dockerJenkinsConfig, dockerFileName + "_config", dockerFilePath , ".xml");
		writeFile(dockerJenkinsConfig, "config" + dockerFileName, environmentVariable, ".xml");
		String protocol = jenkinsURL.contains("https://") ? "https://" : "http://";
		String finalJenkinsURL = protocol + jenkinsUsername + ":" + jenkinsPassword + "@" + jenkinsURL.replace(protocol, "");
		String apiAuthenticationToken = " --user " + jenkinsUsername + ":" + jenkinsToken;
		String bashrc = iscentOS ? "" : "source ~/.bash_rc\n";

		if(isMac()){
			bashrc = "source ~/.bash_profile\n";
		} else if(isWindows()) {
			bashrc = "";
		}
		String finalTestName = ((testsetName == null || testsetName.equalsIgnoreCase("null") || testsetName.isEmpty())? testCaseName : testsetName);
		String jenkinsScript = bashrc
				+ "cd " + environmentVariable + "\n"
				+ "curl -d @config" + dockerFileName + ".xml -H 'Content-Type: text/xml' '" + finalJenkinsURL + "createItem?name=" + projectName + "_" + finalTestName + "_Docker'"+  apiAuthenticationToken + "\n"
				+ "curl -X POST " + finalJenkinsURL + "job/" + projectName + "_" + finalTestName + "_Docker" + "/config.xml" + apiAuthenticationToken +  " --data-binary @config" + dockerFileName + ".xml\n"
				+ "curl -X POST " + finalJenkinsURL + "job/" + projectName + "_" + finalTestName +  "_Docker" + "/build" + apiAuthenticationToken + "\n";
		Utils.writeLog("Docker Writing files started" + jenkinsScript+", "+"jenkins" + dockerFileName +","+environmentVariable + ".sh");

		writeFile(jenkinsScript, "jenkins" + dockerFileName , environmentVariable, ".sh");
		Utils.writeLog("Jenkins Docker Execution Started" +"jenkins"+  dockerFileName + ".sh");

		bacthFileExecution("jenkins"+  dockerFileName + ".sh");
	}
	//************************************* Jenkins Config File Creation and Execution ********************************//



	private void createJenkinsConfigurationFile(String gitUrl,String goal) throws Exception {
		String jenkinsConfigFileName = testsetName + ((testcaseId != 0) ? testcaseId : testsetId);

		String branchName = "master";

		String finalName = (newTestsetName ==null || newTestsetName.isEmpty() || newTestsetName.equalsIgnoreCase("null")) ? newTestCaseName : newTestsetName;

		if (finalName.contains("@")) {
			String test[] = finalName.split("@");
			//testCaseName = test[0];

			if (!test[1].toLowerCase().equalsIgnoreCase("windows")) {
				branchName = test[1];
			}
		}



		String configFile = "<?xml version='1.1' encoding='UTF-8'?>\r\n" + 
				"<maven2-moduleset plugin=\"maven-plugin@3.2\">\r\n" + 
				"  <actions/>\r\n" + 
				"  <description></description>\r\n" + 
				"  <keepDependencies>false</keepDependencies>\r\n" + 
				"  <properties/>\r\n" + 
				"  <scm class=\"hudson.plugins.git.GitSCM\" plugin=\"git@3.9.1\">\r\n" + 
				"    <configVersion>2</configVersion>\r\n" + 
				"    <userRemoteConfigs>\r\n" + 
				"      <hudson.plugins.git.UserRemoteConfig>\r\n" + 
				"        <url>" + gitUrl + "</url>\r\n" + 
				"      </hudson.plugins.git.UserRemoteConfig>\r\n" + 
				"    </userRemoteConfigs>\r\n" + 
				"    <branches>\r\n" + 
				"      <hudson.plugins.git.BranchSpec>\r\n" + 
				"        <name>*/" + branchName + "</name>\r\n" + 
				"      </hudson.plugins.git.BranchSpec>\r\n" + 
				"    </branches>\r\n" + 
				"    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>\r\n" + 
				"    <submoduleCfg class=\"list\"/>\r\n" + 
				"    <extensions/>\r\n" + 
				"  </scm>\r\n" + 
				"  <canRoam>true</canRoam>\r\n" + 
				"  <disabled>false</disabled>\r\n" + 
				"  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\r\n" + 
				"  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\r\n" + 
				"  <triggers/>\r\n" + 
				"  <concurrentBuild>false</concurrentBuild>\r\n" + 
				"  <rootModule>\r\n" + 
				"    <groupId>Framework</groupId>\r\n" + 
				"    <artifactId>Framework</artifactId>\r\n" + 
				"  </rootModule>\r\n" + 
				"  <goals>" + goal + "</goals>\r\n" + 
				"  <aggregatorStyleBuild>true</aggregatorStyleBuild>\r\n" + 
				"  <incrementalBuild>false</incrementalBuild>\r\n" + 
				"  <ignoreUpstremChanges>false</ignoreUpstremChanges>\r\n" + 
				"  <ignoreUnsuccessfulUpstreams>false</ignoreUnsuccessfulUpstreams>\r\n" + 
				"  <archivingDisabled>false</archivingDisabled>\r\n" + 
				"  <siteArchivingDisabled>false</siteArchivingDisabled>\r\n" + 
				"  <fingerprintingDisabled>false</fingerprintingDisabled>\r\n" + 
				"  <resolveDependencies>false</resolveDependencies>\r\n" + 
				"  <processPlugins>false</processPlugins>\r\n" + 
				"  <mavenValidationLevel>-1</mavenValidationLevel>\r\n" + 
				"  <runHeadless>false</runHeadless>\r\n" + 
				"  <disableTriggerDownstreamProjects>false</disableTriggerDownstreamProjects>\r\n" + 
				"  <blockTriggerWhenBuilding>true</blockTriggerWhenBuilding>\r\n" + 
				"  <settings class=\"jenkins.mvn.DefaultSettingsProvider\"/>\r\n" + 
				"  <globalSettings class=\"jenkins.mvn.DefaultGlobalSettingsProvider\"/>\r\n" + 
				"  <reporters/>\r\n" + 
				"  <publishers/>\r\n" + 
				"  <buildWrappers/>\r\n" + 
				"  <prebuilders/>\r\n" + 
				"  <postbuilders/>\r\n" + 
				"  <runPostStepsIfResult>\r\n" + 
				"    <name>FAILURE</name>\r\n" + 
				"    <ordinal>2</ordinal>\r\n" + 
				"    <color>RED</color>\r\n" + 
				"    <completeBuild>true</completeBuild>\r\n" + 
				"  </runPostStepsIfResult>\r\n" + 
				"</maven2-moduleset>";
		Utils.writeLog("Create Jenkins writing files Started" +configFile+","+"config" + jenkinsConfigFileName+","+environmentVariable+".xml");
		writeFile(configFile, "config" + jenkinsConfigFileName, environmentVariable, ".xml");
		String protocol = jenkinsURL.contains("https://") ? "https://" : "http://";
		String finalJenkinsURL = protocol + jenkinsUsername + ":" + jenkinsPassword + "@" + jenkinsURL.replace(protocol, "");
		String apiAuthenticationToken = " --user " + jenkinsUsername + ":" + jenkinsToken;
		String bashrc = iscentOS ? "" : "source ~/.bash_rc\n";

		if(isMac()){
			bashrc = "source ~/.bash_profile\n";
		} else if(isWindows()) {
			bashrc = "";
		}



		String jenkinsScript = bashrc
				+ "cd " + environmentVariable + "\n"
				+ "curl -d @config" + jenkinsConfigFileName + ".xml -H 'Content-Type: text/xml' '" + finalJenkinsURL + "createItem?name=" + projectName + "'"+  apiAuthenticationToken + "\n"
				+ "curl -X POST " + finalJenkinsURL + "job/" + projectName + "/config.xml" + apiAuthenticationToken +  " --data-binary @config" + jenkinsConfigFileName + ".xml\n"
				+ "curl -X POST " + finalJenkinsURL + "job/" + projectName + "/build" + apiAuthenticationToken + "\n";
		Utils.writeLog("Create Jenkins script  Writing files Started" + jenkinsScript+"jenkins" + jenkinsConfigFileName + environmentVariable+".sh");
		writeFile(jenkinsScript, "jenkins" + jenkinsConfigFileName , environmentVariable, ".sh");
		Utils.writeLog("Create Jenkins script  Writing files Started" + jenkinsScript+"jenkins" + jenkinsConfigFileName + environmentVariable+".sh");
		bacthFileExecution("jenkins"+  jenkinsConfigFileName + ".sh");
	}


	/**
	 *  Dynamically creating bat file for tesset by testsetname
	 */
	private void createBatFile(String name, String extension) throws Exception {
		String batFileSh = "";
		String batFilebat = "";
		tcGoal = tcGoal + " -P" + name;
		batFileSh="export PATH=$PATH:$M2_HOME/bin\n" + 
				"source ~/.bash_profile"
				+"\ncd $AUTOMATION_PATH\n" + 
				"mvn clean test -P"+ name +"\n" + 
				"pause\n" + 
				"\n" + 
				"";

		batFilebat ="%AUTOMATION_DRIVE%\n" + 
				"cd %AUTOMATION_PATH%\n" + 
				"mvn clean test -P"+ name +"\n" + 
				"pause\n" + 
				"\n" + 
				"";
		Utils.writeLog(batFilebat+name+environmentVariable+ ".bat");
		writeFile(batFilebat, name, environmentVariable, ".bat");
		Utils.writeLog(batFileSh+name+environmentVariable+ ".sh");
		writeFile(batFileSh, name, environmentVariable, ".sh");

	}

	/**
	 * Creating testng file for testset by module name
	 * 
	 */

	private void writeTestSuiteFile(String packageFolder, String testClasses) throws Exception {
		String[] browsersList = browsers.split(",");
		String finalSet = "";
		for (int i = 0; i < browsersList.length; i++) {
			String browserName = browsersList[i];
			finalSet = finalSet + "	<test name=\"" + browserName + "test\">\r\n" + 
					"  	<parameter name=\"browser\" value=\"" + browserName + "\"/>\r\n" +
					"		<classes>" + testClasses + "\r\n\t\t</classes>\r\n" + 
					"	</test>\r\n";
		}

		String  testngFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >\r\n" + 
				"<suite name=\"Main Test Suite\" parallel=\"tests\">\r\n" + 
				finalSet +
				"</suite>\r\n" + 
				"";
		writeFile(testngFile, packageFolder, environmentVariable, ".xml");
	}


	/** 
	 * Creating profiles in pom.xml
	 * Important Note : Don't change pom.xml 
	 * we just appending the profile before last 2 lines from pom.xml 
	 */

	private static void createProfiles(String profileName) throws Exception {
		String profile = "\t\t<profile>\n" + 
				"			<id>" + profileName + "</id>\n" + 
				"			<build>\n" + 
				"				<plugins>\n" + 
				"					<!-- Compiler plug-in -->\n" + 
				"					<plugin>\n" + 
				"						<groupId>org.apache.maven.plugins</groupId>\n" + 
				"						<artifactId>maven-compiler-plugin</artifactId>\n" + 
				"\n" + 
				"						<configuration>\n" + 
				"							<source>${jdk.level}</source>\n" + 
				"							<target>${jdk.level}</target>\n" + 
				"						</configuration>\n" + 
				"					</plugin>\n" + 
				"					<!-- Below plug-in is used to execute tests -->\n" + 
				"					<plugin>\n" + 
				"						<groupId>org.apache.maven.plugins</groupId>\n" + 
				"						<artifactId>maven-surefire-plugin</artifactId>\n" + 
				"						<version>2.19.1</version>\n" + 
				"						<configuration>\n" + 
				"							<testFailureIgnore>true</testFailureIgnore>\n" + 
				"							<suiteXmlFiles>	\n"+
				"								<suiteXmlFile>"+ profileName +".xml</suiteXmlFile>\n" + 
				"							</suiteXmlFiles>\n" + 
				"						</configuration>\n" + 
				"					</plugin>\n" + 
				"				</plugins>\n" + 
				"			</build>\n" + 
				"		</profile>\n";

		/* Input appending the specific line */

		Path path = Paths.get(environmentVariable, "pom.xml");

		List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

		// position of appending the line

		int position = lines.size() - 2;

		/**
		 * pom.xml
		 * checking the profile is exists or not
		 */

		String uniqString = profile;
		if (!lines.toString().contains(profileName + ".xml")) {
			lines.add(position,uniqString);
			Files.write(path, lines, StandardCharsets.UTF_8);
		} 
	}

	private static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].trim().equals(""))
				continue;
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append("");
		}
		return sb.toString().trim();
	}

	/* Executing the bat/sh file to get the environment variables */

	private  ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + environmentVariable + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}

	/* Finding the OS */

	private static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	private static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
}