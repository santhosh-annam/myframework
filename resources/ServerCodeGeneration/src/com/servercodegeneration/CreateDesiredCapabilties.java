package com.servercodegeneration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

 /**
 * 
 * This class used to get the Android Device Information and creating the desired capabilities 
 * @ Device Name
 * @ Platform version
 * @ Package Name
 * @ UDID
 * @ Launcher Activity
 * 
 */

public class CreateDesiredCapabilties {
	
	private static final String GET_PACKAGE_LAUNCHER_NAME = "package";
	private final String GET_DEVICE_UDID = "devices";
	private final String GET_PRODUCT_DETAILS = "productDetails";
	private final String KILL_ADB = "killadb";
	private String automationPath = System.getenv("AUTOMATION_PATH") + File.separator + "mobile" + File.separator + "android";
	public String filePath = "";
	private String sendPackageName;
	private String sendLauncherActivity;
	private String sendUdid;
	@SuppressWarnings("unused")
	private String  sendVersion;
	public String deviceId = "";
	private String deviceName = "";
	private String deviceVersion = "";
	private String fileName = "";
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static String extension = "";
	private static String batFile = "";
	private static String environMentVariable =  System.getenv("AUTOMATION_PATH");
	private static String toogleExtension = "*.apk";
	private String addBash= "";
	public String nodeJSPath = System.getenv("NODE_PATH");
	public String appiumServerJSPath=  System.getenv("APPIUM_JS_PATH");;
	private String MAC_HOME_DIRECTORY;
	
	
	/**
	 *  Used to create and execute the bat/sh files  
	 *  Finding the OS
	 * @param name
	 * @param version
	 * @param bundleId
	 * @param envVariable 
	 */
	public void presetText(String envVariable, String name, String version, String bundleId) {
		automationPath = envVariable;
		try {
			//Here Creating script files based on platform
			if (isWindows()) {
				extension = ".bat";
				System.out.println("This is Windows");
			} else if (isMac()) {
				extension = ".sh";
				MAC_HOME_DIRECTORY = System.getProperty("user.home");
				automationPath = "bash " + MAC_HOME_DIRECTORY;
				createEnvironmentFile(MAC_HOME_DIRECTORY);
				ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
				if(envVariables.size() >= 3) {
				automationPath = envVariables.get(0);
				nodeJSPath = envVariables.get(1);
				appiumServerJSPath = envVariables.get(2);
				environMentVariable = automationPath;
				automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
				System.out.println("This is for Mac");
				addBash = "bash ";
				}
			} else if (isUnix() || isSolaris()) {
			
				automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
				System.out.println("This is for unix/solaris");
			} else {
				System.out.println("Your OS is not support!!");
			}
			
			if (name != null && !name.isEmpty()) {
				if(name.contains(".apk")) {
					toogleExtension = "*.apk";
					String path = File.separator + "mobile" + File.separator + "android" + File.separator + name;
					if (isWindows()) {
						batFile = "%AUTOMATION_MOBILE_DRIVE%"
								+ "\ncd %AUTOMATION_PACKAGE%"
								+ "\naapt dumb badging %AUTOMATION_PATH%" + path
								+ "\n";
						extension  = ".bat";
					} else if (isMac()) {
						 batFile = "source ~/.bash_profile\ncd $AUTOMATION_PACKAGE"
									+ "\naapt dumb badging $AUTOMATION_PATH" + path
									+ "\n";
					} else if (isUnix() || isSolaris()) {
						batFile = "cd $AUTOMATION_PACKAGE"
								+ "\naapt dumb badging $AUTOMATION_PATH" + path
								+ "\n";
						extension  = ".sh";
						environMentVariable = automationPath;
						automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
						//addBash = "bash ";
						
					}  else {
						System.out.println("Your OS is not support!!");
					}
					createBatFile("package", batFile, extension);
				} else {
					toogleExtension = "*.ipa";
					sendPackageName = bundleId; // Here for iOS :  package name field treat as bundleID in UI
					sendLauncherActivity = "";
				}
			} 
			
			sendVersion = version;
			filePath  = automationPath + File.separator + name;
			fileName  = name;
		
			String batFile = toogleExtension.contains(".apk") ? "adb devices" : "ios-deploy -c" ;
			if(isWindows()) {
			} else if(isMac()){
				batFile = "source ~/.bash_profile\n" + batFile;
			} else {
				batFile = "source ~/.bashrc\n" + batFile;
			}
			createBatFile(GET_DEVICE_UDID, batFile , extension);
			
			
			if(toogleExtension.contains(".apk")) { // Android
				executeCommand(GET_PACKAGE_LAUNCHER_NAME, "package",extension); // Here we get the Android UDID
				 deviceId = executeCommand(GET_DEVICE_UDID, "adb",extension);
			} else { //iOS
				try {
					deviceId = executeCommand(GET_DEVICE_UDID, "adb", extension); // Only for iOS : Here we get the iOS deviceName,platformName and UDID
					if (deviceId != null) {
						String[] splitDevices = deviceId.split("-");
						if (splitDevices != null && splitDevices.length == 3) {
							deviceName = splitDevices[0];// DeviceName
							platformName = "iOS"; 
							deviceId = splitDevices[2]; //UDID
							sendVersion = "";
							sendVersion = executeCommand("instruments", "INSTRUMENTS", extension); // Here we get the iOS version
							deviceVersion = sendVersion;
							if(version != null && !version.trim().isEmpty()) {
								String[] finalSliptOfVersion = version.split("\\.");
								if (finalSliptOfVersion.length >= 1) {
									deviceVersion = finalSliptOfVersion[0] + "." + finalSliptOfVersion[1];
								}
							
							}
							
						}
					}
				} catch (Exception e) {
					System.out.println();
				}
			}
			if(deviceId != null && !deviceId.isEmpty()) {
				if(toogleExtension.contains(".apk")) {
					String fileScript = "\nadb -s " + deviceId + " shell getprop ro.product.model"
							+ "\nadb -s " + deviceId + " shell getprop ro.build.version.release"
							+ "\n";
					if (isMac()) {
						 batFile = "source ~/.bash_profile" + fileScript;
						 System.out.println("This is for Mac");	
					} else {
					 batFile = fileScript;
					}
					createBatFile(GET_PRODUCT_DETAILS, batFile, extension);
					executeCommand(GET_PRODUCT_DETAILS, "product details", extension);
				}
			}
			executeCommand(KILL_ADB, "adb", extension);
			if (deviceId != null && !deviceId.isEmpty()) {
				sendUdid = deviceId;
			}
			desiredCaps();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		automationPath = envVariable;
	}

	/**
	 * Creating the file in User Home directory to get the environment variables
	 * This file returns AUTOMATION_PATH,NODE_PATH,APPIUM_JS_PATH and ANDROID_HOME
	 * @param path
	 */
	
	private void createEnvironmentFile(String path) {
		 try {
			 File fileExists = new File(path + File.separator + "environmentVariables.sh");
				boolean exists = fileExists.exists();
				if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
					"printenv AUTOMATION_PATH\n" + 
					"printenv NODE_PATH\n" + 
					"printenv APPIUM_JS_PATH\n" + 
					"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * File settings
	 * Setting the permissions to read, write and execute
	 * @param path
	 * @throws Exception
	 */
	private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}
    
	
	/** 
	 * used to execute the batch file / Shell script file 
	 * @ Launcher Activity
	 * @ Device UUID
	 * @ Package Name
	 * @ Device Version
	 * @ Device Model 
	 * it returns iOS platform version
	 * it returns deviceName platformName and deviceId for iOS
	 * it returns device UDID  for android
	 * it returns packageName and launcherActivity name for android
	 */
	
	private String executeCommand(String batFilePath, String executionName, String extension) {
			try {
				String line;
				String packageName = "";
				String launcherActivity = "";
				boolean listDevices = false;
				Process p = Runtime.getRuntime().exec(environMentVariable + File.separator + "mobile" + File.separator + "utilities" + File.separator + batFilePath + extension);
				BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
				BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				Boolean isProductModel =  null;
				String isMacProduct = null;
				while ((line = bri.readLine()) != null) {

				if (executionName.equalsIgnoreCase("INSTRUMENTS")) { // it returns iOS platform version
					System.out.println(deviceId.replaceAll(" ", ""));
					if (line.contains(deviceId.replaceAll(" ", ""))) {
						String[] version = line.split("\\(");
						version = version[1].split("\\)");
						String finalVersion = "12";
						if(version != null && version.length > 0 && !version[0].trim().isEmpty()) {
							String[] finalSliptOfVersion = version[0].split("\\.");
							if (finalSliptOfVersion.length >= 1) {
								finalVersion = finalSliptOfVersion[0] + "." + finalSliptOfVersion[1];
							}
						}
						
						return finalVersion;
					}
				} else {
					if (executionName.equalsIgnoreCase("adb")) {
						if (toogleExtension.contains(".ipa")) {  // it returns deviceName platformName and deviceId for iOS
							if (line.contains("Found") && line.contains("iPhone 5s")) {
								String[] udidSplit = line.split("]");
								String[] osUdid = udidSplit[1].split("\\(");
								String deviceName = osUdid[1].split(",")[1].replaceAll("\\s+", "");
								String platformName = "";
								
								try {
									 platformName = osUdid[2].split(",")[2].replaceAll("\\s+", "");;
									} catch(Exception e) {
										platformName = "iPhone";
									}
								
								String finalString = osUdid[0].replaceAll("Found", "").replaceAll("\\s+", "");
								// String[] finalString = osUdid[1].split(",");
								return deviceName + "-" + platformName + "-" + finalString.replaceAll(" ", "");
							} else if (line.contains("Found")) {
								String[] udidSplit = line.split("]");
								String[] osUdid = udidSplit[1].split("\\(");
								String deviceName = osUdid[1].split(",")[1].replaceAll("\\s+", "");
								String platformName;
								try {
								 platformName = osUdid[1].split(",")[2].replaceAll("\\s+", "");
								} catch(Exception e) {
									try {
									platformName = osUdid[2].split(",")[1].replaceAll("\\s+", "");
									} catch (Exception e1) {
										platformName = "iPhone";
									}
								}
								String finalString = osUdid[0].replaceAll("Found", "").replaceAll("\\s+", "");
								return deviceName + "-" + platformName + "-" + finalString.replaceAll(" ", "");
							}
						} else { // it returns device UDID  for android
							if (listDevices) {
								String[] deviceUDID = line.split("device");
								System.out.println(deviceUDID[0]);
								listDevices = false;
								return deviceUDID[0]; // return Android DeviceId
							}
							if (line.contains("List of devices attached")) {
								listDevices = true;
							}
						}
					} else if (executionName.equalsIgnoreCase("package")) { // it returns packageName and launcherActivity name for android
						if (line != null && line.contains("launchable-activity:")) {
							launcherActivity = line.split("=")[1].split(" ")[0];
							System.out.println("Launcher Activity : " + launcherActivity);
							sendLauncherActivity = launcherActivity.replace("'", "");
							return (packageName + "$$" + launcherActivity).replaceAll("'", ""); // return PackageName and Launcher Activity Name
						}

						if (line != null && line.contains("package:")) {
							packageName = line.split("=")[1].split(" ")[0];
							sendPackageName = packageName.replace("'", "");
							System.out.println("packageName : " + packageName);
						}

					} else { // it returns model and version for android
						
						if (isMac()) {
							if (!line.contains("adb") && !line.contains("daemon")) {

								if (isMacProduct == null) {
									deviceName = line;
									isMacProduct = line;
								} else {
									try {
									if (line.trim().split("\\.").length > 0) {
										deviceVersion = line.trim().split("\\.")[0];
									}
									}catch (Exception e) {
										System.out.println("array index bound");
									}
								}
							}

						} else {
							if (line != null && !line.isEmpty()) {
								if (line.contains("ro.product.model")) {
									isProductModel = true;
									continue;
								} else if (line.contains("ro.build.version")) {
									isProductModel = false;
									continue;
								}

								if (isProductModel != null) {
									if (isProductModel) {
										deviceName = line;
									} else {
										if (line.trim().split("\\.").length > 0) {
											deviceVersion = line.trim().split("\\.")[0];
										}
									}
								}
							}
						}
						
					
					}
				}
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

			} catch (Exception err) {
				err.printStackTrace();
			}
			return "";
	}
	
	/**
	 * Used to write the file to Framework ConfigFiles(DeviceCapabilities.properties) 
	 * @throws Exception
	 */
	private String platformName = "";
	private void desiredCaps() throws Exception {
		if (fileName.contains(".apk")) {
			if(!fileName.equals("mobiletest.apk")) {
			if (sendPackageName == null || sendPackageName.isEmpty() || sendLauncherActivity == null
					|| sendLauncherActivity.isEmpty() || sendUdid == null || sendUdid.isEmpty()) {
				return;
			}
			}
			platformName = "ANDROID";
			deviceName = deviceName.trim().isEmpty() ? "ANDROID" : deviceName;
		} else {
			if (deviceName.trim().isEmpty()) {
				deviceName = deviceName.trim().isEmpty() ? "iPhone" : deviceName;
			}
			if (platformName.trim().isEmpty()) {
				platformName = platformName.trim().isEmpty() ? "iOS" : platformName;
			}
		}
		String createFile = "";
		
		if(fileName.equalsIgnoreCase("mobiletest.apk")) {
			
		createFile = "deviceName = " + deviceName + "\r\n" + 
						"platformName = " + platformName + "\r\n" + 
						"platformVersion = " + deviceVersion + "\r\n" + 
						"fullreset = true\r\n" + 
						"udid = "+ (sendUdid == null ? "<!-- Enter UDID -->" : sendUdid.trim()) + "\r\n" + 
						"newCommandTimeout = 60*5\r\n" + 
						"nodePath = " + nodeJSPath + "\r\n" + 
						"appiumJSPATH = " + appiumServerJSPath + "\r\n" + "";
			
		} else {
		 createFile = 
				"deviceName = " + deviceName + "\r\n" + 
				"platformName = " + platformName + "\r\n" + 
				"platformVersion = " + deviceVersion + "\r\n" + 
				"appPackage = " + sendPackageName + "\r\n" + 
				"fullreset = true\r\n" + 
				"udid = "+ (sendUdid == null ? "<!-- Enter UDID -->" : sendUdid.trim()) + "\r\n" + 
				"newCommandTimeout = 60*5\r\n" + 
				"appActivity = " + sendLauncherActivity + "\r\n" + 
				"app = " + fileName  + "\r\n" + 
				"nodePath = " + nodeJSPath + "\r\n" + 
				"appiumJSPATH = " + appiumServerJSPath + "\r\n" + "";
		}
		
		try {
			writeFile(createFile, "DeviceCapabilities", environMentVariable + File.separator + "ConfigFiles", ".properties");
			String file = "executionPlatform = " + "local" + "\r\n";
			writeFile(file, "mobileconfig", environMentVariable + File.separator + "ConfigFiles", ".properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	private void createBatFile(String batFileName, String data, String extension) throws Exception {
		writeFile(data, batFileName , environMentVariable + File.separator + "mobile" + File.separator + "utilities", extension);
	}
	
	/* Creating the file */
	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator + className + fileExtension);
		} catch (Exception e) {
			System.out.println("File not created");
		} finally {
			out.close();
		}
	}
	
	/* Find the Client OS */
	
	private static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}
	private static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
	
	/**
	 * Getting the environment variables from environmentVariables.sh
	 */
	
	private ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + automationPath + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}
	
	
	public void createSauceDesiredCaps(String envVariable, String projectName, String userName, String accessKey, String testObjApiKey, String devices, String url, String executionPlatform) throws Exception {
	
		environMentVariable = envVariable;
		
		
		String createFile = "projectName = " + projectName + "\r\n" + 
				"userName = " + userName + "\r\n" + 
				"accessKey = " + accessKey + "\r\n" + 
				"testObjApiKey = " + testObjApiKey + "\r\n" + 
				"app = " + url + "\r\n" + 
				"devices = " + devices + "\r\n" +
				"executionPlatform = " + executionPlatform + "\r\n";
		try {
			writeFile(createFile, "ServerDeviceCapabilities", environMentVariable + File.separator + "ConfigFiles", ".properties");
			String file = "executionPlatform = " + executionPlatform + "\r\n";
			writeFile(file, "mobileconfig", environMentVariable + File.separator + "ConfigFiles", ".properties");
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	
}
