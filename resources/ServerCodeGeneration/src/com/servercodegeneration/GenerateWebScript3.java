package com.servercodegeneration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringEscapeUtils;

import com.dto.DatasetsOfTestcase;
import com.dto.Element;
import com.dto.PrimaryInfo;
import com.dto.Screen;
import com.dto.WebScript;
import com.google.gson.Gson;
import com.utils.Utils;


/**
 * This class used to generating the Web Testcase's and Dataset's 
 * @TESTNG bat/sh file creation
 * @Tescase File Creation
 * @Datasets Creation
 * 
 */

public class GenerateWebScript3 {

	private String webPageClasses;
	private String webTestClasses;
	private PrimaryInfo primaryInfo;
	private String screenObjects = "";
	private String pageClassPath = "";
	private String projectName;
	private String browsers;

	@SuppressWarnings("unused")
	public void generateCode(boolean testSet, WebScript output, String environmentVariable) throws Exception {
		boolean isDriverInIframe = false;
		boolean isDriverInFrame = false;
		String previousIframeName = "";
		String previousFrameName = "";
		screenObjects = "";
		primaryInfo =  output.getPrimaryInfo();
		String testcaseClassStart = "";
		String chromeDriverPath = "chromeDriverPath", URL, screenObjs = "", screenImports = "";
		String fieldType = "", fieldName = "", fieldValue = "", fieldsInfo = "", screenName = "",secondaryFieldType= "",
				propertiesInfo = "\n\t", xmlFile = "";
		URL = primaryInfo.getProjectUrl();
		String testCaseName = Utils.removeUnWantedChars(toTitleCase(primaryInfo.getTestcaseName()));

		projectName = Utils.removeUnWantedChars(primaryInfo.getProjectName()).replaceAll(" ", "").toLowerCase();
		int projectId = primaryInfo.getProjectId();
		String reportsPath = "reportsPath";
		boolean executeAction = primaryInfo.getIsExecute();
		boolean generateAction = primaryInfo.getIsGenerate();
		boolean testcaseOverwrite = primaryInfo.isTestcase_overwrite();
		browsers = primaryInfo.getBrowserType();
		String baseURL = primaryInfo.getProjectUrl();
		long timeStamp = primaryInfo.getExecutedTimestamp();
		String moduleFolder = Utils.removeUnWantedChars(primaryInfo.getModuleName().toLowerCase());
		String subModule = primaryInfo.getSubModuleName() == null ? "" : primaryInfo.getSubModuleName();
		String subModuleFolder = Utils.removeUnWantedChars(subModule.toLowerCase());
		String testSetName = primaryInfo.getTestsetName() == null ? "" : primaryInfo.getTestsetName();
		String testsetName = Utils.removeUnWantedChars(testSetName);
		String moduleDescription = primaryInfo.getModuleDescription();
		String mobileWebBrowser = Utils.removeUnWantedChars(toTitleCase(primaryInfo.getTestcaseName()));
		String mobileType = primaryInfo.getMobileType();
		propertiesInfo += "PrimaryInfo = " + StringEscapeUtils.escapeJava(new Gson().toJson(primaryInfo)) +"\n\t";

		String modulename = moduleFolder + ".";
		if (!subModuleFolder.equalsIgnoreCase("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			modulename = modulename + subModuleFolder + ".";
		}
		String webFolder = "";
		if(primaryInfo.getMobilePlatform().equalsIgnoreCase("Android") || primaryInfo.getMobilePlatform().equalsIgnoreCase("iOS")) {
			propertiesInfo += "mobilePlatform = " +  primaryInfo.getMobilePlatform() +"\n\t";
			propertiesInfo += "type = " +  primaryInfo.getMobileType() +"\n\t";
			propertiesInfo += "executionEnvironment = " +  primaryInfo.getExecutionEnvironment() +"\n\t";
			webPageClasses = "mobile." + projectName + "." + modulename + "webpageclasses";
			webTestClasses = "mobile." + projectName + "." + modulename + "webtestclasses";
			webFolder = environmentVariable + File.separator + "src"+ File.separator + "test"+ File.separator + "java"+ File.separator + "mobile";
		} else {
			webPageClasses = "web." + projectName + "." + modulename + "webpageclasses";
			webTestClasses = "web." + projectName + "." + modulename + "webtestclasses";
			webFolder = environmentVariable + File.separator + "src"+ File.separator + "test"+ File.separator + "java"+ File.separator + "web";
		}



		createDirectory(webFolder);

		String projectFolder = webFolder + File.separator +  projectName;
		createDirectory(projectFolder);

		String packageNameFolder = projectFolder + File.separator +  moduleFolder;
		// create module
		createDirectory(packageNameFolder);

		// create sub module
		if (!subModuleFolder.equalsIgnoreCase("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			packageNameFolder = packageNameFolder + File.separator +  subModuleFolder;
			createDirectory(packageNameFolder);
		}

		String testClassPath = packageNameFolder + File.separator + "webtestclasses";
		pageClassPath = packageNameFolder + File.separator +  "webpageclasses";

		/* Creating directories if not exists */
		createDirectory(String.valueOf(testClassPath));

		createDirectory(String.valueOf(pageClassPath));

		propertiesInfo += "URL = " + URL + "\n\t";
		if(!testSet) {
			String[] browsersList = browsers.split(",");
			String finalSet = "";
			for (int i = 0; i < browsersList.length; i++) {
				String browserName = browsersList[i];
				finalSet = finalSet + "	\n\t<test name=\"" + browserName + "test\">\r\n" + 
						"  	<parameter name=\"browser\" value=\"" + browserName + "\"/>\r\n" 
						+ "\t\t\t<classes>"
						+ "\n\t\t\t\t<class name=\"" + webTestClasses +"."
						+ testCaseName + "\" />"
						+ "\n\t\t\t\t<class name=\"com.fileupload.ReportUpload\" />"
						+ "\n\t\t\t</classes>\n"
						+ "	</test>\r\n";
			}
			xmlFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
					+ "<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >"
					+ "\n<suite name=\"Main Test Suite\" parallel=\"tests\">"
					+ finalSet
					+ "</suite>";

			writeFile(xmlFile, "testng", environmentVariable, ".xml");
		}

		File directory = new File(String.valueOf(testClassPath));
		if (!directory.exists()) {
			directory.mkdir();
		}

		int screensCount = 0;	
		List<DatasetsOfTestcase> datasets = output.getDatasetsOfTestcase();
		int size = datasets.size();
		String names = "";
		for(int i = 0; i < size; i++) {
			names = names + (names.isEmpty() ? "" : "\",\"")  + datasets.get(i).getDatasetName();
		}
		//String datasetNames = "\n\tString[] datasetNames = new String[] {\"" + names + "\"};";
		System.out.println(names);
		boolean isScreensCreated =  false;
		if(!testSetName.isEmpty()) {
			propertiesInfo += "testSetName = " + testSetName +"\n\t";
		}
		propertiesInfo += "datasetsLength = " + size +"\n\t";
		/* This loop for generating config file and Screens Creating (GenerateSeleniumScreenObjectScript(screenName, fieldsInfo)) */



		String screenPackages = "package " + webPageClasses + ";";

		String testcasePackages = "package " + webTestClasses + ";\n"
				+ "import org.openqa.selenium.WebDriver;\n"
				+ "import org.testng.annotations.BeforeTest;\n"
				+ "import org.testng.annotations.AfterTest;\n"
				+ "import org.testng.annotations.Parameters;\n"
				+ "import org.testng.annotations.Test;\n"
				+ "import com.utilities.BaseClass;\n"
				+ "import com.utilities.ConfigFilesUtility;\n"
				+ "import com.utilities.QFUtilities;";

		//Testcase loop
		for(int i=0; i<datasets.size(); i++) {

			List<Screen> screens = datasets.get(i).getScreens();
			String test =  "";
			if (i == 1) {
				isScreensCreated = true;
			}

			//Screens

			screensCount = screensCount + 1;
			propertiesInfo += "dataset" + screensCount + " = " + datasets.get(i).getDatasetName() + "\n\t";
			int datasetScreencount = 0;
			//Screens loop
			for(int j = 0; j < screens.size(); j++) {



				datasetScreencount = datasetScreencount + 1;
				screenName = screens.get(j).getName();
				List<Element> elements = screens.get(j).getScreenElements();
				//Screen Elements Loop
				String screen = screenPackages + "\n\npublic class " + toTitleCase(screenName) + " { \n\n";
				for(int k = 0; k < elements.size(); k++) {


					String selectedXpath = elements.get(k).getSelectedXpath();
					String selectedElementfieldName = elements.get(k).getFieldName();

					Element element = elements.get(k);


					screen = screen + "\tpublic String _" + convertToFieldName(selectedElementfieldName) + " = \"" + selectedXpath + "\";" + "\n";	

					String elementFieldType = elements.get(k).getFieldType();
					String secondaryElementFieldType = elements.get(k).getSecondaryFieldType();
					if(secondaryElementFieldType == null) 
						secondaryElementFieldType = "";
					String elementFieldName = elements.get(k).getFieldName();
					String elementFieldValue = elements.get(k).getFieldValue();
					String elementDropDownValue = elements.get(k).getDropdownValue();

					if(elementDropDownValue == null) 
						elementDropDownValue = "";

					String validateData = "";
					try {
						if(element.getIsSelected()) {
							validateData = StringEscapeUtils.escapeJava(element.getValidateText());
							propertiesInfo += convertToFieldName(elementFieldName) + datasetScreencount + screensCount + " = " + StringEscapeUtils.escapeJava(validateData) + "\n\t";
						} 

						if(element.isDisplayed()) {
							String data = StringEscapeUtils.escapeJava(element.getFieldName());
							propertiesInfo += convertToFieldName(elementFieldName) + datasetScreencount + screensCount + " = " + data + "\n\t";
						} 

						if(element.isClick()) {
							//String data = StringEscapeUtils.escapeJava(element.getFieldName());
							//propertiesInfo += convertToFieldName(elementFieldName) + datasetScreencount + screensCount + "click = " + element.isClick() + "\n\t";
						} else if(!elementFieldType.equalsIgnoreCase("InputText")) {
							propertiesInfo += convertToFieldName(elementFieldName) + datasetScreencount + screensCount + "click = " + element.isClick() + "\n\t";
						}

						if(element.isCustomCode()) {
							String data = StringEscapeUtils.escapeJava(element.getFieldName());
							propertiesInfo += convertToFieldName(elementFieldName) + datasetScreencount + screensCount + " = " + data + "\n\t";
						} 


					} catch (Exception e) {
						validateData = elementFieldName;
					}

					if (true && !propertiesInfo.contains("random_number"  + screensCount)) {
						propertiesInfo += "random_number"  + screensCount + " = " + generateRandomIntRange(100, 9999) + "\n\t";
					}  

					if (elementFieldType.equalsIgnoreCase("InputText")) {
						propertiesInfo += convertToFieldName(elementFieldName) + datasetScreencount + screensCount + "input = " + StringEscapeUtils.escapeJava(elementFieldValue) + "\n\t";
					}  

					if (elementDropDownValue != null && !elementDropDownValue.isEmpty()) {
						propertiesInfo += convertToFieldName(elementFieldName)  + "Option" + datasetScreencount + screensCount +  " = " + StringEscapeUtils.escapeJava(elementDropDownValue) + "\n\t";
					}

				}

				if(!isScreensCreated) {
					String finalScreen = screen + "\n }"; 
					writeFile(finalScreen, toTitleCase(screenName), pageClassPath, ".java");
				}
			}




			// Testcase
			File classisExists = new File(String.valueOf(testClassPath + File.separator + toTitleCase(testCaseName) + ".java"));
			if (!classisExists.exists() || testcaseOverwrite) {

				testcaseClassStart = "\n\npublic class " + toTitleCase(testCaseName) + " extends BaseClass { \n\n"
						+ " \tQFUtilities qf;\n"
						+ "	private ConfigFilesUtility configFileObj;\n"
						+ "	public boolean isElementDispalyed = false;\n"
						+ " \tprivate String browserName = \"chrome\";\n"
						+ "	private WebDriver driver;\n"
						+ "\n"
						+ "\n"
						+ "	public " + toTitleCase(testCaseName) + "() throws Exception {\n"
						+ "		configFileObj = new ConfigFilesUtility();\n"
						+ "		qf = new QFUtilities();\n"
						+ "		configFileObj.loadPropertyFile(\"" + toTitleCase(testCaseName).toLowerCase() +".properties\");\n"
						+ "	}\n"
						+ "\n"
						+ "	@BeforeTest\n"
						+ "	@Parameters(\"browser\")\n"
						+ "	public void launchBrowser(String browser) throws Exception {\n"
						+ "		browserName = browser;\n"
						+ "	}\n\n"
						+ "	private void setup() {\n"
						+ "		driver = launchBrowser(browserName, configFileObj);\n"
						+ "	}"
						+ "\n";

				boolean isIframe = false;
				boolean isWindowHandle = false;

				String datasetsLoop = "";
				String flushReport = "";
				for(int m = 0; m < screens.size(); m++) {
					screenName = screens.get(m).getName();
					List<Element> elements = screens.get(m).getScreenElements();
					//Screen Elements Loop

					test = test + "\n\tprivate void " + convertToFieldName(screenName) + "Test(int i) throws Exception {\n\n";

					String screenObj = convertToFieldName(screenName) + "Obj";
					String scObj  = "\t\t" + toTitleCase(screenName)  + " " + screenObj + " = new " + toTitleCase(screenName) + "();\n";
					test = test + scObj +  "\t\ttry { \n";
					if(isIframe) {
						//isIframe = false;
						// test = test + "\t\t\tdriver.switchTo().defaultContent();\n";
					}
					
					if(!isScreensCreated) 
						screenObjects =  screenObjects + "\t\t\tif(isElementDispalyed) {"  + convertToFieldName(screenName) + "Test(datasets);}\n";
					

					if(!testcasePackages.contains(webPageClasses)) {
						testcasePackages = testcasePackages + "\nimport " + webPageClasses + ".*;";
					}


					for(int k = 0; k < elements.size(); k++) {


						String actualFieldName = elements.get(k).getFieldName();
						//String selectedXpath = elements.get(k).getSelectedXpath();

						String elementFieldType = elements.get(k).getFieldType();
						String tagName = elements.get(k).getTagFieldName();
						secondaryFieldType = elements.get(k).getSecondaryFieldType();
						if(secondaryFieldType == null) {
							secondaryFieldType = "";
						}
						boolean clickAction = elements.get(k).isClick();
						boolean iframe = elements.get(k).getIsIframe() || elements.get(k).getIsFrame();
						boolean validateText = elements.get(k).getIsSelected();
						boolean isSkip = elements.get(k).isIs_optionalElement();
						String selectedElementfieldName = convertToFieldName(actualFieldName);
						boolean isRandomNumber = elements.get(k).isRandomNumber();
						boolean isEnter = elements.get(k).isEnter();



						try {

							if(actualFieldName.contains("${custommethod")) {
								elements.get(k).setCustomCode(true);
								//selectedElementfieldName = actualFieldName.split("\\$\\{custommethod")[0];
							} 

							if(actualFieldName.contains("${displayed}")) {
								elements.get(k).setDisplayed(true);
								//selectedElementfieldName = actualFieldName.split("\\$\\{displayed")[0];
							} 

							if(actualFieldName.contains("${validate-")) {
								elements.get(k).setIsSelected(true);
								//selectedElementfieldName = actualFieldName.split("\\$\\{validate")[0];
								elements.get(k).setValidateText(actualFieldName.split("\\$\\{validate")[0]);
							}


						} catch (Exception e) {

						}

						if(actualFieldName.contains("$random")) {
							elements.get(k).setRandomNumber(true);
						}

						if(actualFieldName.contains("$Enter")) {
							elements.get(k).setEnter(true);
						}



						boolean customMethod = false;

						String position = (m+1) + "\" "+" + i " ;

						String validateOpt = "validatedText" + k;
						try {
							if(elements.get(k).getIsSelected()) {
								validateText =  true;
							}
						} catch (Exception e) {

						}

						boolean displayed = false;

						customMethod = elements.get(k).isCustomCode();

						displayed = elements.get(k).isDisplayed();



						selectedElementfieldName = convertToFieldName(actualFieldName);
						String validateData = "configFileObj.getProperty(\"" + selectedElementfieldName + position + ")";

						if(validateData == null) 
							validateData = "";

						String randomText = "";
						if(isRandomNumber && validateText) {
							randomText = "+ configFileObj.getProperty(\"random_number\" + i)";

							validateData = validateData + randomText;
						}

						// Window Handle
						if(secondaryFieldType != null && secondaryFieldType.equalsIgnoreCase("Window Switch")) {
							isWindowHandle = true;
							test = test + "\t\t\twindowHandle(driver);\n\t\t\t"; 

						} else if(isWindowHandle && (secondaryFieldType != null && !secondaryFieldType.equalsIgnoreCase("Window Switch"))) {
							isWindowHandle = false;
							test = test + "\t\t\tqf.switchToParentWindow(driver);\n\t\t\t";
						}

						//Iframe
						if(elements.get(k).isElementWait()) {
							test = test + "\t\t\tqf.wait(driver, " + screenObj + "._" + selectedElementfieldName + ");\n";	
						}

						if(iframe) {
							isIframe = true;
							test = test + "\t\t\tqf.switchToIframe(driver, " + screenObj + "._" + selectedElementfieldName + ");\n"; 

						} 

						if(!iframe && isIframe) {
							test = test + "\t\t\tdriver.switchTo().defaultContent();\n"; 
							isIframe = false;

						}


						// Optional element
						if(isSkip) {
							test = test + "\t\t\tif(qf.isElementDisplayed(driver, " + screenObj + "._" + selectedElementfieldName + ")) { \n";
						}

						if(customMethod) {
							if(!testcasePackages.contains("import com.utilities.QFCustomizedCode;")) {
								testcasePackages = testcasePackages + "import com.utilities.QFCustomizedCode;\n";
							}
							test = test + "\t\t\tString " + validateOpt + " = QFCustomizedCode.customCode(driver," + screenObj + "._" + selectedElementfieldName + ", " + validateData + ", i, \""+projectName+"\");\n";
							test = test + "\t\t\tprintInfoLogAndReport( " + validateOpt + ");\n";

							continue;
						}

						if(displayed) {

							test = test + "\t\t\tString " + validateOpt + " = qf.isElementDisplayed(driver, " + screenObj + "._" + selectedElementfieldName + ", " + validateData +  "," + clickAction + ");\n"; 

							test = test + "\t\t\tprintValidateLogAndReport( " + validateOpt + ");\n";

							continue;
						}

						if(elements.get(k).isScrollUp()) {
							test = test + "\t\t\tqf.scrollUp(driver, " + screenObj + "._" + selectedElementfieldName+");\n";	
						}

						if(elements.get(k).isScrollDown()) {
							test = test + "\t\t\tqf.scrollDown(driver, " + screenObj + "._" + selectedElementfieldName+");\n";	
						}

						if(secondaryFieldType != null && secondaryFieldType.equalsIgnoreCase("alert")) {
							test = test + "\t\t\tqf.alertHandle(driver, true);\n";
						} else if (secondaryFieldType.equalsIgnoreCase("MouseHover")) {

							test = test + "\t\t\tqf.mouseHover(driver, " + screenObj + "._" + selectedElementfieldName + "," + clickAction +");";

						} else if(!elementFieldType.equalsIgnoreCase("inputtext") && tagName.equalsIgnoreCase("select")) { //Select

							{
								test = test + "\t\t\tString selectedText" + (k) +" = qf.selectOption(driver, " + screenObj + "._" + selectedElementfieldName + ", configFileObj.getProperty(\"" + selectedElementfieldName + "Option" + position + "));\n";

								test = test + "\t\t\tprintSuccessLogAndReport( \"Selected Option is: \" + selectedText" + k + ");\n";

							}

							if(validateText) {
								test = test + "\t\t\tString " + validateOpt + " = qf.validateSelectOption(driver, " + screenObj + "._" + selectedElementfieldName + ", " + validateData + ");\n";

								test = test + "\t\t\tprintValidateLogAndReport( " + validateOpt + ");\n";
							} 


						}  else if(elementFieldType.equalsIgnoreCase("inputtext")) { //Input

							String randomText1 = "";
							if(isRandomNumber) {
								randomText1 = "+ configFileObj.getProperty(\"random_number\" + i)";
							}

							if(validateText) {
								test = test + "\t\t\tString " + validateOpt + " = qf.validateInput(driver, " + screenObj + "._" + selectedElementfieldName + ", " + validateData  + ");\n"; 

								test = test + "\t\t\tprintValidateLogAndReport( " + validateOpt + ");\n";
							} else {

								test = test + "\t\t\tqf.sendKeys(driver, " + screenObj + "._" + selectedElementfieldName + ", configFileObj.getProperty(\"" + selectedElementfieldName + position + "+ \"input\")" + randomText1 + ");\n"; 
								test = test + "\t\t\tprintSuccessLogAndReport( \"Entered " + actualFieldName + " input: \" + configFileObj.getProperty(\"" + selectedElementfieldName + position + "+ \"input\")" + randomText1 + ");\n";
							}

						} else if (elementFieldType.equalsIgnoreCase("button") || elementFieldType.equalsIgnoreCase("link")) {

							if(validateText && elements.get(k).getTagFieldName().equalsIgnoreCase("input")) {
								test = test + "\t\t\tString " + validateOpt + " = qf.validateInputButton(driver, " + screenObj + "._" + selectedElementfieldName + ", " + validateData + ", configFileObj.getBooleanProperty(\"" + selectedElementfieldName + position + "+ \"click\"));\n"; 
								test = test + "\t\t\tprintValidateLogAndReport( " + validateOpt + ");\n";
							} else if(validateText) {
								test = test + "\t\t\tString " + validateOpt + " = qf.validateAndClickAction(driver, " + screenObj + "._" + selectedElementfieldName + ", " + validateData+ ", configFileObj.getBooleanProperty(\"" + selectedElementfieldName + position + "+ \"click\"));\n"; 
								test = test + "\t\t\tprintValidateLogAndReport( " + validateOpt + ");\n";
							} else {
								test = test + "\t\t\tqf.clickAction(driver, " + screenObj + "._" + selectedElementfieldName + ", configFileObj.getBooleanProperty(\"" + selectedElementfieldName + position + "+ \"click\"));\n"; 
								test = test + "\t\t\tif(configFileObj.getBooleanProperty(\"" + selectedElementfieldName + position + "+ \"click\"))\n";
								test = test + "\t\t\tprintSuccessLogAndReport( \"Clicked on " + actualFieldName + "\");\n";
							}


						} else if (elementFieldType.equalsIgnoreCase("label")) {

							if(validateText) {
								test = test + "\t\t\tString " + validateOpt + " = qf.validateAndClickAction(driver, " + screenObj + "._" + selectedElementfieldName + ", " + validateData + ", " + clickAction + ");\n"; 
								test = test + "\t\t\tprintValidateLogAndReport( " + validateOpt + ");\n";
							} else if(clickAction) {
								test = test + "\t\t\tqf.clickAction(driver, " + screenObj + "._" + selectedElementfieldName + ");\n"; 
							}

							if(clickAction) {
								test = test + "\t\t\tprintSuccessLogAndReport( \"Clicked on " + actualFieldName + "\");\n";
							}
						} 

						if(isSkip) {
							test = test + "\t\t\t}\n";
						}

						if(isEnter) {
							test = test + "\t\t\tqf.enter(driver, " + screenObj + "._" + selectedElementfieldName + ");\n";	
						}

						if(actualFieldName.contains("${tab")) {
							test = test + "\t\t\tqf.tab(driver, " + screenObj + "._" + selectedElementfieldName + ");\n";	
						}

					}

					test = test + "\n\t\t} catch (Exception e) {\n"
							+ "\t\t\tisElementDispalyed = false;\n"
							+ "\t\t\tprintFailureLogAndReport(   \"Element is not found\" + e.getLocalizedMessage());\n"
							+ "\t\t}"
							+ " finally {\n"
							+ "\t\t\tcleanupObject("+ screenObj +");\n"
							+ "\t\t}\n"
							+ "\t}\n"; 
				}

				if(primaryInfo.getMobilePlatform().equalsIgnoreCase("Android") || primaryInfo.getMobilePlatform().equalsIgnoreCase("iOS")) {
					datasetsLoop = "	 @Test(dataProvider = \"capabilities\")\r\n" + 
							"	 public void setUP(String deviceName,\r\n" + 
							"	                   String platformName,\r\n" + 
							"	                   String platformVersion,\r\n" + 
							"	                   String appiumVersion,\r\n" + 
							"	                   String deviceOrientation,\r\n" + 
							"	               	   String projectName,\r\n" + 
							"	                   String testObjId,\r\n"+
							"	                   String app\r\n"+
							"\t\t) throws Exception {\r\n" + 
							"		for(int datasets = 1; datasets <= configFileObj.getIntProperty(\"datasetsLength\"); datasets++) {"
							+ 		"\r\n\t\t\tisElementDispalyed = true;"+
							"			\r\n\t\t\tdriver = this.createDriver(deviceName,platformName, platformVersion, appiumVersion, deviceOrientation, projectName, testObjId, this.getClass().getSimpleName(), app,configFileObj );" +
							"			\r\n\t\t\tsetTestcaseName(browserName,\"" + testCaseName + " - \" + configFileObj.getProperty(\"dataset\" + (datasets)));"
							+ "\n" + screenObjects + "\n" + 
							"		}\r" + 
							"	}\n";
					flushReport = "\n}";
				} else {
					datasetsLoop = "\n\t@Test\r\n" + 
							"	public void screensTest() throws Exception {\r\n" + 
							"		for(int datasets = 1; datasets <= configFileObj.getIntProperty(\"datasetsLength\"); datasets++) {"
							+ 		"\r\n\t\t\tisElementDispalyed = true;" +
							"		 \r\n\t\t\tsetup();" +
							"			\r\n\t\t\tsetTestcaseName(browserName,\"" + testCaseName + " - \" + configFileObj.getProperty(\"dataset\" + (datasets)));"
							+ "\n" + screenObjects + "\t\t\ttearDown();\n" + 
							"		}\r" + 
							"	}\n";

					flushReport = "\n\t\n\tpublic void tearDown() throws Exception "
							+ "{"
							+ "\n\t\tdriver.quit();"
							+ "\n\t\tdriver=null;"
							+ "\n\t}"

							+ "\n\t\n\t@AfterTest\n"
							+ "\tprivate void cleanup() {\n"
							+ "		configFileObj =  null;\n"
							+ "		qf = null;\n"
							+ "		System.runFinalization();\n"
							+ "\t}\n"
							+ "\n}";
				}

				writeFile(testcasePackages + testcaseClassStart + test + datasetsLoop + flushReport  , toTitleCase(testCaseName), testClassPath, ".java");
			}
			writeFile(propertiesInfo, testCaseName.toLowerCase(), environmentVariable + File.separator + "ConfigFiles", ".properties"); // writing the config file

		}
	}


	private String convertToFieldName(String str) {
		StringBuffer sb = new StringBuffer();
		sb.append(Character.toLowerCase(str.charAt(0))).append(str.substring(1)).append("");
		return sb.toString().replaceAll(" ", "").replaceAll("[^a-zA-Z0-9]", "").trim();
	}


	private String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].trim().equalsIgnoreCase(""))
				continue;
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append("");
		}
		String name = sb.toString().replace("$", "").replaceAll("^\\d+", "");
		return name.trim();
	}

	private void writeFile(String testcaseClassStart, String className, String filePath, String fileExtension)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(testcaseClassStart);
		} finally {
			out.close();
		}
	}

	private boolean createDirectory(String directory) {
		File fileDirectory = new File(directory);
		if (!fileDirectory.exists()) {
			fileDirectory.mkdir();
			return true;
		}
		return false;
	}


	public static int generateRandomIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}


}
