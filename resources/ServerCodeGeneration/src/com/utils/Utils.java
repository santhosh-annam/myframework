package com.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Utils {

	/*
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
			String val = value.replaceAll(" ", "");
			val = val.replaceAll("[^a-z_A-Z0-9]", "");
			return val;
		} catch (Exception e) {
			return value;
		}
	}

	public static void writeLog(String text) {
		try {
			String path = File.separator + "home" + File.separator + "prolifics" + File.separator + "QualityFusion"
					+ File.separator + "Logs" + File.separator + "log.txt";
			File logFile = new File(path);
			if (!logFile.exists()) {
				logFile.createNewFile();
				setPermissionsToFile(path);
			}

			BufferedWriter out = new BufferedWriter(new FileWriter(path, true));
			out.write("\n" + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) + " " + text);
			out.close();

		} catch (Exception e) {
			System.out.println("logerror");
		}
	}

	private static void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}

	

	public static void createReportPortalProperties(String environmentVariable,String projectName, String testName) {

		try {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
					String reportPortalPropertiesfilePath = environmentVariable + File.separator + "src"+ File.separator + "test" + File.separator + "resources" +File.separator + "reportportal.properties";
					
					//for dv
						/*
						 * String data = "rp.endpoint = http://183.82.106.91:8080\n" +
						 * "rp.uuid = 699ff82a-16e8-4912-a063-020eb11e177a\n" + "rp.launch = QA_" +
						 * projectName + "_" + testName + "\n" + "rp.project = " + projectName + "\n" +
						 * "rp.enable = true";
						 */
					
					//For qf
					String data = "rp.endpoint = http://10.11.12.241:8080\n" + 
							"rp.uuid = 54766d56-9e56-42ea-a07f-d811195c5653\n" + 
							"rp.launch = QA_" + projectName + "_" + testName + "\n" + 
							"rp.project = " + projectName +  "\n" +  
							"rp.enable = true";
					
						//writeFile(reportPortalPropertiesfilePath, data);
						//setPermissionsToFile(reportPortalPropertiesfilePath);
					} catch (Exception e) {
						e.printStackTrace();
					}	
					
				}
			}).start();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private static void writeFile(String filePath, String data)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"));
		try {
			out.write(data);
		} finally {
			out.close();
		}
	}
	
	public static void writeFile(String str, String fileName, String filePath, String fileExtension) {
		try {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator + fileName + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			Utils.writeLog("Writen files" + str);
			setPermissionsToFile(filePath + File.separator + fileName + fileExtension);
		} finally {
			out.close();
		}
		} catch (Exception e) {
			Utils.writeLog("Writen file not created " + filePath + "," + fileName);
		}
	}


	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	private static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	private static String OS = System.getProperty("os.name").toLowerCase();

	

}
