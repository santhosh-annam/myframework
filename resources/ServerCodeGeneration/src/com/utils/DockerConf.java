package com.utils;

import java.io.File;

public class DockerConf {

	public static void dockerFile(String branchName, String gitURL, String newTestcaseName, String newTestsetName, String testsetName, String environmentVariable, String dockerFileName, boolean isGitops) {

		String[] splitProjectName = gitURL.split("/");
		String dockerProjectName = splitProjectName[splitProjectName.length-1].replaceAll(".git", "");
		String dockerGitURL = gitURL;

		String dockerFileCreation = "FROM ubuntu\r\n" + 
				"MAINTAINER QE\r\n" + 
				" \r\n" + 
				"RUN apt-get clean\r\n" + 
				"RUN apt-get -y update\r\n" + 
				"RUN apt-get install -qq -y curl\r\n" + 
				"RUN apt-get install -y unzip\r\n" + 
				"RUN apt-get install -y git\r\n" + 
				"RUN apt-get install -y wget\r\n" + 
				"ARG DEBIAN_FRONTEND=noninteractive\r\n" + 
				"ENV TZ=Pacific\r\n" +
				"RUN apt-get install --reinstall -y software-properties-common\r\n" + 
				" \r\n" + 
				"\r\n" +
				"# Install OpenJDK-8\r\n" + 
				"RUN apt-get update && \\\r\n" + 
				"    apt-get install -y openjdk-8-jdk && \\\r\n" + 
				"    apt-get install -y ant && \\\r\n" + 
				"    apt-get clean;\r\n" + 
				"\r\n" + 
				"# Fix certificate issues\r\n" + 
				"RUN apt-get update && \\\r\n" + 
				"    apt-get install ca-certificates-java && \\\r\n" + 
				"    apt-get clean && \\\r\n" + 
				"    update-ca-certificates -f;\r\n" + 
				"\r\n" + 
				"# Setup JAVA_HOME -- useful for docker commandline\r\n" + 
				"ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/jre\r\n" + 
				"RUN export JAVA_HOME\r\n"+
				"# Define default command.\r\n" + 
				"CMD [\"bash\"]\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"ENV MAVEN_VERSION 3.5.4\r\n" + 
				"\r\n" + 
				"RUN mkdir -p /usr/share/maven \\\r\n" + 
				"  && curl -fsSL http://apache.osuosl.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz \\\r\n" + 
				"    | tar -xzC /usr/share/maven --strip-components=1 \\\r\n" + 
				"  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn\r\n" + 
				"\r\n" + 
				"ENV MAVEN_HOME /usr/share/maven\r\n" + 
				"\r\n" + 
				"VOLUME /root/.m2\r\n" + 
				"\r\n" + 
				"CMD [\"mvn\"] \r\n" + 
				"\r\n" + 
				"\r\n"
				+ "RUN apt-get update && \\\r\n" + 
				"  apt-get install -y --no-install-recommends \\\r\n" + 
				"  g++ \\\r\n" + 
				"  gcc \\\r\n" + 
				"  libc6-dev \\\r\n" + 
				"  libffi-dev \\\r\n" + 
				"  zlib1g-dev \\\r\n" + 
				"  libssl-dev \\\r\n" + 
				"  make\r\n" + 
				"#install Google-chrome and Google driver\r\n" + 
				"RUN apt-get -y update\r\n" + 
				"RUN apt-get install -y unzip xvfb libxi6 libgconf-2-4 \r\n" + 
				"RUN apt-get install -y gnupg2\r\n" + 
				"RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add\r\n" + 
				"RUN echo \"deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main\" >> /etc/apt/sources.list.d/google-chrome.list\r\n" + 
				"RUN apt-get -y update\r\n" + 
				"RUN apt-get -y install google-chrome-stable\r\n" + 
				"RUN google-chrome --version\r\n" + 
				"RUN wget https://chromedriver.storage.googleapis.com/91.0.4472.19/chromedriver_linux64.zip\r\n" + 
				"RUN unzip chromedriver_linux64.zip\r\n" + 
				"RUN mv -f chromedriver /usr/local/share/chromedriver\r\n" + 
				"RUN ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver\r\n" + 
				"RUN ln -s /usr/local/share/chromedriver /usr/bin/chromedriver\r\n" +
				"ARG GIT_VERSION=" + System.currentTimeMillis() + "\r\n" +
				"RUN cd $HOME && GIT_VERSION=${GIT_VERSION} git config --global http.sslverify false\r\n" +
				"RUN cd $HOME && GIT_VERSION=${GIT_VERSION} git  clone -b " + branchName +" "+ dockerGitURL + "\r\n" +
				"RUN cd $HOME" + File.separator + dockerProjectName + "&& GIT_VERSION=${GIT_VERSION}  mvn clean test -P" + (!testsetName.equals("") ? testsetName : "testng.xml") + "\r\n";



		if(isGitops) {
			Utils.writeLog("Docker Writing files started" + dockerFileCreation +","+ dockerFileName +","+environmentVariable + "/DockerFiles");
			Utils.writeFile(dockerFileCreation, dockerFileName , environmentVariable, "");
		} else {
			Utils.writeLog("Docker Writing files started" + dockerFileCreation +","+ dockerFileName +","+environmentVariable + "/DockerFiles");
			Utils.writeFile(dockerFileCreation, dockerFileName , environmentVariable+"/DockerFiles", "");
		}


	}

}
