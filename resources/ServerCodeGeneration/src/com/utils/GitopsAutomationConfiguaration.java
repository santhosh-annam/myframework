package com.utils;

public class GitopsAutomationConfiguaration {

	public static String setupGitOps(String goal, long millis, String gitopsName) {

		String gitopsYaml = "#" + millis + "\r\n"
				+ "variables:\r\n"
				+ "  MAVEN_OPTS: -Dmaven.repo.local=.m2/repository\r\n"
				+ "\r\n"
				+ "image: openjdk:8-jdk\r\n"
				+ "image: maven:latest\r\n"
				+ "\r\n"
				+ "stages:\r\n"
				+ "  - testing\r\n"
				+ "\r\n"
				+ "cache:\r\n"
				+ "  paths:\r\n"
				+ "    - .m2/repository\r\n"
				+ "    - target\r\n"
				+ "\r\n"
				+ gitopsName + ":\r\n"
				+ "  stage: testing\r\n"
				+ "  tags:\r\n"
				+ "    - shell\r\n"
				+ "  script:\r\n"
				+ "    - \""+goal+"\"\r\n"
				+ "";

		return gitopsYaml;

	}
	
	
	public static String setupGitOpsDocker(String command, long millis, String gitopsName) {

		String gitopsYaml = "#" + millis +"\r\n"
				+ "stages:\r\n"
				+ "  - testing\r\n"
				+ "\r\n"
				+ gitopsName + ":\r\n"
				+ "  stage: testing\r\n"
				+ "  tags:\r\n"
				+ "    - docker\r\n"
				+ "  script:\r\n"
				+ "    - \""+command+"\"\r\n"
				+ "";

		return gitopsYaml;

	}

}
