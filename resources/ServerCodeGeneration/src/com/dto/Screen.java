
package com.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Screen {

    @SerializedName("screen_id")
    @Expose
    private Integer screenId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_at")
    @Expose
    private Integer createdAt;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("module_id")
    @Expose
    private Integer moduleId;
    @SerializedName("web_page_id")
    @Expose
    private Integer webPageId;
    @SerializedName("is_default")
    @Expose
    private Boolean isDefault;
    @SerializedName("elements")
    @Expose
    private List<Element> screenElements = null;

    public Integer getScreenId() {
        return screenId;
    }

    public void setScreenId(Integer screenId) {
        this.screenId = screenId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getWebPageId() {
        return webPageId;
    }

    public void setWebPageId(Integer webPageId) {
        this.webPageId = webPageId;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public List<Element> getScreenElements() {
        return screenElements;
    }

    public void setScreenElements(List<Element> screenElements) {
        this.screenElements = screenElements;
    }

}
