package com.qf.mobile;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	
	public static void main(String[] data) throws JSONException, Exception {
	data = new String[]{"{\"mobile_type\":\"mobileWeb\",\"mobile_platform\":\"android\",\"web_page_name\":\"\",\"web_page_description\":\"\",\"diff_page_id\":0,\"jar_type\":3,\"file_name\":\"\",\"bundle_id\":\"\",\"module_id\":724,\"user_id\":7,\"web_page_url\":\"https://pep.prolifics.com/\",\"save_web_page_and_elements_url\":\"http://10.11.12.241:80/CreateWebPage\"}"};
	
	/*InputStream is = new FileInputStream(data[0]);
	@SuppressWarnings("resource")
	BufferedReader buf = new BufferedReader(new InputStreamReader(is));
	String line = buf.readLine();
	StringBuilder sb = new StringBuilder();
	while(line != null){ 
		sb.append(line).append("\n"); 
		line = buf.readLine(); 
	} 
	String finalData = sb.toString();*/

	String finalData = data[0];

		System.out.println(finalData);
    	JSONObject jsonObj = new JSONObject(finalData);
    	DesiredCapabilitiesController.JSON = finalData;
    	DesiredCapabilitiesController.JAR_TYPE = jsonObj.optInt("jar_type");
    	DesiredCapabilitiesController.DIFF_ID = jsonObj.optInt("diff_page_id");
    	DesiredCapabilitiesController.WEB_PAGE_NAME = jsonObj.optString("web_page_name");
    	DesiredCapabilitiesController.WEB_PAGE_DESC = jsonObj.optString("web_page_description");
    	DesiredCapabilitiesController.ELEMENTS_URL = jsonObj.getString("save_web_page_and_elements_url");
    	DesiredCapabilitiesController.BUNDLE_ID = jsonObj.optString("bundle_id");
    	DesiredCapabilitiesController.MOBILE_PLATFORM = jsonObj.optString("mobile_platform");
    	DesiredCapabilitiesController.URL = jsonObj.optString("web_page_url", "http://appiumpro.com/contact");
        launch(finalData); 
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		String fxmlFile = "/fxml/desire_capabilities_form.fxml";

		FXMLLoader loader = new FXMLLoader();
		Parent browser = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));

		DesiredCapabilitiesController controller = loader.getController();
		StackPane root = new StackPane();
		root.getChildren().add(browser);
		//FileInputStream input = new FileInputStream("resources/images/iconmonstr-home-6-48.png");
       
		Scene scene = new Scene(root, 300, 250, Color.WHITE);
		primaryStage.setTitle("Automation Framework");
		//primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/automaion_browser.png")));
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
            	try {
            		controller.closeApp("d");
            	} catch (Exception e) {
					e.printStackTrace();
				}
                Platform.exit();
                System.out.println("closed");
                System.exit(0);
            }
        });
		primaryStage.setMaximized(true);
	}
	 
}
