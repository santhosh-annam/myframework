package com.qf.mobile;

import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.SwingUtilities;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.html.HTMLIFrameElement;
import org.xml.sax.InputSource;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qf.dto.PageDetails;
import com.qf.dto.PageElements;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.github.bonigarcia.wdm.WebDriverManager;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class DesiredCapabilitiesController implements Initializable {
	
	public static final String Column1MapKey = "A";
	public static final String Column2MapKey = "B";
	public static final String Column3MapKey = "C";
	@FXML
	ImageView imageView;
	@FXML
	private TextField deviceNameField;	
	@FXML
	private TextField platformNameField;
	@FXML
	private TextField versionField;
	@FXML
	private TextField udidField;	
	@FXML
	private TextField packageNameField;
	@FXML
	private TextField launcherField;
	@FXML
	private Label headerLabel;
	@FXML
	private Button submitButton;
	
	@FXML
	private Button elementsButton;
	@FXML
	private Label errorLabel;
	
	@FXML
	private ToggleGroup androidOrIOS;
	@FXML
	private RadioButton androidRadioBtn;
	@FXML
	private RadioButton iosRadioBtn;
	@FXML
	private Label packageNameLabel;
	@FXML
	private Label launcherActivityLabel;
	@FXML
    private ImageView boximage;
	
	private String nodeJSPath = System.getenv("NODE_PATH");
	private String appiumServerJSPath = System.getenv("APPIUM_JS_PATH");
	private AppiumDriverLocalService appiumDriverService;
	@SuppressWarnings("rawtypes")
	private AppiumDriver driver;
	private String automationPath = System.getProperty("user.dir") + File.separator + "mobile" + File.separator + "android";
	//private static String sdkPath =  System.getenv("ANDROID_HOME");
	private final String GET_ADB_DEVICES = "devices";
	private final String KILL_ADB = "killadb";
	private final String KILL_NODE = "killnode";
	private final String GET_PRODUCT_DETAILS = "productDetails";
	public boolean isAppiumStarted;

    private final String OS = System.getProperty("os.name").toLowerCase();
	private String addBash = "";
	private String extension = "";
	private String toogleExtension = "*.apk";
	private String environMentVariables = System.getenv("AUTOMATION_PATH");
	
	public static String ELEMENTS_URL;
	public static int JAR_TYPE;
	public static String JSON;
	public static int DIFF_ID;
	public static String WEB_PAGE_NAME;
	public static String WEB_PAGE_DESC;
	public static String BUNDLE_ID;
	public static String MOBILE_PLATFORM;
	public static String URL;
	String selectPreferField2 = "";
	protected boolean isSavingElements;
	ArrayList<PageElements> pageElementsList;
	ArrayList<PageElements> pageElementsList1;
	
	
	@Override
	public void initialize(URL url, ResourceBundle bundle) {
		
		
		imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

		     @Override
		     public void handle(MouseEvent event) {
		        updateImage();
		        event.consume();
		     }
		});
	    
		//apkChooserField.setStyle("-fx-text-fill: black; -fx-base: #EAE1DF;");
		elementsButton.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
		//elementsButton.setVisible(false);
		Platform.runLater(() -> {	
			elementsButton.setText("Inspect");
			//elementsButton.setVisible(false);
			
		});
		submitButton.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
		versionField.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d*(\\.\\d*)?")) {
	            	versionField.setText(oldValue);
	            }
	        }
	    });
		
		 if(MOBILE_PLATFORM.equalsIgnoreCase("android")) {
			androidRadioBtn.setSelected(true);
			radioButtonsEnableOrDisable(false,true);
			toogleExtension = "*.apk";
			setText("ANDROID");
		}  else if(MOBILE_PLATFORM.equalsIgnoreCase("ios")) {
			iosRadioBtn.setSelected(true);
			radioButtonsEnableOrDisable(true,false);
			toogleExtension = "*.ipa";
			setText("iOS");
		} else {
			androidRadioBtn.setSelected(true);
			toogleExtension = "*.apk";
	    	deviceNameField.setText("Android");
			platformNameField.setText("Android");
		}
		androidOrIOS.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle,Toggle new_toggle) {
		          System.out.println(androidOrIOS.getSelectedToggle().toString());
		          String isAndroidOrIOS = androidOrIOS.getSelectedToggle().toString();
		          versionField.setText("");
		          udidField.setText("");
		          errorLabel.setVisible(false);
		          setText(isAndroidOrIOS);
		      }
		});
		
		Platform.runLater(new Runnable() {
			@Override
		    public void run() {
				if (isWindows()) {
					extension = ".bat";
					System.out.println("This is Windows");
				} else if (isMac()) {
					String MAC_HOME_DIRECTORY = System.getProperty("user.home");
					extension = ".sh";
					automationPath = "bash " + MAC_HOME_DIRECTORY;
					createEnvironmentFile(MAC_HOME_DIRECTORY);
					ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
					if(envVariables.size() >= 3) {
					automationPath = envVariables.get(0);
					environMentVariables = automationPath;
					nodeJSPath = envVariables.get(1);
					appiumServerJSPath = envVariables.get(2);
					automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
					System.out.println("This is for Mac");
					addBash = "bash ";
					}
				} else if (isUnix() || isSolaris()) {
					extension = ".sh";
					automationPath = System.getProperty("user.home");
					ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
					automationPath = envVariables.get(0);
				//	sdkPath = envVariables.get(3);
					environMentVariables = automationPath;
					automationPath = automationPath + File.separator + "mobile" + File.separator + "android";
					System.out.println("This is for unix/solaris");
				} else {
					System.out.println("Your OS is not support!!");
				}
				
		    	headerLabel.requestFocus();
		    	if (MOBILE_PLATFORM != null && !MOBILE_PLATFORM.isEmpty()) {
		    		
					try {
						commandScripts(extension);
					} catch (Exception e) {
						e.printStackTrace();
					}
					//submitButton.fire();
				}
		    }
		});	
	}
	
	/**
	 * Creating the file in User Home directory to get the environment variables
	 * @param path
	 */
	private void createEnvironmentFile(String path) {
		 try {
			 File fileExists = new File(path + File.separator + "environmentVariables.sh");
				boolean exists = fileExists.exists();
				if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
					"printenv AUTOMATION_PATH\n" + 
					"printenv NODE_PATH\n" + 
					"printenv APPIUM_JS_PATH\n" + 
					"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Setting the permissions to read, write and execute
	 * @param path
	 * @throws Exception
	 */
	private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			} 
		} else {
			File file = new File(path);
			if(file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}
	
	/**
	 * Setting the text based on platform
	 * @param isAndroidOrIOS 
	 */
   private void setText(String isAndroidOrIOS) {
		 if(isAndroidOrIOS.contains("iOS")) {
      	  toogleExtension = "*.ipa";
     
      	  platformNameField.setText("iOS");
      	  deviceNameField.setText("iPhone");
      	 // apkChooserField.setText("Choose IPA File");
      	//  versionField.setText("11.3");
        } else if(isAndroidOrIOS.contains("ANDROID")) {
      	 
      	  platformNameField.setText("Android");
      	  toogleExtension = "*.apk";
      	  deviceNameField.setText("Android");
      	  //apkChooserField.setText("Choose APK File");
      	  //versionField.setText("6");
        }
	}
	
   /**
    * Validating the fields 
    * After Validation is done launching the Application in mobile
    */
	String errorMsg = "";
	@FXML
	private void handleSubmitButtonAction(ActionEvent event) {
		errorMsg = "";
		String text = submitButton.getText().toString();
		errorLabel.setVisible(false);
		if(text.equalsIgnoreCase("Processing..")) {
			return;
		}
		String platformName = platformNameField.getText().toString().trim();
		 new Thread() {
             public void run() {
            	 if (text.equalsIgnoreCase("connect")) {
         			isAppiumStarted = false;
         			String deviceName = deviceNameField.getText().toString().trim();
         			
         			String version = versionField.getText().toString().trim();
         			String udid = udidField.getText().toString().trim();	
         			if (deviceName.isEmpty()) {
         				errorMsg = "Device Name";
         			} else if (platformName.isEmpty()) {
         				errorMsg = "Platform Name";
         			} else if (toogleExtension.contains("apk") && version.isEmpty()) {
         				errorMsg = "Platform Version";
         			} else if (udid.isEmpty()) {
         				errorMsg = "UDID";
         			}else {
         				 Platform.runLater(() -> {
                			 submitButton.setText("Processing..");
                			 elementsButton.setVisible(false);	
             			});
         				errorLabel.setVisible(false);
         				appiumDriverService = setUpAppiumDriver();
         				isAppiumStarted = startAppiumServer();
         				if(MOBILE_PLATFORM.equalsIgnoreCase("Android")) {
         					radioButtonsEnableOrDisable(false, true);
         				} else {
         					radioButtonsEnableOrDisable(true, false);
         				}

         				launchApp(deviceName, 
         						platformName,
         						version, 
         						udid,
         						"");
         				return;
         			   
         			}
         			errorLabel.setVisible(true);
         			Platform.runLater(() -> {
         				errorLabel.setText("Enter " + errorMsg);
        			});
         		} else if(text.equalsIgnoreCase("Disconnect")) {
         			imageView.setVisible(false);
         			catchBlock();
         			closeApp(platformName);
         		}
             }
         }.start();
	}
	
	private void catchBlock() {
		
			if (MOBILE_PLATFORM.equalsIgnoreCase("Android")) {
				radioButtonsEnableOrDisable(false, true);
			} else {
				radioButtonsEnableOrDisable(true, false);
			}
		
	}
	
	/**
	 * Android or iOS enabling
	 * @param isAndroid
	 * @param isiOS
	 */
	private void radioButtonsEnableOrDisable(boolean isAndroid, boolean isiOS) {
		androidRadioBtn.setDisable(isAndroid);
		iosRadioBtn.setDisable(isiOS);
	}
	
	
	/**
	 * used to grab the page source from URL and extracting the elements
	 */
	protected Window theStage;
	@FXML
	private void handleElementsButtonAction(ActionEvent event) {
		if(elementsButton.getText().toString().equalsIgnoreCase("Inspect")) {
			 try {
		            Desktop.getDesktop().browse(new URI("http://chrome://inspect/#devices"));
		        } catch (IOException e1) {
		            e1.printStackTrace();
		        } catch (URISyntaxException e1) {
		            e1.printStackTrace();
		        }
		        
		} else {
		pageElementsList = new ArrayList<PageElements>();
		try {
			if (driver != null) {
				
			
				driver.getPageSource();
				writeFile(driver.getPageSource(), "Elements", automationPath, ".xml");
				pageElementsList = new ArrayList<PageElements>();
				javafx.scene.Node source = (javafx.scene.Node) event.getSource();
				theStage = source.getScene().getWindow();
				getElementXpaths(false,false,"","",-1);
				getattributes(false,false);
			}
		} catch (Exception e1) {
			//if(timer != null)
			//	timer.cancel();
			errorLabel.setVisible(true);
			Platform.runLater(() -> {	
			if(submitButton.getText().toString().equalsIgnoreCase("connect")) { 
			elementsButton.setText("Inspect");
			//elementsButton.setVisible(false);
			} else {
				elementsButton.setVisible(false);
			}
			});
			//
			catchBlock();
			stopAppiumServer();
			errorLabel.setText("A session is either terminated or not started.");
			e1.printStackTrace();
		}
		}
	}
	
	
	boolean isPresentIframes;
	boolean isPresentFrames;
	private String seperatorVal = "=FIELD_VALUE=";
	int size = 0;
	private void getElementXpaths(boolean isIframe, boolean isframe, String iframeid, String iframeName, int iframeIndex) throws Exception {
		isPresentIframes = false;
		isPresentFrames = false;
		File inputFile = null;
		Document doc = null;
		if(isIframe || isframe) {
		 inputFile = new File(automationPath + File.separator + "iframe.xml");
		 doc = Jsoup.parse(inputFile, null);
		} else {
		 inputFile = new File(automationPath + File.separator  + "Elements.xml");
		 doc = Jsoup.parse(inputFile, null);
		 size = doc.getElementsByTag("iframe").size();
		}
		ArrayList<String> tagNames = new ArrayList<>();
		tagNames.add("iframe");
		tagNames.add("frame");
		tagNames.add("a");
		tagNames.add("span");
		tagNames.add("button");
		tagNames.add("label");
		tagNames.add("input");
		tagNames.add("textarea");
		tagNames.add("select");
		tagNames.add("img");
		tagNames.add("ul");
		tagNames.add("li");
		tagNames.add("table");
		tagNames.add("div");
	
		Elements elements = null;
		for (int i = 0; i < tagNames.size(); i++) {
			 elements = doc.getElementsByTag(tagNames.get(i));
			if (tagNames.get(i).equalsIgnoreCase("frame")) {
				if (elements.size() > 0) {
					isPresentFrames = true;
				}
			} 
			if (tagNames.get(i).equalsIgnoreCase("iframe")) {
				if (elements.size() > 0) {
					isPresentIframes = true;
				}
			}
			 for (Element element : elements) {
					String fieldName = "";
					String name = nullToEmpty(element.attr("name"));
					String id = nullToEmpty(element.attr("id"));
					String placeholder = nullToEmpty(element.attr("placeholder"));
					String title = nullToEmpty(element.attr("title"));
					String toolTipTitle = nullToEmpty(element.attr("data-original-title"));
					String ariaLabel = nullToEmpty(element.attr("aria-label"));
					String href = nullToEmpty(element.attr("href"));
					String alt = nullToEmpty(element.attr("alt"));
					String src = nullToEmpty(element.attr("src"));
					String value = nullToEmpty(element.attr("value"));
					String role = nullToEmpty(element.attr("role"));
					String fieldType = element.attr("type");
					String classAttr = element.attr("class");
					String ariaOwns = element.attr("aria-owns");
					String ariaDescribedBy = element.attr("aria-describedby");
					//String dataTestId = element.attr("data-test-id");
					
					
					String tagName = element.tagName();
					
					if (tagName.equalsIgnoreCase("UL") || tagName.equalsIgnoreCase("table")) {
						fieldType = "Label";
					}
					
					if (tagName.equalsIgnoreCase("li") && !role.isEmpty() && !role.equalsIgnoreCase("textbox")) {
						fieldType  = role.equalsIgnoreCase("button") ? "Button" : "Link";
					} else if (tagName.equalsIgnoreCase("li") && role.equalsIgnoreCase("textbox")) {
						fieldType = "InputText";
					} else if (tagName.equalsIgnoreCase("li")) {
						fieldType = "Label";
					}
					
					if(tagName.equalsIgnoreCase("li")  && nullToEmpty(element.ownText()).isEmpty()) {
						continue;
					}
					
					System.out.println("\n ==================================================");
					
					boolean validate = false;
					String selectOptions = "";
					if (element.tagName().equalsIgnoreCase("select")) {
						selectOptions = element.html();
						if (fieldName.isEmpty()) {
							fieldName = element.previousElementSibling()!= null ? element.previousElementSibling().text() : "";
						}
						//validate = true;
						System.out.println("Element select Text:" + element.html());
					} 
					
				if (fieldType.equalsIgnoreCase("Checkbox") || tagName.equalsIgnoreCase("input")
						|| tagName.equalsIgnoreCase("textarea") || fieldType.equalsIgnoreCase("radio")) {

					if (placeholder.isEmpty()) {
						fieldName = element.ownText();
						if (fieldName.isEmpty()) {
							fieldName = element.text();
						}
						if (fieldName.isEmpty()) {
							fieldName = element.previousElementSibling() != null ? element.previousElementSibling().text() : "";
						}
					} else {
						fieldName = placeholder;
					}
				} else if (tagName.equalsIgnoreCase("div") &&  (role.equalsIgnoreCase("button") || role.equalsIgnoreCase("link"))) {
						System.out.println("Element parent Text:" + element.parent().text());
						fieldName = element.text();
						fieldType  = role.equalsIgnoreCase("button") ? "Button" : "Link";
				} else if (tagName.equalsIgnoreCase("a")) {
					if (!element.ownText().isEmpty()) {
						fieldName = nullToEmpty(element.ownText());
					} else {
						fieldName = nullToEmpty(element.text());
					}
				} else if (element.ownText().isEmpty()) {
						System.out.println("Element parent Text:" + element.parent().text());
						//fieldName = element.parent().text().length() > 20 ? "" : element.parent().text();
				} else {
						System.out.println("Element Text:" + element.text());
						fieldName = element.ownText();
				}
					System.out.println("Element Placeholder:" + placeholder);
					System.out.println("Element name:" + name);
					System.out.println("Element title:" + title);
					System.out.println("Element alt:" + alt);
					System.out.println("Element value:" + value);
					System.out.println("Element tooltipTitle:" + toolTipTitle);
					System.out.println("Element href:" + href);
					System.out.println("Element src:" + src);
					System.out.println("Element tel:" + fieldType);
		
					if (fieldName.trim().isEmpty()) {
						fieldName = getFieldName(placeholder, name, toolTipTitle, ariaLabel, title, alt, value, href, src,role,fieldType,ariaOwns,id,ariaDescribedBy,classAttr);
					}
					
					
					// String path = CSS2XPath.css2xpath(element.cssSelector(), true);
					System.out.println("Node name : " + element.nodeName());
					System.out.println("      Tag : " + element.tagName());
					System.out.println("      Type : " + element.attr("type"));
					
					if (tagName.equalsIgnoreCase("input") || tagName.equalsIgnoreCase("textarea")) {
						if (fieldType.equalsIgnoreCase("submit") || fieldType.equalsIgnoreCase("reset")
								|| fieldType.equalsIgnoreCase("image") || fieldType.equalsIgnoreCase("button")
								|| fieldType.equalsIgnoreCase("file") || fieldType.equalsIgnoreCase("checkbox")
								|| fieldType.equalsIgnoreCase("radio")) {
							fieldType = "Button";
						} else {
							fieldType = "InputText";
						}
					} else if (tagName.equalsIgnoreCase("img") &&  (role.equalsIgnoreCase("button") || role.equalsIgnoreCase("link"))) {
						//System.out.println("Element parent Text:" + element.parent().text());
						fieldType  = role.equalsIgnoreCase("button") ? "Button" : "Link";
					} else if (tagName.equalsIgnoreCase("a") || tagName.equalsIgnoreCase("select") || tagName.equalsIgnoreCase("img")) {
						fieldType = "Link";
					} else if (tagName.equalsIgnoreCase("button")) {
						fieldType = "Button";
					} else if (tagName.equalsIgnoreCase("label") || tagName.equalsIgnoreCase("span")) {
						fieldType = "Label";
					}  else if (tagName.equalsIgnoreCase("iframe") || tagName.equalsIgnoreCase("frame")) {
						fieldType = "Link";
					}
					String cssSelector = "";
					try {
					cssSelector = element.cssSelector();
					} catch (Exception e) {
						e.printStackTrace();
					}
					ArrayList<String> xpathArrrayList = new ArrayList<>();
					ArrayList<String> cssxpathArrrayList = new ArrayList<>();
//					if(id.isEmpty()) {
//						
//					}  else if (!name.isEmpty() && !value.isEmpty()){
//						//css=input[name=email]
//						cssSelector = tagName + "[name=" + name + "][value=" + value + "]";
//					} else if (!name.isEmpty()){
//						//css=input[name=email]
//						cssSelector = tagName + "[name=" + name + "]";
//					}
					String idValue = "",preferedidValue = "",preferednameValue= "",preferedtextValue="",preferedtitleValue="",preferedlinkText="", preferedcssXpath = "",preferedabsolutexPath="";
					
					
					cssxpathArrrayList.add("css_xpath" + seperatorVal + cssSelector);
					preferedcssXpath = "css_xpath" + seperatorVal  + cssSelector;
					
					
					Iterator<Attribute> it = element.attributes().iterator();
					while (it.hasNext()) {
						Attribute a = it.next();
						
						if (a.getValue() != null && !a.getValue().toString().trim().isEmpty()) {
							String xpath = "xpath_" + a.getKey() +  seperatorVal  + ("//" + tagName + "[@" + a.getKey() + "='" + a.getValue() + "']");
							String cssXpath = tagName + "[" + a.getKey() + "='" + a.getValue() + "']";
							if(a.getKey().equalsIgnoreCase("id")) {
								idValue = xpath;
								xpathArrrayList.add(0,xpath);
								preferedidValue = xpath;
							} else if(a.getKey().equalsIgnoreCase("name")) {
								if(idValue.isEmpty()) {
									xpathArrrayList.add(0,xpath);
								} else {
									xpathArrrayList.add(1,xpath);
								}	
								preferednameValue = xpath;
							}  else if (a.getKey().equalsIgnoreCase("title")) {
								preferedtitleValue = xpath;
							}  else {
								xpathArrrayList.add(xpath);
							}
							
						 if(a.getKey().equalsIgnoreCase("value")) {
								if(!name.isEmpty()) {
									cssXpath = tagName + "[name='" + name + "'][value='" + value + "']";
									//xpathArrrayList.add(xpath);
								} else {
									cssXpath = tagName + "[value='" + value + "']";
								}
						 }
						 cssxpathArrrayList.add("css_" + a.getKey() + seperatorVal  + cssXpath);
						 System.out.println("Xpaths : " + xpath);
						}
					}

					String ownElementText = element.ownText();
					if (!nullToEmpty(ownElementText).isEmpty()) { // tagname[contains(text(),'Signin')]
						String containsXpath = ("//" + tagName + "[contains(text(),'" + ownElementText + "')]");
						//String cssXpath = tagName + "[contains(text='" + ownElementText + "')]";
						System.out.println("contains  Xpath : " + containsXpath);
						
						if(tagName.equalsIgnoreCase("a")) {
							String linkText =  ownElementText;
							xpathArrrayList.add("linkText" + seperatorVal + linkText);
							preferedlinkText = "linkText" + seperatorVal + linkText;
						}
						//cssxpathArrrayList.add("css_text" + seperatorVal + cssXpath);
						xpathArrrayList.add("xpath_text" + seperatorVal + containsXpath);
						 
						preferedtextValue = "xpath_text" + seperatorVal + containsXpath;
						 
						fieldName = element.ownText();
					}
					
					
					String[] xpaths = getXPaths(element, element.tagName()).split("dlready");
					System.out.println(xpaths);
					String idPath = ""; String absoluteXpath = "";
					int xpathLength = xpaths.length;
					if(xpathLength > 0) {
						if(xpathLength == 1) {
							absoluteXpath = xpaths[0];
							
							if (absoluteXpath.contains("a[") || absoluteXpath.contains("A[") || absoluteXpath.contains("/a") ) {
								fieldType = "Link";
							} else if (absoluteXpath.contains("button[") || absoluteXpath.contains("Button[")) {
								fieldType = "Button";
							}
							xpathArrrayList.add("absolute_xpath" + seperatorVal  + absoluteXpath);
							preferedabsolutexPath = "absolute_xpath" + seperatorVal + absoluteXpath;
						} else if (xpathLength >= 2) {
							absoluteXpath = xpaths[0];
							if (absoluteXpath.contains("a[") || absoluteXpath.contains("A[") || absoluteXpath.contains("/a") ) {
								fieldType = "Link";
							} else if (absoluteXpath.contains("button[") || absoluteXpath.contains("Button[")) {
								fieldType = "Button";
							}
							idPath = xpaths[1];
							
								xpathArrrayList.add("xpath_id_relative" + seperatorVal + idPath);
								if(id.isEmpty()) {
									preferedidValue = "xpath_id_relative" + seperatorVal + idPath;
								}
							xpathArrrayList.add("absolute_xpath" + seperatorVal + absoluteXpath);
							preferedabsolutexPath = "absolute_xpath" + seperatorVal  + absoluteXpath;
						} 
					}
				
				if (tagName.equals("span") && fieldName.isEmpty()) {
					continue;
				} else if (fieldName.isEmpty() && tagName.equals("div")) {
					continue;
				} else if (!fieldName.isEmpty() && tagName.equals("div")) {
					if(!fieldType.isEmpty()) {
						//fieldType = "Button";
					} else if (absoluteXpath.contains("a[") || absoluteXpath.contains("A[") || absoluteXpath.contains("/a") ) {
						fieldType = "Link";
					} else if (absoluteXpath.contains("button[") || absoluteXpath.contains("Button[")) {
						fieldType = "Button";
					} else {
						fieldType = "Label";
					}
				}	
				xpathArrrayList.addAll(cssxpathArrrayList);
				
				if(tagName.equalsIgnoreCase("select") && fieldName.isEmpty()) {
					if(!id.isEmpty()) {
						fieldName =  id;
					} else if(!classAttr.isEmpty()){
						fieldName =  classAttr;
					}
				}
				
				if((!element.ownText().isEmpty() || !element.text().isEmpty()) && (nullToEmpty(element.ownText()).equalsIgnoreCase(fieldName) || nullToEmpty(element.text()).equalsIgnoreCase(fieldName))) {
					validate = true;
				}
				PageElements pageElements = new PageElements();
				System.out.println(xpathArrrayList.toString());
				String list = "";
				for (int k = 0; k < xpathArrrayList.size(); k++) {
					list = list + xpathArrrayList.get(k) + "XPATH_SEPARATOR";
				}
				
				preferField(preferedidValue,preferednameValue,preferedtextValue,preferedlinkText,preferedtitleValue,preferedcssXpath, preferedabsolutexPath, pageElements);
				
				pageElements.setAll_xpaths(list);
				pageElements.setIs_validate(validate);
				//pageElements.setRelative_xpath_by_name();
				//pageElements.setRelative_xpath_by_id(xpathArrrayList.get(0));
				pageElements.setName(fieldName.trim());// field name
				pageElements.setInput_type(fieldType.trim()); // field type
				// System.out.println(pageElements.getName());
				if (isIframe) {
					pageElements.setIframe_id(iframeid);
					pageElements.setIframe_position(iframeIndex);
					pageElements.setIframe_name(iframeName.trim().isEmpty() ? "IFRAME" : iframeName);
					pageElements.setIs_iframe(true);
				}
				if (isframe) {
					pageElements.setIframe_id(iframeid);
					pageElements.setIframe_position(iframeIndex);
					pageElements.setIframe_name(iframeName.trim().isEmpty() ? "FRAME" : iframeName);
					pageElements.setIs_frame(true);
				}
				
				pageElements.setChildRelativeXpathId(xpathArrrayList.get(0)); // only for display
				pageElements.setTag_name(tagName.trim());
				pageElements.setChild_id(fieldName.trim());
				if(tagName.equals("select")) {
					pageElements.setChild_text(selectOptions);
				} else {
					pageElements.setChild_text(fieldName);
				}
				pageElementsList.add(pageElements);
			}
			 
			if(pageElementsList != null)
			pageElementsList.sort(Comparator.comparing(PageElements::getName).reversed());
					
		}
		
	}
	
	private static String nullToEmpty(String elementData) {
		return (elementData == null) ? "" : elementData.trim();
	}
	
	
	private  String getFieldName(String placeholder, String name, String toolTipTitle, String ariaLabel,
			String title, String alt, String value, String href, String src, String role, String type,String ariaOwns,String id,String ariaDescribedBy, String classAttr) {
		
		if (!placeholder.isEmpty()) {
			return placeholder;
		} else if (!name.isEmpty()) {
			return name;
		} else if (!toolTipTitle.isEmpty()) {
			return toolTipTitle;
		} else if (!ariaLabel.isEmpty()) {
			return ariaLabel;
		} else if (!title.isEmpty()) {
			return title;
		} else if (!alt.isEmpty()) {
			return alt;
		} else if (!value.isEmpty()) {
			return value;
		} else if (!href.isEmpty()) {
			return href;
		} else if (!ariaDescribedBy.isEmpty()) {
			return ariaDescribedBy;
		} else if (!ariaOwns.isEmpty()) {
			return ariaOwns;
		}  else if (!src.isEmpty()) {
			return src;
		} else if (!id.isEmpty()) {
			return id;
		} else if (!type.isEmpty()) {
			return type;
		} else if (!role.isEmpty()) {
			return role;
		}  else if (!classAttr.isEmpty()) {
			return classAttr;
		} 
		
		return "";
	}
	
	
	private static String getXPaths(Node node, String tagName) {
		Node parent = node.parent();
		return getXPath(parent, tagName);
	}

	private static String getXPath(Node root, String tagName) {
		String xpathByID = "";
		Node current = root;
		String output = "";
		while (current.parentNode() != null) {
			Node parent = current.parentNode();
			if (parent != null &&parent.childNodes()!=null && parent.childNodes().size() > 1) {
				int nthChild = 1;
				Node siblingSearch = current;
				while ((siblingSearch = siblingSearch.previousSibling()) != null) {
					// only count siblings of same type
					if (siblingSearch.nodeName().equals(current.nodeName())) {
						nthChild++;
					}
				}

				Element eElement = (Element) current;
				if (xpathByID.isEmpty() && !eElement.attr("id").isEmpty()) {
					xpathByID = eElement.attr("id");
					xpathByID = "//" + eElement.tagName() + "[@id='" + xpathByID + "']" + output + "/" + tagName;
				}
				output = "/" + current.nodeName() + "[" + nthChild + "]" + output;
			} else {
				output = "/" + current.nodeName() + output;
			}
			current = current.parentNode();
		}
		return output + "/" + tagName +"dlready" + xpathByID;
	}

	
	

	private void preferField(String xpathId,String xpathName, String preferedtextValue, String preferedlinkText, String preferedtitleValue, String preferedcssXpath, String absoluteXpath,PageElements pageElements) {
		//Id,Name,xpath Name,xpath text,Css xpath,absolutexpath,xpath,Linktext
		try {
			if (!xpathId.isEmpty() && xpathId.contains("xpath_id" + seperatorVal) &&  !xpathId.contains("xpath_id_relative" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_id");
				pageElements.setSelected_xpath(xpathId.split("xpath_id"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!xpathId.isEmpty() && xpathId.contains("xpath_id_relative" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_id_relative");
				pageElements.setSelected_xpath(xpathId.split("xpath_id_relative"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			}else if (!xpathName.isEmpty() && xpathName.contains("xpath_name" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_name");
				pageElements.setSelected_xpath(xpathName.split("xpath_name"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!preferedtextValue.isEmpty() && preferedtextValue.contains("xpath_text" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_text");
				pageElements.setSelected_xpath(preferedtextValue.split("xpath_text"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!preferedcssXpath.isEmpty() && preferedcssXpath.contains("css_xpath" + seperatorVal)) {
				pageElements.setPrefered_field("css_xpath");
				pageElements.setSelected_xpath(preferedcssXpath.split("css_xpath"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!absoluteXpath.isEmpty() && absoluteXpath.contains("absolute_xpath" + seperatorVal)) {
				pageElements.setPrefered_field("absolute_xpath");
				pageElements.setSelected_xpath(absoluteXpath.split("absolute_xpath"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!preferedlinkText.isEmpty() && preferedlinkText.contains("linktext" + seperatorVal)) {
				pageElements.setPrefered_field("linktext");
				pageElements.setSelected_xpath(preferedlinkText.split("linktext"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/* Showing the elements in Dialog */

	@SuppressWarnings({ "rawtypes", "static-access", "unchecked" })
	public void getattributes(boolean fromIframe,boolean fromFrame) {

		try {
			/*---------------------------------------- TABLEVIEW -------------------------------------------*/

			if (pageElementsList.size() > 0) {
				dialog = new Stage();
				dialog.initModality(Modality.WINDOW_MODAL);
				dialog.initOwner(theStage);
				VBox dialogVbox = new VBox(20);
				dialog.initStyle(StageStyle.UTILITY);
				dialog.resizableProperty().setValue(Boolean.FALSE);
				HBox elementsHbox = new HBox();
				final Label label = new Label("Elements");
				TextField filterField = new TextField();
				filterField.setFocusTraversable(false);
				filterField.setFont(new Font("proximaNovaSemiBold", 12));
				filterField.setPromptText("Search Elements");
				filterField.setMinWidth(200);
				label.setPadding(new Insets(0, 10, 0, 0));
				progress = new ProgressIndicator();
				progress.setVisible(false);
				final TextField textField = new TextField();
				final TextField descTextField = new TextField();
				
				errorTextField = new Label();
				textColor(errorTextField);
				elementsHbox.getChildren().addAll(label, filterField);
				HBox hBox = new HBox();
				hBox.setAlignment(Pos.CENTER);

				saveElements = new Button("Save Elements");

				textColor(saveElements);

				hBox.getChildren().addAll(textField, descTextField, saveElements, progress);

				textField.setFont(new Font("proximaNovaSemiBold", 14));
				textField.setPromptText("Enter Page Name");
				textField.setMinWidth(200);

				descTextField.setFont(new Font("proximaNovaSemiBold", 14));
				descTextField.setPromptText("Enter Page Description");
				descTextField.setMinWidth(200);

				label.setFont(new Font("Arial", 20));

				hBox.setMargin(saveElements, new Insets(5));
				hBox.setMargin(descTextField, new Insets(5));
				hBox.setMargin(textField, new Insets(5));

				saveElements.setMaxHeight(60);
				HBox iframeBox = new HBox();
				// saveElements.setPadding(new Insets(10, 0, 0, 10));

				TableColumn<Map, String> firstDataColumn = new TableColumn<>("Field Name");
				TableColumn<Map, String> secondDataColumn = new TableColumn<>("Field Type");
				TableColumn<Map, String> thirdDataColumn = new TableColumn<>("Field Locator");

				firstDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");

				// firstDataColumn.setEditable(false);
				firstDataColumn.setCellValueFactory(new MapValueFactory(Column1MapKey));
				firstDataColumn.setMinWidth(200);

				secondDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");
				// secondDataColumn.setEditable(false);
				secondDataColumn.setCellValueFactory(new MapValueFactory(Column2MapKey));
				secondDataColumn.setMinWidth(200);

				thirdDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");
				// thirdDataColumn.setEditable(false);
				thirdDataColumn.setCellValueFactory(new MapValueFactory(Column3MapKey));
				thirdDataColumn.setMinWidth(420);

				firstDataColumn.setSortable(false);
				secondDataColumn.setSortable(false);
				thirdDataColumn.setSortable(false);

				ObservableList<Map> data;

				TableView tableView = null;
				
				tableView = new TableView<>(data = generateDataInMap(pageElementsList));
				
				tableView.setEditable(true);
				tableView.setBackground(null);
				tableView.setFocusTraversable(false);
				tableView.getSelectionModel().setCellSelectionEnabled(true);
				if (isPresentIframes) {
					Button iframeBtn = new Button("Get Iframes");
					iframeBtn.setVisible(true);
					iframeBtn.setStyle("-fx-font: 13 proximaNovaSemiBold; -fx-base: #00bebf;");
					iframeBtn.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent actionEvent) {
							try {
								for(int i=0;i<size;i++) {
									try {
									driver.switchTo().frame(i);
									String s = driver.getPageSource();
										if (s != null) {
											writeFile(s, "Iframe", automationPath + File.separator, ".xml");

											if (pageElementsList == null) {
												pageElementsList = new ArrayList<>();
											}
											getElementXpaths(true, false, "", "", i);
											driver.switchTo().parentFrame();
										}
									} catch (Exception e) {
										// TODO: handle exception
									}
									
								}
								
								//getIframeElements();
								iframeBtn.setVisible(false);
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					iframeBox.setMargin(iframeBtn, new Insets(5));
					iframeBox.getChildren().addAll(iframeBtn);
				}
				
				if (isPresentFrames) {
					Button frameBtn = new Button("Get Frames");
					frameBtn.setVisible(true);
					frameBtn.setStyle("-fx-font: 13 proximaNovaSemiBold; -fx-base: #00bebf;");
					frameBtn.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent actionEvent) {
							try {
								getFrameElements();
								frameBtn.setVisible(false);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					iframeBox.setMargin(frameBtn, new Insets(5));
					iframeBox.getChildren().addAll(frameBtn);
				}
				
				tableView.getColumns().setAll(firstDataColumn, secondDataColumn, thirdDataColumn);
				Callback<TableColumn<Map, String>, TableCell<Map, String>> cellFactoryForMap = (
						TableColumn<Map, String> p) -> new TextFieldTableCell(new StringConverter() {
							@Override
							public String toString(Object t) {
								return t.toString();
							}

							@Override
							public Object fromString(String string) {
								return string;
							}
				});
				firstDataColumn.setCellFactory(cellFactoryForMap);
				secondDataColumn.setCellFactory(cellFactoryForMap);
				thirdDataColumn.setCellFactory(cellFactoryForMap);
				saveElements.setFocusTraversable(false);
				textField.setFocusTraversable(false);
				descTextField.setFocusTraversable(false);
				if (DIFF_ID > 0) {
					textField.setText(WEB_PAGE_NAME);
					descTextField.setText(WEB_PAGE_DESC);
					descTextField.setDisable(true);
					textField.setDisable(true);
				}

				/* -------------------------- Save Elements Button * -------------------------------- */
				PageDetails details = new PageDetails();
				details.setElements_list(pageElementsList);
				saveElements.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent actionEvent) {

						if (isSavingElements) {
							return;
						}

						new Thread() {
							public void run() {
								if (textField.getText().trim().length() > 0) {
									try {
										progress.setVisible(true);
										isSavingElements = true;
										JSONObject jsonObj = new JSONObject(JSON);
										
										details.setWeb_page_name(textField.getText());
										details.setWeb_page_url(driver.getCurrentUrl());
										details.setModule_id(jsonObj.getInt("module_id"));
										details.setDiff_page_id(DIFF_ID);
										details.setWeb_page_description(descTextField.getText() == null ? "" : descTextField.getText());
										details.setUser_id(jsonObj.getInt("user_id"));
										try {
											Gson gsonObj = new GsonBuilder().disableHtmlEscaping().create();
											String json = gsonObj.toJson(details);
											System.out.println(json);
											boolean isPosted = doSaveElementsToServer(json);

											Platform.runLater(new Runnable() {
												@Override
												public void run() {
													isSavingElements = false;
													if (!isPosted) {
														progress.setVisible(false);
													} else if (isPosted && dialog != null) {
														dialog.close();
													}
												}
											});

										} catch (Exception e) {
											e.printStackTrace();
										}

									} catch (JSONException e1) {
										e1.printStackTrace();
									}
								} else {
									try {
										 final Tooltip toolTip = new Tooltip("This field is required.");
										 addToolTipAndBorderColor(textField, toolTip);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}.start();
					}
				});
				// 1. Wrap the ObservableList in a FilteredList (initially display all data).
				FilteredList<Map> filteredData = new FilteredList<>(data, p -> true);

				// 2. Set the filter Predicate whenever the filter changes.
				filterField.textProperty().addListener((observable, oldValue, newValue) -> {
					filteredData.setPredicate(person -> {
						// If filter text is empty, display all persons.
						if (newValue == null || newValue.isEmpty()) {
							return true;
						}

						// Compare first name and last name of every person with filter text.
						String lowerCaseFilter = newValue.toLowerCase();

						if (person.get(Column1MapKey).toString().toLowerCase().contains(lowerCaseFilter)) {
							return true; // Filter matches first name.
						} else if (person.get(Column2MapKey).toString().toLowerCase().contains(lowerCaseFilter)) {
							return true; // Filter matches last name.
						}
						return false; // Does not match.
					});
				});

				// 3. Wrap the FilteredList in a SortedList.
				SortedList<Map> sortedData = new SortedList<>(filteredData);

				// 4. Bind the SortedList comparator to the TableView comparator.
				sortedData.comparatorProperty().bind(tableView.comparatorProperty());

				// 5. Add sorted (and filtered) data to the table.
				tableView.setItems(sortedData);

				final VBox vbox = new VBox();

				vbox.setSpacing(5);
				vbox.setPadding(new Insets(10, 0, 10, 10));
				hBox.setPadding(new Insets(10, 10, 10, 10));
				vbox.getChildren().addAll(elementsHbox, tableView, errorTextField, hBox, iframeBox);

				dialogVbox.getChildren().addAll(vbox);

				Scene dialogScene = new Scene(dialogVbox, 850, 500);
				dialog.setAlwaysOnTop(true);
				dialog.setScene(dialogScene);
				dialog.show();

			}
		} catch (Exception e) {
			if (errorTextField != null) {
				textColor(errorTextField);
				errorTextField.setText("Something went wrong.");
			}

			e.printStackTrace();
		}
	}
	
	
/*-------------------------------- Extracting the elements Node by Node from source code -------------------------------*/
	
	private static org.w3c.dom.Document convertStringToXMLDocument(String xmlString) 
    {
        //Parser that produces DOM object trees from XML content
		javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
         
        //API to obtain DOM Document instance
		javax.xml.parsers.DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
             
            //Parse the content to Document object
            org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return null;
    }

	
	
	private void getIframeElements() throws Exception {
		/*org.w3c.dom.Document doc = view.getEngine().getDocument();
		if(doc == null) {
			return;
		}*/
		
		//driver.executeScript("return document.documentElement.outerHTML");
		writeFile(driver.executeScript("return document.documentElement.outerHTML").toString(), "Iframe", environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator, ".xml");
		
		org.w3c.dom.Document doc = convertStringToXMLDocument(driver.getPageSource());
		
		
		NodeList nodes = doc.getElementsByTagName("iframe");
		String id = "";
		String name = "";
		pageElementsList1 = new ArrayList<PageElements>();
		for (int i = 0; i < nodes.getLength(); i++) {
			if(nodes.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
			HTMLIFrameElement iframeElement = (HTMLIFrameElement) nodes.item(i);
			NamedNodeMap attributes = iframeElement.getAttributes();
			 id = ""; name = "";
			for (int j = 0; j < attributes.getLength(); j++) {
				
				String nodeName = attributes.item(j).getNodeName().trim();
				String nodeValue = attributes.item(j).getNodeValue().trim();
				if(nodeName.equalsIgnoreCase("id") && !nodeValue.isEmpty()) {
					id = nodeValue;
				} else if(nodeName.equalsIgnoreCase("name")) {
					name = nodeValue;
				} 
				String s = attributes.item(j).getNodeName();
				System.out.println(s +", " + attributes.item(j).getNodeValue());
			
			}
			if (iframeElement == null || iframeElement.getContentDocument() == null) 
				return;
			
			org.w3c.dom.Document iframeContentDoc = iframeElement.getContentDocument();
			org.w3c.dom.Element rootElement = iframeContentDoc.getDocumentElement();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			//transformer.setOutputProperty(OutputKeys.ENCODING, XML_ENCODER_FORMAT);
			DOMSource source = new DOMSource(rootElement);
			StreamResult result = new StreamResult(new File(automationPath + File.separator + "Iframe.xml"));
			// StreamResult result1 = new StreamResult(System.out);
			transformer.transform(source, result);
			// System.out.println(result1);
			System.out.flush();
			if(pageElementsList == null) {
				pageElementsList = new ArrayList<>();
			}
			getElementXpaths(true,false, id,name, i);
			
		}
		}
		if (dialog != null) {
			dialog.close();
		}
		getattributes(true,false);
		
	}
	
	
	private void getFrameElements() throws Exception {
		pageElementsList = new ArrayList<>();
		writeFile(driver.getPageSource(), "Iframe", environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator, ".xml");
		
		org.w3c.dom.Document doc = convertStringToXMLDocument(driver.getPageSource());
		NodeList nodes = doc.getElementsByTagName("frame");
		String id = "";
		String name = "";
		pageElementsList1 = new ArrayList<PageElements>();
		for (int i = 0; i < nodes.getLength(); i++) {
			id = "";
			name = "";
			HTMLIFrameElement iframeElement = (HTMLIFrameElement) nodes.item(i);
			for (int j = 0; j < iframeElement.getAttributes().getLength(); j++) {
				String nodeName = iframeElement.getAttributes().item(j).getNodeName().trim();
				String nodeValue = iframeElement.getAttributes().item(j).getNodeValue().trim();
				
				if(nodeName.equalsIgnoreCase("id") && !nodeValue.isEmpty()) {
					id = nodeValue;
				} else if(nodeName.equalsIgnoreCase("name")) {
					name = nodeValue;
				} 
				String s = iframeElement.getAttributes().item(j).getNodeName();
				System.out.println(s +", " + iframeElement.getAttributes().item(j).getNodeValue());
			}
			if (iframeElement == null || iframeElement.getContentDocument() == null)
				return;
			org.w3c.dom.Document iframeContentDoc = iframeElement.getContentDocument();
			org.w3c.dom.Element rootElement = iframeContentDoc.getDocumentElement();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, XML_ENCODER_FORMAT);
			DOMSource source = new DOMSource(rootElement);
			StreamResult result = new StreamResult(new File(automationPath + File.separator + "Iframe.xml"));
			// StreamResult result1 = new StreamResult(System.out);
			transformer.transform(source, result);
			// System.out.println(result1);
			System.out.flush();
			if(pageElementsList == null) {
				pageElementsList = new ArrayList<>();
			}
			getElementXpaths(false,true, id,name, i);
			
		}
		getattributes(false,true);
		
	}
	
	/*---------------------------------- TABLEVIEW DATA ------------------------------------------ */

	@SuppressWarnings("rawtypes")
	private ObservableList<Map> generateDataInMap(ArrayList<PageElements> pagedetailsList) {
		ObservableList<Map> allData = FXCollections.observableArrayList();
		for (int i = 0; i < pagedetailsList.size(); i++) {
			Map<String, String> dataRow = new HashMap<>();
			String value1 = pageElementsList.get(i).getName();
			String value2 = pageElementsList.get(i).getInput_type();
			String value3 = pageElementsList.get(i).getSelected_xpath();
			dataRow.put(Column1MapKey, value1);
			dataRow.put(Column2MapKey, value2);
			dataRow.put(Column3MapKey, value3);
			allData.add(dataRow);
		}
		return allData;
	}
	
	
	/**
	 * Fields enable or disable 
	 * @param isEnable
	 */
	private void setFieldsEnableorDisable(boolean isEnable) {
		deviceNameField.setDisable(isEnable);
		platformNameField.setDisable(isEnable);
		versionField.setDisable(isEnable);
		udidField.setDisable(isEnable);
		//packageNameField.setDisable(isEnable);
		//launcherField.setDisable(isEnable);
	//	apkChooserField.setDisable(isEnable);
	}

	/**
	 * Creating bat file/ shell script file
	 * @param batFileName
	 * @param data
	 * @param extension
	 * @throws Exception
	 */
	private void createBatFile(String batFileName, String data, String extension) throws Exception {
		try {
			writeFile(data, batFileName , environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator , extension);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	private Stage dialog;
	private ProgressIndicator progress;
	private Label errorTextField;
	private Button saveElements;
	private String XML_ENCODER_FORMAT = "ISO-8859-1";
	private void textColor(Label errorTextField) {
		if (errorTextField != null) {
			errorTextField.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
			errorTextField.setStyle("-fx-text-fill: red; -fx-base: #ffffff;");
		}
	}

	private void textColor(Button btn) {
		btn.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
		btn.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
	}
	
	private static final String STILE_BORDER_VALIDATION = "-fx-border-color: #FF0000";
	private static void addToolTipAndBorderColor(javafx.scene.Node n, Tooltip t) {
		javafx.scene.control.Tooltip.install(n, t);
		n.setStyle(STILE_BORDER_VALIDATION);
	}
	
	/*----------------------------------------- SAVE ELEMENETS TO SERVER ----------------------*/
	/**
	 * This method used to push the json string to server 
	 * @param json
	 * @return boolean
	 * @throws Exception
	 */

	private boolean doSaveElementsToServer(String json) throws Exception {
		try {
			
			System.out.println(json);
			
			URL obj = new URL(ELEMENTS_URL);
			 TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}


					@Override
					public void checkClientTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void checkServerTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}
				} };

				// Install the all-trusting trust manager
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

				// Create all-trusting host name verifier
				HostnameVerifier allHostsValid = new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				};
				// Install the all-trusting host verifier
				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(200000);
			con.setConnectTimeout(200000);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			byte[] isoString = json.getBytes(XML_ENCODER_FORMAT);
			wr.write(isoString, 0, isoString.length);
			//wr.writeBytes(json);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(response.toString());

			JSONObject jsonObject = new JSONObject(response.toString());

			if (jsonObject.has("status") && !jsonObject.getString("status").equalsIgnoreCase("SUCCESS")) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (errorTextField != null) {
							textColor(errorTextField);
							errorTextField.setText(jsonObject.optString("message"));

						}
					}
				});
				return false;

			}
		} catch (Exception e) {
			e.printStackTrace();
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					if (errorTextField != null) {
						textColor(errorTextField);
						errorTextField.setText("Something went wrong.");
					}
				}
			});
			return false;
		}
		return true;
	}
	
	
	
	// ====================================== ENVIRONMENT VARAIBLES =================== //
	
	/**
	 * Getting the environment variables from environmentVariables.sh
	 * returns @NODE_PATH/
	 * @APPIUM_PATH/
	 * @ANDROID_HOME/
	 * @AUTOMATION_PATH
	 */
	
	private ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + automationPath + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}
	
	//========================= USED TO GET THE DEVICE ID , PACKAGE NAME AND LAUNCHER ACTIVITY ==========================//
	
	/**
	 * used to get the android/iOS deviceId create and execute script file 
	 */
	private String deviceId = "";
	private void commandScripts(String extension) throws Exception {
		
		try {
			//String batFile = toogleExtension.contains(".apk") ? "adb devices" : "idevice_id -l" ;
			String batFile = toogleExtension.contains(".apk") ? "adb devices" : "ios-deploy -c";
			if(isWindows()) {
			} else if(isMac()){
				batFile = "source ~/.bash_profile\n" + batFile;
			} else {
				batFile = "source ~/.bashrc\n" + batFile;
			}
			createBatFile(GET_ADB_DEVICES, batFile , extension);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String  deviceName = "";
		if(toogleExtension.contains(".apk")) {
			//executeCommand(GET_PACKAGE_LAUNCHER_NAME, "package",extension);
			 deviceId = executeCommand(GET_ADB_DEVICES, "adb",extension);
		} else {
			try {
				deviceId = executeCommand(GET_ADB_DEVICES, "adb", extension);
				String[] splitDevices = deviceId.split("-");
				if (splitDevices != null && splitDevices.length == 3) {
					deviceName = splitDevices[0];
					deviceId = splitDevices[2];
					platformNameField.setText("iOS");
					deviceNameField.setText(deviceName);
					String version = "";
					version = executeCommand("instruments", "INSTRUMENTS", extension);

					if(version != null && !version.trim().isEmpty()) {
						String[] finalSliptOfVersion = version.split("\\.");
						if (finalSliptOfVersion.length >= 1) {
							versionField.setText(finalSliptOfVersion[0] + "." + finalSliptOfVersion[1]);
						}
					
					}
				}
			} catch (Exception e) {
				System.out.println();
			}
		}
		
		if (deviceId != null && !deviceId.isEmpty()) {
			udidField.setText(deviceId);
			if(toogleExtension.contains(".apk")) {
				String batFile ="adb -s " + deviceId + " shell getprop ro.product.model"
						+ "\nadb -s " + deviceId + " shell getprop ro.build.version.release"
						+ "\n";
				
				if(isWindows()) {
				} else if(isMac()){
					batFile = "source ~/.bash_profile\n" + batFile;
				} else {
					batFile = "source ~/.bashrc\n" + batFile;
				}
				createBatFile("productDetails", batFile,extension);
				executeCommand(GET_PRODUCT_DETAILS, "", extension);
			}
		}
		
		executeCommand(KILL_ADB, "adb", extension);

	}
	
	/**
	 * used to execute the command
	 * @param batFilePath
	 * @param executionName
	 * @param extension
	 * @return android deviceId / iOS deviceId / node path /  package name / launcher activity name / device model / device version
	 */
	private String executeCommand(String batFilePath, String executionName, String extension) {
		try {
			String isMacProduct = null;
			String line;
			String packageName = "";
			String launcherActivity = "";
			boolean listDevices = false;
			Process p = Runtime.getRuntime().exec(environMentVariables + File.separator + "mobile" + File.separator + "utilities" + File.separator + batFilePath + extension);
			
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			Boolean isProductModel =  null;
			while ((line = bri.readLine()) != null) {
				if (executionName.equalsIgnoreCase("INSTRUMENTS")) {
					System.out.println(deviceId.replaceAll(" ", ""));
					if (line.contains(deviceId.replaceAll(" ", ""))) {
						String[] version = line.split("\\(");
						version = version[1].split("\\)");
						return version[0];
					}
				} else {
					if (executionName.equals("node")) {
						return line;
					} else if (executionName.equalsIgnoreCase("adb")) {
						if (toogleExtension.contains(".ipa")) {
							if (line.contains("Found") && line.contains("iPhone 5s")) {
								String[] udidSplit = line.split("]");
								String[] osUdid = udidSplit[1].split("\\(");
								String deviceName = osUdid[1].split(",")[1].replaceAll("\\s+", "");
								String platformName = osUdid[2].split(",")[2].replaceAll("\\s+", "");
								String finalString = osUdid[0].replaceAll("Found", "").replaceAll("\\s+", "");
								// String[] finalString = osUdid[1].split(",");
								return deviceName + "-" + platformName + "-" + finalString.replaceAll(" ", "");
							}
							
							else if (line.contains("Found")) {
								String[] udidSplit = line.split("]");
								String[] osUdid = udidSplit[1].split("\\(");
								String deviceName = osUdid[1].split(",")[1].replaceAll("\\s+", "");
								String platformName;
								try {
								 platformName = osUdid[1].split(",")[2].replaceAll("\\s+", "");
								} catch(Exception e) {
									try {
										platformName = osUdid[2].split(",")[1].replaceAll("\\s+", "");
										} catch (Exception e1) {
											platformName = "iPhone";
										}
								}
								String finalString = osUdid[0].replaceAll("Found", "").replaceAll("\\s+", "");
								// String[] finalString = osUdid[1].split(",");
								return deviceName + "-" + platformName + "-" + finalString.replaceAll(" ", "");
							}
						} else {
							if (listDevices) {
								String[] deviceUDID = line.split("device");
								System.out.println(deviceUDID[0]);
								listDevices = false;
								return deviceUDID[0];
							}
							if (line.contains("List of devices attached")) {
								listDevices = true;
							}
						}
					} else if (executionName.equalsIgnoreCase("package")) {
						if (line != null && line.contains("launchable-activity:")) {
							launcherActivity = line.split("=")[1].split(" ")[0];
							System.out.println("Launcher Activity : " + launcherActivity);
							launcherField.setText(launcherActivity.replace("'", ""));
							return (packageName + "$$" + launcherActivity).replaceAll("'", "");
						}

						if (line != null && line.contains("package:")) {
							packageName = line.split("=")[1].split(" ")[0];
							packageNameField.setText(packageName.replace("'", ""));
							System.out.println("packageName : " + packageName);
						}

					} else {
						if (isMac()) {
							if (!line.contains("adb") && !line.contains("daemon")) {

								if (isMacProduct == null) {
									isMacProduct = line;
									deviceNameField.setText(line);
								} else {
									if (line.trim().split("\\.").length > 0) {
										versionField.setText(line.trim().split("\\.")[0]);
									}
								}
							}

						} else {
							if (line != null && !line.isEmpty()) {
								if (line.contains("ro.product.model")) {
									isProductModel = true;
									continue;
								} else if (line.contains("ro.build.version")) {
									isProductModel = false;
									continue;
								}

								if (isProductModel != null) {
									if (isProductModel) {
										deviceNameField.setText(line);
									} else {
										if (line.trim().split("\\.").length > 0) {
											versionField.setText(line.trim().split("\\.")[0]);
										}
									}
								}
							}
						}
					}
				}
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
			
			System.out.println("Done.");
		
		} catch (Exception err) {
			err.printStackTrace();
		}
		return "";
	}
	
	
	// ============================================= APPIUM SETUP ==============================================//
	/**
	 * Configuring the node and appium path
	 * @return
	 */
	 
	private AppiumDriverLocalService setUpAppiumDriver() {	
		AppiumDriverLocalService appiumDriverService = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
				.usingAnyFreePort()
				.usingDriverExecutable(new File(nodeJSPath))
				.withAppiumJS(new File(appiumServerJSPath)));
		return appiumDriverService;
	}
	
	/**
	 * Used to start Appium Server 
	 * @return boolean appium is started or not
	 */
	private boolean startAppiumServer() {
		if (appiumDriverService != null) {
			appiumDriverService.start();
			
			return appiumDriverService.isRunning();
		}
		return false;
	}
	
	/**
	 * used to stop the Appium  server
	 */
	private void stopAppiumServer() {
		//if(timer !=null)
		//	timer.cancel();
		if (appiumDriverService != null) {
			
			//elementsButton.setVisible(false);
			Platform.runLater(() -> {
				
				setFieldsEnableorDisable(false);
				submitButton.setText("Connect");
				if(submitButton.getText().toString().equalsIgnoreCase("connect")) { 
					elementsButton.setText("Inspect");
					//elementsButton.setVisible(false);
					} else {
						elementsButton.setVisible(false);
					}
				
			});
			
			appiumDriverService.stop();
			System.out.println(" Stopped Appium Server");
		}   
	}
	
	//======================================== LAUNCH APP =========================================//
	
	private static BufferedImage resize(BufferedImage img, int height, int width) {
        java.awt.Image tmp = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
	/**
	 * Desired Capabilities to launch iOS and Android
	 * @param deviceName
	 * @param paltformName
	 * @param platformVersion
	 * @param udid
	 * @param filePath
	 * @param packageName
	 * @param launcherActivity
	 */
	public static String captureScreenshot(WebDriver driver, String screenShotName) {
		String path = "";
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
		Date dt = new Date();
		String html = "";
		try {
			/*if(Constants.IS_TESTCASE && Constants.iS_WEB || !Constants.iS_WEB) {
				Constants.TOTAL_TC_FAILED = Constants.TOTAL_TC_FAILED + 1;
				Constants.IS_TESTCASE = false;
			} */
			
			System.out.println(dateFormat.format(dt));
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			path = System.getProperty("user.dir") + File.separator + "screenshots";
			FileUtils.copyFile(source, new File(path + File.separator   + "pd.png"));
			System.out.println("screenshot is taken");
		
		} catch (Exception e) {
			System.out.println("exception while taking screenshot" + e.getMessage());
		}
		
		return html;
	}
	private String driversPath = System.getenv("AUTOMATION_PATH").replace(File.separator + "WebDrivers", "") + File.separator + "Resources" + File.separator;
	private String chromeDriverPath = driversPath + "chromedriver.exe";
	//Timer timer;
	private String previousURL;
	private void launchApp(String deviceName, String paltformName, String platformVersion, String udid, String filePath) {
		
		try {
		
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("platformName", paltformName);
			capabilities.setCapability("deviceName", deviceName);
			if(paltformName.equalsIgnoreCase("Android")) {
				if (!isWindows()) {
					if (isSolaris() || isUnix()) {
						chromeDriverPath = chromeDriverPath.replace(".exe", "");
					} else if (isMac()) {
						chromeDriverPath = chromeDriverPath.replace("chromedriver.exe", "macChromeDriver");
					}
				}
				//chromeDriverPath = "D:\\FrameworkCode\\QualityFusionQE\\Resources\\chromedriver.exe";
				//chromeDriverPath = "/Users/prolifics/FrameworkCode/QualityFusionQE/Resources/macChromeDriver";
				capabilities.setCapability("browserName", "chrome");
				capabilities.setCapability("chromedriverExecutable", chromeDriverPath);
				capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
				capabilities.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));
			} else {
				capabilities.setCapability("browserName", "Safari");
				capabilities.setCapability("automationName", "XCUITest");
				capabilities.setCapability("startIWDP", true);
			}
		capabilities.setCapability("orientation", "PORTRAIT");
		//	capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60*10); //ending the session 10 minutes
			
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
			capabilities.setCapability(MobileCapabilityType.UDID, udid);
			//capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true);
			//capabilities.setCapability(MobileCapabilityType.FORCE_MJSONWP, true);
			//capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
			//capabilities.setCapability(AndroidMobileCapabilityType.SUPPORTS_ALERTS,true);
			 driver = new AppiumDriver<WebElement>(new URL(appiumDriverService.getUrl().toString()), capabilities);
			 driver.get(URL);
			 previousURL = URL;
				
				Platform.runLater(() -> {
					
					submitButton.setText("Disconnect");
				});
				setFieldsEnableorDisable(true);
				Platform.runLater(() -> {		
				elementsButton.setText("Get Elements");
				imageView.setVisible(true);
				elementsButton.setVisible(true);
		/*		new Thread() {
					public void run() {
						timer.scheduleAtFixedRate(new TimerTask() {

							public void run() {
							String url = driver.getCurrentUrl();
							System.err.println("===========" + url + "=============" + previousURL);
							if(!previousURL.equalsIgnoreCase(url)) {
								try {
									Thread.sleep(1000);
									updateImage();
									previousURL = url;
									elementsButton.setVisible(true);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}
							}
						}, 2000, 2000);
					}
				}.start();*/
				});
				
				//stopAppiumServer();
			
		} catch (Exception e) {
			//if(timer != null)
			//timer.cancel();
			if(MOBILE_PLATFORM.equalsIgnoreCase("ANDROID")) {
				radioButtonsEnableOrDisable(false, true);
			} else {
				radioButtonsEnableOrDisable(true, false);
			}
			errorLabel.setVisible(true);
			Platform.runLater(() -> {			
				submitButton.setText("Connect");
				String message = e.getLocalizedMessage();
				if(message.contains("Could not find a connected") ||  message.contains("No device connected")) {
					errorLabel.setText("Could not find a connected Android device.");
				} else if(message.contains("was not in the list of connected devices")) {
					errorLabel.setText("Device "+ udid +" was not in the list of connected devices");
				} else if(message.contains("Platform version must be")) {
					errorLabel.setText("Platform version must be 9.3 or above.");
				} else if(message.contains("Could not proxy command to remote server")) {
					errorLabel.setText("Device is offine (or) not connected.");
				} else if(message.contains("Could not install app: 'Command '")){
					errorLabel.setText("Device is not resgistered.");
				} else if(message.contains("Connection refused (Connection refused)")){
					submitButton.fire();
				} else {
					
					errorLabel.setText(e.getLocalizedMessage());
				}
			});
			
			e.printStackTrace();
		}	
		

	}
	
	private void updateImage() {
		// simple displays ImageView the image as is
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				FileInputStream inputstream = null;
				try {

					captureScreenshot(driver, "pd");
					File input = new File(System.getProperty("user.dir") + File.separator + "screenshots" + File.separator + "pd.png");
					BufferedImage image = null;
					image = ImageIO.read(input);
					BufferedImage resized = resize(image, 400, 270);
					File output = new File(System.getProperty("user.dir") + File.separator + "screenshots" + File.separator + "pd.png");
					ImageIO.write(resized, "png", output);
					inputstream = new FileInputStream(System.getProperty("user.dir") + File.separator + "screenshots" + File.separator + "pd.png");
					imageView.setImage(new Image(inputstream));
					inputstream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				imageView.setPreserveRatio(true);
				imageView.setSmooth(true);
				imageView.setCache(false);

			}
		});
	}
	
	
	/* Getting the OS */
	private boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	private boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
	
	/*
	 * Killing the node and adb while closing the app and on click of disconnect button
	 * 
	 */
	
	public void closeApp(String platformName){
		//if(timer !=null)
		//	timer.cancel();
		if(platformName.equalsIgnoreCase("Android")) {
		executeCommand(KILL_ADB, "adb", extension);
		} else {
		executeCommand(KILL_NODE, "node", extension);
		}
		stopAppiumServer();	
	}
	
	// creating the file 
	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator +  className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator +  className + fileExtension);
		} finally {
			out.close();
		}
	}
	
	
	
	
	/*
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
			String val = value.replaceAll(" ", "");
			val = val.replaceAll("[^a-z_A-Z0-9]", "");
			return val;
		} catch (Exception e) {
			return value;
		}
	}

}
