
package com.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrimaryInfo {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    
    @SerializedName("executed_user_id")
    @Expose
    private Integer executedUserId;
    
    @SerializedName("is_generate")
    @Expose
    private Boolean isGenerate;
    @SerializedName("is_execute")
    @Expose
    private Boolean isExecute;
    
    @SerializedName("mobile_platform")
    @Expose
    private String mobilePlatform;
    
	@SerializedName("is_web")
    @Expose
    private Boolean isWeb;
    @SerializedName("project_url")
    @Expose
    private String projectUrl;
    
    @SerializedName("report_upload_url")
    @Expose
    private String reportUploadUrl;
    
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("project_description")
    @Expose
    private String projectDescription;
    @SerializedName("project_id")
    @Expose
    private Integer projectId;
    @SerializedName("module_name")
    @Expose
    private String moduleName;
    @SerializedName("module_description")
    @Expose
    private String moduleDescription;
    @SerializedName("sub_module_id")
    @Expose
    private Integer subModuleId;
    @SerializedName("sub_module_name")
    @Expose
    private String subModuleName;
    @SerializedName("sub_module_description")
    @Expose
    private String subModuleDescription;
    @SerializedName("module_id")
    @Expose
    private Integer moduleId;
    @SerializedName("testcase_name")
    @Expose
    private String testcaseName;
    @SerializedName("testcase_id")
    @Expose
    private Integer testcaseId;
    @SerializedName("testset_name")
    @Expose
    private String testsetName;
    @SerializedName("testset_id")
    @Expose
    private Integer testsetId;
    @SerializedName("executed_timestamp")
    @Expose
    private Integer executedTimestamp;
    
    @SerializedName("browser_type")
    @Expose
    private String browserType;
    
    @SerializedName("file_name")
    @Expose
    private String fileName;
    
    @SerializedName("testcase_overwrite")
    @Expose
    private boolean testcase_overwrite;
    
    
    @SerializedName("client_timezone_id")
    @Expose
    private String client_time_zone_id;

    
    @SerializedName("execution_platform")
    @Expose
    private String execution_platform;
    
 
    @SerializedName("mobile_type")
    @Expose
    private String mobileType;
    
    @SerializedName("execution_environment")
    @Expose
    private String executionEnvironment;
    
    
    
	
	public String getExecutionEnvironment() {
		return executionEnvironment;
	}

	public void setExecutionEnvironment(String executionEnvironment) {
		this.executionEnvironment = executionEnvironment;
	}

	public String getMobileType() {
		return mobileType;
	}

	public void setMobileType(String mobileType) {
		this.mobileType = mobileType;
	}

	public Integer getExecutedUserId() {
		return executedUserId;
	}

	public void setExecutedUserId(Integer executedUserId) {
		this.executedUserId = executedUserId;
	}



	public String getExecution_platform() {
		return execution_platform;
	}

	public void setExecution_platform(String execution_platform) {
		this.execution_platform = execution_platform;
	}

	public String getClient_time_zone_id() {
		return client_time_zone_id;
	}

	public void setClient_time_zone_id(String client_time_zone_id) {
		this.client_time_zone_id = client_time_zone_id;
	}

	public boolean isTestcase_overwrite() {
		return testcase_overwrite;
	}

	public void setTestcase_overwrite(boolean testcase_overwrite) {
		this.testcase_overwrite = testcase_overwrite;
	}

	public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getIsGenerate() {
        return isGenerate;
    }

    public void setIsGenerate(Boolean isGenerate) {
        this.isGenerate = isGenerate;
    }

    public Boolean getIsExecute() {
        return isExecute;
    }

    public void setIsExecute(Boolean isExecute) {
        this.isExecute = isExecute;
    }

    public Boolean getIsWeb() {
        return isWeb;
    }

    public void setIsWeb(Boolean isWeb) {
        this.isWeb = isWeb;
    }

    public String getProjectUrl() {
        return projectUrl;
    }

    public void setProjectUrl(String projectUrl) {
        this.projectUrl = projectUrl;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleDescription() {
        return moduleDescription;
    }

    public void setModuleDescription(String moduleDescription) {
        this.moduleDescription = moduleDescription;
    }

    public Integer getSubModuleId() {
        return subModuleId;
    }

    public void setSubModuleId(Integer subModuleId) {
        this.subModuleId = subModuleId;
    }

    public String getSubModuleName() {
        return subModuleName;
    }

    public void setSubModuleName(String subModuleName) {
        this.subModuleName = subModuleName;
    }

    public String getSubModuleDescription() {
        return subModuleDescription;
    }

    public void setSubModuleDescription(String subModuleDescription) {
        this.subModuleDescription = subModuleDescription;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public String getTestcaseName() {
        return testcaseName;
    }

    public void setTestcaseName(String testcaseName) {
        this.testcaseName = testcaseName;
    }

    public Integer getTestcaseId() {
        return testcaseId;
    }

    public void setTestcaseId(Integer testcaseId) {
        this.testcaseId = testcaseId;
    }

    public String getTestsetName() {
        return testsetName;
    }

    public void setTestsetName(String testsetName) {
        this.testsetName = testsetName;
    }

    public Integer getTestsetId() {
        return testsetId;
    }

    public void setTestsetId(Integer testsetId) {
        this.testsetId = testsetId;
    }

    public Integer getExecutedTimestamp() {
        return executedTimestamp;
    }

    public void setExecutedTimestamp(Integer executedTimestamp) {
        this.executedTimestamp = executedTimestamp;
    }

	public String getReportUploadUrl() {
		return reportUploadUrl;
	}

	public void setReportUploadUrl(String reportUploadUrl) {
		this.reportUploadUrl = reportUploadUrl;
	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}

	public String getMobilePlatform() {
		return mobilePlatform==null?"":mobilePlatform;
	}

	public void setMobilePlatform(String mobilePlatform) {
		this.mobilePlatform = mobilePlatform;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
