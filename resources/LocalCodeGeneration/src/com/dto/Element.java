
package com.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Element {

    @SerializedName("element_id")
    @Expose
    private Integer elementId;
    @SerializedName("screen_id")
    @Expose
    private Integer screenId;
    @SerializedName("web_page_element_id")
    @Expose
    private Integer webPageElementId;
    @SerializedName("element_order")
    @Expose
    private Integer elementOrder;
    @SerializedName("web_page_id")
    @Expose
    private Integer webPageId;
    @SerializedName("field_name")
    @Expose
    private String fieldName;
    
    @SerializedName("secondary_field_type")
    @Expose
    private String secondaryFieldType;
    
    @SerializedName("selected_xpath")
    @Expose
    private String selectedXpath;
    
    
    @SerializedName("prefered_field")
    @Expose
    private String prefered_field;

	public String getSelectedXpath() {
		return selectedXpath;
	}

	public void setSelectedXpath(String selectedXpath) {
		this.selectedXpath = selectedXpath;
	}

	public String getPrefered_field() {
		return prefered_field;
	}

	public void setPrefered_field(String prefered_field) {
		this.prefered_field = prefered_field;
	}

	public String getSecondaryFieldType() {
		return secondaryFieldType;
	}

	public void setSecondaryFieldType(String secondaryFieldType) {
		this.secondaryFieldType = secondaryFieldType;
	}

	@SerializedName("field_type")
    @Expose
    private String fieldType;
    @SerializedName("relative_xpath_by_id")
    @Expose
    private String relativeXpathById;
    @SerializedName("relative_xpath_by_field_name")
    @Expose
    private String relativeXpathByFieldName;
    @SerializedName("xpath")
    @Expose
    private String xpath;
   
    @SerializedName("is_iframe")
    @Expose
    private Boolean isIframe;
    
    @SerializedName("field_value")
    @Expose
    private String fieldValue;
   
    @SerializedName("tag_field_name")
    @Expose
    private String tagFieldName;
    @SerializedName("child_id")
    @Expose
    private String childId;
    @SerializedName("child_text")
    @Expose
    private String childText;
    @SerializedName("prefered_field")
    @Expose
    private String preferedField;
    
    
    @SerializedName("is_selected")
    @Expose
    private Boolean isSelected;
    
    @SerializedName("iframe_name")
    @Expose
    private String iframeName;
    
    @SerializedName("iframe_id")
    @Expose
    private String iframeId;
    
    @SerializedName("iframe_position")
    @Expose
    private Integer iFramePosition;
    
    
    @SerializedName("frame_name")
    @Expose
    private String frameName;
    
    @SerializedName("frame_position")
    @Expose
    private Integer framePosition;

	@SerializedName("frame_id")
    @Expose
    private String frameId;
    
    @SerializedName("is_frame")
    @Expose
    private boolean isFrame;
    
    @SerializedName("dropdown_value")
    @Expose
    private String dropdownValue;
    
    @SerializedName("is_click")
    @Expose
    private boolean isClick;
    
    
    @SerializedName("is_optionalElement")
    @Expose
    private boolean is_optionalElement;
    
    @SerializedName("scroll_required")
    @Expose
    private boolean scroll_required;
    
    @SerializedName("validate_text")
    @Expose
    private String validateText;
    
    @SerializedName("custom_code")
    @Expose
    private boolean customCode;
    
    @SerializedName("displayed")
    @Expose
    private boolean displayed;
    
    @SerializedName("element_wait")
    @Expose
    private boolean elementWait;
    

    @SerializedName("scrollup")
    @Expose
    private boolean scrollUp;
    

    @SerializedName("scrolldown")
    @Expose
    private boolean scrollDown;
    
    
    @SerializedName("is_random")
    @Expose
    private boolean isRandomNumber;
    
    @SerializedName("is_enter")
    @Expose
    private boolean enter;
       
    public boolean isEnter() {
		return enter;
	}

	public void setEnter(boolean enter) {
		this.enter = enter;
	}

	public boolean isRandomNumber() {
		return isRandomNumber;
	}

	public void setRandomNumber(boolean isRandomNumber) {
		this.isRandomNumber = isRandomNumber;
	}

	public boolean isScrollUp() {
		return scrollUp;
	}

	public void setScrollUp(boolean scrollUp) {
		this.scrollUp = scrollUp;
	}

	public boolean isScrollDown() {
		return scrollDown;
	}

	public void setScrollDown(boolean scrollDown) {
		this.scrollDown = scrollDown;
	}

	public String getValidateText() {
		return validateText;
	}

	public void setValidateText(String validateText) {
		this.validateText = validateText;
	}

	public boolean isCustomCode() {
		return customCode;
	}

	public void setCustomCode(boolean customCode) {
		this.customCode = customCode;
	}

	public boolean isDisplayed() {
		return displayed;
	}

	public void setDisplayed(boolean displayed) {
		this.displayed = displayed;
	}

	public boolean isElementWait() {
		return elementWait;
	}

	public void setElementWait(boolean elementWait) {
		this.elementWait = elementWait;
	}

	public boolean isScroll_required() {
		return scroll_required;
	}

	public void setScroll_required(boolean scroll_required) {
		this.scroll_required = scroll_required;
	}

	public boolean isIs_optionalElement() {
		return is_optionalElement;
	}

	public void setIs_optionalElement(boolean is_optionalElement) {
		this.is_optionalElement = is_optionalElement;
	}

	public boolean isClick() {
		return isClick;
	}

	public void setClick(boolean isClick) {
		this.isClick = isClick;
	}

	public String getDropdownValue() {
		return dropdownValue;
	}

	public void setDropdownValue(String dropdownValue) {
		this.dropdownValue = dropdownValue;
	}

	public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public Integer getScreenId() {
        return screenId;
    }

    public void setScreenId(Integer screenId) {
        this.screenId = screenId;
    }

    public Integer getWebPageElementId() {
        return webPageElementId;
    }

    public void setWebPageElementId(Integer webPageElementId) {
        this.webPageElementId = webPageElementId;
    }

    public Integer getElementOrder() {
        return elementOrder;
    }

    public void setElementOrder(Integer elementOrder) {
        this.elementOrder = elementOrder;
    }

    public Integer getWebPageId() {
        return webPageId;
    }

    public void setWebPageId(Integer webPageId) {
        this.webPageId = webPageId;
    }

    public String getFieldName() {
        return fieldName.replaceAll("^\\d+", "");
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getRelativeXpathById() {
        return relativeXpathById;
    }

    public void setRelativeXpathById(String relativeXpathById) {
        this.relativeXpathById = relativeXpathById;
    }

    public String getRelativeXpathByFieldName() {
        return relativeXpathByFieldName;
    }

    public void setRelativeXpathByFieldName(String relativeXpathByFieldName) {
        this.relativeXpathByFieldName = relativeXpathByFieldName;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Boolean getIsIframe() {
        return isIframe;
    }

    public void setIsIframe(Boolean isIframe) {
        this.isIframe = isIframe;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getIframeName() {
        return iframeName;
    }

    public void setIframeName(String iframeName) {
        this.iframeName = iframeName;
    }

    public String getTagFieldName() {
        return tagFieldName;
    }

    public void setTagFieldName(String tagFieldName) {
        this.tagFieldName = tagFieldName;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildText() {
        return childText;
    }

    public void setChildText(String childText) {
        this.childText = childText;
    }

    public String getPreferedField() {
        return preferedField;
    }

    public void setPreferedField(String preferedField) {
        this.preferedField = preferedField;
    }

	public String getFrameName() {
		return frameName;
	}

	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}
 
	public Integer getFramePosition() {
		return framePosition;
	}

	public void setFramePosition(Integer framePosition) {
		this.framePosition = framePosition;
	}

	public String getFrameId() {
		return frameId;
	}

	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}

	public boolean getIsFrame() {
		return isFrame;
	}

	public void setIsFrame(boolean isFrame) {
		this.isFrame = isFrame;
	}

	public String getIframeId() {
		return iframeId;
	}

	public void setIframeId(String iframeId) {
		this.iframeId = iframeId;
	}
	
	public Integer getiFramePosition() {
		return iFramePosition;
	}

	public void setiFramePosition(Integer iFramePosition) {
		this.iFramePosition = iFramePosition;
	}

	public void setFrame(boolean isFrame) {
		this.isFrame = isFrame;
	}

	
}
