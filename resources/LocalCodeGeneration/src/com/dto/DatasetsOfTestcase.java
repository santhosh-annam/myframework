
package com.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatasetsOfTestcase {

    @SerializedName("dataset_name")
    @Expose
    private String datasetName;
    @SerializedName("dataset_id")
    @Expose
    private Integer datasetId;
    @SerializedName("apis")
    @Expose
    private Object apis;
    @SerializedName("screens")
    @Expose
    private List<Screen> screens = null;

    public String getDatasetName() {
        return datasetName;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public Integer getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(Integer datasetId) {
        this.datasetId = datasetId;
    }

    public Object getApis() {
        return apis;
    }

    public void setApis(Object apis) {
        this.apis = apis;
    }

    public List<Screen> getScreens() {
        return screens;
    }

    public void setScreens(List<Screen> screens) {
        this.screens = screens;
    }

}
