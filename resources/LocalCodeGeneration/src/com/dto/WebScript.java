
package com.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebScript {

    @SerializedName("primary_info")
    @Expose
    private PrimaryInfo primaryInfo;
    @SerializedName("datasets_of_testcase")
    @Expose
    private List<DatasetsOfTestcase> datasetsOfTestcase = null;

    public PrimaryInfo getPrimaryInfo() {
        return primaryInfo;
    }

    public void setPrimaryInfo(PrimaryInfo primaryInfo) {
        this.primaryInfo = primaryInfo;
    }

    public List<DatasetsOfTestcase> getDatasetsOfTestcase() {
        return datasetsOfTestcase;
    }

    public void setDatasetsOfTestcase(List<DatasetsOfTestcase> datasetsOfTestcase) {
        this.datasetsOfTestcase = datasetsOfTestcase;
    }

}
