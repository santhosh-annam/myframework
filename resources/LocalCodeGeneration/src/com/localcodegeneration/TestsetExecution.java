package com.localcodegeneration;
import java.awt.GraphicsEnvironment;
import java.io.Console;
import java.io.File;
import java.io.IOException;

public class TestsetExecution {

	
	public static void main(String path){
	    Console console = System.console();
	    if(console == null && !GraphicsEnvironment.isHeadless()) {
	        try {
	            File batch = new File(path);
	            Runtime.getRuntime().exec("cmd /c start \"\" "+batch.getPath());
	        } catch(IOException e) {
	            e.printStackTrace();
	        }
	    } else {
	        //your program code...
	    }
	}
}
