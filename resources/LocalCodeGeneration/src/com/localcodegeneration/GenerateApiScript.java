package com.localcodegeneration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.utils.Utils;


/**
 * This class used to generating the API Testcase's and Dataset's 
 * @TESTNG bat file creation
 * @Tescase File Creation
 * @DatasetCreation
 * 
 */

@SuppressWarnings("unused")
public class GenerateApiScript {
	
	private String reportName;
	private boolean isDataset;
	private String configProperties = "";
	public void generateCode(boolean isTestSet, int position, JSONObject jsonObject, int testCaseSize,String environmentVariable) throws Exception {
		isDataset = true;
		String[] types = new String[] { "", "GET", "POST", "PUT", "DELETE" };
		JSONObject primaryInfoObj = jsonObject.getJSONObject("primary_info");
		String testCaseName = primaryInfoObj.optString("testcase_name");
		String projectName = Utils.removeUnWantedChars(primaryInfoObj.optString("project_name")).toLowerCase();
		String projectId = primaryInfoObj.optString("project_id");
		String reportsPath = "reportsPath";
		String requestType = types[primaryInfoObj.optInt("request_type")];
		boolean executeAction = primaryInfoObj.optBoolean("is_execute");
		boolean generateAction = primaryInfoObj.optBoolean("is_generate");
		String returnString = primaryInfoObj.optString("returnString");
		boolean testcaseOverwrite = primaryInfoObj.optBoolean("testcase_overwrite");
		String baseURL = primaryInfoObj.optString("project_url");
		String moduleFolder = Utils.removeUnWantedChars(primaryInfoObj.optString("module_name").toLowerCase());
		String subMdule = primaryInfoObj.optString("sub_module_name") == null ? "" : Utils.removeUnWantedChars(primaryInfoObj.optString("sub_module_name"));
		String testSetName = primaryInfoObj.optString("testset_name") == null ? "" : primaryInfoObj.optString("testset_name");
		String testsetName = Utils.removeUnWantedChars(testSetName);	
		String subModuleFolder = Utils.removeUnWantedChars(subMdule.toLowerCase());
		String url = primaryInfoObj.optString("api_url");
		int apiId = primaryInfoObj.optInt("api_id");

		String apiFolder = environmentVariable +  File.separator + "src"+ File.separator + "test"+ File.separator + "java"+ File.separator + "api";
		createDirectory(apiFolder);
		
		String projectFolder = apiFolder + File.separator +  projectName;
		createDirectory(projectFolder);

		String packageNameFolder = projectFolder + File.separator +  moduleFolder;
		// create module
		createDirectory(packageNameFolder);
		
		//create sub module
		if (!subModuleFolder.equals("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			packageNameFolder = packageNameFolder + File.separator +  subModuleFolder;
			createDirectory(packageNameFolder);
		}

		String testClassPath = packageNameFolder + File.separator +  "apitestclasses";
		String pageClassPath = packageNameFolder + File.separator +  "apipageclasses";

		/* Creating directories if not exists */
		createDirectory(String.valueOf(testClassPath));

		createDirectory(String.valueOf(pageClassPath));

		String primaryInfo = jsonObject.getJSONObject("primary_info").toString();
		
		String dataset = "{}";
		
		String subModule = ((!subModuleFolder.equals("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) ? ("." + subModuleFolder) :"");

		/* --------------------------------------- TESTNG CREATION (testng.bat) ----------------------------------------------------------*/
		
		if (!isTestSet) {
			String testngFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
					+ "<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >\r\n"
					+ "<suite name=\"Main Test Suite\" verbose=\"1\" >\n" 
					+ "<test name=\"test\">\r\n" 
					+ "\t<classes>\r"
					+ "		" + "<class name= \"api." + projectName  + "." + moduleFolder + subModule + ".apitestclasses" + "." + Utils.removeUnWantedChars(toTitleCase(testCaseName))+ "\" />"
					+"\r\t\t<class name=\"com.fileupload.ReportUpload\" />"
					+ "\n" + "\t</classes>\r\n" + "" + "</test>\n" + "" + "</suite>";
			writeFile(testngFile, "testng", environmentVariable, ".xml");
		}

		String[] requestTypes = new String[] { "", "GET", "POST", "PUT", "DELETE" };
		
		
		/* --------------------------------------- DATASET(s) CREATION---------------------------------------------------------- */

		JSONArray jsonArray = jsonObject.getJSONArray("datasets_of_testcase");
		
		String names = "";
		for(int i = 0; i < jsonArray.length(); i++) {
			//names = names + (names.isEmpty() ? "" : "\",\"") + jsonArray.getJSONObject(i).optString("dataset_name");
			configProperties += "\n\tdataset"+ (i + 1) + " = " + jsonArray.getJSONObject(i).optString("dataset_name") ;
		}
		System.err.println(names);
		configProperties += "\n\tdatasetsLength = " + jsonArray.length() +"\n\t";	
			for (int dataSet = 0; dataSet < jsonArray.length(); dataSet++) { // this loop for testcase datasets
				
				JSONObject jsonObj = jsonArray.getJSONObject(dataSet);

				JSONArray apisArray = jsonObj.getJSONArray("apis");
				
				for (int apis = 0; apis < apisArray.length(); apis++) { // this loop for api's 

					JSONObject datasetObj = apisArray.getJSONObject(apis);
					
					String apiName =  Utils.removeUnWantedChars(toTitleCase(datasetObj.getString("api_name")));
					String prjctName = "\n\npublic static String projectName = \"" + projectName + "\";";

					dataset = "\npublic static final int datasetLength = " + jsonArray.length() + "; "
							+ prjctName;

					String body = "{}";
					int reqType = datasetObj.optInt("request_type");
					int bodyType = datasetObj.optInt("body_type");
					
					if (reqType > 1) {
						if (bodyType == 3) {
							body = datasetObj.getString("body_raw");
						} /*
							 * else if(reqType == 3) { //form-data String body =
							 * jsonObj.optJSONArray("form-data").toString(); } else if(reqType == 4) {
							 * //form-data{} String body = jsonObj.optJSONArray("form-data").toString(); }
							 */
					}
					
					int count =  dataSet + 1;
					
					/* datset is creating first time rewriting the file otherwise append the data based on dataset flag */
					if(dataSet == 0) {
						isDataset = true;
					} else {
						isDataset = false;
					}
					
					String datasetName = jsonObj.optString("dataset_name");

					String dataName = "public static final String datasetHeader" + (count) + " = \""
							+ primaryInfoObj.optString("testcase_name") + "-" + datasetName + "\";";

					String datasetResources = "public static final String datasetResources" + (count) + " = \""
							+ datasetObj.optString("dataset_resource") + "\";";
					
					String apiNamecurrent = "public static final String apiName" + (count) + " = \""
							+ datasetObj.optString("api_name") + "\";";
					String reqstType = "public static final int requestType" + (count) + " = " + reqType + ";";

					String bdtType = "public static final int bodyType" + (count) + " = " + bodyType + ";";
					String jsonFormat = ",\"\r\n + \"";
					
					
					String urlParams = datasetObj.optJSONArray("params_list") == null ? new JSONArray().toString():datasetObj.optJSONArray("params_list").toString();
					String headers = datasetObj.optJSONArray("headers_list") == null ? new JSONArray().toString():datasetObj.optJSONArray("headers_list").toString();
					
					
					
					//=========x-www-formurl-encode
					JSONArray formurlencodeObj =  datasetObj.optJSONArray("body_form_url_encoded_list");
					String formurlEncodedData = datasetObj.optJSONArray("body_form_url_encoded_list") == null ? new JSONArray().toString():datasetObj.optJSONArray("body_form_url_encoded_list").toString();
					
					//=========form-data
					JSONArray formDataObj =  datasetObj.optJSONArray("body_form_data_list");
					String formData = formDataObj == null ? new JSONArray().toString():formDataObj.toString();
					
					
					//===== Authorization bearer=====
					JSONObject authJsonData = datasetObj.optJSONObject("auth") == null ? new JSONObject():datasetObj.optJSONObject("auth");
					String autData = authJsonData.optString("auth_data");
					String authData = "";
					if(!autData.isEmpty()) {
					    authData = "public static final String authenticationData" + (count) + " = \"" + StringEscapeUtils.escapeJava(autData).replaceAll(",", jsonFormat) + "\";\n";
					} 
					String  data = "";
					if(formurlencodeObj != null) {
						  data = "\n public static String formurlEncodedData" + (count) + "  = \""
								+ StringEscapeUtils.escapeJava(formurlEncodedData).replaceAll(",", jsonFormat) + "\";\n";
								
					}
					
					if(formDataObj != null) {
						  data = "\n public static String formData" + (count) + "  = \""
								+ StringEscapeUtils.escapeJava(formData).replaceAll(",", jsonFormat) + "\";\n";
								
					}
					String apiLinkageParamters = datasetObj.optJSONArray("apiLinkProperties") == null ? new JSONArray().toString():datasetObj.optJSONArray("apiLinkProperties").toString();
					String apiSuccessParamters = datasetObj.optJSONArray("apiSuccessProperties") == null ? new JSONArray().toString():datasetObj.optJSONArray("apiSuccessProperties").toString();
						
					//========= Link Params
					String linkParams = "";
					if(datasetObj.optJSONArray("apiLinkProperties") != null) {
						linkParams = "\n\n public static String linkParams" + (count) + "  = \""
								+ StringEscapeUtils.escapeJava(apiLinkageParamters).replaceAll(",", jsonFormat) + "\";\n";
					}
					
					
					//========= Status Params
					String statusParams = "";
					if(datasetObj.optJSONArray("apiSuccessProperties") != null) {
						statusParams = "\n\n public static String statusParams" + (count) + "  = \""
								+ StringEscapeUtils.escapeJava(apiSuccessParamters).replaceAll(",", jsonFormat) + "\";\n";
					}
					
					
					String appendDataSet = "\t\t\t/*--------" + datasetName + " Dataset-----------*/" + "\n\n"
							+ dataName 
							+ "\n\n" + datasetResources 
							+ "\n" + reqstType 
							+ "\n" + bdtType
							+  "\n" + apiNamecurrent
							+ "\n\n public static String urlParams" + (count) + "  = \""
							+ StringEscapeUtils.escapeJava(urlParams).replaceAll(",", jsonFormat) + "\";\n"
							+ "\n public static String headers" + (count) + "  = \""
							+ StringEscapeUtils.escapeJava(headers).replaceAll(",", jsonFormat) + "\";\n"
							+ data
							+ linkParams
							+ statusParams
							+ "\n" + authData + "\n"
							+ "\n public static String body" + (count) + "  = \""
							+ StringEscapeUtils.escapeJava(body).replaceAll(",", jsonFormat) + "\";\n" + "\r\n";

					String jsonClass = "package api." + projectName  + "." + moduleFolder + subModule + ".apipageclasses; "
							+ "\n\npublic class " + apiName + "Dataset" + "{\r\n" 
							+ dataset + "\n\r\n" + "";
					
					
					writeDatasetFile(jsonClass, pageClassPath, appendDataSet, apiName + "Dataset");
					
				}

			}

		

		/*--------------------------------------- TESTCASE CREATION ----------------------------------------------------------*/
		String TAB_FEED = "\t\t\t";
		String importPackages = "package api." + projectName  + "." + moduleFolder + subModule + ".apitestclasses;\n" 
				+ "\nimport com.restassured.services.*;" 
				+ "\nimport org.testng.annotations.Test;"
				+ "\nimport com.utilities.BaseClass;"
				+ "\nimport com.utilities.QFCustomizedCode;"
				+ "\nimport com.utilities.ConfigFilesUtility;";
				
		configProperties += "PrimaryInfo = " + primaryInfo +"\n\t";		
		/* --------------------------------------- CONFIGURATION FILE (apiconfig.properties)------------------------------------------------------- */

		/* Generating the config.properties file in to framework */
		configProperties = configProperties  + "BASEURL = " + baseURL;
		writeFile(configProperties, testCaseName, environmentVariable + File.separator +  "ConfigFiles" , ".properties");
		
		if (executeAction) {
			File classisExists = new File(String.valueOf(testClassPath + File.separator +  Utils.removeUnWantedChars(toTitleCase(testCaseName)) + ".java"));
			if (classisExists.exists() && !testcaseOverwrite) {
				return;
			}
		}
		
		
		String startClass = "\npublic class " + Utils.removeUnWantedChars(toTitleCase(testCaseName)) + " extends BaseClass {"
				+ "\n\tprivate ConfigFilesUtility configFileObj;\n";
		
		String apiConfigFileName = testCaseName +".properties";

		String methods = "";
		String primary_info = "";
		String serviceParams = "";
		for (int dataSet = 0; dataSet < 1; dataSet++) { // this loop for testcase dataset's

			JSONObject jsonObj = jsonArray.getJSONObject(dataSet);
			JSONArray apisArray = jsonObj.getJSONArray("apis");
		
			for (int apis = 0; apis < apisArray.length(); apis++) { // this loop for api's

				JSONObject datasetObj = apisArray.getJSONObject(apis);
				String apiName = Utils.removeUnWantedChars(toTitleCase(datasetObj.getString("api_name")));
				String objectName = apiName + "Dataset";
				
				importPackages = importPackages + "\nimport api." + projectName  + "." + moduleFolder + subModule + ".apipageclasses." + objectName + ";";
				
				reportName =  "\"" + (isTestSet ? testsetName : testCaseName) +"\"";

				primary_info = "\n\t\tfor(int i = 1; i <= configFileObj.getIntProperty(\"datasetsLength\"); i++) {"
						 							//	+ "\n\n\t\t\tsetTestcaseName(\"" + toTitleCase(testCaseName)+ " - \" + configFileObj.getProperty(\"dataset\" +i));" 
						 								+ "\n"+TAB_FEED + "boolean isExecutionFlag = true;";
				
				int reqType = datasetObj.optInt("request_type");
				
				String comment = "\t\t/*--------------------- " + requestTypes[reqType] + " --------------------- */\n";
				JSONObject authJsonData = datasetObj.optJSONObject("auth") == null ? new JSONObject():datasetObj.optJSONObject("auth");
				
				String autData = authJsonData.optString("auth_data");
				String authData = "";
				if(!autData.isEmpty()) {
					authData =  TAB_FEED + "	String authenticationData = (String) c.getField(\"authenticationData\" + i).get(object);\r\n";
				} 
				
				JSONArray formurlencodeObj =  datasetObj.optJSONArray("body_form_url_encoded_list");
				String data = "";
				if(formurlencodeObj !=  null) {
					data =  TAB_FEED + "	String formurlEncodedData = (String) c.getField(\"formurlEncodedData\" + i).get(object);\r\n";
				}
				//=========form-data
				JSONArray formDataObj =  datasetObj.optJSONArray("body_form_data_list");
				if(formDataObj !=  null) {
					data =  TAB_FEED + "	String formData = (String) c.getField(\"formData\" + i).get(object);\r\n";
					
				}
				
				String linkParams = ""; 
				if(datasetObj.optJSONArray("apiLinkProperties") != null) {
					linkParams =  TAB_FEED + "	String linkParams = (String) c.getField(\"linkParams\" + i).get(object);\r\n";
					
				}
				
				String statusParams = ""; 
				if(datasetObj.optJSONArray("apiSuccessProperties") != null) {
					statusParams =  TAB_FEED + "	String statusParams = (String) c.getField(\"statusParams\" + i).get(object);\r\n";		
				}
				
				String params = TAB_FEED +"if(isExecutionFlag) {\r\n "
					+ TAB_FEED + " \tObject object = (Object) new " + objectName + "();\r\n"
					+ TAB_FEED + "	Class<?> c = object.getClass();\r\n"
					+ TAB_FEED + "	String urlParams = (String) c.getField(\"urlParams\" + i).get(object);\r\n"
					+ TAB_FEED + "	if(urlParams == null) return;\n"
					+ linkParams
					+ statusParams
					+ TAB_FEED + "	String headers = (String) c.getField(\"headers\" + i).get(object);\r\n"
					+  data
					+ TAB_FEED + "	String body = (String) c.getField(\"body\" + i).get(object);\r\n"
					+ authData
					+ TAB_FEED + "	String apiName = (String) c.getField(\"apiName\" + i).get(object);\r\n"
					+ TAB_FEED + "	String datasetResources = (String) c.getField(\"datasetResources\" + i).get(object);\r\n"
					+ TAB_FEED + "	int reqType = (Integer) c.getField(\"requestType\" + i).get(object);\r\n" 
					+ TAB_FEED + "	int bodyType = (Integer) c.getField(\"bodyType\" + i).get(object);\r\n"	
					+ TAB_FEED + "	String header = (String) c.getField(\"datasetHeader\" + i).get(object);\r\n"
					+ TAB_FEED + "	String response = APIService.callRequest(configFileObj,apiName, urlParams, headers, reqType, bodyType, body, configFileObj.getProperty(\"dataset\" +i), datasetResources, authenticationData, formurlEncodedData, formData, linkParams,statusParams);\r\n"
					+ TAB_FEED + "	QFCustomizedCode.customCode(apiName, body, response,\""+projectName+"\");\r\n"
					+ TAB_FEED + "}\n";
			
				serviceParams = serviceParams + "\n" + comment + "\n" + params;
			}
			
		}
		
		String method = "\n\n\t@Test(priority = 1)" 
				+ "\n\tpublic void " + requestType.toLowerCase()
				+   "doRequest() throws Exception { "
				
				+ "\n\t" + primary_info + TAB_FEED + serviceParams + "\t\t" + "}\n";
		methods = methods + method +"\t}}\n";
		
		

		String constructor = "\n\tpublic " + Utils.removeUnWantedChars(toTitleCase(testCaseName)) + "() throws Exception {"
				+ "\n\t\tconfigFileObj= new ConfigFilesUtility();"
				+ "\n\t\tconfigFileObj.loadPropertyFile(\""+ apiConfigFileName + "\");" 
				+ "\n\t" 
				+ "}";

		String flushReportFile = "";
	

		String createClass = importPackages + startClass  + constructor + methods + flushReportFile;
		
		/*
		 * Generating the scripts in to client machine framework(i.e., AUTOMATION_PATH)
		 */
		
			writeFile(createClass, Utils.removeUnWantedChars(toTitleCase(testCaseName)), testClassPath, ".java");
		

	
	
		/*--------------------------------------- EXTENT REPORTS FILE (extent-config.xml) ---------------------------------------------------------- */

		//String extentReportConfigFile = customizeTestngConfigFile(isTestSet ? testsetName : testCaseName); 
		//writeFile(extentReportConfigFile, "extent-config", environmentVariable, ".xml");
		
	}
	
	/**
	 * Writing the screens
	 * @param startingClassAppendData
	 * @param filePath
	 * @param data
	 * @param className
	 * @throws Exception
	 */
	private void writeDatasetFile( String startingClassAppendData, String filePath, String data, String className) throws Exception {
		
		File classFile = new File(filePath + File.separator +  className + ".java");
		if (classFile.exists() && !isDataset) {

			/* Input appending the specific line */

			Path path = Paths.get(filePath, className + ".java");

			List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

			// position of appending the line

			int position = lines.size() - 1;
			
			lines.add(position, data);
			Files.write(path, lines, StandardCharsets.UTF_8);
		} else {
			writeFile(startingClassAppendData + data + "}", className, filePath, ".java");
			isDataset = false;
		}
	}


	/* Creating directories */
	
	private boolean createDirectory(String directory) {
		File fileDirectory = new File(directory);
		if (!fileDirectory.exists()) {
			fileDirectory.mkdir();
			return true;
		}
		return false;
	}


	private String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].trim().equals(""))
				continue;
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append("");
		}
		return sb.toString().trim();
	}

	private void writeFile(String str, String className, String filePath, String fileExtension)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(str);
		} finally {
			out.close();
		}
	}

}
