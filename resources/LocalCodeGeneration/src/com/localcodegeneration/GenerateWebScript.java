package com.localcodegeneration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import com.dto.DatasetsOfTestcase;
import com.dto.Element;
import com.dto.PrimaryInfo;
import com.dto.Screen;
import com.dto.WebScript;
import com.google.gson.Gson;
import com.utils.Utils;


 /**
 * This class used to generating the Web Testcase's and Dataset's 
 * @TESTNG bat/sh file creation
 * @Tescase File Creation
 * @Datasets Creation
 * 
 */

public class GenerateWebScript {
	
	private String webPageClasses;
	private String webTestClasses;
	private PrimaryInfo primaryInfo;
	private String screenObjects = "";
	private String pageClassPath = "";
	private String projectName;
	private String browsers;

	@SuppressWarnings("unused")
	public void generateCode(boolean testSet, WebScript output, String environmentVariable) throws Exception {
		
		boolean isDriverInIframe = false;
		boolean isDriverInFrame = false;
		String previousIframeName = "";
		String previousFrameName = "";
		screenObjects = "";
		primaryInfo =  output.getPrimaryInfo();
		String testcaseClassStart = "";
		String chromeDriverPath = "chromeDriverPath", URL, screenObjs = "", screenImports = "";
		String fieldType = "", fieldName = "", fieldValue = "", fieldsInfo = "", screenName = "",secondaryFieldType= "",
		propertiesInfo = "\n\t", xmlFile = "";
		URL = primaryInfo.getProjectUrl();
		String testCaseName = Utils.removeUnWantedChars(toTitleCase(primaryInfo.getTestcaseName()));
		
		projectName = Utils.removeUnWantedChars(primaryInfo.getProjectName()).replaceAll(" ", "").toLowerCase();
		int projectId = primaryInfo.getProjectId();
		String reportsPath = "reportsPath";
		boolean executeAction = primaryInfo.getIsExecute();
		boolean generateAction = primaryInfo.getIsGenerate();
		boolean testcaseOverwrite = primaryInfo.isTestcase_overwrite();
		browsers = primaryInfo.getBrowserType();
		String baseURL = primaryInfo.getProjectUrl();
		long timeStamp = primaryInfo.getExecutedTimestamp();
		String moduleFolder = Utils.removeUnWantedChars(primaryInfo.getModuleName().toLowerCase());
		String subModule = primaryInfo.getSubModuleName() == null ? "" : primaryInfo.getSubModuleName();
		String subModuleFolder = Utils.removeUnWantedChars(subModule.toLowerCase());
		String testSetName = primaryInfo.getTestsetName() == null ? "" : primaryInfo.getTestsetName();
		String testsetName = Utils.removeUnWantedChars(testSetName);
		String moduleDescription = primaryInfo.getModuleDescription();
		String mobileWebBrowser = Utils.removeUnWantedChars(toTitleCase(primaryInfo.getTestcaseName()));
		String mobileType = primaryInfo.getMobileType();
		propertiesInfo += "PrimaryInfo = " + StringEscapeUtils.escapeJava(new Gson().toJson(primaryInfo)) +"\n\t";
		
		String modulename = moduleFolder + ".";
		if (!subModuleFolder.equalsIgnoreCase("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			modulename = modulename + subModuleFolder + ".";
		}
		String webFolder = "";
		if(primaryInfo.getMobilePlatform().equalsIgnoreCase("Android") || primaryInfo.getMobilePlatform().equalsIgnoreCase("iOS")) {
			propertiesInfo += "mobilePlatform = " +  primaryInfo.getMobilePlatform() +"\n\t";
			propertiesInfo += "type = " +  primaryInfo.getMobileType() +"\n\t";
			propertiesInfo += "executionEnvironment = " +  primaryInfo.getExecutionEnvironment() +"\n\t";
			webPageClasses = "mobile." + projectName + "." + modulename + "webpageclasses";
			webTestClasses = "mobile." + projectName + "." + modulename + "webtestclasses";
			webFolder = environmentVariable + File.separator + "src"+ File.separator + "test"+ File.separator + "java"+ File.separator + "mobile";
		} else {
			webPageClasses = "web." + projectName + "." + modulename + "webpageclasses";
			webTestClasses = "web." + projectName + "." + modulename + "webtestclasses";
			webFolder = environmentVariable + File.separator + "src"+ File.separator + "test"+ File.separator + "java"+ File.separator + "web";
		}
		
		

		createDirectory(webFolder);

		String projectFolder = webFolder + File.separator +  projectName;
		createDirectory(projectFolder);

		String packageNameFolder = projectFolder + File.separator +  moduleFolder;
		// create module
		createDirectory(packageNameFolder);

		// create sub module
		if (!subModuleFolder.equalsIgnoreCase("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) {
			packageNameFolder = packageNameFolder + File.separator +  subModuleFolder;
			createDirectory(packageNameFolder);
		}

		String testClassPath = packageNameFolder + File.separator + "webtestclasses";
		pageClassPath = packageNameFolder + File.separator +  "webpageclasses";

		/* Creating directories if not exists */
		createDirectory(String.valueOf(testClassPath));

		createDirectory(String.valueOf(pageClassPath));
		 	
		propertiesInfo += "URL = " + URL + "\n\t";
		if(!testSet) {
			String[] browsersList = browsers.split(",");
			String finalSet = "";
			for (int i = 0; i < browsersList.length; i++) {
				String browserName = browsersList[i];
				finalSet = finalSet + "	\n\t<test name=\"" + browserName + "test\">\r\n" + 
						   "  	<parameter name=\"browser\" value=\"" + browserName + "\"/>\r\n" 
							+ "\t\t<classes>"
							+ "\n\t\t\t<class name=\"" + webTestClasses +"."
							+ testCaseName + "\" />"
							+ "\n\t\t\t<class name=\"com.fileupload.ReportUpload\" />"
							+ "\n\t\t</classes>\n"
							+ "	</test>\r\n";
			}
			xmlFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
					+ "<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >"
					+ "\n<suite name=\"Main Test Suite\" parallel=\"tests\">"
					+ finalSet
					+ "</suite>";
			
			writeFile(xmlFile, "testng", environmentVariable, ".xml");
		}
		
		File directory = new File(String.valueOf(testClassPath));
		if (!directory.exists()) {
			directory.mkdir();
		}
		
		int screensCount = 0;	
		List<DatasetsOfTestcase> datasets = output.getDatasetsOfTestcase();
		int size = datasets.size();
		String names = "";
		for(int i = 0; i < size; i++) {
			names = names + (names.isEmpty() ? "" : "\",\"")  + datasets.get(i).getDatasetName();
		}
		//String datasetNames = "\n\tString[] datasetNames = new String[] {\"" + names + "\"};";
		System.out.println(names);
		boolean isScreensCreated =  false;
		if(!testSetName.isEmpty()) {
		propertiesInfo += "testSetName = " + testSetName +"\n\t";
		}
		propertiesInfo += "datasetsLength = " + size +"\n\t";
		/* This loop for generating config file and Screens Creating (GenerateSeleniumScreenObjectScript(screenName, fieldsInfo)) */
		
		for(int i = 0; i < size; i++) {
			if (i == 1) {
				isScreensCreated = true;
			}
			screensCount = screensCount + 1;
			int datasetScreencount = 0;
			propertiesInfo += "dataset" + screensCount + " = " + datasets.get(i).getDatasetName() + "\n\t";
			for (Screen screen : datasets.get(i).getScreens()) {
				datasetScreencount = datasetScreencount + 1;
				screenName = Utils.removeUnWantedChars(toTitleCase(screen.getName()));
				fieldsInfo = "";
				for (Element elements : screen.getScreenElements()) {
					
					String elementFieldType = elements.getFieldType();
					String secondaryElementFieldType = elements.getSecondaryFieldType();
					String elementFieldName = elements.getFieldName();
					String elementFieldValue = elements.getFieldValue();
					
					
					if (elementFieldType.equalsIgnoreCase("InputText")) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elementFieldName)) + screensCount + datasetScreencount + " = " + StringEscapeUtils.escapeJava(elementFieldValue) + "\n\t";
					} else if (elementFieldType.equalsIgnoreCase("Label") && elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elementFieldName))  + screensCount +  datasetScreencount +  " = " + StringEscapeUtils.escapeJava(elementFieldName) + "\n\t";
					} else if ((elementFieldType.equalsIgnoreCase("Link")||elementFieldType.equalsIgnoreCase("Link")) && elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elementFieldName))  + screensCount +  datasetScreencount +  " = " + StringEscapeUtils.escapeJava(elementFieldName) + "\n\t";
					} else if ((elementFieldType.equalsIgnoreCase("Button")||elementFieldType.equalsIgnoreCase("Button")) && elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elementFieldName))  + screensCount +  datasetScreencount +  " = " + StringEscapeUtils.escapeJava(elementFieldName) + "\n\t";
					} else  if (elements.getIsSelected()) {
						propertiesInfo += Utils.removeUnWantedChars(toTitleCase(elementFieldName))  + screensCount +  datasetScreencount +  " = " + StringEscapeUtils.escapeJava(elementFieldName) + "\n\t";
					}
					
					String xpathFinal = "";
					String preferedField = elements.getPreferedField();
					
					xpathFinal = elements.getSelectedXpath();
   
					if (fieldsInfo.isEmpty()) {
						fieldsInfo += elementFieldName 
								+ "#sep#" + elementFieldType 
								+ "#sep#" + secondaryElementFieldType 
								+ "#sep#" + elementFieldValue 
								+ "#sep#" + xpathFinal 
								+ "#sep#" + elements.getIsSelected() 
								+ "#sep#" + elements.getTagFieldName() 
								+  "#sep#" +elements.getWebPageElementId() 
								+  "#sep#" + elements.isClick() 
								+ "#sep#" + elements.getDropdownValue() 
								+  "#sep#" + "" 
								+  "#sep#" + elements.getPrefered_field() 
								+  "#sep#" + elements.isIs_optionalElement();
					} else {
					fieldsInfo += "&&" 
					+ elementFieldName 
					+ "#sep#" + elementFieldType 
					+ "#sep#" + secondaryElementFieldType 
					+ "#sep#"+ elementFieldValue 
					+ "#sep#" + xpathFinal 
					+ "#sep#" + elements.getIsSelected() 
					+ "#sep#" + elements.getTagFieldName() 
					+ "#sep#" +elements.getWebPageElementId() 
					+ "#sep#" + elements.isClick() 
					+ "#sep#" + elements.getDropdownValue() 
					+ "#sep#" + "" 
					+ "#sep#" + elements.getPrefered_field() 
					+ "#sep#" + elements.isIs_optionalElement() ;
					}

				}
				
				if (!isScreensCreated) {
					screenObjects =  screenObjects + "\t\t\tif(isElementDispalyed) {"  + screenName + "Test(datasets);}\n";
					GenerateSeleniumScreenObjectScript(screenName, fieldsInfo);
					screenObjs += "public " + screenName + " obj" + screenName + ";\n\t";
					screenImports += "import " + webPageClasses + "." + screenName + ";\n";
				}
			}
		}
		// Config files
		writeFile(propertiesInfo, testCaseName.toLowerCase(), environmentVariable + File.separator + "ConfigFiles", ".properties"); // writing the config file
		
		/*------------------------------------------ TestCase -------------------------------------------------------*/

		File classisExists = new File(String.valueOf(testClassPath + File.separator + toTitleCase(testCaseName) + ".java"));
		if (classisExists.exists() && !testcaseOverwrite) {
			return;
		} 
		String switchIframe = "";
		String switchFrame = "";
		String imports = "package " + webTestClasses + ";"
				+ "\nimport org.apache.log4j.Logger;"
				+ "\nimport org.apache.log4j.PropertyConfigurator;"
				+ "\nimport org.openqa.selenium.support.PageFactory;"
				+ "\nimport org.testng.annotations.Test;"
				+ "\nimport org.testng.annotations.BeforeTest;\r\n"
				+ "import org.testng.annotations.Parameters;\n"
				+ screenImports
				+ "import com.utilities.BaseClass;"
				+ "\nimport com.utilities.ConfigFilesUtility;"
				+ "\nimport com.utilities.Utilities;\n"
				+ "import org.json.JSONObject;\n\n";
		
		String startClass = "@SuppressWarnings(\"unused\")\npublic class " + testCaseName + " extends BaseClass {"
				+ "\n\tprivate Logger logger;"
				+ "\n\tprivate ConfigFilesUtility configFileObj;"
				+ "\n\tprivate String browserName = \"chrome\";"
				+ "\n\tpublic boolean isElementDispalyed = false;\n";
		
		List<Screen>  screens = datasets.get(0).getScreens();		
				
		String constructor =  "\n\n\tpublic " + testCaseName + "() throws Exception {"
				+ "\n\t\tPropertyConfigurator.configure(\"log4j.properties\");"
				+ "\n\t\tlogger = Logger.getLogger("+ testCaseName + ".class);"
				+ "\n\t\tconfigFileObj = new ConfigFilesUtility();"
				+ "\n\t\tconfigFileObj.loadPropertyFile(\"" + testCaseName.toLowerCase() + ".properties\");"
				
				+ "\n\t}";
	
		String multiBrowserSetup = "\n\n\t@BeforeTest\r" + 
				"\n\t@Parameters(\"browser\")\r" + 
				"\n\tpublic void browserName(String browser) throws Exception {\r\n" + 
				"\t\tbrowserName = browser;\r\n" +
				"\t}\n"; 
		
		String setUp = "";
		if(!primaryInfo.getMobilePlatform().equalsIgnoreCase("Android") && !primaryInfo.getMobilePlatform().equalsIgnoreCase("iOS")) {
		 setUp = multiBrowserSetup + "\n\t\n\tpublic void setUP() throws Exception "
				+ "{"
				+ "\n\t\tdriver = launchBrowser(browserName, configFileObj);\n"
			//	+ "\t\tprintSuccessLogAndReport(test, logger, \"Browser Name : \" + browserName);" 
				+ "\t}\n";
		}
		testcaseClassStart += imports + startClass + constructor + setUp;
		int datasetScreencount = 0;
		for (Screen screen : screens) {
			datasetScreencount = datasetScreencount + 1;
			screenName = Utils.removeUnWantedChars(toTitleCase(screen.getName()));
			String screenMethodStart = ""
					+ "\n\tpublic void " + screenName + "Test(int i) throws Exception "
					+ "{\n\n\t"
					+ " try{"
					+ "\n\t\t"
					+ "int datasetScreencount = " + datasetScreencount + ";"
					+ "\n\t\t"
					+ screenName +" obj" + screenName + " = PageFactory.initElements(driver, " + screenName + ".class);"
					+ "\n\t\ttestLogHeader(\"Verify " + screenName + " page\");\n";
			String screenMethodImpl = "";
			int elementCount = 0;
			String datasetCount = "\"+ i + datasetScreencount";
			
			for (Element elements : screen.getScreenElements()) {
				elementCount = elementCount + 1;
			
				fieldName = Utils.removeUnWantedChars(toTitleCase(StringEscapeUtils.escapeJava(elements.getFieldName())));
				fieldValue = elements.getFieldValue();
				fieldType = elements.getFieldType();
			
				secondaryFieldType = elements.getSecondaryFieldType();
				String dropDownValue = elements.getDropdownValue();
				String tagName = Utils.removeUnWantedChars(elements.getTagFieldName());
				String elementOrder = "";
				boolean isValidate = elements.getIsSelected();
				boolean isClick = elements.isClick();
				boolean isIframe = elements.getIsIframe();
				String iframeName = elements.getIframeName() == null ? "" : elements.getIframeName();
				String iframeID = elements.getIframeId() == null ? "" : elements.getIframeId();
				int iframePosition = elements.getiFramePosition() == null ? -1 : elements.getiFramePosition();

					switchIframe = "";
					if ((isIframe && !isDriverInIframe)) {
						if (iframeName.equalsIgnoreCase("IFRAME")) {
							if (iframeID != null && !iframeID.trim().isEmpty()) {
								
								switchIframe = "\t\tThread.sleep(1000);\n\t\tdriver.switchTo().frame(\"" + iframeID + "\");\n";
							} else {
								switchIframe = "\t\t" + "switchToIframe(\"" + elements.getSelectedXpath() + "\");\n";
							}
						} else {
							if (iframeID != null && !iframeID.trim().isEmpty()) {
								switchIframe = "\t\tThread.sleep(1000);\n\t\tdriver.switchTo().frame(\"" + iframeID + "\");\n";
							} else {
								switchIframe = "\t\tThread.sleep(1000);\n\t\tdriver.switchTo().frame(\"" + iframeName + "\");\n";
							}
						}
						previousIframeName = iframeName;
						isDriverInIframe = true;
					} else if ((isIframe && isDriverInIframe)) {
						switchIframe = "";
					} else {
						if ((!isIframe && isDriverInIframe)) {
							switchIframe = "\t\tdriver.switchTo().defaultContent();\n";
							isDriverInIframe = false;
							previousIframeName = "";
						} else {
							isDriverInIframe = false;
							previousIframeName = "";
							switchIframe = "";
						}
					}
					
					// ====================== Skip Element ============================
					boolean isElementOptional = elements.isIs_optionalElement();
					String elementOptionalOpen = "" , elementOptionalClosed = "";
					if(isElementOptional) {
						elementOptionalOpen = "\n\t\tif(skipifElementisNotDisplayed()) {";
						elementOptionalClosed = "\n\t\t}";
					}
					
					boolean isFrame = elements.getIsFrame();
					String frameName = elements.getFrameName() == null ? "" : elements.getFrameName();
					String frameID = elements.getFrameId() == null ? "" : elements.getFrameId();
					int framePosition = elements.getFramePosition() == null ? -1 : elements.getFramePosition();
				
					switchFrame = "";
					if ((isFrame && !isDriverInFrame)) {
						if (frameName.equalsIgnoreCase("FRAME")) {
							if (frameID != null && !frameID.trim().isEmpty()) {
								switchFrame = "\t\tdriver.switchTo().frame(\"" + frameID + "\");\n";
							} else {
								switchIframe = "\t\t" + "switchToIframe(\"" + elements.getSelectedXpath() + "\");\n";
							}
						} else {
							if (frameID != null && !frameID.trim().isEmpty()) {
								switchFrame = "\t\tdriver.switchTo().frame(\"" + frameID + "\");\n";
							} else {
								switchFrame = "\t\tdriver.switchTo().frame(\"" + frameName + "\");\n";
							}
						}
						previousFrameName = frameName;
						isDriverInFrame = true;
					} else if ((isFrame && isDriverInFrame)) {
						switchFrame = "";
					} else {
						if ((!isFrame && isDriverInFrame)) {
							switchFrame = "\t\tdriver.switchTo().defaultContent();\n";
							isDriverInFrame = false;
							previousFrameName = "";
						} else {
							isDriverInFrame = false;
							previousFrameName = "";
							switchFrame = "";
						}
					}
					
					if (tagName.equalsIgnoreCase("table")) {
						
						
						screenMethodImpl += switchIframe + switchFrame +  "\t\ttableDataHandle(\"" + elements.getSelectedXpath() + "\");"+ "\n\t\t";
						
//							screenMethodImpl += switchIframe + switchFrame + "\t\tdropDownHandle(\"" + elements.getSelectedXpath() + "\",\"" +  elements.getDropdownValue() + "\");"
//								+ "\n\t\tprintInfoLogAndReport(test, logger, \"Clicked on " + toTitleCase(fieldName) + " dropdown and selected " + elements.getDropdownValue() + "\");"
//								+ "\n\t\t";
					} else if (tagName.equalsIgnoreCase("select")) {
						
						
						screenMethodImpl += switchIframe + switchFrame +  "\t\tobj" + screenName + ".clk" + tagName + Utils.removeUnWantedChars(toTitleCase(fieldName))  + "_" + elements.getWebPageElementId() + "();"
								+  elementOptionalOpen  + "\n\t\tprintInfoLogAndReport(logger, \"Clicked on " + toTitleCase(fieldName) + " dropdown and selected " + elements.getDropdownValue() + "\");"+ elementOptionalClosed
								+ "\n\t\t";
						
//							screenMethodImpl += switchIframe + switchFrame + "\t\tdropDownHandle(\"" + elements.getSelectedXpath() + "\",\"" +  elements.getDropdownValue() + "\");"
//								+ "\n\t\tprintInfoLogAndReport(test, logger, \"Clicked on " + toTitleCase(fieldName) + " dropdown and selected " + elements.getDropdownValue() + "\");"
//								+ "\n\t\t";
					} else if (fieldType.equalsIgnoreCase("Button")) {
						if (isValidate) {
							String isActionClick = "";
							if(isClick) {
								isActionClick =    elementOptionalOpen  + "\n\t\t\tprintSuccessLogAndReport(logger,  \"Clicked on Button : \" + configFileObj.getProperty(\""+ Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));"+ elementOptionalClosed;  
							}
							
						screenMethodImpl += switchIframe + switchFrame +  "\tString text" + elementCount + " = obj" + screenName + ".clk" + tagName + Utils.removeUnWantedChars(toTitleCase(fieldName)+ "_" + elements.getWebPageElementId()) 
								+ "();"
								+ elementOptionalOpen  
								+ "\n\t\t" 
								+ "if(text" + elementCount + ".contains(configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  + ")))"
								+ "{"
								+ isActionClick
								+ "\n\t\t\tprintSuccessLogAndReport(text" + elementCount + ",  logger,  \"Validated Button Text : \" + configFileObj.getProperty(\""+Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" 
								+ "\n\t\t"
								+ "} else {" 
								+ "\n\t\t\tprintFailureLogAndReport(text" + elementCount + ",  logger,  \"Text is displayed as : \" + configFileObj.getProperty(\""+Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" 
								+ "\n\t\t"
								+ "}\n" + elementOptionalClosed;
						} else {
							String isActionClick = "";
							if(isClick) {
								isActionClick = elementOptionalOpen  + "\n\t\tprintInfoLogAndReport(logger, \"Clicked on " + toTitleCase(fieldName) + "Button\");"+ elementOptionalClosed;
							}
						screenMethodImpl += switchIframe + switchFrame + "\t\tobj" + screenName + ".clk"+ tagName + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "_" + elements.getWebPageElementId() + "();"
								+ isActionClick
								+ "\n\t\t";
					}
				} else if (fieldType.equalsIgnoreCase("Link")) {
					String order =  "";
					if(tagName.equalsIgnoreCase("select")) {
						order = ""+elementOrder;
					} else {
						order = "";
					}
					
					if (isValidate) {
						String isActionClick = "";
						if(isClick) {
							isActionClick =   elementOptionalOpen  +"\n\t\t\tprintSuccessLogAndReport(text" + elementCount + ",  logger,  \"Clicked on : \" + configFileObj.getProperty(\""+ Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));"+ elementOptionalClosed;
						}
						screenMethodImpl += switchIframe + switchFrame + "\t\tString text" + elementCount + " = obj" + screenName + ".clk"
								+ tagName + Utils.removeUnWantedChars(toTitleCase(fieldName)) + order +  "_" + elements.getWebPageElementId() +"();"
								+ elementOptionalOpen  
								+ "\n\t\t" 
								+ "if(text" + elementCount + ".contains(configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount + ")))"
								+ "{"
								+ isActionClick
								+ "\n\t\t\tprintSuccessLogAndReport(text" + elementCount + ",  logger,  \"Validated Link Text : \" + configFileObj.getProperty(\""+Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" 
								+ "\n\t\t} else {" 
							    + "\n\t\t\tprintFailureLogAndReport(text" + elementCount + ",  logger,  \"Link Text is not displayed  : \" + configFileObj.getProperty(\""+ Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" 
								+ "\n\t\t}\n" +  elementOptionalClosed;

					} else {
						String isActionClick = "";
						if(isClick) {
							isActionClick =  elementOptionalOpen + "\n\t\tlogger.info(\"Clicked on " + toTitleCase(fieldName) + " Link\");"+ elementOptionalClosed;
						}
						screenMethodImpl += switchIframe + switchFrame +  "\t\tobj" + screenName + ".clk" + tagName + Utils.removeUnWantedChars(toTitleCase(fieldName)) + order + "_" + elements.getWebPageElementId() + "();"
								+ elementOptionalOpen  + "\n\t\tprintSuccessLogAndReport( logger,  \"Text is displayed as : " + toTitleCase(fieldName) + "Link\");" + elementOptionalClosed
								+ "\n";
					}
	
				}  else if (fieldType.equalsIgnoreCase("image")) {
					if (false) {
						// Image validation

					} else {
						String isActionClick = "";
						if(isClick) {
							isActionClick = elementOptionalOpen +  "\n\t\tlogger.info(\"Clicked on " + toTitleCase(fieldName) + " Image\");"+ elementOptionalClosed;
						}
						screenMethodImpl += switchIframe + switchFrame + "\t\tobj" + screenName + ".clk" + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "_" + elements.getWebPageElementId() + "();"
								+ isActionClick
								+ elementOptionalOpen  + "\n\t\tprintInfoLogAndReport( logger, \"Image is displayed as : " + toTitleCase(fieldName) + elementOptionalClosed
								+ "Image\");"
								+ "\n";
					}
				} else if (fieldType.equalsIgnoreCase("InputText")) {
					screenMethodImpl += switchIframe + switchFrame + "\t\tobj" + screenName + ".fill" + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "_" + elements.getWebPageElementId() 
							+ "(configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount + "));"
							+ elementOptionalOpen  + "\n\t\tprintSuccessLogAndReport(logger,  \"Entered "+ toTitleCase(fieldName) + " : \" + configFileObj.getProperty(\""+Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" + elementOptionalClosed
							+ "\n";
				} else if (fieldType.equalsIgnoreCase("Label")) {
					if (isValidate) {
						
						screenMethodImpl += switchIframe + switchFrame + "String text" + elementCount + " = obj" + screenName  + ".text"+ tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "_" + elements.getWebPageElementId() + "();"
								+ elementOptionalOpen 
								+ "\n\t\t" 
								+ "if(text" + elementCount + ".contains(configFileObj.getProperty(\"" + Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount + "))){"
								+  "\n\t\t\tprintSuccessLogAndReport(text" + elementCount + ",  logger,  \"Validating Text is displayed as : \" + configFileObj.getProperty(\""+ Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" 
								+ "\n\t\t} else {" 
								+  "\n\t\t\tprintFailureLogAndReport(text" + elementCount + ",  logger,  \"Label Text is not displayed as : \" + configFileObj.getProperty(\""+ Utils.removeUnWantedChars(toTitleCase(fieldName)) + datasetCount  +"));" 
								+ "\n\t\t}\n" + elementOptionalClosed;

					} else {
						screenMethodImpl += switchIframe + switchFrame + "\t\tobj" + screenName + ".text" + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + "_" + elements.getWebPageElementId() + "();"
								+ elementOptionalOpen  + "\n\t\tprintInfoLogAndReport( logger, \"Label text value is:  "+ toTitleCase(fieldName) + "\");\n"+ elementOptionalClosed;
					}
				}
					
				if(secondaryFieldType != null && secondaryFieldType.equalsIgnoreCase("alert")) {
					screenMethodImpl = screenMethodImpl + "\t\talertHandle(true);\n";
				}
				
			}
			
			String catchBlock = "   } catch (Exception e) {\r\n" + 
					"		  isElementDispalyed = false;\r\n" + 
					"		  printFailureLogAndReport( logger,  \"Element is not found\" + e.getLocalizedMessage());\r\n" + 
				
					"		}";
			
			String closeMethod = "\n\t"+ catchBlock + "\n\t}\n\t\n";
			testcaseClassStart +=  screenMethodStart  + screenMethodImpl + closeMethod;	
		}
		
	
		
		String datasetsLoop = "";
		String flushReport = "";
		if(primaryInfo.getMobilePlatform().equalsIgnoreCase("Android") || primaryInfo.getMobilePlatform().equalsIgnoreCase("iOS")) {
		 datasetsLoop = "	 @Test(dataProvider = \"capabilities\")\r\n" + 
				"	 public void setUP(String deviceName,\r\n" + 
				"	                   String platformName,\r\n" + 
				"	                   String platformVersion,\r\n" + 
				"	                   String appiumVersion,\r\n" + 
				"	                   String deviceOrientation,\r\n" + 
				"	               	   String projectName,\r\n" + 
				"	                   String testObjId,\r\n"+
				"	                   String app\r\n"+
				"\t\t) throws Exception {\r\n" + 
				"		for(int datasets = 1; datasets <= configFileObj.getIntProperty(\"datasetsLength\"); datasets++) {"
				+ 		"\r\n\t\t\tisElementDispalyed = true;"+
				"			\r\n\t\t\tdriver = this.createDriver(deviceName,platformName, platformVersion, appiumVersion, deviceOrientation, projectName, testObjId, this.getClass().getSimpleName(), app,configFileObj );" +
				"			\r\n\t\t\tsetTestcaseName(browserName,\"" + testCaseName + " - \" + configFileObj.getProperty(\"dataset\" + (datasets)));"
				+ "\n" + screenObjects + "\n" + 
				"		}\r" + 
				"	}\n";
		 flushReport = "\n}";
		} else {
			 datasetsLoop = "\t@Test\r\n" + 
					"	public void screensTest() throws Exception {\r\n" + 
					"		for(int datasets = 1; datasets <= configFileObj.getIntProperty(\"datasetsLength\"); datasets++) {"
					+ 		"\r\n\t\t\tisElementDispalyed = true;"+
					"			\r\n\t\t\tsetUP();" +
					"			\r\n\t\t\tsetTestcaseName(browserName,\"" + testCaseName + " - \" + configFileObj.getProperty(\"dataset\" + (datasets)));"
					+ "\n" + screenObjects + "\t\t\ttearDown();\n" + 
					"		}\r" + 
					"	}\n";
			 
			  flushReport = "\n\t\n\tpublic void tearDown() throws Exception "
						+ "{"
						+ "\n\t\tdriver.quit();"
						+ "\n\t}"
						+ "\n}";
				
		}
	
		
		testcaseClassStart += datasetsLoop + flushReport;
		writeFile(testcaseClassStart, toTitleCase(testCaseName), testClassPath, ".java");

	}
	
	/*---------------------------------------- Screens -----------------------------------------------------*/
	private boolean isWindowHandle;
	private void GenerateSeleniumScreenObjectScript(String screenName, String fieldsInfo) throws IOException {

		String labelCheck = "false";
		String testcaseClassStart;
		String xpath;
		
		String fieldName, funcName;
		String  methodInsideBlock = "";
		String fieldType = "Button", fieldNamePrefix = "";
		String secondaryFieldType = "";
		String  catchBlock = "";
		String method = "";
		@SuppressWarnings("unused")
		String fieldValue = "", fieldName1 = "", text = "",returnString = "",selctedXpathKey="";
		String[] fieldValues, fieldString;
		String clickAction = "";
		String Beforestr6 = "";
		String endClass = "\n}";
		
		String packages = "package " + webPageClasses + ";"
		+"\nimport org.openqa.selenium.WebDriver;"
		+ "\nimport org.openqa.selenium.WebElement;"
		+ "\nimport org.openqa.selenium.support.FindBy;"
		+ "\nimport org.openqa.selenium.support.How;"
		+ "\nimport com.utilities.BaseClass;\n";
		
		

		String prjctName = "public static String projectName = \"" + projectName + "\";";
		
		String startClass = "\npublic class " + screenName + " extends BaseClass {"
				+ "\n\t" + prjctName
				+ "\n\tpublic WebDriver driver;"
				+ "\n\n\tpublic " + screenName +  "(WebDriver driver) "
				+ "{\n\t\t"
				+ "this.driver = driver;"	
				+ "\n\t"
				+ "}\n";
	
		String xpathStart = "\n\t@FindBy(how = How.XPATH, using = \"";
		String webEleemnt = "\")\t\n\tprivate WebElement\t";

		testcaseClassStart =  startClass;
		if (!fieldsInfo.isEmpty()) {
			fieldString = fieldsInfo.split("&&");
			for (int i = 0; i < fieldString.length; i++) {
				
				Beforestr6 = "()";
				fieldValues = fieldString[i].split("#sep#");
				secondaryFieldType = fieldValues[2]!=null ? fieldValues[2] : "";
				String windowSwitchCode = "";
				if (secondaryFieldType.equalsIgnoreCase("Window Switch")) {
					windowSwitchCode = "windowHandle(driver);\n\t\t";
					isWindowHandle = true;
				} 
				
				if(isWindowHandle && !secondaryFieldType.equalsIgnoreCase("Window Switch")) {
					windowSwitchCode =  "switchToParentWindow(driver);\n\t\t";
					isWindowHandle = false;
				}
				
				
				returnString = ";\n\tpublic String ";
				clickAction = ".click();";
				if(isWindowHandle && !secondaryFieldType.equalsIgnoreCase("Window Switch")) {
					clickAction = clickAction + "\n\t\tswitchToParentWindow(driver);";
					isWindowHandle = false;
				} 
				text = ".getText();";
				
				fieldName = StringEscapeUtils.escapeJava(fieldValues[0]).replaceAll("^\\d+", "");
				fieldType = fieldValues[1];
				
				fieldValue = fieldValues[3];
				selctedXpathKey = fieldValues[11];
				xpath = fieldValues[4];
				
				
				if(selctedXpathKey.contains("css_")) {
					xpathStart = "\n\t@FindBy(how = How.CSS, using = \"";
				} else {
					xpathStart = "\n\t@FindBy(how = How.XPATH, using = \"";
				} 
//				} else if (selctedXpathKey.equalsIgnoreCase("xpath_id")){
//					xpathStart = "\n\t@FindBy(how = How.ID, using = \"";
//				} else if (selctedXpathKey.equalsIgnoreCase("absolute_xpath_id")){
//					xpathStart = "\n\t@FindBy(how = How.XPATH, using = \"";
//				} else if (selctedXpathKey.equalsIgnoreCase("xpath_name")){
//					xpathStart = "\n\t@FindBy(how = How.NAME, using = \"";
//				} else if (selctedXpathKey.equalsIgnoreCase("xpath_class")){
//					xpathStart = "\n\t@FindBy(how = How.CLASS_NAME, using = \"";
//				} else if(selctedXpathKey.equalsIgnoreCase("linktext")) {
//					xpathStart = "\n\t@FindBy(how = How.LINK_TEXT, using = \"";
//				} 
				
				labelCheck = fieldValues[5];	
				String tagName = fieldValues[6];
				String webPageElementId = "_" + fieldValues[7];
				boolean isClick =  Boolean.parseBoolean(fieldValues[8]);
		
				String dropDownValue = "";
				String elementOrder = "";
				try {
				 dropDownValue = fieldValues[9];
				 elementOrder =  fieldValues[10];
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				boolean isElementOptional = Boolean.parseBoolean(fieldValues[12]);
				catchBlock = "\n\t\treturn text;\n\t}\n";
				
				
				String isElementOpen = "", isElementClosed = "";
				String addReturnEmpty = "";
				if(isElementOptional) {
					isElementOpen = "if(isElementPresent("+ Utils.removeUnWantedChars(toTitleCase(fieldName))  + webPageElementId + tagName + ")) { \n\t\t";
					isElementClosed = "\n\t\t}";
					catchBlock =  isElementClosed + addReturnEmpty +  catchBlock;
				}
				
				methodInsideBlock = " {\n\t\t" + isElementOpen + windowSwitchCode  +  "waitForExpectedElement(driver, ";
				if (tagName.equalsIgnoreCase("table")) {
					continue;
				} else if (tagName.equalsIgnoreCase("select")) {
					packages = packages + "import org.openqa.selenium.support.ui.Select;\n";
					if (labelCheck.equalsIgnoreCase("true")) {
						fieldNamePrefix = "clk"+ tagName;
					} else {
					returnString = ";\n\tpublic String ";
					fieldNamePrefix = "clk"+ tagName;
					}
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName)) + elementOrder + webPageElementId;

					methodInsideBlock = Beforestr6 + methodInsideBlock;
				
				} 
				else if (fieldType.equalsIgnoreCase("Button") || fieldType.equalsIgnoreCase("Link") || secondaryFieldType.equalsIgnoreCase("MouseHover")) {
					if (labelCheck.equalsIgnoreCase("true")) {
						fieldNamePrefix = "clk"+ tagName;

				} else {
					returnString = ";\n\tpublic String ";
					fieldNamePrefix = "clk"+ tagName;
					}
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName)) + webPageElementId;

					methodInsideBlock = Beforestr6 + methodInsideBlock;

					} 

				if (fieldType.equalsIgnoreCase("image")) {
					
					fieldNamePrefix = "clk"+ tagName;
					
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName)) + webPageElementId;

					methodInsideBlock = Beforestr6 + methodInsideBlock;
					returnString = ";\n\tpublic String ";
				} else if (fieldType.equalsIgnoreCase("InputText")) {
					fieldNamePrefix = "fill"+ tagName;
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName)) + webPageElementId;
					methodInsideBlock = "(String varInputValue)" + methodInsideBlock;
					//catchBlock =  tagName +".sendKeys(varInputValue);" + (windowSwitchCode.isEmpty() ? "" : "\n\t\tswitchToParentWindow(driver);") + catchBlock;
					
					catchBlock =  tagName +".sendKeys(varInputValue);"  + catchBlock;
					returnString = ";\n\tpublic String ";
				} else if (fieldType.equalsIgnoreCase("Label")) {
					fieldName1 = Utils.removeUnWantedChars(toTitleCase(fieldName)) + webPageElementId;
					if (labelCheck.equalsIgnoreCase("true")) {
						fieldNamePrefix = "text"+ tagName;
					} else {
						fieldNamePrefix = "text"+ tagName;
						returnString = ";\n\tpublic String ";
					}
					// catchBlock=Beforestr9+"\n\t\treturn "+toTitleCase(fieldName1)+text+catchBlock;
					methodInsideBlock = Beforestr6 + methodInsideBlock;
				} 
				
				
				if (fieldType.equalsIgnoreCase("image")) {
					String isActionClick = "";
					if(isClick) {
						isActionClick = "\n\t\t" +  Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + clickAction;
					}
					
					returnString = ";\n\tpublic String ";
					funcName = fieldNamePrefix + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + methodInsideBlock 
							+ Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + ");\t\t" 
							+ isActionClick
							+ "\n\t\treturn \"image\";"
							+ catchBlock;
					

				} else if (!fieldType.equalsIgnoreCase("InputText")) {
					String order = "";
					if(tagName.equals("select")) {
						
						order =  elementOrder;
						
						funcName = "clk" + tagName  + Utils.removeUnWantedChars(toTitleCase(fieldName)) + elementOrder + webPageElementId 
								+ methodInsideBlock + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + ");\t\t" 
								+"\n\t\tSelect selectOption = new Select(" + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName+");" + 
								"\n\t\tselectOption.selectByVisibleText(\"" + dropDownValue + "\");"
								+ "\n\t\tString text = selectOption.getFirstSelectedOption().getText();";
								String returnn = "";
								if(isElementOptional) {
									returnn = "\n\t\treturn text;";
								}
								funcName = funcName 
								+ returnn;
								//xpathStart =""; xpath="";  webEleemnt =""; fieldName1="";  tagName=""; 
								returnString = ";\n\tpublic String ";
								funcName = funcName + catchBlock;
					} else {
						order = "";
						String isActionClick = "";
						if(secondaryFieldType.equalsIgnoreCase("MouseHover")) {
							isActionClick =   "\n\t\t" + "mouseHover(driver," + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + ");";
							if(isClick) {
								isActionClick =  isActionClick	+  "\n\t\t" + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName  + clickAction; 
							}
						} else if(isClick) {
							isActionClick =   "\n\t\t" + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName  + clickAction;  
						}
						funcName = fieldNamePrefix + Utils.removeUnWantedChars(toTitleCase(fieldName1))  + order
								+ methodInsideBlock + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + ");\t\t" 
								+ "\n\t\tString text = " + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + text + isActionClick;
								
								//if(labelCheck.equalsIgnoreCase("true")) {
									if(!fieldType.equalsIgnoreCase("image")) {
										String returnn = "";
										if(isElementOptional) {
											returnn = "\n\t\treturn text;";
										}
										funcName = funcName 
										+ returnn;
								}
								//}
								funcName = funcName + catchBlock;
					}
					
					method += xpathStart + xpath + webEleemnt + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName  + returnString + funcName;
					
				} else {
					funcName = fieldNamePrefix + Utils.removeUnWantedChars(toTitleCase(fieldName1)) +  methodInsideBlock 
							+ Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName +");\n\t\t" 
							+ Utils.removeUnWantedChars(toTitleCase(fieldName1)) + catchBlock;
					method += xpathStart + xpath + webEleemnt + Utils.removeUnWantedChars(toTitleCase(fieldName1)) + tagName + returnString + funcName;
					
				}
				
				// select option  
				if(dropDownValue!= null && !dropDownValue.isEmpty()) {}
			}
		} else {
			System.out.println("Invalid input string!");
		}
		 testcaseClassStart =  packages + testcaseClassStart;
		testcaseClassStart += method + endClass;
		writeFile(testcaseClassStart, toTitleCase(screenName), pageClassPath, ".java");
	}

	private String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].trim().equalsIgnoreCase(""))
				continue;
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append("");
		}
		String name = sb.toString().replace("$", "").replaceAll("^\\d+", "");
		return name.trim();
	}

	private void writeFile(String testcaseClassStart, String className, String filePath, String fileExtension)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(testcaseClassStart);
		} finally {
			out.close();
		}
	}
	
	private boolean createDirectory(String directory) {
		File fileDirectory = new File(directory);
		if (!fileDirectory.exists()) {
			fileDirectory.mkdir();
			return true;
		}
		return false;
	}
}
