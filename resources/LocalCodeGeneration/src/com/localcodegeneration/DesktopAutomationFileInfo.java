package com.localcodegeneration;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javafx.concurrent.Task;

 /**
 * This class used for progress information 
 */
public class DesktopAutomationFileInfo extends Task<List<File>> {
	private static String environmentVariable = System.getenv("AUTOMATION_PATH");
	private static String finaldata;
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static String addBash = "";
	
	/**
	 * Here will get the json data from Main.class 
	 * For MAC/Linux getting the home directory and executing the environmentVariables.sh for getting the AUTOMATION_PATH from environment variables 
	 * @param executeAction2 
	 * @param testsetName2 
	 * @param testCaseName 
	 * @param projectNamee
	 * @throws InterruptedException 
	 */
	
    public DesktopAutomationFileInfo(String jsonData)  {
   	 try {
			printMessage("Configuring the project  .... \n \n 0 / 4 tasks completed");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}  
		// checking linux or windows
		System.out.println(OS);
		if (isWindows()) {
			environmentVariable = System.getenv("AUTOMATION_PATH");
			System.out.println("This is Windows");
		} else if (isMac()) {
			String MAC_HOME_DIRECTORY = System.getProperty("user.home");
			environmentVariable = "bash " + MAC_HOME_DIRECTORY;
			createEnvironmentFile(MAC_HOME_DIRECTORY);
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables.size() > 0) {
			environmentVariable = envVariables.get(0);
			System.out.println("This is for Mac");
			addBash = "bash ";
			}
		} else if (isUnix() || isSolaris()) {
			environmentVariable = "bash " + System.getProperty("user.home");
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables != null && envVariables.size() > 0) {
			environmentVariable = envVariables.get(0);
			System.out.println("This is for Mac");
			addBash = "bash ";
			}
			System.out.println("This is for unix/solaris");
		} else {
			System.out.println("Your OS is not support!!");
		}

   	finaldata = jsonData;
   	primaryInfoFile();
   	
	}
    
    /**
     * used to create environmentVariables.sh file if not exists in Home Directory
     * @param path
     */
    private void createEnvironmentFile(String path) {
		 try {
			 File fileExists = new File(path + File.separator + "environmentVariables.sh");
				boolean exists = fileExists.exists();
				if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
					"printenv AUTOMATION_PATH\n" + 
					"printenv NODE_PATH\n" + 
					"printenv APPIUM_JS_PATH\n" + 
					"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    private void primaryInfoFile() {
    	try {
    	JSONArray array = new JSONArray(finaldata);
    	JSONObject jsonObject = array.getJSONObject(0);
		JSONObject primaryInfoObj = jsonObject.getJSONObject("primary_info");
    	String fileData = "package com.utilities;\r\n" + 
    			"\r\n" + 
    			"\r\n" + 
    			"public class PrimaryInfo {\r\n" + 
    			"public String primaryInfoData() {\r\n" + 
    			"		String data = \"" + StringEscapeUtils.escapeJava(primaryInfoObj.toString()).replaceAll(",", ",\"\r\n + \"")  + "\";\r\n" 
    			+		"return data;\r\n" + 
    			"	}\r\n" + 
    			"}";
    	
    	writeFile(fileData, "PrimaryInfo", environmentVariable + File.separator + "src" + File.separator + "test" + File.separator + "java" + File.separator + "com" +File.separator + "utilities" , ".java");
    	} catch (Exception e) {
			System.err.println("error" + e.getLocalizedMessage());
		}
    }
    
    
/* Executing the bat/sh file to get the environment variables */
	
	private  ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + environmentVariable + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}

   
    /**
	 * files : setting the permissions to read, write and execute
	 * @param path
	 * @throws Exception
	 */
    private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}


	@Override
	public List<File> call() {

        List<File> files = new ArrayList<File>(); 
		try {
			System.out.println(finaldata);
			JSONArray array = new JSONArray(finaldata);
			JSONObject jsonObject = array.getJSONObject(0);
			JSONObject primaryInfoObj = jsonObject.getJSONObject("primary_info");
		    String testCaseName = primaryInfoObj.optString("testcase_name");
		    String inputCommand = primaryInfoObj.optString("input_command");
			String projectName = primaryInfoObj.optString("project_name").toUpperCase();
			String testsetName = primaryInfoObj.optString("testset_name") == null ? "" : primaryInfoObj.optString("testset_name").toUpperCase();
			String name = "profilename";
			if(testCaseName != null && !testCaseName.equals("null") && !testCaseName.isEmpty() ) {
				name = projectName.toLowerCase() + testCaseName.toLowerCase().toLowerCase();
			} else {
				name = projectName.toLowerCase() + testsetName.toLowerCase();
			}
			
			
			String command = inputCommand;
			if (isWindows()) {
				createBatFile(".bat",command, name);
			} else if (isUnix() || isSolaris() || isMac()) {
				createBatFile(".sh",command, name);
			} 
			//==========END ==============
			
			for (int percentage = 0; percentage <= 10; percentage++) {
				if (percentage == 1) {
					this.updateProgress(percentage, 10);
					printMessage("Configuring the project ... \n \n 0 / 4 tasks completed");

				} else if (percentage == 6) {
					this.updateProgress(percentage, 10);
					if (isWindows()) {
						bacthFileExecution(environmentVariable + File.separator + name + ".bat", true);
					} else if (isUnix() || isSolaris() || isMac()) {
						bacthFileExecution(environmentVariable + File.separator + name + ".sh",true);
					} 	
				}

				if (percentage == 10) {
					this.updateProgress(10, 10);
					String msg = "Sucessfully Executed";
					// executeCommands(frameworkPath,gitURL,false,false,true);//push to git
					this.printMessage(msg + "\n \n 4 / 4 tasks completed");
					this.printMessage(msg + "\n \n 4 / 4 tasks completed");

					System.exit(1);
				}
			}
			  
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("completed");
		return files;	
	}
	
	
	/** 
	 * printing the file Info
	 */
	
	private void printMessage(String file) throws InterruptedException {
		 this.updateMessage(file);
	        Thread.sleep(1000);	
	}

	/**
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
		String val = value.replaceAll(" ", "");
		val = val.replaceAll("[^a-z_A-Z0-9]", "");
		return val;
		}catch (Exception e) {
			return value;
		}
	}
	
	/**
	 * File writer method
	 */

	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator + className + fileExtension);
		} finally {
			out.close();
		}
	}
		
	
	
	/**
	 *  Using this method to executing the Batch file 
	 */
	private String response, errorResponse;
	
	private void bacthFileExecution(String batFileName,boolean isExecute) throws Exception {

		try {	
			this.printMessage("Configuring Maven ... \n \n 3 / 4 tasks completed");
			if(isExecute) {
			this.updateProgress(7, 10);
			}
			Process p = Runtime.getRuntime().exec(batFileName);
			
			final BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			final BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			this.printMessage("Configuring Maven ... \n \n 3 / 4 tasks completed");
			
			DesktopAutomationFileInfo current = this;
			 try {
				
						String line1;
						boolean isRunning = true;
						while (((line1 = (bri.readLine())) != null)) {
							
							response = response + line1;
							System.out.println(line1);
							try {
								if(isRunning && line1.contains("Running TestSuite")) {
									isRunning = false;
									current.printMessage("Running TestSuite ... \n \n 3 / 4 tasks completed");
									current.updateProgress(8.5, 10);
								
								}
							} catch (InterruptedException e) {
								e.printStackTrace();
								p.destroy();
							}

						}
						
					
				p.waitFor();
				//Future<String> futureResult = worker.submit(task);
				//futureResult.get(10, TimeUnit.SECONDS);
			} catch (Exception e) {}
			
			try {
				ExecutorService executor = Executors.newSingleThreadExecutor();
				Callable<String> tasker = new Callable<String>() {
					public String call() throws IOException {
						String line1;
						while (((line1 = (bre.readLine())) != null)) {
							System.out.println(line1);
							errorResponse = errorResponse + line1;

						}
						return errorResponse;
					}
				};
				Future<String> futureResult = executor.submit(tasker);
				futureResult.get(10, TimeUnit.SECONDS);
			} catch (Exception e) {}
			
		  
			bre.close();
			bri.close();
		
			System.out.println("Done.");
		} catch (Exception err) {
			err.printStackTrace();
		}
	}
	
	
	
	/**
	 *  Dynamically creating bat file for tesset by testsetname
	 */
	private void createBatFile(String extension,String command, String name) throws Exception {
		String batFile = "";
		if(extension.equals(".sh")) {
			batFile="export PATH=$PATH:$M2_HOME/bin\n" + 
					"source ~/.bash_profile"
					+"\ncd $AUTOMATION_PATH\n" + 
					command +"\n" + 
					"";
		}else {
			batFile="%AUTOMATION_DRIVE%\n" + 
					"cd %AUTOMATION_PATH%\n" + 
					 command + "\n" +
					"";
		}
		writeFile(batFile, name, environmentVariable, extension);
	}

	/* Finding the OS */
	
	private static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	private static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
	
}