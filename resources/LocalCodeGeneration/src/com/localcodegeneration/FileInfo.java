package com.localcodegeneration;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.dto.WebScript;
import com.google.gson.Gson;

import javafx.concurrent.Task;

 /**
 * This class used for progress information 
 */
public class FileInfo extends Task<List<File>> {
	private static String environmentVariable = System.getenv("AUTOMATION_PATH");
	private static String testClasses = "";
	private static String testsetName = "";
	private static boolean executeAction;
	private static String finaldata;
	private int testSetLength = 0;
	private boolean isWeb;
	private String mobilePlatform;
	private String fileName;
	private String version;
	private String bundleId;
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static String addBash = "";
	
	/**
	 * Here will get the json data from Main.class 
	 * For MAC/Linux getting the home directory and executing the environmentVariables.sh for getting the AUTOMATION_PATH from environment variables 
	 * @param executeAction2 
	 * @param testsetName2 
	 * @param testCaseName 
	 * @param projectName 
	 * @throws InterruptedException 
	 */
	
    public FileInfo(String jsonData)  {
    	 try {
			printMessage("Configuring the project  .... \n \n 0 / 4 tasks completed");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		// checking linux or windows
		System.out.println(OS);
		if (isWindows()) {
			environmentVariable = System.getenv("AUTOMATION_PATH");
			System.out.println("This is Windows");
		} else if (isMac()) {
			String MAC_HOME_DIRECTORY = System.getProperty("user.home");
			environmentVariable = "bash " + MAC_HOME_DIRECTORY;
			createEnvironmentFile(MAC_HOME_DIRECTORY);
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables.size() > 0) {
			environmentVariable = envVariables.get(0);
			System.out.println("This is for Mac");
			addBash = "bash ";
			}
		} else if (isUnix() || isSolaris()) {
			environmentVariable = "bash " + System.getProperty("user.home");
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables != null && envVariables.size() > 0) {
			environmentVariable = envVariables.get(0);
			System.out.println("This is for Mac");
			addBash = "bash ";
			}
			System.out.println("This is for unix/solaris");
		} else {
			System.out.println("Your OS is not support!!");
		}

    	finaldata = jsonData;
    	
	}

    /**
     * used to create environmentVariables.sh file if not exists in Home Directory
     * @param path
     */
    private void createEnvironmentFile(String path) {
		 try {
			 File fileExists = new File(path + File.separator + "environmentVariables.sh");
				boolean exists = fileExists.exists();
				if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
					"printenv AUTOMATION_PATH\n" + 
					"printenv NODE_PATH\n" + 
					"printenv APPIUM_JS_PATH\n" + 
					"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    /**
	 * files : setting the permissions to read, write and execute
	 * @param path
	 * @throws Exception
	 */
    private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}
    
  
	

    /**
     * Checking the testcase or testset 
     * if it is testset here looping the testcases 
     * Creating the testset bat file
     * Creating the profiles in pom.xml for running the testset.
     * running the batch file execution
     * dividing 10 levels for showing the execution progress 
     */
    private String browsers;
	private String projectName;
	private String mobileType;
	private String testCaseName;
	@Override
	public List<File> call() {

        List<File> files = new ArrayList<File>(); 
		try {
			
			//data = new String[] {"[{&quot;primary_info&quot;:{&quot;browser_type&quot;:&quot;chrome&quot;,&quot;report_upload_url&quot;:&quot;https://192.168.1.142:8080/TAF_Automation_DR/UploadReportFile&quot;,&quot;user_id&quot;:7,&quot;is_generate&quot;:false,&quot;is_execute&quot;:true,&quot;is_web&quot;:true,&quot;mobile_platform&quot;:null,&quot;file_name&quot;:null,&quot;bundle_id&quot;:null,&quot;device_os_version&quot;:null,&quot;project_url&quot;:&quot;http://selenium4testing.com/hms/index.php&quot;,&quot;project_name&quot;:&quot;WindowHandleTest&quot;,&quot;project_description&quot;:&quot;&quot;,&quot;project_id&quot;:166,&quot;module_name&quot;:&quot;WindowHandleTest&quot;,&quot;module_description&quot;:&quot;&quot;,&quot;sub_module_id&quot;:0,&quot;sub_module_name&quot;:null,&quot;sub_module_description&quot;:null,&quot;module_id&quot;:303,&quot;testcase_name&quot;:&quot;TC_EndToEndScenario&quot;,&quot;testcase_id&quot;:336,&quot;testset_name&quot;:null,&quot;testset_id&quot;:0,&quot;executed_timestamp&quot;:1546581472703},&quot;regression_datasets_of_testcase&quot;:null,&quot;datasets_of_testcase&quot;:[{&quot;dataset_name&quot;:&quot;Ds1&quot;,&quot;dataset_id&quot;:428,&quot;apis&quot;:null,&quot;regression_screens&quot;:null,&quot;screens&quot;:[{&quot;screen_id&quot;:799,&quot;name&quot;:&quot;LoginScreen&quot;,&quot;description&quot;:&quot;d&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:303,&quot;web_page_id&quot;:889,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:799,&quot;web_page_element_id&quot;:282112,&quot;element_order&quot;:1,&quot;element_id&quot;:282112,&quot;web_page_id&quot;:889,&quot;field_name&quot;:&quot;username&quot;,&quot;field_value&quot;:&quot;admin&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='username']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/FORM[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;username&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:799,&quot;web_page_element_id&quot;:282115,&quot;element_order&quot;:2,&quot;element_id&quot;:282115,&quot;web_page_id&quot;:889,&quot;field_name&quot;:&quot;password&quot;,&quot;field_value&quot;:&quot;admin&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='password']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/FORM[1]/INPUT[2]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;password&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:799,&quot;web_page_element_id&quot;:282113,&quot;element_order&quot;:3,&quot;element_id&quot;:282113,&quot;web_page_id&quot;:889,&quot;field_name&quot;:&quot;submit&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:&quot;//INPUT[@name='submit']&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/FORM[1]/INPUT[3]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;submit&quot;,&quot;prefered_field&quot;:&quot;xpath_name&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:800,&quot;name&quot;:&quot;FeedBackClickScreen&quot;,&quot;description&quot;:&quot;s&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:303,&quot;web_page_id&quot;:890,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:800,&quot;web_page_element_id&quot;:282152,&quot;element_order&quot;:1,&quot;element_id&quot;:282152,&quot;web_page_id&quot;:890,&quot;field_name&quot;:&quot;Feedback&quot;,&quot;field_value&quot;:&quot;A&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:&quot;//UL[@id='navigation']/LI[3]/A[1]&quot;,&quot;relative_xpath_by_field_name&quot;:null,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/UL[1]/LI[3]/A[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;A&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;A&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]},{&quot;screen_id&quot;:804,&quot;name&quot;:&quot;LogoutScreen&quot;,&quot;description&quot;:&quot;d&quot;,&quot;created_at&quot;:null,&quot;created_by&quot;:7,&quot;module_id&quot;:303,&quot;web_page_id&quot;:891,&quot;is_default&quot;:false,&quot;is_select&quot;:false,&quot;regression_screen&quot;:false,&quot;selected&quot;:&quot;&quot;,&quot;screen_elements&quot;:null,&quot;regression_screen_elements&quot;:null,&quot;elements&quot;:[{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282208,&quot;element_order&quot;:1,&quot;element_id&quot;:282208,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Your Name:&quot;,&quot;field_value&quot;:&quot;rajasekhar@gmail.com&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;//INPUT[@id='name']&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[1]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;name&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282210,&quot;element_order&quot;:2,&quot;element_id&quot;:282210,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Your Email:&quot;,&quot;field_value&quot;:&quot;rajasekhar@gmail.com&quot;,&quot;field_type&quot;:&quot;InputText&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;//INPUT[@id='email']&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[2]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;email&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282213,&quot;element_order&quot;:3,&quot;element_id&quot;:282213,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Volvo&quot;,&quot;field_value&quot;:&quot;select&quot;,&quot;field_type&quot;:&quot;link&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;//SELECT[@id='car']/OPTION[1]&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;select&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;[{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[1]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;What's my options:\\&quot;,\\&quot;name\\&quot;:\\&quot;Volvo\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[1]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[2]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Saab\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[2]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[3]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Mercedes\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[3]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Audi\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[4]\\&quot;},{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[5]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Other\\\\u2026\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[5]\\&quot;}]&quot;,&quot;prefered_field&quot;:&quot;xpath_id&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;select&quot;,&quot;dropdown_value&quot;:&quot;{\\&quot;xpath\\&quot;:\\&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[3]/SELECT[1]/OPTION[4]\\&quot;,\\&quot;fieldName\\&quot;:\\&quot;\\&quot;,\\&quot;name\\&quot;:\\&quot;Audi\\&quot;,\\&quot;input_type\\&quot;:\\&quot;link\\&quot;,\\&quot;fieldType\\&quot;:\\&quot;select\\&quot;,\\&quot;relative_xpath_by_id\\&quot;:\\&quot;//SELECT[@id='car']/OPTION[4]\\&quot;}&quot;,&quot;is_click&quot;:false,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282215,&quot;element_order&quot;:4,&quot;element_id&quot;:282215,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Upload a file:&quot;,&quot;field_value&quot;:&quot;INPUT&quot;,&quot;field_type&quot;:&quot;Button&quot;,&quot;secondary_field_type&quot;:&quot;Window Switch&quot;,&quot;relative_xpath_by_id&quot;:&quot;&quot;,&quot;relative_xpath_by_field_name&quot;:&quot;&quot;,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/DIV[1]/FORM[1]/UL[1]/LI[6]/INPUT[1]&quot;,&quot;is_selected&quot;:false,&quot;tag_field_name&quot;:&quot;INPUT&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;INPUT&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null},{&quot;screen_id&quot;:804,&quot;web_page_element_id&quot;:282221,&quot;element_order&quot;:5,&quot;element_id&quot;:282221,&quot;web_page_id&quot;:891,&quot;field_name&quot;:&quot;Logout&quot;,&quot;field_value&quot;:&quot;A&quot;,&quot;field_type&quot;:&quot;Link&quot;,&quot;secondary_field_type&quot;:null,&quot;relative_xpath_by_id&quot;:null,&quot;relative_xpath_by_field_name&quot;:null,&quot;xpath&quot;:&quot;//HTML/BODY[1]/DIV[2]/DIV[1]/H3[1]/A[1]&quot;,&quot;is_selected&quot;:true,&quot;tag_field_name&quot;:&quot;A&quot;,&quot;child_id&quot;:&quot;&quot;,&quot;child_text&quot;:&quot;&quot;,&quot;prefered_field&quot;:&quot;xpath&quot;,&quot;is_iframe&quot;:false,&quot;iframe_name&quot;:&quot;&quot;,&quot;frame_name&quot;:null,&quot;frame_id&quot;:null,&quot;is_frame&quot;:false,&quot;frame_position&quot;:0,&quot;iframe_position&quot;:0,&quot;iframe_id&quot;:null,&quot;tag_name&quot;:&quot;A&quot;,&quot;dropdown_value&quot;:&quot;&quot;,&quot;is_click&quot;:true,&quot;dataset_values_temp&quot;:null}]}]}]}]"};
			//MOBILE ANDROID
			//data = new String[] {"[{\"primary_info\":{\"browser_type\":\"6\",\"report_upload_url\":\"http://192.168.1.142:8030/TAF_Automation_DR/UploadReportFile\",\"user_id\":7,\"is_generate\":false,\"is_execute\":true,\"is_web\":false,\"mobile_platform\":\"Android\",\"file_name\":\"app-staging-debug.apk\",\"bundle_id\":\"\",\"device_os_version\":\"6\",\"project_url\":null,\"project_name\":\"MobileDemo\",\"project_description\":\"Having Android and iOS Applications\",\"project_id\":147,\"module_name\":\"AndroidDemo\",\"module_description\":\"-Having Android Test cases\",\"sub_module_id\":0,\"sub_module_name\":null,\"sub_module_description\":null,\"module_id\":260,\"testcase_name\":\"TC_Demo_001\",\"testcase_id\":213,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1532068327279},\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"Dataset\",\"dataset_id\":252,\"apis\":null,\"regression_screens\":null,\"screens\":[{\"screen_id\":609,\"name\":\"SplashScreen\",\"description\":\"SplashScreen Elemnts\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":699,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":609,\"web_page_element_id\":206766,\"element_order\":1,\"element_id\":206766,\"web_page_id\":699,\"field_name\":\"LOGIN\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/loginBTN']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='LOGIN']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[2]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/loginBTNtextLOGIN\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":610,\"name\":\"LoginScreen\",\"description\":\"LoginScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":700,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":610,\"web_page_element_id\":206779,\"element_order\":1,\"element_id\":206779,\"web_page_id\":700,\"field_name\":\"Cancel\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/cancelTV']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Cancel']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/cancelTVtextCancel\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":610,\"web_page_element_id\":206772,\"element_order\":2,\"element_id\":206772,\"web_page_id\":700,\"field_name\":\"Username\",\"field_value\":\"buck.l\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/usernameET']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='Username']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/usernameETtextUsername\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":610,\"web_page_element_id\":206770,\"element_order\":3,\"element_id\":206770,\"web_page_id\":700,\"field_name\":\"passwordEditText\",\"field_value\":\"legionco1\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/passwordEditText']\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/passwordEditText\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":610,\"web_page_element_id\":206776,\"element_order\":4,\"element_id\":206776,\"web_page_id\":700,\"field_name\":\"Login\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/login']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Login']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/logintextLogin\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":611,\"name\":\"DashboardScreen\",\"description\":\"DashboardScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":701,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":611,\"web_page_element_id\":206795,\"element_order\":1,\"element_id\":206795,\"web_page_id\":701,\"field_name\":\"Schedule\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/tab_text']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Schedule']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.TabWidget[1]/android.widget.RelativeLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":true,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/tab_texttextSchedule\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":612,\"name\":\"ScheduleOverviewScreen\",\"description\":\"ScheduleOverviewPage elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":702,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":612,\"web_page_element_id\":206824,\"element_order\":1,\"element_id\":206824,\"web_page_id\":702,\"field_name\":\"MAY 22\\\\n-\\\\nMAY 28\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/weekDateTV']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='MAY 22\\\\n-\\\\nMAY 28']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.ListView[1]/android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/weekDateTVtextMAY 22\\\\n-\\\\nMAY 28\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":613,\"name\":\"PastScheduleScreen\",\"description\":\"PastScheduleScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":703,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":613,\"web_page_element_id\":206873,\"element_order\":1,\"element_id\":206873,\"web_page_id\":703,\"field_name\":\"SUMMARY\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='SUMMARY']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"textSUMMARY\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":613,\"web_page_element_id\":206859,\"element_order\":2,\"element_id\":206859,\"web_page_id\":703,\"field_name\":\"nextIv\",\"field_value\":\"ImageView\",\"field_type\":\"ImageView\",\"relative_xpath_by_id\":\"//android.widget.ImageView[@resource-id='co.legion.client.staging:id/nextIv']\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.ImageView[2]\",\"is_selected\":false,\"tag_field_name\":\"ImageView\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/nextIv\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"ImageView\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":613,\"web_page_element_id\":206877,\"element_order\":3,\"element_id\":206877,\"web_page_id\":703,\"field_name\":\"More\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/tabTv']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='More']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.TabWidget[1]/android.widget.RelativeLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/tabTvtextMore\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":614,\"name\":\"ProfileScreen\",\"description\":\"ProfileScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":704,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":614,\"web_page_element_id\":206930,\"element_order\":1,\"element_id\":206930,\"web_page_id\":704,\"field_name\":\"Profile\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/profile']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Profile']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[2]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/profiletextProfile\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":616,\"name\":\"ProfileUpdateScreen\",\"description\":\"ProfileScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":705,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":616,\"web_page_element_id\":206969,\"element_order\":1,\"element_id\":206969,\"web_page_id\":705,\"field_name\":\"hello test\",\"field_value\":\"buck\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/firstName']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='hello test']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/firstNametexthello test\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206973,\"element_order\":2,\"element_id\":206973,\"web_page_id\":705,\"field_name\":\"dgved\",\"field_value\":\"Buck.l\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/nickName']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='dgved']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/nickNametextdgved\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206978,\"element_order\":3,\"element_id\":206978,\"web_page_id\":705,\"field_name\":\"adsfsd\",\"field_value\":\"Dublin\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"//android.widget.EditText[@resource-id='co.legion.client.staging:id/streetEdittext']\",\"relative_xpath_by_field_name\":\"//android.widget.EditText[@text='adsfsd']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[5]/android.widget.EditText[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/streetEdittexttextadsfsd\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206980,\"element_order\":4,\"element_id\":206980,\"web_page_id\":705,\"field_name\":\"Save\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/imageviewOk']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Save']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.widget.RelativeLayout[1]/android.widget.TextView[2]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/imageviewOktextSave\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null},{\"screen_id\":616,\"web_page_element_id\":206975,\"element_order\":5,\"element_id\":206975,\"web_page_id\":705,\"field_name\":\"closeSetup\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"//android.widget.ImageButton[@resource-id='co.legion.client.staging:id/closeSetup']\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.widget.RelativeLayout[1]/android.widget.ImageButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/closeSetup\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":618,\"name\":\"MoreScreen\",\"description\":\"MoreScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":703,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":618,\"web_page_element_id\":206877,\"element_order\":1,\"element_id\":206877,\"web_page_id\":703,\"field_name\":\"More\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/tabTv']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='More']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TabHost[1]/android.widget.LinearLayout[1]/android.widget.TabWidget[1]/android.widget.RelativeLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/tabTvtextMore\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":615,\"name\":\"LogoutScreen\",\"description\":\"LogoutScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":704,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":615,\"web_page_element_id\":206938,\"element_order\":1,\"element_id\":206938,\"web_page_id\":704,\"field_name\":\"Logout\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/logout']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Logout']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[2]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[7]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/logouttextLogout\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":617,\"name\":\"LogoutAlertScreen\",\"description\":\"LogoutScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":260,\"web_page_id\":706,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":617,\"web_page_element_id\":206986,\"element_order\":1,\"element_id\":206986,\"web_page_id\":706,\"field_name\":\"Logout\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"//android.widget.TextView[@resource-id='co.legion.client.staging:id/saveTv']\",\"relative_xpath_by_field_name\":\"//android.widget.TextView[@text='Logout']\",\"xpath\":\"/hierarchy[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[2]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"resource-idco.legion.client.staging:id/saveTvtextLogout\",\"prefered_field\":\"xpath_id\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]}]}]}]\r\n" + 
				//	""};
			
			//Mobile iOS
			//data = new String[] {"[{\"primary_info\":{\"browser_type\":\"iOS\",\"report_upload_url\":\"http://192.168.1.142:8030/TAF_Automation_DR/UploadReportFile\",\"user_id\":7,\"is_generate\":false,\"is_execute\":true,\"is_web\":false,\"mobile_platform\":\"iOS\",\"file_name\":\"Notifii Track.ipa\",\"bundle_id\":\"com.DevRabbit.LIC\",\"device_os_version\":\"iOS\",\"project_url\":null,\"project_name\":\"MobileDemo\",\"project_description\":\"Having Android and iOS Applications\",\"project_id\":147,\"module_name\":\"iOSDemo\",\"module_description\":\"iOS Mobile Test Cases\",\"sub_module_id\":0,\"sub_module_name\":null,\"sub_module_description\":null,\"module_id\":261,\"testcase_name\":\"TC_DemoiOS_001\",\"testcase_id\":214,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1532068564500},\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"Dataset1\",\"dataset_id\":254,\"apis\":null,\"regression_screens\":null,\"screens\":[{\"screen_id\":619,\"name\":\"LoginScreen\",\"description\":\"LoginScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":707,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":619,\"web_page_element_id\":206990,\"element_order\":1,\"element_id\":206990,\"web_page_id\":707,\"field_name\":\"Username\",\"field_value\":\"devacc1\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"valueUsername\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":619,\"web_page_element_id\":206991,\"element_order\":2,\"element_id\":206991,\"web_page_id\":707,\"field_name\":\"Password\",\"field_value\":\"devacc1@123\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"valuePassword\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":619,\"web_page_element_id\":206995,\"element_order\":3,\"element_id\":206995,\"web_page_id\":707,\"field_name\":\"Log In\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Log In']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameLog In\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":620,\"name\":\"LogPackageOutScreen\",\"description\":\"Click on More button\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":708,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":620,\"web_page_element_id\":207003,\"element_order\":1,\"element_id\":207003,\"web_page_id\":708,\"field_name\":\"??? More\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='\\\\u2022\\\\u2022\\\\u2022 More']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[4]\",\"is_selected\":true,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"name\\\\u2022\\\\u2022\\\\u2022 More\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":621,\"name\":\"MoreScreen\",\"description\":\"MoreScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":709,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":621,\"web_page_element_id\":207055,\"element_order\":1,\"element_id\":207055,\"web_page_id\":709,\"field_name\":\"Search Resident\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeStaticText[@name='Search Resident']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[3]/XCUIElementTypeStaticText[1]\",\"is_selected\":false,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"nameSearch ResidentvalueSearch Resident\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":623,\"name\":\"SearchResidentScreen\",\"description\":\"SearchResidentScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":710,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":623,\"web_page_element_id\":207101,\"element_order\":1,\"element_id\":207101,\"web_page_id\":710,\"field_name\":\"First Name\",\"field_value\":\"Naveen\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[1]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207097,\"element_order\":2,\"element_id\":207097,\"web_page_id\":710,\"field_name\":\"Last Name\",\"field_value\":\"QA\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[2]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207085,\"element_order\":3,\"element_id\":207085,\"web_page_id\":710,\"field_name\":\"Unit Number\",\"field_value\":\"1256455646\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[3]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207104,\"element_order\":4,\"element_id\":207104,\"web_page_id\":710,\"field_name\":\"Email\",\"field_value\":\"naveen@gmail.com\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[4]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207108,\"element_order\":5,\"element_id\":207108,\"web_page_id\":710,\"field_name\":\"Cellphone\",\"field_value\":\"1234567890\",\"field_type\":\"InputText\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[5]\",\"is_selected\":false,\"tag_field_name\":\"InputText\",\"child_id\":\"\",\"child_text\":\"\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"InputText\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":623,\"web_page_element_id\":207090,\"element_order\":6,\"element_id\":207090,\"web_page_id\":710,\"field_name\":\"Search\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Search']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameSearch\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":625,\"name\":\"SearchResultsScreen\",\"description\":\"SearchResultsScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":711,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":625,\"web_page_element_id\":207118,\"element_order\":1,\"element_id\":207118,\"web_page_id\":711,\"field_name\":\"Search Results\",\"field_value\":\"Label\",\"field_type\":\"Label\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeStaticText[@name='Search Results']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]\",\"is_selected\":true,\"tag_field_name\":\"Label\",\"child_id\":\"\",\"child_text\":\"nameSearch ResultsvalueSearch Results\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Label\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":625,\"web_page_element_id\":207124,\"element_order\":2,\"element_id\":207124,\"web_page_id\":711,\"field_name\":\"Name\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Name']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]\",\"is_selected\":true,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameNamevalue1\",\"prefered_field\":\"xpath\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":false,\"dataset_values_temp\":null},{\"screen_id\":625,\"web_page_element_id\":207126,\"element_order\":3,\"element_id\":207126,\"web_page_id\":711,\"field_name\":\"HeaderBackIcon\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='HeaderBackIcon']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameHeaderBackIcon\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":624,\"name\":\"SearchResidentBackScreen\",\"description\":\"SearchResidentBackScreen\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":710,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":624,\"web_page_element_id\":207099,\"element_order\":1,\"element_id\":207099,\"web_page_id\":710,\"field_name\":\"HeaderBackIcon\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='HeaderBackIcon']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameHeaderBackIcon\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]},{\"screen_id\":622,\"name\":\"LogOutScreen\",\"description\":\"LogOutScreen Elements\",\"created_at\":null,\"created_by\":7,\"module_id\":261,\"web_page_id\":709,\"is_default\":false,\"is_select\":false,\"regression_screen\":false,\"screen_elements\":null,\"regression_screen_elements\":null,\"elements\":[{\"screen_id\":622,\"web_page_element_id\":207072,\"element_order\":1,\"element_id\":207072,\"web_page_id\":709,\"field_name\":\"Log Out\",\"field_value\":\"Button\",\"field_type\":\"Button\",\"relative_xpath_by_id\":\"\",\"relative_xpath_by_field_name\":\"//XCUIElementTypeButton[@name='Log Out']\",\"xpath\":\"/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]\",\"is_selected\":false,\"tag_field_name\":\"Button\",\"child_id\":\"\",\"child_text\":\"nameLog Out\",\"prefered_field\":\"xpath_name\",\"is_iframe\":false,\"iframe_name\":null,\"frame_name\":null,\"frame_id\":null,\"is_frame\":false,\"frame_position\":0,\"iframe_position\":0,\"iframe_id\":null,\"tag_name\":\"Button\",\"dropdown_value\":\"\",\"is_click\":true,\"dataset_values_temp\":null}]}]}]}]\r\n" + 
				//	""};
			//API
			//data = new String[] {"[{\"primary_info\":{\"browser_type\":null,\"report_upload_url\":\"http://192.168.1.142:8030/TAF_Automation_DR/UploadReportFile\",\"user_id\":7,\"is_generate\":false,\"is_execute\":true,\"is_web\":false,\"mobile_platform\":null,\"file_name\":null,\"bundle_id\":null,\"device_os_version\":null,\"project_url\":\"http://veda-dev.internal.nio.io\",\"project_name\":\"NIO\",\"project_description\":\"NIO Project\",\"project_id\":80,\"module_name\":\"Laser Tag\",\"module_description\":\"Laser Tag URL\\nhttp://veda-dev.internal.nio.io:\",\"sub_module_id\":94,\"sub_module_name\":\"Tag\",\"sub_module_description\":\"Sub module of laser tag\",\"module_id\":77,\"testcase_name\":\"TC_Tag_007\",\"testcase_id\":199,\"testset_name\":null,\"testset_id\":0,\"executed_timestamp\":1532068010728},\"regression_datasets_of_testcase\":null,\"datasets_of_testcase\":[{\"dataset_name\":\"DS1\",\"dataset_id\":165,\"apis\":[{\"api_name\":\"API_Tag_Post_007\",\"api_description\":\"Tag Post 007 with only mandatory key values \",\"api_id\":408,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":913,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":885,\"raw_text\":\" {\\n        \\\"value\\\": null,\\n        \\\"startevent\\\": 0,\\n        \\\"tagdefinitionoid\\\": \\\"5ad679b4734a7f000b9136bd\\\",\\n        \\\"endevent\\\": 0,\\n        \\\"tripoid\\\": \\\"\\\",\\n        \\\"endtimestamp\\\": 0,\\n        \\\"entityoid\\\": \\\"\\\",\\n        \\\"vehicleoid\\\": \\\"5a6a46b4406b28000c6d2051\\\",\\n        \\\"entitytype\\\": \\\"event\\\",\\n        \\\"starttimestamp\\\": 0\\n    }\",\"raw_type_id\":3,\"dataset_id\":913},\"request_type\":2,\"body_type\":3},{\"api_name\":\"API_Tag_Get_007\",\"api_description\":\"Tag Get 007\",\"api_id\":409,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":912,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":0,\"raw_text\":null,\"raw_type_id\":0,\"dataset_id\":0},\"request_type\":1,\"body_type\":-1},{\"api_name\":\"API_Tag_Put_007\",\"api_description\":\"Tag TC 007 Updating Start time stamp\",\"api_id\":410,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":911,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":886,\"raw_text\":\" {\\n        \\\"value\\\": null,\\n        \\\"startevent\\\": 0,\\n        \\\"tagdefinitionoid\\\": \\\"5ad679b4734a7f000b9136bd\\\",\\n        \\\"endevent\\\": 0,\\n        \\\"tripoid\\\": \\\"\\\",\\n        \\\"endtimestamp\\\": 0,\\n        \\\"entityoid\\\": \\\"\\\",\\n        \\\"vehicleoid\\\": \\\"5a6a46b4406b28000c6d2051\\\",\\n        \\\"entitytype\\\": \\\"event\\\",\\n        \\\"starttimestamp\\\": 13254125\\n    }\",\"raw_type_id\":3,\"dataset_id\":911},\"request_type\":3,\"body_type\":3},{\"api_name\":\"API_Tag_Delete_007\",\"api_description\":\"Tag Tc 007 Delete with oid\",\"api_id\":411,\"dataset_name\":\"Default\",\"dataset_resource\":\":30080/v1/tag/\",\"dataset_id\":910,\"params_list\":[],\"headers_list\":[],\"body_raw\":{\"raw_id\":0,\"raw_text\":null,\"raw_type_id\":0,\"dataset_id\":0},\"request_type\":4,\"body_type\":3}],\"regression_screens\":null,\"screens\":null}]}]\r\n"};
			
			System.out.println(finaldata);
			JSONArray array = new JSONArray(finaldata);
			boolean isTestSet = array.length() > 1;
			String executionPlatform = "";
			//==========END ==============
			
			  for (int percentage = 0; percentage <= 10; percentage++) {
				  if(percentage == 1) {
					  this.updateProgress(percentage, 10);
					  printMessage("Configuring the project ... \n \n 0 / 4 tasks completed"); 
						
				  } else if(percentage == 2) {
					/*// ===========PULL FROM GIT =======
						try {
							JSONObject jsonObject = array.getJSONObject(0);
							JSONObject primaryInfoData = jsonObject.getJSONObject("primary_info");
							frameworkPath = environmentVariable;
							gitURL = primaryInfoData.optString("repository_url");
							executeCommands(frameworkPath,gitURL,false,true,false);//pull from git	
						} catch (Exception e) {
							e.printStackTrace();
						}
						//==========END ==============*/
					  if (isTestSet) {
						    testSetLength = array.length();
						    
						    // testcases loop : TestSet
							for (int i = 0; i < testSetLength; i++) {
								JSONObject jsonObject = array.getJSONObject(i);
								JSONObject primaryInfoObj = jsonObject.getJSONObject("primary_info");
								testCaseName = removeUnWantedChars(primaryInfoObj.optString("testcase_name"));
								browsers = primaryInfoObj.optString("browser_type");
								String moduleName = removeUnWantedChars(primaryInfoObj.optString("module_name").toLowerCase());
								String subModuleName = primaryInfoObj.optString("sub_module_name") == null ? "" : primaryInfoObj.optString("sub_module_name");
								String subModuleFolder = removeUnWantedChars(subModuleName.toLowerCase());
								executeAction = primaryInfoObj.optBoolean("is_execute");
								String projectName = removeUnWantedChars(primaryInfoObj.optString("project_name")).replaceAll(" ", "").toLowerCase();
								testsetName = removeUnWantedChars(primaryInfoObj.optString("testset_name") == null ? "" : primaryInfoObj.optString("testset_name").toLowerCase());
								isWeb = primaryInfoObj.optBoolean("is_web");
								mobilePlatform = primaryInfoObj.optString("mobile_platform").trim();
								mobileType = primaryInfoObj.getString("mobile_type");
								fileName = primaryInfoObj.optString("file_name").trim();
								version = primaryInfoObj.optString("device_os_version").trim();
								bundleId = primaryInfoObj.optString("bundle_id").trim();
								String subModule = ((!subModuleFolder.equals("null") && subModuleFolder != null && !subModuleFolder.isEmpty()) ? ("." + subModuleFolder) :"");
								String packageName = "";
								 executionPlatform = primaryInfoObj.optString("execution_environment","local");
								if(!mobilePlatform.equalsIgnoreCase("null") && mobilePlatform != null && !mobilePlatform.isEmpty()) {	
									
									/*if(fileName!= null && !fileName.isEmpty() ) {
										if(executionPlatform.equalsIgnoreCase("local")) {
											new CreateDesiredCapabilties().presetText(fileName, version, bundleId);
										} else {
											JSONObject sauceObj = jsonObject.getJSONObject("mobile_device_info");
											String userName = sauceObj.optString("userName");
											String accessKey = sauceObj.optString("accessKey");
											String testObjApiKey = sauceObj.optString("objApiKey");
											String url = sauceObj.optString("url");
											String devices = sauceObj.getJSONArray("sauceDevices").toString();
											new CreateDesiredCapabilties().createSauceDesiredCaps(projectName,userName,accessKey, testObjApiKey, devices, url, executionPlatform);
										}
									}*/
									packageName= "mobile." + projectName  + "." 
											+ moduleName + subModule + ".mobiletestclasses" + "."
											+ removeUnWantedChars(toTitleCase(primaryInfoObj.optString("testcase_name"))) ;
								} else if(isWeb) {
									
									packageName= "web." + projectName  + "." 
											+ moduleName + subModule + ".webtestclasses" + "."
											+(removeUnWantedChars(toTitleCase(primaryInfoObj.optString("testcase_name")))) ;
								} else {
									packageName= "api." + projectName  + "." 
											+ moduleName + subModule + ".apitestclasses" + "."
											+ removeUnWantedChars(toTitleCase(primaryInfoObj.optString("testcase_name"))) ;
								}
								testClasses = testClasses 
										+ "\r\t\t\t<class name=\"" + packageName + "\" />";
							}
							
							this.printMessage("Configuring the project ... \n \n 0 / 4 tasks completed");
							
							com.utils.Utils.createReportPortalProperties(environmentVariable,projectName, testsetName);
							if (isWindows()) {
								createBatFile(testsetName, ".bat");
							} else if (isUnix() || isSolaris() || isMac()) {
								createBatFile(testsetName, ".sh");
							} 
							
							this.printMessage("Configuring the project ... \n \n 0 / 4 tasks completed"); 
							testClasses = testClasses + "\n\t\t\t<class name=\"com.fileupload.ReportUpload\" />";
							writeTestSuiteFile(testsetName, testClasses);
							
							this.printMessage("Configuring the project ... \n \n 0 / 4 tasks completed");
							createProfiles(testsetName);
							this.updateProgress(percentage, 10);
					} else {
							testSetLength = 1;
							this.printMessage("Configuring the project ... \n \n 1 / 4 tasks completed");
							JSONObject primaryInfoJsonObj= array.getJSONObject(0).getJSONObject("primary_info");
							isWeb = primaryInfoJsonObj.optBoolean("is_web");
							 testCaseName = primaryInfoJsonObj.optString("testcase_name");
							bundleId = primaryInfoJsonObj.optString("bundle_id").trim();
							mobilePlatform = primaryInfoJsonObj.getString("mobile_platform");
							mobileType = primaryInfoJsonObj.optString("mobile_type");
							fileName = primaryInfoJsonObj.optString("file_name").trim();
							version = primaryInfoJsonObj.optString("device_os_version").trim();
							executeAction = primaryInfoJsonObj.optBoolean("is_execute");	
							executionPlatform = primaryInfoJsonObj.optString("execution_environment","local");
						   projectName =removeUnWantedChars(primaryInfoJsonObj.optString("project_name")).replaceAll(" ", "").toLowerCase();
								
					}
					  com.utils.Utils.createReportPortalProperties(environmentVariable, projectName, testCaseName);
				  } else if(percentage == 4) {
					  //this.printMessage("Generating the Testcases");
					  this.updateProgress(percentage, 10);
					  this.printMessage("Generating the Testcases ... \n \n 2 / 4 tasks completed");
				} else if(percentage == 5) {
					this.updateProgress(percentage, 10);
					  //this.printMessage("Generating the Datasets");
					  this.printMessage("Generating the Datasets ... \n \n 3 / 4 tasks completed");
				} else if (percentage == 6) {
					this.updateProgress(percentage, 10);
					// Checking the API (or) WEB (or) Mobile scripts
					
					if(!mobileType.equals("null") && mobileType != null && !mobileType.isEmpty() && mobileType.equalsIgnoreCase("mobileWeb")) {
						if(executionPlatform.equalsIgnoreCase("local")) {
							fileName = (mobilePlatform.equalsIgnoreCase("android") ?"mobiletest.apk": "mobiletest.ipa");
							new CreateDesiredCapabilties().presetText(fileName, version, bundleId);
						} else if(executionPlatform.equalsIgnoreCase("local")) {
							fileName = (mobilePlatform.equalsIgnoreCase("android") ?"mobiletest.apk": "mobiletest.ipa");
							new CreateDesiredCapabilties().presetText(fileName, version, bundleId);
						} else {
							JSONObject sauceObj =  array.getJSONObject(0).getJSONObject("mobile_device_info");
							
							String userName = sauceObj.optString("userName");
							String accessKey = sauceObj.optString("accessKey");
							String testObjApiKey = sauceObj.optString("objApiKey");
							String url = sauceObj.optString("url");
							String devices = sauceObj.getJSONArray("devices").toString();
							new CreateDesiredCapabilties().createSauceDesiredCaps(projectName,userName,accessKey, testObjApiKey, devices, url, executionPlatform);
						}
						
						System.out.println("-------------" + finaldata);
						JSONArray jsonArray = new JSONArray(finaldata);		
						for (int i = 0; i < jsonArray.length(); i++) {
							WebScript output = jsonParse(jsonArray.get(i).toString());
							new GenerateWebScript3().generateCode(isTestSet, output, environmentVariable);
						}
					} else if(!mobilePlatform.equals("null") && mobilePlatform != null && !mobilePlatform.isEmpty()) {
					
						if(executionPlatform.equalsIgnoreCase("local")) {
							new CreateDesiredCapabilties().presetText(fileName, version, bundleId);
						} else {
							JSONObject sauceObj =  array.getJSONObject(0).getJSONObject("mobile_device_info");
							
							String userName = sauceObj.optString("userName");
							String accessKey = sauceObj.optString("accessKey");
							String testObjApiKey = sauceObj.optString("objApiKey");
							String url = sauceObj.optString("url");
							String devices = sauceObj.getJSONArray("sauceDevices").toString();
							new CreateDesiredCapabilties().createSauceDesiredCaps(projectName,userName,accessKey, testObjApiKey, devices, url, executionPlatform);
						}
						
						System.out.println("-------------" + finaldata);
						JSONArray jsonArray = new JSONArray(finaldata);		
						for (int i = 0; i < jsonArray.length(); i++) {
							WebScript output = jsonParse(jsonArray.get(i).toString());
							new GenerateMobileScript().generateCode(isTestSet, output, environmentVariable);
						}
					}  else if (isWeb) {
						System.out.println("-------------" + finaldata);
						JSONArray jsonArray = new JSONArray(finaldata);		
						for (int i = 0; i < jsonArray.length(); i++) {
							WebScript output = jsonParse(jsonArray.get(i).toString());
							new GenerateWebScript3().generateCode(isTestSet, output, environmentVariable);
						}
					} else {
						for (int i = 0; i < array.length(); i++) {
							GenerateApiScript scriptGenerate = new GenerateApiScript();
							scriptGenerate.generateCode(isTestSet, i, array.getJSONObject(i), array.length(), environmentVariable);
						}
					}
					
					// batch file execution
					if (executeAction) {
						if (isWindows()) {
							bacthFileExecution(isTestSet == true ? testsetName + ".bat" : "testng.bat", true);
						} else if (isUnix() || isSolaris() || isMac()) {
							bacthFileExecution(isTestSet == true ? testsetName + ".sh" : "testng.sh",true);
						} 			
					}

				}
				 
				  if(percentage == 10) {	
					  this.updateProgress(10, 10);
					  String  msg = executeAction == true ? "Sucessfully Executed" : "Sucessfully generated files";
					//  executeCommands(frameworkPath,gitURL,false,false,true);//push to git
					  this.printMessage(msg + "\n \n 4 / 4 tasks completed");
					  this.printMessage(msg + "\n \n 4 / 4 tasks completed");
					  
					  System.exit(0);
				  } 
			  }
			  
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("completed");
		return files;	
	}
	
	
	/**
	 * conversion : jsonobject to model
	 *  return dataset object;
	 */
	private static WebScript jsonParse(String output) {
		Gson gson = new Gson();
		WebScript datasetValues = (WebScript) gson.fromJson(output, WebScript.class);
		return datasetValues;
	}
	
	/** 
	 * printing the file Info
	 */
	
	private void printMessage(String file) throws InterruptedException {
		 this.updateMessage(file);
	        Thread.sleep(1000);	
	}

	/**
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
		String val = value.replaceAll(" ", "");
		val = val.replaceAll("[^a-z_A-Z0-9]", "");
		return val;
		}catch (Exception e) {
			return value;
		}
	}
	
	/**
	 * File writer method
	 */

	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator + className + fileExtension);
		} finally {
			out.close();
		}
	}
		
	
	
	/**
	 *  Using this method to executing the Batch file 
	 */
	private String response, errorResponse;
	
	private void bacthFileExecution(String batFileName,boolean isExecute) throws Exception {

		try {	
			this.printMessage("Configuring Maven ... \n \n 3 / 4 tasks completed");
			if(isExecute) {
			this.updateProgress(7, 10);
			}
			if(testSetLength >= 2 && isWindows()) {
				TestsetExecution.main(environmentVariable + File.separator +  batFileName);
				System.exit(0);
			} if(testSetLength >= 2 && isMac()) {
				TestsetExecution.main("/bin/bash -c" + environmentVariable + File.separator +  batFileName);
				System.exit(0);
			}
			Process p = Runtime.getRuntime().exec(environmentVariable + File.separator +  batFileName);
			
			
			final BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			final BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			this.printMessage("Configuring Maven ... \n \n 3 / 4 tasks completed");
			
			FileInfo current = this;
			 try {
				
						String line1;
						boolean isRunning = true;
						while (((line1 = (bri.readLine())) != null)) {
							
							response = response + line1;
							System.out.println(line1);
							try {
								if(isRunning && line1.contains("Running TestSuite")) {
									isRunning = false;
									current.printMessage("Running TestSuite ... \n \n 3 / 4 tasks completed");
									current.updateProgress(8.5, 10);
								
								}
							} catch (InterruptedException e) {
								e.printStackTrace();
								p.destroy();
							}

						}
						
					
				p.waitFor();
				//Future<String> futureResult = worker.submit(task);
				//futureResult.get(10, TimeUnit.SECONDS);
			} catch (Exception e) {}
			
			try {
				
				ExecutorService executor = Executors.newSingleThreadExecutor();
				Callable<String> tasker = new Callable<String>() {
					public String call() throws IOException {
						String line1;
						while (((line1 = (bre.readLine())) != null)) {
							System.out.println("======="+line1);
							errorResponse = errorResponse + line1;

						}
						return errorResponse;
					}
				};
				Future<String> futureResult = executor.submit(tasker);
				futureResult.get(10, TimeUnit.SECONDS);
			} catch (Exception e) {}
			
				
			bre.close();
			bri.close();
			
			System.out.println("========Done=======.");
			 
		} catch (Exception err) {
			err.printStackTrace();
		}
		
	}
	
	
	
	/**
	 *  Dynamically creating bat file for tesset by testsetname
	 */
	private void createBatFile(String name, String extension) throws Exception {
		String batFile = "";
		if(extension.equals(".sh")) {
			batFile="export PATH=$PATH:$M2_HOME/bin\n" + 
					"source ~/.bash_profile"
					+"\ncd $AUTOMATION_PATH\n" + 
					"mvn clean test -P"+ name +"\n" + 
					"pause\n" + 
					"\n" + 
					"";
		}else {
			 batFile="%AUTOMATION_DRIVE%\n" + 
				"cd %AUTOMATION_PATH%\n" + 
				"mvn clean test -P"+ name +"\n" + 
				"pause\n" + 
				"\n" + 
				"";
		}
		writeFile(batFile, name, environmentVariable, extension);
	}

	/**
	 * Creating testng file for testset by module name
	 * 
	 */
	
	private void writeTestSuiteFile(String packageFolder, String testClasses) throws Exception {
		String[] browsersList = browsers.split(",");
		String finalSet = "";
		for (int i = 0; i < browsersList.length; i++) {
			String browserName = browsersList[i];
			finalSet = finalSet + "	<test name=\"" + (browserName == null ? "" : browserName) + "test\">\r\n" + 
					   "  	<parameter name=\"browser\" value=\"" + (browserName == null ? "NoBrowser" : browserName) + "\"/>\r\n" +
					   "		<classes>" + testClasses + "\r\n\t\t</classes>\r\n" + 
					   "	</test>\r\n";
		}
		
		String  testngFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >\r\n" + 
				"<suite name=\"Main Test Suite\" parallel=\"tests\">\r\n" + 
				finalSet +
				"</suite>\r\n" + 
				"";
		writeFile(testngFile, packageFolder, environmentVariable, ".xml");
	}
	
	
	/** 
	 * Creating profiles in pom.xml
	 * Important Note : Don't change pom.xml 
	 * we just appending the profile before last 2 lines from pom.xml 
	 */
	
	private static void createProfiles(String profileName) throws Exception {
		String profile = "\t\t<profile>\n" + 
				"			<id>" + profileName + "</id>\n" + 
				"			<build>\n" + 
				"				<plugins>\n" + 
				"					<!-- Compiler plug-in -->\n" + 
				"					<plugin>\n" + 
				"						<groupId>org.apache.maven.plugins</groupId>\n" + 
				"						<artifactId>maven-compiler-plugin</artifactId>\n" + 
				"						<version>3.8.1</version>\n" + 
				"						<configuration>\n" + 
				"							<source>${jdk.level}</source>\n" + 
				"							<target>${jdk.level}</target>\n" + 
				"						</configuration>\n" + 
				"					</plugin>\n" + 
				"					<!-- Below plug-in is used to execute tests -->\n" + 
				"					<plugin>\n" + 
				"						<groupId>org.apache.maven.plugins</groupId>\n" + 
				"						<artifactId>maven-surefire-plugin</artifactId>\n" + 
				"						<version>3.0.0-M4</version>\n" + 
				"						<configuration>\n" + 
				"							 <forkMode>once</forkMode>\n"+ 
				"        					 <argLine>-Xms512m -Xmx512m</argLine>\n" + 
				"							<testFailureIgnore>true</testFailureIgnore>\n" + 
				"							<suiteXmlFiles>	\n"+
				"								<suiteXmlFile>"+ profileName +".xml</suiteXmlFile>\n" + 
				"							</suiteXmlFiles>\n" + 
				"						</configuration>\n" + 
				"					</plugin>\n" + 
				"				</plugins>\n" + 
				"			</build>\n" + 
				"		</profile>\n";
		
		/* Input appending the specific line */

		Path path = Paths.get(environmentVariable, "pom.xml");

		List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
		
		// position of appending the line
		
		int position = lines.size() - 2;

		/**
		 * pom.xml
		 * checking the profile is exists or not
		 */
		
		String uniqString = profile;
		if (!lines.toString().contains(profileName + ".xml")) {
			lines.add(position,uniqString);
			Files.write(path, lines, StandardCharsets.UTF_8);
		} 
	}
	
	private static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].trim().equals(""))
				continue;
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append("");
		}
		return sb.toString().trim();
	}
	
	/* Executing the bat/sh file to get the environment variables */
	
	private  ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + environmentVariable + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}
	
	/* Finding the OS */
	
	private static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	private static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	private static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
}