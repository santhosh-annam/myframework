package com.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class Utils {

	/*
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
			String val = value.replaceAll(" ", "");
			val = val.replaceAll("[^a-z_A-Z0-9]", "");
			return val;
		} catch (Exception e) {
			return value;
		}
	}
	
	
	public static void createReportPortalProperties(String environmentVariable,String projectName, String testName) {

		try {
			String reportPortalPropertiesfilePath = environmentVariable + File.separator + "src"+ File.separator + "test" + File.separator + "resources" +File.separator + "reportportal.properties";
			
			//for dv
			/*String data = "rp.endpoint = http://183.82.106.91:8080\n" + 
					"rp.uuid = 699ff82a-16e8-4912-a063-020eb11e177a\n" + 
					"rp.launch = QA_" + projectName + "_" + testName + "\n" + 
					"rp.project = " + projectName + "\n" + 
					"rp.enable = true";*/
			
			//For qf
			String data = "rp.endpoint = http://10.11.12.241:8080\n" + 
					"rp.uuid = 54766d56-9e56-42ea-a07f-d811195c5653\n" + 
					"rp.launch = QA_" + projectName + "_" + testName + "\n" + 
					"rp.project = " + projectName + "\n" +  
					"rp.enable = true";
			writeFile(reportPortalPropertiesfilePath, data);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private static void writeFile(String filePath, String data)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"));
		try {
			out.write(data);
		} finally {
			out.close();
		}
	}

	
}
