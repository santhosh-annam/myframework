package browser;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONObject;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


/*
 * This class used to bat/sh file creation  for Launching the Browser (Web/Mobile)
 */

public class Main extends Application {
	
	private static String environmentVariable = System.getenv("AUTOMATION_PATH") + File.separator + "WebDrivers";
	private static String[] data;
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static String addBash = "";
	
	public static void main(String[] args) {
		data = args;
		launch(args);
	}

	public void start(Stage primaryStage) throws Exception {
		ProgressIndicator progressIndicator = new ProgressIndicator();
		progressIndicator.setMaxSize(64, 64);
		progressIndicator.setProgress(0.5);
		StackPane root = new StackPane();
		root.getChildren().add(progressIndicator);
		Scene scene = new Scene(root, 300, 250);
		primaryStage.setTitle("Automation Framework");
		primaryStage.setScene(scene);
		primaryStage.show();
		
		// managing the code - Based on OS
		System.out.println(OS);
		String batName = "android.sh";
		String batBrowserName = "browser.sh";
		String extension = ".sh";
		if (isWindows()) {
			System.out.println("This is Windows");
			batName = "android.bat";
			batBrowserName = "browser.bat";
			extension = ".bat";
		} else if (isMac()) {
			try {
				String MAC_HOME_DIRECTORY = System.getProperty("user.home");
				environmentVariable = "bash " + MAC_HOME_DIRECTORY;
				createEnvironmentFile(MAC_HOME_DIRECTORY);

				ArrayList<String> envVariables = executeCommand("environmentVariables.sh");
				int size = envVariables.size();
				if (size > 0) {
					environmentVariable = envVariables.get(0) + File.separator + "WebDrivers";
					System.out.println("This is for Mac");
					addBash = "bash ";
				}
			} catch (Exception e) {
				ArrayList<String> envVariables = executeCommand("environmentVariables.sh");
				int size = envVariables.size();
				if (size > 0) {
					environmentVariable = envVariables.get(0) + File.separator + "WebDrivers";
					System.out.println("This is for Mac");
					addBash = "bash ";
				}
			}

		} else if(isUnix() || isSolaris()){
			environmentVariable = "bash " + System.getProperty("user.home");
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");	
			if(envVariables.size() > 0) {
				environmentVariable = envVariables.get(0) + File.separator + "WebDrivers";
				System.out.println("This is for Unix/solaris");
				addBash = "bash ";
			}
		} else {
			System.out.println("Your OS is not support!!");
		}
		
		InputStream is = new FileInputStream(data[0]);
		@SuppressWarnings("resource")
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();
		while(line != null){ 
			sb.append(line).append("\n"); 
			line = buf.readLine(); 
		} 
		String fileAsString = sb.toString();
		System.out.println("Contents : " + fileAsString);

		
		JSONObject jsonObject = new JSONObject(fileAsString);
		int jarType =  jsonObject.optInt("jar_type");
		String browserName = jsonObject.optString("browser_name");
		progressIndicator.setProgress(1);
		
		if (jarType == 2) { // For Android
			createJarLaunchFile("android", extension);
			bacthFileExecution(batName);
		} else if (jarType == 3) { // For Android browser
			createJarLaunchFileBrowser("android", extension);
			bacthFileExecution(batName);
		} else if(jarType == 1) { 
			
			createBatFile("browser", extension, browserName);
			
			if(isMac() || isWindows()) {
				bacthFileExecution(batBrowserName);
			} else {
				runScript(batBrowserName);
			}
		}
	
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
                System.out.println("closed");
                System.exit(0);
            }
        });
		
		primaryStage.close();
	}

	/**
	 * Creating the file in User Home directory to get the environment variables
	 * @param path
	 */
	private void createEnvironmentFile(String path) {
		 try {
			 File fileExists = new File(path + File.separator + "environmentVariables.sh");
				boolean exists = fileExists.exists();
				if(!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + 
					"printenv AUTOMATION_PATH\n" + 
					"printenv NODE_PATH\n" + 
					"printenv APPIUM_JS_PATH\n" + 
					"printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Setting the permissions to file for read, write and execute
	 * @param path
	 * @throws Exception
	 */
	private static void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}

	/* Creating the bat/sh files for Web Launching the Browser */
	
	private static void createBatFile(String name, String extension, String browserName) throws Exception {
		String finalData = StringEscapeUtils.unescapeHtml(data[0]);
		
		String jarName= "";
		if(browserName != null && !browserName.isEmpty() && browserName.equalsIgnoreCase("import_side_file")) {
			jarName = "seleniuemtoqf.jar"; //side file
		} else if(browserName != null && !browserName.isEmpty() && !browserName.equalsIgnoreCase("custom")) {
			jarName = "installedbrowser.jar"; //chrome, mozilla,ie,safari
		} else {
			jarName = "custombrowser.jar";
		}
		
		String batFile = "";
		if(extension.equals(".sh")) {
			 batFile =  "cd  " + environmentVariable
						+ "\njava -jar " + jarName + " "
						+ "\"" + StringEscapeUtils.escapeJava(finalData) + "\"" 
						+ "\n";
		} else {
			 batFile = "%AUTOMATION_DRIVE%"
						+ "\ncd %AUTOMATION_PATH%"
						+ "\ncd WebDrivers"
						+ "\njava -jar " + jarName + " "
						+ "\"" + data[0] + "\""
						+ "\n" + "pause\n" + "\n";
		}
		writeFile(batFile, name, environmentVariable, extension);
	}
	
	/* Creating the bat/sh files for Android Launching the Browser */
	private  void createJarLaunchFile(String name, String extension) throws Exception {
		//String finalData = StringEscapeUtils.unescapeHtml(data[0]);
		String batFile = "";
		if(extension.equals(".sh")) {
		 batFile = "export ANDROID_HOME=$ANDROID_HOME\n"
		 		+ "source ~/.bash_profile\n"
		 		+ "cd  " + environmentVariable
				+ "\njava -jar grabelements.jar " 
				+ "\""+  data[0] + "\""
				+ "\n";
		} else {
			 batFile = "%AUTOMATION_DRIVE%"
						+ "\ncd %AUTOMATION_PATH%"
						+ "\ncd WebDrivers"
						+ "\njava -jar grabelements.jar "
						+ "\""+  data[0] + "\"" 
						+ "\n";
		}
		writeFile(batFile, name, environmentVariable, extension);
	}
	
	/* Creating the bat/sh files for Android Launching the Browser */
	private  void createJarLaunchFileBrowser(String name, String extension) throws Exception {
		//String finalData = StringEscapeUtils.unescapeHtml(data[0]);
		String batFile = "";
		if(extension.equals(".sh")) {
		 batFile = "export ANDROID_HOME=$ANDROID_HOME\n"
		 		+ "source ~/.bash_profile\n"
		 		+ "cd  " + environmentVariable
				+ "\njava -jar mobileweb.jar " 
				+ "\""+  data[0] + "\""
				+ "\n";
		} else {
			 batFile = "%AUTOMATION_DRIVE%"
						+ "\ncd %AUTOMATION_PATH%"
						+ "\ncd WebDrivers"
						+ "\njava -jar mobileweb.jar "
						+ "\""+  data[0] + "\"" 
						+ "\n";
		}
		writeFile(batFile, name, environmentVariable, extension);
	}
	
	// writing the file and setting the permissions to read,write and Execute
	public static void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + File.separator +  className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator + className + fileExtension);
		} finally {
			out.close();
		}
	}
	
	/* Finding the Operating System */
	
	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
	
	/* Executing the bat/sh files */
	
	private  ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + environmentVariable + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}

	
	public static void runScript(String name) throws IOException, InterruptedException {
	    ProcessBuilder processBuilder = new ProcessBuilder(environmentVariable +File.separator + name);
	    //Sets the source and destination for subprocess standard I/O to be the same as those of the current Java process.
	    processBuilder.inheritIO();
	    Process process = processBuilder.start();

	    int exitValue = process.waitFor();
	    if (exitValue != 0) {
	        // check for errors
	        new BufferedInputStream(process.getErrorStream());
	        throw new RuntimeException("execution of script failed!");
	    }
	}
	
	
	private void bacthFileExecution(String batFileName) {
		try {
		   Runtime.getRuntime().exec(environmentVariable + File.separator + batFileName);
		   System.out.println("Done.");
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	
}