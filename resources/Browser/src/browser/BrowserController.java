package browser;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JOptionPane;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.html.HTMLIFrameElement;

import com.dto.PageDetails;
import com.dto.PageElements;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.PopupFeatures;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.StringConverter;

/* This class used to extracting the elements from the site */

public class BrowserController implements Initializable {

	@FXML
	BorderPane browserBP;
	@FXML
	WebView browserWV;
	@FXML
	WebView windowView;
	@FXML
	ImageView stopReloadIV;
	@FXML
	TextField addressBarTF;
	@FXML
	ProgressIndicator progressPI;
	@FXML
	Label statusL;
	@FXML
	Button getElements;

	private static String OS = System.getProperty("os.name").toLowerCase();
	public static final String Column1MapKey = "A";
	public static final String Column2MapKey = "B";
	public static final String Column3MapKey = "C";
	private boolean isSavingElements = false;
	ArrayList<PageElements> pageElementsList;
	ArrayList<PageElements> pageElementsList1;
	private Button saveElements;
	private String automationPath;
	private Stage dialog;
	Button frameBtn = null, frameBtn1 = null, frameBtn2 = null;
	private Label errorTextField;
	private ProgressIndicator progress;
	private String XML_ENCODER_FORMAT = "ISO-8859-1";
	private static String addBash = "";
	public static String JSON;
	public static String WEB_URL;
	public static String ELEMENTS_URL;
	public static int DIFF_ID;
	public static String WEB_PAGE_NAME;
	public static String WEB_PAGE_DESC;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		browserWV.setContextMenuEnabled(true);
		browserWV.getOnContextMenuRequested();
		
		browserWV.getEngine().setJavaScriptEnabled(true);
		String newUA = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36";
		browserWV.getEngine().setUserAgent(newUA);
		//browserWV.getEngine().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");
		// checking linux or windows
		System.out.println(OS);
		if (isWindows()) {
			automationPath = System.getenv("AUTOMATION_PATH");
			System.out.println("This is Windows");
		} else if (isMac()) {
			String MAC_HOME_DIRECTORY = System.getProperty("user.home");
			automationPath = "bash " + MAC_HOME_DIRECTORY;
			createEnvironmentFile(MAC_HOME_DIRECTORY);
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");
			if (envVariables.size() > 0) {
				automationPath = envVariables.get(0);
				System.out.println("This is for Mac");
				addBash = "bash ";
			}

		} else if (isUnix() || isSolaris()) {
			automationPath = "bash " + System.getProperty("user.home");
			ArrayList<String> envVariables = executeCommand("environmentVariables.sh");
			if (envVariables.size() > 0) {
				automationPath = envVariables.get(0);
				System.out.println("This is for Unix/solaris");
				addBash = "bash ";
			}
		}

		textColor(getElements);
		addressBarTF.textProperty().addListener((observable, oldValue, newValue) -> {
			getElements.setVisible(false);
		});

		
		browserWV.getEngine().setOnAlert(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText(event.getData());
            alert.showAndWait();
        });
		
//		browserWV.getEngine().setOnAlert(event -> {
//            Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setContentText(event.getData());
//            alert.showAndWait();
//        });
//		
//		
//		browserWV.getEngine().setOnAlert(event -> {
//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setContentText(event.getData());
//            alert.showAndWait();
//        });
//		
//		browserWV.getEngine().setOnAlert(event -> {
//            Alert alert = new Alert(Alert.AlertType.NONE);
//            alert.setContentText(event.getData());
//            alert.showAndWait();
//        });
//		browserWV.getEngine().setOnAlert(event -> {
//            Alert alert = new Alert(Alert.AlertType.WARNING);
//            alert.initOwner(browserWV.getScene().getWindow());
//            alert.setContentText(event.getData());
//            alert.showAndWait();
//        });
		

		browserWV.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
			@Override
			public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue,
					Worker.State newValue) {
				getElements.setVisible(false);
				statusL.setText("loading... " + browserWV.getEngine().getLocation());
				stopReloadIV.setImage(new Image(getClass().getResourceAsStream("/images/stoploading.png")));
				progressPI.setVisible(true);
				if (newValue == Worker.State.SUCCEEDED) {
					addressBarTF.setText(browserWV.getEngine().getLocation());
					statusL.setText("loaded");
			
					
					addressBarTF.positionCaret(addressBarTF.getText().length());
					getElements.setVisible(true);
					progressPI.setVisible(false);
					stopReloadIV.setImage(new Image(getClass().getResourceAsStream("/images/reload.png")));
					if (browserBP.getParent() != null) {
						TabPane tp = (TabPane) browserBP.getParent().getParent();
						for (Tab tab : tp.getTabs()) {
							if (tab.getContent() == browserBP) {
								tab.setText(browserWV.getEngine().getTitle());
								break;
							}
						}
					}
				}

			}

		});

		com.sun.javafx.webkit.WebConsoleListener.setDefaultListener((webView, message, lineNumber,
				sourceId) -> System.out.println("Console: [" + sourceId + ":" + lineNumber + "] " + message));

		// loading the URL
		if (WEB_URL != null) {
			addressBarTF.setText(WEB_URL);
			if (WEB_URL.isEmpty()) {
				JOptionPane.showMessageDialog(null, "No url provided");
				return;
			}
			if (!WEB_URL.startsWith("http://") && !WEB_URL.startsWith("https://")) {
				WEB_URL = "http://" + WEB_URL;
			}
			browserWV.getEngine().load(WEB_URL);
			
			browserWV.getEngine().setCreatePopupHandler(new Callback<PopupFeatures, WebEngine>() {

				@Override
				public WebEngine call(PopupFeatures param) {
					Stage stage = new Stage(StageStyle.UTILITY);
					VBox vbox = new VBox();
					windowView = new WebView();

					stage.initOwner(browserWV.getScene().getWindow());
					Button getElmnts = new Button("Get Elements");
					vbox.setAlignment(Pos.CENTER);
					textColor(getElmnts);
					vbox.getChildren().addAll(getElmnts, windowView);
					getElmnts.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent actionEvent) {
							try {
								 getWindowElements();
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					});
					Scene dialogScene = new Scene(vbox);
					stage.show();
					stage.setAlwaysOnTop(true);
					stage.setScene(dialogScene);
					stage.show();
					Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
					stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
					stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
					return windowView.getEngine();
				}

			});
		}

	}

	private void createEnvironmentFile(String path) {
		try {
			File fileExists = new File(path + File.separator + "environmentVariables.sh");
			boolean exists = fileExists.exists();
			if (!exists) {
				String environmentFileData = "source ~/.bash_profile\n" + "printenv AUTOMATION_PATH\n"
						+ "printenv NODE_PATH\n" + "printenv APPIUM_JS_PATH\n" + "printenv ANDROID_HOME\n";
				writeFile(environmentFileData, "environmentVariables", path, ".sh");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Setting the permissions to read, write and execute
	 * 
	 * @param path
	 * @throws Exception
	 */
	private void setPermissionsToFile(String path) throws Exception {
		if (isMac() || isSolaris() || isUnix()) {
			File file = new File(path);
			boolean isFile = file.exists();
			if (isFile) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);

				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);

				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);

				Files.setPosixFilePermissions(file.toPath(), perms);
				System.out.println("File permissions changed.");
			}
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.setReadable(true);
				file.setExecutable(true);
				file.setWritable(true);
			}
		}
	}

	/* Browser back button action */

	@FXML
	private void browserBackButtonAction(ActionEvent event) {
		if (browserWV.getEngine().getHistory().getCurrentIndex() <= 0) {
			return;
		}
		browserWV.getEngine().getHistory().go(-1);
	}

	/* Browser forward button action */
	@FXML
	private void browserForwardButtonAction(ActionEvent event) {
		if ((browserWV.getEngine().getHistory().getCurrentIndex() + 1) >= browserWV.getEngine().getHistory()
				.getEntries().size()) {
			return;
		}
		browserWV.getEngine().getHistory().go(1);
	}

	/* Browser Go button action */
	@FXML
	private void browserGoButtonAction(ActionEvent event) {
		String url = addressBarTF.getText().trim();
		if (url.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No url provided");
			return;
		}
		if (!url.startsWith("http://") && !url.startsWith("https://")) {
			url = "http://" + url;
		}
		browserWV.getEngine().load(url);

	}

	/* Browser X(stop) button action */
	@FXML
	private void browserStopReloadButtonAction(ActionEvent event) {
		if (browserWV.getEngine().getLoadWorker().isRunning()) {
			browserWV.getEngine().getLoadWorker().cancel();
			statusL.setText("loaded");
			progressPI.setVisible(false);
			stopReloadIV.setImage(new Image(getClass().getResourceAsStream("/images/reload.png")));
		} else {
			browserWV.getEngine().reload();
			stopReloadIV.setImage(new Image(getClass().getResourceAsStream("/images/stoploading.png")));
		}

	}

	/* Browser Home button action */
	@FXML
	private void browserHomeButtonAction(ActionEvent event) {
		browserWV.getEngine().loadContent("<html><title>New Tab</title></html>");
		addressBarTF.setText("");
	}


	@FXML
	private void getElements() throws Exception {
		String html = (String) browserWV.getEngine().executeScript("document.documentElement.outerHTML");
		// html = stripNonValidXMLCharacters(html);
		Writer out1 = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(automationPath + File.separator + "Elements.xml")));
		out1.write(html);
		out1.flush();
		out1.close();
		
		pageElementsList = new ArrayList<PageElements>();
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					getElementXpaths(false,false,"","",-1);
					getattributes(false,false);
				} catch (Exception e) {
					e.printStackTrace();
				}
		
			}
		});

	}

	@FXML
	private void getWindowElements() throws Exception {
		String html = (String) windowView.getEngine().executeScript("document.documentElement.outerHTML");
		// html = stripNonValidXMLCharacters(html);
		Writer out1 = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(automationPath + File.separator + "Elements.xml"), "UTF-8"));
		out1.write(html);
		out1.flush();
		out1.close();
		pageElementsList = new ArrayList<PageElements>();
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				
				try {
					getElementXpaths(false,false,"","",-1);
					getattributes(false,false);
					getIframeElements(windowView);
					getFrameElements(windowView);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});

	}
	


	/*-------------------------------- Extracting the elements Node by Node from source code -------------------------------*/
	
	
	private void getIframeElements(WebView view) throws Exception {
		org.w3c.dom.Document doc = view.getEngine().getDocument();
		if(doc == null) {
			return;
		}
		NodeList nodes = doc.getElementsByTagName("iframe");
		String id = "";
		String name = "";
		pageElementsList1 = new ArrayList<PageElements>();
		for (int i = 0; i < nodes.getLength(); i++) {
			if(nodes.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
			HTMLIFrameElement iframeElement = (HTMLIFrameElement) nodes.item(i);
			NamedNodeMap attributes = iframeElement.getAttributes();
			 id = ""; name = "";
			for (int j = 0; j < attributes.getLength(); j++) {
				
				String nodeName = attributes.item(j).getNodeName().trim();
				String nodeValue = attributes.item(j).getNodeValue().trim();
				if(nodeName.equalsIgnoreCase("id") && !nodeValue.isEmpty()) {
					id = nodeValue;
				} else if(nodeName.equalsIgnoreCase("name")) {
					name = nodeValue;
				} 
				String s = attributes.item(j).getNodeName();
				System.out.println(s +", " + attributes.item(j).getNodeValue());
			
			}
			if (iframeElement == null || iframeElement.getContentDocument() == null) 
				return;
			
			org.w3c.dom.Document iframeContentDoc = iframeElement.getContentDocument();
			org.w3c.dom.Element rootElement = iframeContentDoc.getDocumentElement();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			//transformer.setOutputProperty(OutputKeys.ENCODING, XML_ENCODER_FORMAT);
			DOMSource source = new DOMSource(rootElement);
			StreamResult result = new StreamResult(new File(automationPath + File.separator + "Iframe.xml"));
			// StreamResult result1 = new StreamResult(System.out);
			transformer.transform(source, result);
			// System.out.println(result1);
			System.out.flush();
			if(pageElementsList == null) {
				pageElementsList = new ArrayList<>();
			}
			getElementXpaths(true,false, id,name, i);
			
		}
		}
		if (dialog != null) {
			dialog.close();
		}
		getattributes(true,false);
		
	}
	
	private void getFrameElements(WebView view) throws Exception {
		pageElementsList = new ArrayList<>();
		org.w3c.dom.Document doc = view.getEngine().getDocument();
		NodeList nodes = doc.getElementsByTagName("frame");
		String id = "";
		String name = "";
		pageElementsList1 = new ArrayList<PageElements>();
		for (int i = 0; i < nodes.getLength(); i++) {
			id = "";
			name = "";
			HTMLIFrameElement iframeElement = (HTMLIFrameElement) nodes.item(i);
			for (int j = 0; j < iframeElement.getAttributes().getLength(); j++) {
				String nodeName = iframeElement.getAttributes().item(j).getNodeName().trim();
				String nodeValue = iframeElement.getAttributes().item(j).getNodeValue().trim();
				
				if(nodeName.equalsIgnoreCase("id") && !nodeValue.isEmpty()) {
					id = nodeValue;
				} else if(nodeName.equalsIgnoreCase("name")) {
					name = nodeValue;
				} 
				String s = iframeElement.getAttributes().item(j).getNodeName();
				System.out.println(s +", " + iframeElement.getAttributes().item(j).getNodeValue());
			}
			if (iframeElement == null || iframeElement.getContentDocument() == null)
				return;
			org.w3c.dom.Document iframeContentDoc = iframeElement.getContentDocument();
			org.w3c.dom.Element rootElement = iframeContentDoc.getDocumentElement();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, XML_ENCODER_FORMAT);
			DOMSource source = new DOMSource(rootElement);
			StreamResult result = new StreamResult(new File(automationPath + File.separator + "Iframe.xml"));
			// StreamResult result1 = new StreamResult(System.out);
			transformer.transform(source, result);
			// System.out.println(result1);
			System.out.flush();
			if(pageElementsList == null) {
				pageElementsList = new ArrayList<>();
			}
			getElementXpaths(false,true, id,name, i);
			
		}
		getattributes(false,true);
		
	}
			
	boolean isPresentIframes;
	boolean isPresentFrames;
	private String seperatorVal = "=FIELD_VALUE=";
	private void getElementXpaths(boolean isIframe, boolean isframe, String iframeid, String iframeName, int iframeIndex) throws Exception {
		isPresentIframes = false;
		isPresentFrames = false;
		File inputFile = null;
		if(isIframe || isframe) {
		 inputFile = new File(automationPath + File.separator + "iframe.xml");
		} else {
		 inputFile = new File(automationPath + File.separator  + "Elements.xml");
		}
		Document doc = Jsoup.parse(inputFile, null);
		ArrayList<String> tagNames = new ArrayList<>();
		tagNames.add("iframe");
		tagNames.add("frame");
		tagNames.add("a");
		tagNames.add("span");
		tagNames.add("button");
		tagNames.add("label");
		tagNames.add("input");
		tagNames.add("textarea");
		tagNames.add("select");
		tagNames.add("img");
		tagNames.add("ul");
		tagNames.add("li");
		tagNames.add("table");
		tagNames.add("div");
		tagNames.add("tr");
		tagNames.add("td");
		tagNames.add("H");
	
		Elements elements = null;
		for (int i = 0; i < tagNames.size(); i++) {
			 elements = doc.getElementsByTag(tagNames.get(i));
			if (tagNames.get(i).equalsIgnoreCase("frame")) {
				if (elements.size() > 0) {
					isPresentFrames = true;
				}
			} 
			if (tagNames.get(i).equalsIgnoreCase("iframe")) {
				if (elements.size() > 0) {
					isPresentIframes = true;
				}
			}
			 for (Element element : elements) {
					String fieldName = "";
					String name = nullToEmpty(element.attr("name"));
					String id = nullToEmpty(element.attr("id"));
					String placeholder = nullToEmpty(element.attr("placeholder"));
					String title = nullToEmpty(element.attr("title"));
					String toolTipTitle = nullToEmpty(element.attr("data-original-title"));
					String ariaLabel = nullToEmpty(element.attr("aria-label"));
					String href = nullToEmpty(element.attr("href"));
					String alt = nullToEmpty(element.attr("alt"));
					String src = nullToEmpty(element.attr("src"));
					String value = nullToEmpty(element.attr("value"));
					String role = nullToEmpty(element.attr("role"));
					String fieldType = element.attr("type");
					String classAttr = element.attr("class");
					String ariaOwns = element.attr("aria-owns");
					String ariaDescribedBy = element.attr("aria-describedby");
					//String dataTestId = element.attr("data-test-id");
					
					
					String tagName = element.tagName();
					
					if (tagName.equalsIgnoreCase("UL") || tagName.equalsIgnoreCase("table") || tagName.equalsIgnoreCase("tr") || tagName.equalsIgnoreCase("td")) {
						fieldType = "Label";
					}
					
					
					if (tagName.equalsIgnoreCase("li") && !role.isEmpty() && !role.equalsIgnoreCase("textbox")) {
						fieldType  = role.equalsIgnoreCase("button") ? "Button" : "Link";
					} else if (tagName.equalsIgnoreCase("li") && role.equalsIgnoreCase("textbox")) {
						fieldType = "InputText";
					} else if (tagName.equalsIgnoreCase("li")) {
						fieldType = "Label";
					}
					
					if(tagName.equalsIgnoreCase("li")  && nullToEmpty(element.ownText()).isEmpty()) {
						continue;
					}
					
					System.out.println("\n ==================================================");
					
					boolean validate = false;
					String selectOptions = "";
					if (element.tagName().equalsIgnoreCase("select")) {
						selectOptions = element.html();
						if (fieldName.isEmpty()) {
							fieldName = element.previousElementSibling()!= null ? element.previousElementSibling().text() : "";
						}
						//validate = true;
						System.out.println("Element select Text:" + element.html());
					} 
					
				if (fieldType.equalsIgnoreCase("Checkbox") || tagName.equalsIgnoreCase("input")
						|| tagName.equalsIgnoreCase("textarea") || fieldType.equalsIgnoreCase("radio")) {

					if (placeholder.isEmpty()) {
						fieldName = element.ownText();
						if (fieldName.isEmpty()) {
							fieldName = element.text();
						}
						if (fieldName.isEmpty()) {
							fieldName = element.previousElementSibling() != null ? element.previousElementSibling().text() : "";
						}
					} else {
						fieldName = placeholder;
					}
				} else if (tagName.equalsIgnoreCase("div") &&  (role.equalsIgnoreCase("button") || role.equalsIgnoreCase("link"))) {
						System.out.println("Element parent Text:" + element.parent().text());
						fieldName = element.text();
						fieldType  = role.equalsIgnoreCase("button") ? "Button" : "Link";
				} else if (tagName.equalsIgnoreCase("a")) {
					if (!element.ownText().isEmpty()) {
						fieldName = nullToEmpty(element.ownText());
					} else {
						fieldName = nullToEmpty(element.text());
					}
				} else if (element.ownText().isEmpty()) {
						System.out.println("Element parent Text:" + element.parent().text());
						//fieldName = element.parent().text().length() > 20 ? "" : element.parent().text();
				} else {
						System.out.println("Element Text:" + element.text());
						fieldName = element.ownText();
				}
					System.out.println("Element Placeholder:" + placeholder);
					System.out.println("Element name:" + name);
					System.out.println("Element title:" + title);
					System.out.println("Element alt:" + alt);
					System.out.println("Element value:" + value);
					System.out.println("Element tooltipTitle:" + toolTipTitle);
					System.out.println("Element href:" + href);
					System.out.println("Element src:" + src);
					System.out.println("Element tel:" + fieldType);
		
					if (fieldName.trim().isEmpty()) {
						fieldName = getFieldName(placeholder, name, toolTipTitle, ariaLabel, title, alt, value, href, src,role,fieldType,ariaOwns,id,ariaDescribedBy,classAttr);
					}
					
					
					// String path = CSS2XPath.css2xpath(element.cssSelector(), true);
					System.out.println("Node name : " + element.nodeName());
					System.out.println("      Tag : " + element.tagName());
					System.out.println("      Type : " + element.attr("type"));
					
					if (tagName.equalsIgnoreCase("input") || tagName.equalsIgnoreCase("textarea")) {
						if (fieldType.equalsIgnoreCase("submit") || fieldType.equalsIgnoreCase("reset")
								|| fieldType.equalsIgnoreCase("image") || fieldType.equalsIgnoreCase("button")
								|| fieldType.equalsIgnoreCase("file") || fieldType.equalsIgnoreCase("checkbox")
								|| fieldType.equalsIgnoreCase("radio")) {
							fieldType = "Button";
						} else {
							fieldType = "InputText";
						}
					} else if (tagName.equalsIgnoreCase("img") &&  (role.equalsIgnoreCase("button") || role.equalsIgnoreCase("link"))) {
						//System.out.println("Element parent Text:" + element.parent().text());
						fieldType  = role.equalsIgnoreCase("button") ? "Button" : "Link";
					} else if (tagName.equalsIgnoreCase("a") || tagName.equalsIgnoreCase("select") || tagName.equalsIgnoreCase("img")) {
						fieldType = "Link";
					} else if (tagName.equalsIgnoreCase("button")) {
						fieldType = "Button";
					} else if (tagName.equalsIgnoreCase("label") || tagName.equalsIgnoreCase("span")) {
						fieldType = "Label";
					}  else if (tagName.equalsIgnoreCase("iframe") || tagName.equalsIgnoreCase("frame")) {
						fieldType = "Link";
					}
					String cssSelector = "";
					try {
					cssSelector = element.cssSelector(); //css xapth
					} catch (Exception e) {
						e.printStackTrace();
					}
					ArrayList<String> xpathArrrayList = new ArrayList<>();
					ArrayList<String> cssxpathArrrayList = new ArrayList<>();
//					if(id.isEmpty()) {
//						
//					}  else if (!name.isEmpty() && !value.isEmpty()){
//						//css=input[name=email]
//						cssSelector = tagName + "[name=" + name + "][value=" + value + "]";
//					} else if (!name.isEmpty()){
//						//css=input[name=email]
//						cssSelector = tagName + "[name=" + name + "]";
//					}
					String idValue = "",preferedidValue = "",preferednameValue= "",preferedtextValue="",preferedtitleValue="",preferedlinkText="", preferedcssXpath = "",preferedabsolutexPath="";
					
					
					cssxpathArrrayList.add("css_xpath" + seperatorVal + cssSelector);
					preferedcssXpath = "css_xpath" + seperatorVal  + cssSelector;
					
					
					Iterator<Attribute> it = element.attributes().iterator();
					while (it.hasNext()) {
						Attribute a = it.next();
						
						if (a.getValue() != null && !a.getValue().toString().trim().isEmpty()) {
							String xpath = "xpath_" + a.getKey() +  seperatorVal  + ("//" + tagName + "[@" + a.getKey() + "='" + a.getValue() + "']");
							String cssXpath = tagName + "[" + a.getKey() + "='" + a.getValue() + "']";
							if(a.getKey().equalsIgnoreCase("id")) {
								idValue = xpath;
								xpathArrrayList.add(0,xpath);
								preferedidValue = xpath;
							} else if(a.getKey().equalsIgnoreCase("name")) {
								if(idValue.isEmpty()) {
									xpathArrrayList.add(0,xpath);
								} else {
									xpathArrrayList.add(1,xpath);
								}	
								preferednameValue = xpath;
							}  else if (a.getKey().equalsIgnoreCase("title")) {
								preferedtitleValue = xpath;
							}  else {
								xpathArrrayList.add(xpath);
							}
							
						 if(a.getKey().equalsIgnoreCase("value")) {
								if(!name.isEmpty()) {
									cssXpath = tagName + "[name='" + name + "'][value='" + value + "']";
									//xpathArrrayList.add(xpath);
								} else {
									cssXpath = tagName + "[value='" + value + "']";
								}
						 }
						 cssxpathArrrayList.add("css_" + a.getKey() + seperatorVal  + cssXpath);
						 System.out.println("Xpaths : " + xpath);
						}
					}

					String ownElementText = element.ownText();
					if (!nullToEmpty(ownElementText).isEmpty()) { // tagname[contains(text(),'Signin')]
						String containsXpath = ("//" + tagName + "[contains(text(),'" + ownElementText + "')]");
						//String cssXpath = tagName + "[contains(text='" + ownElementText + "')]";
						System.out.println("contains  Xpath : " + containsXpath);
						
						if(tagName.equalsIgnoreCase("a")) {
							String linkText =  ownElementText;
							xpathArrrayList.add("linkText" + seperatorVal + linkText);
							preferedlinkText = "linkText" + seperatorVal + linkText;
						}
						//cssxpathArrrayList.add("css_text" + seperatorVal + cssXpath);
						xpathArrrayList.add("xpath_text" + seperatorVal + containsXpath);
						 
						preferedtextValue = "xpath_text" + seperatorVal + containsXpath;
						 
						fieldName = element.ownText();
					}
					
					
					String[] xpaths = getXPaths(element, element.tagName()).split("dlready");
					System.out.println(xpaths);
					String idPath = ""; String absoluteXpath = "";
					int xpathLength = xpaths.length;
					if(xpathLength > 0) {
						if(xpathLength == 1) {
							absoluteXpath = xpaths[0];
							
							if (absoluteXpath.contains("a[") || absoluteXpath.contains("A[") || absoluteXpath.contains("/a") ) {
								fieldType = "Link";
							} else if (absoluteXpath.contains("button[") || absoluteXpath.contains("Button[")) {
								fieldType = "Button";
							}
							xpathArrrayList.add("absolute_xpath" + seperatorVal  + absoluteXpath);
							preferedabsolutexPath = "absolute_xpath" + seperatorVal + absoluteXpath;
						} else if (xpathLength >= 2) {
							absoluteXpath = xpaths[0];
							if (absoluteXpath.contains("a[") || absoluteXpath.contains("A[") || absoluteXpath.contains("/a") ) {
								fieldType = "Link";
							} else if (absoluteXpath.contains("button[") || absoluteXpath.contains("Button[")) {
								fieldType = "Button";
							}
							idPath = xpaths[1];
							
								xpathArrrayList.add("xpath_id_relative" + seperatorVal + idPath);
								if(id.isEmpty()) {
									preferedidValue = "xpath_id_relative" + seperatorVal + idPath;
								}
							xpathArrrayList.add("absolute_xpath" + seperatorVal + absoluteXpath);
							preferedabsolutexPath = "absolute_xpath" + seperatorVal  + absoluteXpath;
						} 
					}
				
				if (tagName.equals("span") && fieldName.isEmpty()) {
					continue;
				} else if (fieldName.isEmpty() && tagName.equals("div")) {
					continue;
				} else if (!fieldName.isEmpty() && tagName.equals("div")) {
					if(!fieldType.isEmpty()) {
						//fieldType = "Button";
					} else if (absoluteXpath.contains("a[") || absoluteXpath.contains("A[") || absoluteXpath.contains("/a") ) {
						fieldType = "Link";
					} else if (absoluteXpath.contains("button[") || absoluteXpath.contains("Button[")) {
						fieldType = "Button";
					} else {
						fieldType = "Label";
					}
				}	
				xpathArrrayList.addAll(cssxpathArrrayList);
				
				if(tagName.equalsIgnoreCase("select") && fieldName.isEmpty()) {
					if(!id.isEmpty()) {
						fieldName =  id;
					} else if(!classAttr.isEmpty()){
						fieldName =  classAttr;
					}
				}
				
				if((!element.ownText().isEmpty() || !element.text().isEmpty()) && (nullToEmpty(element.ownText()).equalsIgnoreCase(fieldName) || nullToEmpty(element.text()).equalsIgnoreCase(fieldName))) {
					validate = true;
				}
				PageElements pageElements = new PageElements();
				System.out.println(xpathArrrayList.toString());
				String list = "";
				for (int k = 0; k < xpathArrrayList.size(); k++) {
					list = list + xpathArrrayList.get(k) + "XPATH_SEPARATOR";
				}
				
				preferField(preferedidValue,preferednameValue,preferedtextValue,preferedlinkText,preferedtitleValue,preferedcssXpath, preferedabsolutexPath, pageElements);
				
				pageElements.setAll_xpaths(list);
				pageElements.setIs_validate(validate);
				//pageElements.setRelative_xpath_by_name();
				//pageElements.setRelative_xpath_by_id(xpathArrrayList.get(0));
				pageElements.setName(fieldName.trim());// field name
				pageElements.setInput_type(fieldType.trim()); // field type
				// System.out.println(pageElements.getName());
				if (isIframe) {
					pageElements.setIframe_id(iframeid);
					pageElements.setIframe_position(iframeIndex);
					pageElements.setIframe_name(iframeName.trim().isEmpty() ? "IFRAME" : iframeName);
					pageElements.setIs_iframe(true);
				}
				if (isframe) {
					pageElements.setIframe_id(iframeid);
					pageElements.setIframe_position(iframeIndex);
					pageElements.setIframe_name(iframeName.trim().isEmpty() ? "FRAME" : iframeName);
					pageElements.setIs_frame(true);
				}
				
				pageElements.setChildRelativeXpathId(xpathArrrayList.get(0)); // only for display
				pageElements.setTag_name(tagName.trim());
				pageElements.setChild_id(fieldName.trim());
				if(tagName.equals("select")) {
					pageElements.setChild_text(selectOptions);
				} else {
					pageElements.setChild_text(fieldName);
				}
				pageElementsList.add(pageElements);
			}
			 
			if(pageElementsList != null)
			pageElementsList.sort(Comparator.comparing(PageElements::getName).reversed());
					
		}
		
	}

	private static String nullToEmpty(String elementData) {
		return (elementData == null) ? "" : elementData.trim();
	}
	
	
	String selectPreferField = "";
	private void preferField(String xpathId,String xpathName, String preferedtextValue, String preferedlinkText, String preferedtitleValue, String preferedcssXpath, String absoluteXpath,PageElements pageElements) {
		//Id,Name,xpath Name,xpath text,Css xpath,absolutexpath,xpath,Linktext
		try {
			if (!xpathId.isEmpty() && xpathId.contains("xpath_id" + seperatorVal) &&  !xpathId.contains("xpath_id_relative" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_id");
				pageElements.setSelected_xpath(xpathId.split("xpath_id"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!xpathId.isEmpty() && xpathId.contains("xpath_id_relative" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_id_relative");
				pageElements.setSelected_xpath(xpathId.split("xpath_id_relative"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			}else if (!xpathName.isEmpty() && xpathName.contains("xpath_name" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_name");
				pageElements.setSelected_xpath(xpathName.split("xpath_name"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!preferedtextValue.isEmpty() && preferedtextValue.contains("xpath_text" + seperatorVal)) {
				pageElements.setPrefered_field("xpath_text");
				pageElements.setSelected_xpath(preferedtextValue.split("xpath_text"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!preferedcssXpath.isEmpty() && preferedcssXpath.contains("css_xpath" + seperatorVal)) {
				pageElements.setPrefered_field("css_xpath");
				pageElements.setSelected_xpath(preferedcssXpath.split("css_xpath"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!absoluteXpath.isEmpty() && absoluteXpath.contains("absolute_xpath" + seperatorVal)) {
				pageElements.setPrefered_field("absolute_xpath");
				pageElements.setSelected_xpath(absoluteXpath.split("absolute_xpath"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			} else if (!preferedlinkText.isEmpty() && preferedlinkText.contains("linktext" + seperatorVal)) {
				pageElements.setPrefered_field("linktext");
				pageElements.setSelected_xpath(preferedlinkText.split("linktext"+ seperatorVal)[1].replaceAll("=FIELD_VALUE=", ""));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private  String getFieldName(String placeholder, String name, String toolTipTitle, String ariaLabel,
			String title, String alt, String value, String href, String src, String role, String type,String ariaOwns,String id,String ariaDescribedBy, String classAttr) {
		
		if (!placeholder.isEmpty()) {
			return placeholder;
		} else if (!name.isEmpty()) {
			return name;
		} else if (!toolTipTitle.isEmpty()) {
			return toolTipTitle;
		} else if (!ariaLabel.isEmpty()) {
			return ariaLabel;
		} else if (!title.isEmpty()) {
			return title;
		} else if (!alt.isEmpty()) {
			return alt;
		} else if (!value.isEmpty()) {
			return value;
		} else if (!href.isEmpty()) {
			return href;
		} else if (!ariaDescribedBy.isEmpty()) {
			return ariaDescribedBy;
		} else if (!ariaOwns.isEmpty()) {
			return ariaOwns;
		}  else if (!src.isEmpty()) {
			return src;
		} else if (!id.isEmpty()) {
			return id;
		} else if (!type.isEmpty()) {
			return type;
		} else if (!role.isEmpty()) {
			return role;
		}  else if (!classAttr.isEmpty()) {
			return classAttr;
		} 
		
		return "";
	}

	private static String getXPaths(Node node, String tagName) {
		Node parent = node.parent();
		return getXPath(parent, tagName);
	}

	private static String getXPath(Node root, String tagName) {
		String xpathByID = "";
		Node current = root;
		String output = "";
		while (current.parentNode() != null) {
			Node parent = current.parentNode();
			if (parent != null && parent.childNodes().size() > 1) {
				int nthChild = 1;
				Node siblingSearch = current;
				while ((siblingSearch = siblingSearch.previousSibling()) != null) {
					// only count siblings of same type
					if (siblingSearch.nodeName().equals(current.nodeName())) {
						nthChild++;
					}
				}

				Element eElement = (Element) current;
				if (xpathByID.isEmpty() && !eElement.attr("id").isEmpty()) {
					xpathByID = eElement.attr("id");
					xpathByID = "//" + eElement.tagName() + "[@id='" + xpathByID + "']" + output + "/" + tagName;
				}
				output = "/" + current.nodeName() + "[" + nthChild + "]" + output;
			} else {
				output = "/" + current.nodeName() + output;
			}
			current = current.parentNode();
		}
		return output + "/" + tagName +"dlready" + xpathByID;
	}


	

	/*---------------------------------- TABLEVIEW DATA ------------------------------------------ */

	@SuppressWarnings("rawtypes")
	private ObservableList<Map> generateDataInMap(ArrayList<PageElements> pagedetailsList) {
		ObservableList<Map> allData = FXCollections.observableArrayList();
		for (int i = 0; i < pagedetailsList.size(); i++) {
			Map<String, String> dataRow = new HashMap<>();
			String value1 = pageElementsList.get(i).getName();
			String value2 = pageElementsList.get(i).getInput_type();
			String value3 = pageElementsList.get(i).getSelected_xpath();
			dataRow.put(Column1MapKey, value1);
			dataRow.put(Column2MapKey, value2);
			dataRow.put(Column3MapKey, value3);
			allData.add(dataRow);
		}
		return allData;
	}
	
	
	

	public void textColor(Button btn) {
		btn.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
		btn.setStyle("-fx-text-fill: white; -fx-base: #00bebf;");
	}

	public void textColor(Label errorTextField) {
		if (errorTextField != null) {
			errorTextField.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
			errorTextField.setStyle("-fx-text-fill: red; -fx-base: #ffffff;");
		}
	}


	/* Showing the elements in Dialog */

	@SuppressWarnings({ "rawtypes", "static-access", "unchecked" })
	public void getattributes(boolean fromIframe,boolean fromFrame) {

		try {
			/*---------------------------------------- TABLEVIEW -------------------------------------------*/

			if (pageElementsList.size() > 0) {
				dialog = new Stage();
				dialog.initModality(Modality.WINDOW_MODAL);
				dialog.initOwner(browserWV.getScene().getWindow());
				VBox dialogVbox = new VBox(20);
				dialog.initStyle(StageStyle.UTILITY);
				dialog.resizableProperty().setValue(Boolean.FALSE);
				HBox elementsHbox = new HBox();
				final Label label = new Label("Elements");
				TextField filterField = new TextField();
				filterField.setFocusTraversable(false);
				filterField.setFont(new Font("proximaNovaSemiBold", 12));
				filterField.setPromptText("Search Elements");
				filterField.setMinWidth(200);
				label.setPadding(new Insets(0, 10, 0, 0));

				final TextField textField = new TextField();
				final TextField descTextField = new TextField();
				progress = new ProgressIndicator();
				progress.setVisible(false);
				errorTextField = new Label();
				textColor(errorTextField);
				elementsHbox.getChildren().addAll(label, filterField);
				HBox hBox = new HBox();
				hBox.setAlignment(Pos.CENTER);

				saveElements = new Button("Save Elements");

				textColor(saveElements);

				hBox.getChildren().addAll(textField, descTextField, saveElements, progress);

				textField.setFont(new Font("proximaNovaSemiBold", 14));
				textField.setPromptText("Enter Page Name");
				textField.setMinWidth(200);

				descTextField.setFont(new Font("proximaNovaSemiBold", 14));
				descTextField.setPromptText("Enter Page Description");
				descTextField.setMinWidth(200);

				label.setFont(new Font("Arial", 20));

				hBox.setMargin(saveElements, new Insets(5));
				hBox.setMargin(descTextField, new Insets(5));
				hBox.setMargin(textField, new Insets(5));

				saveElements.setMaxHeight(60);
				HBox iframeBox = new HBox();
				// saveElements.setPadding(new Insets(10, 0, 0, 10));

				TableColumn<Map, String> firstDataColumn = new TableColumn<>("Field Name");
				TableColumn<Map, String> secondDataColumn = new TableColumn<>("Field Type");
				TableColumn<Map, String> thirdDataColumn = new TableColumn<>("Field Locator");

				firstDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");

				// firstDataColumn.setEditable(false);
				firstDataColumn.setCellValueFactory(new MapValueFactory(Column1MapKey));
				firstDataColumn.setMinWidth(200);

				secondDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");
				// secondDataColumn.setEditable(false);
				secondDataColumn.setCellValueFactory(new MapValueFactory(Column2MapKey));
				secondDataColumn.setMinWidth(200);

				thirdDataColumn.setStyle("-fx-font: 18; -fx-base: #00bebf;");
				// thirdDataColumn.setEditable(false);
				thirdDataColumn.setCellValueFactory(new MapValueFactory(Column3MapKey));
				thirdDataColumn.setMinWidth(420);

				firstDataColumn.setSortable(false);
				secondDataColumn.setSortable(false);
				thirdDataColumn.setSortable(false);

				ObservableList<Map> data;

				TableView tableView = null;
				
				tableView = new TableView<>(data = generateDataInMap(pageElementsList));
				
				tableView.setEditable(true);
				tableView.setBackground(null);
				tableView.setFocusTraversable(false);
				tableView.getSelectionModel().setCellSelectionEnabled(true);
				if (isPresentIframes) {
					Button iframeBtn = new Button("Get Iframes");
					iframeBtn.setVisible(true);
					iframeBtn.setStyle("-fx-font: 13 proximaNovaSemiBold; -fx-base: #00bebf;");
					iframeBtn.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent actionEvent) {
							try {
								getIframeElements(browserWV);
								iframeBtn.setVisible(false);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					iframeBox.setMargin(iframeBtn, new Insets(5));
					iframeBox.getChildren().addAll(iframeBtn);
				}
				
				if (isPresentFrames) {
					Button frameBtn = new Button("Get Frames");
					frameBtn.setVisible(true);
					frameBtn.setStyle("-fx-font: 13 proximaNovaSemiBold; -fx-base: #00bebf;");
					frameBtn.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent actionEvent) {
							try {
								getFrameElements(browserWV);
								frameBtn.setVisible(false);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					iframeBox.setMargin(frameBtn, new Insets(5));
					iframeBox.getChildren().addAll(frameBtn);
				}
				
				tableView.getColumns().setAll(firstDataColumn, secondDataColumn, thirdDataColumn);
				Callback<TableColumn<Map, String>, TableCell<Map, String>> cellFactoryForMap = (
						TableColumn<Map, String> p) -> new TextFieldTableCell(new StringConverter() {
							@Override
							public String toString(Object t) {
								return t.toString();
							}

							@Override
							public Object fromString(String string) {
								return string;
							}
				});
				firstDataColumn.setCellFactory(cellFactoryForMap);
				secondDataColumn.setCellFactory(cellFactoryForMap);
				thirdDataColumn.setCellFactory(cellFactoryForMap);
				saveElements.setFocusTraversable(false);
				textField.setFocusTraversable(false);
				descTextField.setFocusTraversable(false);
				if (DIFF_ID > 0) {
					textField.setText(WEB_PAGE_NAME);
					descTextField.setText(WEB_PAGE_DESC);
					descTextField.setDisable(true);
					textField.setDisable(true);
				}

				/* -------------------------- Save Elements Button * -------------------------------- */
				PageDetails details = new PageDetails();
				details.setElements_list(pageElementsList);
				saveElements.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent actionEvent) {

						if (isSavingElements) {
							return;
						}

						new Thread() {
							public void run() {
								if (textField.getText().trim().length() > 0) {
									try {
										progress.setVisible(true);
										isSavingElements = true;
										JSONObject jsonObj = new JSONObject(JSON);
										
										details.setWeb_page_name(textField.getText());
										details.setWeb_page_url(addressBarTF.getText());
										details.setModule_id(jsonObj.getInt("module_id"));
										details.setDiff_page_id(DIFF_ID);
										details.setBrowser_name("custom");
										details.setWeb_page_description(descTextField.getText() == null ? "" : descTextField.getText());
										details.setUser_id(jsonObj.getInt("user_id"));
										try {
											Gson gsonObj = new GsonBuilder().disableHtmlEscaping().create();
											String json = gsonObj.toJson(details);
											System.out.println(json);
											boolean isPosted = doSaveElementsToServer(json);

											Platform.runLater(new Runnable() {
												@Override
												public void run() {
													isSavingElements = false;
													if (!isPosted) {
														progress.setVisible(false);
													} else if (isPosted && dialog != null) {
														dialog.close();
													}
												}
											});

										} catch (Exception e) {
											e.printStackTrace();
										}

									} catch (JSONException e1) {
										e1.printStackTrace();
									}
								} else {
									try {
										 final Tooltip toolTip = new Tooltip("This field is required.");
										 addToolTipAndBorderColor(textField, toolTip);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}.start();
					}
				});
				// 1. Wrap the ObservableList in a FilteredList (initially display all data).
				FilteredList<Map> filteredData = new FilteredList<>(data, p -> true);

				// 2. Set the filter Predicate whenever the filter changes.
				filterField.textProperty().addListener((observable, oldValue, newValue) -> {
					filteredData.setPredicate(person -> {
						// If filter text is empty, display all persons.
						if (newValue == null || newValue.isEmpty()) {
							return true;
						}

						// Compare first name and last name of every person with filter text.
						String lowerCaseFilter = newValue.toLowerCase();

						if (person.get(Column1MapKey).toString().toLowerCase().contains(lowerCaseFilter)) {
							return true; // Filter matches first name.
						} else if (person.get(Column2MapKey).toString().toLowerCase().contains(lowerCaseFilter)) {
							return true; // Filter matches last name.
						}
						return false; // Does not match.
					});
				});

				// 3. Wrap the FilteredList in a SortedList.
				SortedList<Map> sortedData = new SortedList<>(filteredData);

				// 4. Bind the SortedList comparator to the TableView comparator.
				sortedData.comparatorProperty().bind(tableView.comparatorProperty());

				// 5. Add sorted (and filtered) data to the table.
				tableView.setItems(sortedData);

				final VBox vbox = new VBox();

				vbox.setSpacing(5);
				vbox.setPadding(new Insets(10, 0, 10, 10));
				hBox.setPadding(new Insets(10, 10, 10, 10));
				vbox.getChildren().addAll(elementsHbox, tableView, errorTextField, hBox, iframeBox);

				dialogVbox.getChildren().addAll(vbox);

				Scene dialogScene = new Scene(dialogVbox, 850, 500);
				dialog.setAlwaysOnTop(true);
				dialog.setScene(dialogScene);
				dialog.show();

			}
		} catch (Exception e) {
			if (errorTextField != null) {
				textColor(errorTextField);
				errorTextField.setText("Something went wrong.");
			}

			e.printStackTrace();
		}
	}
	
	private static final String STILE_BORDER_VALIDATION = "-fx-border-color: #FF0000";
	private static void addToolTipAndBorderColor(javafx.scene.Node n, Tooltip t) {
		Tooltip.install(n, t);
		n.setStyle(STILE_BORDER_VALIDATION);
	}
	
	
	/*----------------------------------------- SAVE ELEMENETS TO SERVER ----------------------*/

	private boolean doSaveElementsToServer(String json) throws Exception {
		try {
			URL obj = new URL(ELEMENTS_URL);
			System.out.println(ELEMENTS_URL);
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO Auto-generated method stub

				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(200000);
			con.setConnectTimeout(200000);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			byte[] isoString = json.getBytes("UTF-8");
			
			wr.write(isoString, 0, isoString.length);
			// wr.writeBytes(json);
			wr.flush();
			wr.close();
			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(response.toString());

			JSONObject jsonObject = new JSONObject(response.toString());

			if (jsonObject.has("status") && !jsonObject.getString("status").equalsIgnoreCase("SUCCESS")) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (errorTextField != null) {
							textColor(errorTextField);
							errorTextField.setText(jsonObject.optString("message"));

						}
					}
				});
				return false;

			}
		} catch (Exception e) {
			e.printStackTrace();
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					if (errorTextField != null) {
						textColor(errorTextField);
						errorTextField.setText(e.getLocalizedMessage());
					}
				}
			});
			return false;
		}
		return true;
	}

	public static int tagsinfo(String tagName, ArrayList<String> pq) {
		int k = 0;
		if (!pq.contains(tagName)) {
			pq.add(tagName);
			return 1;
		} else {
			pq.add(tagName);
			Iterator<String> itr = pq.iterator();
			while (itr.hasNext()) {
				if (itr.next() == tagName)
					k++;
			}
			return k;
		}
	}

	public String writeFile(String str) {
		String pattern = "<!--[\\s\\S]*?-->";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(str);
		str = m.replaceAll("");
		return str;
	}

	
	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	private ArrayList<String> executeCommand(String executionName) {
		ArrayList<String> envList = new ArrayList<String>();
		String line = "";
		try {
			Process p = Runtime.getRuntime().exec(addBash + automationPath + File.separator + executionName);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				envList.add(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();

			System.out.println("Done.");

		} catch (Exception err) {
			err.printStackTrace();
		}
		return envList;
	}

	// creating the file
	private void writeFile(String str, String className, String filePath, String fileExtension) throws Exception {
		Writer out = new java.io.BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(str);
			out.flush();
			out.close();
			setPermissionsToFile(filePath + File.separator + className + fileExtension);
		} finally {
			out.close();
		}
	}

}
