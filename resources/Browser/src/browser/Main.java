package browser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	
    TabPane root;
    @FXML
    TextField addressBarTF;

    @Override
    public void start(Stage stage) throws IOException {     
    	
        Parent browser = FXMLLoader.load(getClass().getResource("Browser.fxml"));
        Tab browserTab = new Tab("New Tab", browser);
        Tab addTab = new Tab("+", null);
        addTab.setClosable(false);        
        
        addTab.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
               addNewTab();
            }
        });
        root = new TabPane(browserTab, addTab);
        root.setStyle("-fx-font: 12 proximaNovaSemiBold; -fx-base:#EBF5FB;");
       

        Scene scene = new Scene(root);
        //stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/automaion_browser.png")));
       // stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/taskbar_icon.png")));
        stage.setScene(scene);
        stage.setTitle("Automation Browser");
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
        stage.show();
        scene.getOnContextMenuRequested();
        stage.setMaximized(true);
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
                System.out.println("closed");
                System.exit(0);
            }
        });
    }

    final void addNewTab() {
        try {
            Parent browser = FXMLLoader.load(getClass().getResource("Browser.fxml"));
            Tab browserTab = new Tab("New Tab", browser);
            root.getTabs().add(root.getTabs().size() - 1, browserTab);
            root.getSelectionModel().select(browserTab);
           /* browserTab.setStyle("-fx-padding: 10;" + "-fx-border-style: solid inside;" +"-fx-border-width: 2;" +
	                "-fx-border-insets: 5;" +
	                "-fx-border-radius: 5;" +
	                "-fx-border-color: blue;");*/
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @param args the command line arguments
     * @throws JSONException 
     * @throws NoSuchAlgorithmException 
     * @throws KeyManagementException 
     */
    
    public static void main(String[] data) throws JSONException, Exception {
    	System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
		
    	/*InputStream is = new FileInputStream(data[0]);
		@SuppressWarnings("resource")
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();
		while(line != null){ 
			sb.append(line).append("\n"); 
			line = buf.readLine(); 
		} 
		String fileAsString = sb.toString();
		System.out.println("Contents : " + fileAsString);
	*/
		String finalData = "{\"web_page_name\":\"\",\"web_page_description\":\"\",\"diff_page_id\":0,\"jar_type\":1,\"module_id\":768,\"user_id\":7,\"web_page_url\":\"https://www.qa10.pbteen.com\",\"save_web_page_and_elements_url\":\"http://10.11.12.241:80/CreateWebPage\"}";		
    //	String finalData = StringEscapeUtils.unescapeHtml(fileAsString);
    	JSONObject jsonObj = new JSONObject(finalData);
    	BrowserController.JSON = finalData;
    	BrowserController.WEB_URL = jsonObj.getString("web_page_url");
    	BrowserController.DIFF_ID = jsonObj.optInt("diff_page_id");
    	BrowserController.WEB_PAGE_NAME = jsonObj.optString("web_page_name");
    	BrowserController.WEB_PAGE_DESC = jsonObj.optString("web_page_description");
    	BrowserController.ELEMENTS_URL = jsonObj.getString("save_web_page_and_elements_url");
		if (BrowserController.WEB_URL.contains("https:")) {
			 TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}


					@Override
					public void checkClientTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void checkServerTrusted(X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}
				} };

				// Install the all-trusting trust manager
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

				// Create all-trusting host name verifier
				HostnameVerifier allHostsValid = new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				};
				// Install the all-trusting host verifier
				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		}
        launch(finalData); 
    }
}
