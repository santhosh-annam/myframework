package com.dto;

public class IframeModel {
	private String xpath;
	private String id;
	private String name;
	private String type;
	private boolean isIframe;
	private int iframePosition;
	
	public boolean isIframe() {
		return isIframe;
	}
	public void setIframe(boolean isIframe) {
		this.isIframe = isIframe;
	}
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public int getIframePosition() {
		return iframePosition;
	}
	public void setIframePosition(int iframePosition) {
		this.iframePosition = iframePosition;
	}
}
