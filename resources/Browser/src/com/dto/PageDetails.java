package com.dto;

import java.util.ArrayList;

public class PageDetails {

	private String web_page_name;
	private String web_page_description;
	private String web_page_url;
	private int diff_page_id;
	private String browser_name;
	
	public String getBrowser_name() {
		return browser_name;
	}

	public void setBrowser_name(String browser_name) {
		this.browser_name = browser_name;
	}

	public int getDiff_page_id() {
		return diff_page_id;
	}

	public void setDiff_page_id(int diff_page_id) {
		this.diff_page_id = diff_page_id;
	}

	private ArrayList<PageElements> elements_list;
	private int user_id;
	private int module_id;

	
	public ArrayList<PageElements> getElements_list() {
		return elements_list;
	}

	public void setElements_list(ArrayList<PageElements> elements_list) {
		this.elements_list = elements_list;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}


	public String getWeb_page_name() {
		return web_page_name;
	}

	public void setWeb_page_name(String web_page_name) {
		this.web_page_name = web_page_name;
	}

	public String getWeb_page_description() {
		return web_page_description;
	}

	public void setWeb_page_description(String web_page_description) {
		this.web_page_description = web_page_description;
	}

	public String getWeb_page_url() {
		return web_page_url;
	}

	public void setWeb_page_url(String web_page_url) {
		this.web_page_url = web_page_url;
	}

}
