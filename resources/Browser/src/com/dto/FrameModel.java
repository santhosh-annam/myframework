package com.dto;

public class FrameModel {
	String frameId;
	int framePosition;
	String frameName;
	boolean isFrame;
	String Url;
	
	public String getUrl() {
		return Url;
	}
	public void setUrl(String url) {
		Url = url;
	}
	public String getFrameId() {
		return frameId;
	}
	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}
	public int getFramePosition() {
		return framePosition;
	}
	public void setFramePosition(int framePosition) {
		this.framePosition = framePosition;
	}
	public String getFrameName() {
		return frameName;
	}
	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}
	
	public boolean isFrame() {
		return isFrame;
	}
	public void setFrame(boolean isFrame) {
		this.isFrame = isFrame;
	}
	
	
}
